cmake_minimum_required(VERSION 3.5)

# major.minor.patch.release_type [release_type: 0-dev, 1-beta, 2-rc, 3-r]
project(AccerionSensorAPI VERSION 7.0.1.3 LANGUAGES CXX)
# related to beta and rc releases, it is important to increment this
# to differentiate between post-dev & pre-release version
set(PROJECT_VERSION_BUILD_NUMBER 0)

message(STATUS "Read version: ${PROJECT_VERSION}.${PROJECT_VERSION_BUILD_NUMBER}")

if(TARGET ${PROJECT_NAME})
    return()
endif()

set(LD "$(CXX)")

# Default to C++11
if(NOT CMAKE_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD 11)
endif()

file(GLOB_RECURSE SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/source/${PROJECT_NAME}/*.cpp
)

add_library(${PROJECT_NAME} "${SOURCES}")

# Propagate CMake variables to code
if (WIN32)
    target_compile_definitions(${PROJECT_NAME} PUBLIC CMAKE_WINDOWS)
elseif (NOT UNIX)
    error("An unsupported system is detected (other than Windows or Unix-like)")
endif()
target_compile_definitions(${PROJECT_NAME} PUBLIC
        CMAKE_API_FULL_VERSION="${PROJECT_VERSION}.${PROJECT_VERSION_BUILD_NUMBER}")

# TODO: in future add all warnings?
if(UNIX)
    # Enable additional warnings than default
    # target_compile_options(${PROJECT_NAME} PRIVATE -Wall -Wextra -Wpedantic)
    # For now, just enabling warnings related to conversion
    target_compile_options(${PROJECT_NAME} PRIVATE -Wconversion)

    # Treat all warnings as errors
    # target_compile_options(${PROJECT_NAME} PRIVATE -Werror)
else()
    # Set warning level 4 (default is 3)
    # target_compile_options(${PROJECT_NAME} PRIVATE /W4)
    # Treat all warnings as errors
    # target_compile_options(${PROJECT_NAME} PRIVATE /WX)
endif()

# Under linux, the library Threads is used to implement std::thread, therefore linking is required in that case.
if (UNIX)
    find_package(Threads REQUIRED)
    target_link_libraries(${PROJECT_NAME} ${CMAKE_THREAD_LIBS_INIT})
endif()

target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/msgpack-c/include>)

include(GNUInstallDirs)

# Install the library include directories
install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
install(DIRECTORY 3rdparty/msgpack-c/include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

# Generate the files that contain all export targets
install(TARGETS ${PROJECT_NAME}
        EXPORT ${PROJECT_NAME}Targets
        # 2 lines below no longer required when CMake version >= v3.14
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}      # Specify archive destination for static libraries
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}      # Specify library destination for shared libraries
        )

# Install all export targets
install(EXPORT ${PROJECT_NAME}Targets
        DESTINATION ${CMAKE_INSTALL_DATADIR}/cmake/${PROJECT_NAME})

# Generate the project ConfigVersion.cmake
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/${PROJECT_NAME}ConfigVersion.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake @ONLY)

# Install the package configuration files
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/cmake/${PROJECT_NAME}Config.cmake
              ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake
        DESTINATION ${CMAKE_INSTALL_DATADIR}/cmake/${PROJECT_NAME}
)

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Accerion")   
set(CPACK_PACKAGE_VENDOR "Accerion")     
set(CPACK_PACKAGE_VERSION ${PROJECT_VERSION}.${PROJECT_VERSION_BUILD_NUMBER})
set(CPACK_PACKAGE_CHECKSUM MD5)   
set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)  
include(CPack)
