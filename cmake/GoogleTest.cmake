cmake_minimum_required(VERSION 3.5)

if (TARGET gtest)
    return()
endif()

# Download and unpack GoogleTest at configure time
configure_file(
        "${CMAKE_CURRENT_LIST_DIR}/GoogleTestDownload.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/googletest-download/CMakeLists.txt")
execute_process(
        COMMAND "${CMAKE_COMMAND}" -G "${CMAKE_GENERATOR}" .
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/googletest-download" )
execute_process(
        COMMAND "${CMAKE_COMMAND}" --build .
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/googletest-download" )

# Prevent GoogleTest from overriding our compiler/linker options when building with Visual Studio
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Prevent installation of GoogleTest, which is enabled by default
set(INSTALL_GTEST OFF CACHE BOOL "Installation of GoogleTest" FORCE)

# Add GoogleTest directly to our build.
add_subdirectory(
        "${CMAKE_CURRENT_BINARY_DIR}/googletest-src"
        "${CMAKE_CURRENT_BINARY_DIR}/googletest-build")

# Targets that will be built: gtest, gtest_main, gmock and gmock_main
