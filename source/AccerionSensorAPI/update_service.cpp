/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/update_service.h"

#include "AccerionSensorAPI/structs/acknowledgement.h"
#include "AccerionSensorAPI/structs/calibration_file_request.h"
#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/utils/memory.h"

namespace accerion {
namespace api {

UpdateService::UpdateService(const Address& address, const SerialNumber& serial_number)
        : address_{address},
          serial_number_{serial_number},
          process_response_functions_{InitializeProcessResponseFunctionsWithReceivedCommands()},
          future_heart_beat_{CommandId::PRD_HEARTBEAT_INFO, process_response_functions_},
          future_diagnostics_{CommandId::STR_DIAGNOSTICS, process_response_functions_},
          rc_handshake_{},
          rc_file_transfer_task_{},
          calibDoneCallBack{},
          communication_thread_{
                  CreateTcpClient(address),
                  serial_number,
                  CreateDefaultParsedMessageCallback(serial_number_, process_response_functions_)} {}

UpdateService::~UpdateService() {
    // To be cleared, such that futures will not be used anymore
    process_response_functions_.clear();
}

Future<HeartBeat>& UpdateService::SubscribeToHeartBeat() {
    future_heart_beat_.ClearData();
    return future_heart_beat_;
}

Future<Diagnostics>& UpdateService::SubscribeToDiagnostics() {
    future_diagnostics_.ClearData();
    return future_diagnostics_;
}

SerialNumber UpdateService::GetSerialNumber() const {
    return serial_number_;
}

bool UpdateService::SendCalibration(std::string sourcePath, DoneCallback doneCB, std::string key) {
    if (!doneCB || key.size() != 16) {
        return false;
    }

    calibDoneCallBack = doneCB;

    auto file_size = accerion::api::GetFileSize(sourcePath);
    if (file_size == 0) {
        return false;
    }

    // Send first packet message
    auto file_handle = std::ifstream(sourcePath, std::ios_base::binary);

    CalibrationFileRequest req{key};
    req.file.resize(static_cast<std::size_t>(file_size));
    file_handle.read(reinterpret_cast<char *>(req.file.data()), static_cast<std::int32_t>(file_size));
    communication_thread_.AddOutgoingMessage(CommandId::CMD_PLACE_CALIB, req);
    return true;
}

void UpdateService::retrievedCalibAck(std::vector<uint8_t> received_command) {
    auto ack = Acknowledgement::Deserialize(std::move(received_command));
    if (calibDoneCallBack) {
        calibDoneCallBack(ack.accepted, ack.message);
    }
}

void UpdateService::SetFileTransferTaskCallbacks(FileTransferTaskStatusCallback status_cb,
                                                 FileTransferTaskProgressCallback progress_cb) {
    rc_file_transfer_task_.callback.Set(std::move(status_cb));
    rc_file_transfer_task_.progress_callback.Set(std::move(progress_cb));
}

void UpdateService::ExecuteFileTransferTask(FileTransferTaskRequest request) {
    communication_thread_.AddOutgoingMessage(CommandId::CMD_FILE_TRANSFER_TASK, request);
}

void UpdateService::FileTransfer(FileUploadDownloadRequest req, std::string filePath) {
    rc_handshake_.SetFilePath(filePath);
    rc_handshake_.SetRemoteIpAddress(address_);
    communication_thread_.AddOutgoingMessage(CommandId::CMD_FILE_TRANSFER_REQUEST, req);
}

void UpdateService::SetFileTransferCallbacks(ProgressCallback progress_cb, DoneCallback done_cb) {
    rc_handshake_.progress_callback.Set(std::move(progress_cb));
    rc_handshake_.done_callback.Set(std::move(done_cb));
}

void UpdateService::UpdateSensor(std::string filepath, ProgressCallback transfer_progress_cb,
                                 FileTransferTaskProgressCallback processing_progress_cb, DoneCallback done_cb) {
    uint64_t fileSize = GetFileSize(filepath);
    if (fileSize == 0) {
        done_cb(false, "File does not seem to exist or is empty!");
        return;
    }

    FileUploadDownloadRequest req{TransferType::kUploadUpdate, fileSize, 1000000};

    auto done_lambda = [this, done_cb, processing_progress_cb](bool success, const std::string&) {
        std::cout << "Transfer done, send command to start processing" << std::endl;
        if (success) {
            FileTransferTaskRequest ftt_request{FileTransferTask::kUpdateSensor};
            auto ftt_done_lambda = [done_cb](FileTransferTask task, FileTransferTaskStatus status, std::string message) {
                if (static_cast<int>(status) == 1) done_cb(true, "Update was successfully sent and installed!");
                if (static_cast<int>(status) == 2) done_cb(false, std::move(message));
            };
            SetFileTransferTaskCallbacks(std::move(ftt_done_lambda), processing_progress_cb);
            ExecuteFileTransferTask(ftt_request);
        } else {
            done_cb(false, "An error occurred while transferring the update.");
        }
    };
    SetFileTransferCallbacks(std::move(transfer_progress_cb), std::move(done_lambda));
    FileTransfer(req, filepath);
}

void UpdateService::GetLogs(const std::string& file_path,
                            ProgressCallback transfer_progress_cb,
                            FileTransferTaskProgressCallback processing_progress_cb,
                            DoneCallback done_cb) {
    FileTransferTaskRequest ftt_request{FileTransferTask::kGatherLogs};
    auto ftt_done_lambda = [this, file_path, transfer_progress_cb, done_cb]
                            (FileTransferTask task, FileTransferTaskStatus status, std::string message) {
        if (status == FileTransferTaskStatus::kTaskFail) {
            if (message.empty()) {
                message = "An error occurred while gathering the logs...";
            }
            done_cb(false, message);
        } else if (status == FileTransferTaskStatus::kTaskSuccess) {
            // Recordings gathered, ready for download:
            FileUploadDownloadRequest req{TransferType::kDownloadLogs, 0, 0};
            SetFileTransferCallbacks(transfer_progress_cb, done_cb);
            FileTransfer(req, file_path);
        }
    };

    SetFileTransferTaskCallbacks(std::move(ftt_done_lambda), std::move(processing_progress_cb));
    ExecuteFileTransferTask(std::move(ftt_request));
}

ProcessResponseFunctions UpdateService::InitializeProcessResponseFunctionsWithReceivedCommands() {
    return {
            {CommandId::ACK_PLACE_CALIB,
                    [this](RawData d) { retrievedCalibAck(std::move(d)); }},
            {CommandId::ACK_FILE_TRANSFER_HANDSHAKE,
                    [this](RawData d) { rc_handshake_.ProcessAck(std::move(d)); }},
            {CommandId::ACK_FILE_TRANSFER_STATUS,
                    [this](RawData d) { rc_file_transfer_task_.ProcessAck(std::move(d)); }},
    };
}

std::unique_ptr<TcpClient> UpdateService::CreateTcpClient(const Address& address) {
    auto tcp_client = MakeUnique<TcpClient>(address, UPDATE_SERVICE_PORT_TCP);

#ifdef CMAKE_WINDOWS
    std::system_error error = tcp_client->ConnectToServer();

    if (error.code().value() == WSAECONNREFUSED)
#else
    std::system_error error;
    do {
        error = tcp_client->ConnectToServer();

    } while (error.code().value() == EINPROGRESS);

    if (error.code().value() == ECONNREFUSED)
#endif
    {
        throw error;
    }
    return tcp_client;
}

}  // namespace api
}  // namespace accerion
