/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

Command::Command(CommandId id, RawData data)
        : command_id{id}, command{std::move(data)} {}

bool Command::operator==(const Command &rhs) const {
    return command_id == rhs.command_id && command == rhs.command;
}
bool Command::operator!=(const Command &rhs) const { return !(*this == rhs); }

}  // namespace api
}  // namespace accerion
