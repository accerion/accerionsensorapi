/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/utils/string.h"

#include <cmath>
#include <iomanip>

namespace accerion {
namespace api {

void ToLower(std::string& s) {
    std::for_each(s.begin(), s.end(), [](char& c) { c = static_cast<char>(std::tolower(c)); });
}

std::vector<std::string> Split(const std::string& s, const std::string& delimiter) {
    std::vector<std::string> parts;

    const auto delimiter_size = delimiter.size();

    std::string::size_type prev_pos = 0, curr_pos;
    while ((curr_pos = s.find(delimiter, prev_pos)) != std::string::npos) {
        parts.push_back(prev_pos == curr_pos ? "" : s.substr(prev_pos, curr_pos - prev_pos));
        prev_pos = curr_pos + delimiter_size;
    }
    parts.push_back(s.substr(prev_pos, curr_pos - prev_pos));

    return parts;
}

bool DoesStringStartWith(const std::string& string, const std::string& search_string) {
    return string.size() >= search_string.size() &&
           0 == string.compare(0, search_string.size(), search_string);
}

bool DoesStringEndWith(const std::string& string, const std::string& search_string) {
    return string.size() >= search_string.size() &&
           0 == string.compare(string.size() - search_string.size(), search_string.size(), search_string);
}

bool DoesStringContain(const std::string& string, const std::string& search_string) {
    return string.size() >= search_string.size() && string.find(search_string) != std::string::npos;
}

std::string ToString(double value, std::uint8_t digits) {
    const double factor = std::pow(10, digits);
    const double rounded_value{std::round(value * factor) / factor};
    std::stringstream ss;
    ss << std::fixed << std::setprecision(digits) << rounded_value;
    return ss.str();
}

}  // namespace api
}  // namespace accerion
