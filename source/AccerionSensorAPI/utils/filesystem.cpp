/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/utils/filesystem.h"

#include <iostream>

#ifdef CMAKE_WINDOWS
    #include <windows.h>
    #include <errhandlingapi.h>
    #include <winerror.h>
#endif

#include <sys/stat.h>

// Cross-platform file information function and -buffer
#ifdef CMAKE_WINDOWS
  #define stat64 _stat64
#endif


namespace accerion {
namespace api {

std::string GetExtension(const std::string& path) {
    const auto last_dot = path.find('.');
    if (last_dot != std::string::npos) {
        return {path.cbegin() + last_dot + 1, path.cend()};
    } else {
        return {};
    }
}

std::uint64_t GetFileSize(const std::string& path) {
    using Stat64 = struct stat64;

    if (FileExists(path)) {
        Stat64 stat_buf{};
        stat64(path.c_str(), &stat_buf);
        return stat_buf.st_size;
    } else {
        return 0;
    }
}

bool FileExists(const std::string& path) {
    struct stat64 stat_buf;
    auto result = stat64(path.c_str(), &stat_buf) == 0;
#ifndef CMAKE_WINDOWS
    if (result) {
        result = stat_buf.st_mode | S_IFDIR;
    }
#endif
    return result;
}

bool RemoveFile(const std::string& path) {
    return std::remove(path.c_str()) == 0;
}

}  // namespace api
}  // namespace accerion
