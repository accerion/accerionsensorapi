/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/utils/time.h"

#include <ctime>
#include <iomanip>
#include <sstream>

namespace accerion {
namespace api {

std::string GetTimeString() {
    std::time_t time_now = std::time(nullptr);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&time_now), "%Y-%m-%d-%H-%M");
    return ss.str();
}

}  // namespace api
}  // namespace accerion
