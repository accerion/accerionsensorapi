/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/utils/reporting.h"

#include <iomanip>
#include <iostream>
#include <sstream>


namespace accerion {
namespace api {

void Report(const BufferProgress& bp) {
    switch (bp.message_type) {
        case BufferedRecoveryMessageType::kProgress:
            ReportProgress(bp.progress);
            break;
        case BufferedRecoveryMessageType::kFinished:
            std::cout << (bp.result ? "Drift" : "No drift") << " correction found." << std::endl;
            break;
        case BufferedRecoveryMessageType::kCanceled:
            std::cout << "Recovery cancelled." << std::endl;
            break;
        case BufferedRecoveryMessageType::kLowOverlap:
            std::cout << "Signatures in buffer (" << +bp.percentage_of_low_overlap << "%) have low overlap." << std::endl;
            break;
        case BufferedRecoveryMessageType::kNoMap:
            std::cout << "No map available." << std::endl;
            break;
        case BufferedRecoveryMessageType::kNoSignatures:
            std::cout << "No signatures within search radius." << std::endl;
            break;
        case BufferedRecoveryMessageType::kEmpty:
            std::cout << "Buffer is empty. Move the camera and retry." << std::endl;
            break;
        default:
            std::cout << "Unknown message type (" << +static_cast<uint8_t>(bp.message_type) << ")." << std::endl;
            break;
    }
}

void ReportProgress(int progress) {
    std::cout << "Progress: " << progress << " %" << std::endl;
}

void ReportDone(const std::string& action, bool success, const std::string& message) {
    if (success) {
        std::cout << action << " successful";
    } else {
        std::cout << action << " unsuccessful";
    }
    if (!message.empty()) {
        std::cout << ": " << message;
    }
    std::cout << std::endl;
}

void ReportMode(const std::string& mode, Acknowledgement result, bool setting) {
    if (result.accepted) {
        std::cout << "Successfully set mode " << mode << std::endl;
    } else {
        std::cout << "Failed to set mode " << mode << std::endl;
    }
}

void ReportMode(const std::string& mode, accerion::api::Optional<accerion::api::Acknowledgement> ack) {
    if (!ack) {
        std::cout << "Timeout when setting mode, unknown result" << std::endl;
        return;
    }
    if (ack.value().accepted) {
        std::cout << "Successfully set mode " << mode << std::endl;
    } else {
        std::cout << "Failed to set mode " << mode << std::endl;
    }
}

ReportModes::ReportModes(uint16_t modes, bool all)
        : ReportBase(modes, kModeDescriptionMap),
          name_max_size_(MaxSize(kModeDescriptionMap)),
          all_(all) {}

std::size_t ReportModes::MaxSize(const NameMap& name_map) {
    std::size_t max_size = 0;
    for (const auto& name_map_iter: name_map) {
        auto size = name_map_iter.second.size();
        if (size > max_size) {
            max_size = size;
        }
    }
    return max_size + 2;
}

std::ostream &operator<<(std::ostream &os, const ReportModes &values) {
    for (const auto& id: values.GetIds()) {
        if (values.all_ | values.Value(id)) {
            os << "    " << values.Name(id) << "\n";
        }
    }
    return os;
}

std::string ReportModes::Name(Id id) const {
    std::stringstream ss;
    ss << std::setfill(' ') << std::setw(static_cast<std::int8_t>(name_max_size_)) << std::left << ReportBase::Name(id)
       << ": " << (Value(id) ? "on" : "off");
    return ss.str();
}

std::ostream &operator<<(std::ostream &os, const ReportErrorCodes &values) {
    return values.Print(os);
}

std::ostream &operator<<(std::ostream &os, const ReportWarningCodes &values) {
    return values.Print(os);
}

}  // namespace api
}  // namespace accerion
