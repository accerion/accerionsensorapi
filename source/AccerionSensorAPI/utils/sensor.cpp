/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/utils/sensor.h"

#include <iostream>

#include "AccerionSensorAPI/connection_manager.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/time.h"


namespace accerion {
namespace api {

std::unique_ptr<Sensor> GetAnySensor(ConnectionType connection_type, Optional<Address> opt_local_ip) {
    // Wait for some time to detect a sensor and try to connect with it
    // If a sensor would be detected, but connection fails, then it will not try to reconnect, unless it was also
    // detected again
    constexpr std::chrono::seconds kTimeOut{2};
    std::unique_ptr<Sensor> sensor{nullptr};
    while (!sensor) {
        std::vector<SensorDetails> sensors = ConnectionManager::GetDetectedSensors(kTimeOut);
        if (sensors.empty()) {
            std::cout << "No sensors found.." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds{2});
            continue;
        }

        std::cout << "Using first found sensor with serial number " << sensors.front().serial_number.value << std::endl;
        auto& future_sensor = ConnectionManager::GetSensor(sensors.front().address, connection_type, opt_local_ip);
        sensor = future_sensor.WaitFor(kTimeOut);
    }
    return sensor;
}

std::pair<UdpSettings, TcpSettings> ConfigureConnection(const ConnectionType& connection_type,
                                                        const Optional<Address>& local_ip) {
    UdpSettings udp_settings{};
    TcpSettings tcp_settings{};

    if (connection_type == ConnectionType::kConnectionUdpUnicast ||
            connection_type == ConnectionType::kConnectionUdpUnicastNoHb) {
        if (!local_ip) {
            throw std::runtime_error("local ip address should be provided when connecting via UDP unicast");
        }
        udp_settings.address = local_ip.value();
    }

    switch (connection_type) {
        case ConnectionType::kConnectionTcp:
            udp_settings.message_type = EnabledMessageType::kBoth;
            udp_settings.strategy = UdpStrategy::kUntouched;
            tcp_settings.message_type = EnabledMessageType::kBoth;
            break;
        case ConnectionType::kConnectionTcpDisableUdp:
            udp_settings.message_type = EnabledMessageType::kInactive;
            udp_settings.strategy = UdpStrategy::kBroadcast;
            tcp_settings.message_type = EnabledMessageType::kBoth;
            break;
        case ConnectionType::kConnectionUdpBroadcast:
            udp_settings.message_type = EnabledMessageType::kBoth;
            udp_settings.strategy = UdpStrategy::kBroadcast;
            tcp_settings.message_type = EnabledMessageType::kInactive;
            break;
        case ConnectionType::kConnectionUdpUnicast:
            udp_settings.message_type = EnabledMessageType::kBoth;
            udp_settings.strategy = UdpStrategy::kUnicast;
            tcp_settings.message_type = EnabledMessageType::kInactive;
            break;
        case ConnectionType::kConnectionUdpUnicastNoHb:
            udp_settings.message_type  = EnabledMessageType::kBoth;
            udp_settings.strategy = UdpStrategy::kUnicastNoHB;
            tcp_settings.message_type = EnabledMessageType::kInactive;
            break;
    }

    return {udp_settings, tcp_settings};
}

std::string GenerateFileName(const FileType& file_type, const accerion::api::SerialNumber& serial_number) {
    static const std::unordered_map<FileType, std::string> kFilePrefixes = {
        {FileType::Coordinates, "coords"},
        {FileType::LoopClosures, "loops"},
        {FileType::Logs, "logs"},
        {FileType::Map, "map"},
        {FileType::Recording, "recordings"}
    };

    std::stringstream ss;
    ss << kFilePrefixes.at(file_type) << '-' << serial_number.value << '-' << GetTimeString() << '.'
       << kFileExtensions.at(file_type);
    return ss.str();
}

}  // namespace api
}  // namespace accerion
