/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/communication/thread.h"


namespace accerion {
namespace api {

CommunicationThread::~CommunicationThread() {
    Stop();
}

CommunicationThread::CommunicationThread(CommunicationThread&& other) noexcept {
    // The outgoing_messages_mutex_ is always nullptr
    Stop();
    {
        std::lock_guard<std::mutex> other_lock{*other.outgoing_messages_mutex_};
        outgoing_messages_ = std::move(other.outgoing_messages_);
    }
    outgoing_messages_mutex_ = std::move(other.outgoing_messages_mutex_);
    run_thread_ = std::move(other.run_thread_);
    thread_ = std::move(other.thread_);
}

void CommunicationThread::AddOutgoingMessage(CommandId command_id, RawData&& serialized_message) {
    std::lock_guard<std::mutex> lock{*outgoing_messages_mutex_};
    outgoing_messages_->emplace_back(command_id, std::move(serialized_message));
}

void CommunicationThread::Stop() {
    if (run_thread_) {
        run_thread_->store(false);
    }
    if (thread_.joinable()) {
        thread_.join();
    }
}

ParsedMessageCallback CreateDefaultParsedMessageCallback(const SerialNumber& desired_serial_number,
                                                         const ProcessResponseFunctions& process_response_functions) {
    // Default filter makes sure that the received serial number and desired serial number are matching
    return [desired_serial_number, &process_response_functions](SerialNumber received_serial_number,
                                                                Command&& received_command) {
        if (received_serial_number != desired_serial_number) { return; }

        // Look up the command processing function identified by the command id
        auto function_iter = process_response_functions.find(received_command.command_id);
        if (function_iter != process_response_functions.cend()) {
            function_iter->second(std::move(received_command.command));
        }
    };
}

}  // namespace api
}  // namespace accerion
