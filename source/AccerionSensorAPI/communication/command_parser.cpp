/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/communication/command_parser.h"

#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/communication/networking.h"
#include "AccerionSensorAPI/crc8.h"
#include "AccerionSensorAPI/structs/plain_uint32.h"
#include "AccerionSensorAPI/utils/enum.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include <iostream>
namespace accerion {
namespace api {

namespace detail {

/// The position and size values of header and footer fields in a message
constexpr std::uint8_t kSerialNumberBeginByte = 0;
constexpr std::uint8_t kSerialNumberLength    = 4;
constexpr std::uint8_t kSerialNumberEndByte   = kSerialNumberBeginByte + kSerialNumberLength;  // past the end

constexpr std::uint8_t kCommandIdBeginByte = kSerialNumberEndByte;
constexpr std::uint8_t kCommandIdLength    = 1;
constexpr std::uint8_t kCommandIdEndByte   = kSerialNumberEndByte + kCommandIdLength;  // past the end

constexpr std::uint8_t kMessageSizeBeginByte = kCommandIdEndByte;
constexpr std::uint8_t kMessageSizeLength    = 4;
constexpr std::uint8_t kMessageSizeEndByte   = kMessageSizeBeginByte + kMessageSizeLength;  // past the end

constexpr std::uint8_t kContentBeginByte = kMessageSizeEndByte;

constexpr std::uint8_t kCrcLength = 1;

static_assert(network_constants::kMessageHeaderLength == kSerialNumberLength + kCommandIdLength + kMessageSizeLength);
static_assert(network_constants::kMessageFooterLength == kCrcLength);

}  // namespace detail

void ParseMessages(bool& last_message_was_broken, RawData& message, const ParsedMessageCallback& call_back) {
    auto bytes_received = static_cast<std::uint32_t>(message.size());
    std::uint32_t number_of_bytes_read = 0;

    while (bytes_received > 0) {
        if (bytes_received < detail::kCommandIdEndByte) {
            last_message_was_broken = true;
            message = {message.end() - bytes_received, message.end()};
            break;
        }

        const auto received_serial_number = SerialNumber::Deserialize(
                {message.data() + number_of_bytes_read, message.data() + number_of_bytes_read + detail::kSerialNumberLength});

        const auto received_command_id = static_cast<CommandId>(message[number_of_bytes_read + detail::kCommandIdBeginByte]);

        const auto total_message_size = PlainUInt32::Deserialize(
                {message.data() + number_of_bytes_read + detail::kMessageSizeBeginByte,
                 message.data() + number_of_bytes_read + detail::kMessageSizeEndByte}).value;

        if (total_message_size > bytes_received) {
            last_message_was_broken = true;
            message = {message.end() - bytes_received, message.end()};
            return;
        }

        last_message_was_broken = false;

        const auto content_end_byte = static_cast<int>(total_message_size - network_constants::kMessageFooterLength);

        if (total_message_size < network_constants::kMessageFooterLength || content_end_byte < detail::kContentBeginByte) {
            return;  //  incorrect size, potentially incompatible version
        }

        RawData received_command{message.data() + number_of_bytes_read + detail::kContentBeginByte,
                                 message.data() + number_of_bytes_read + content_end_byte};

        const auto received_crc8 = *(message.data() + number_of_bytes_read + content_end_byte);
        const auto calculated_crc8 = Crc8::Generate(message.data() + number_of_bytes_read, content_end_byte);
        if (received_crc8 != calculated_crc8) {
            return;
        }

        number_of_bytes_read += total_message_size;
        bytes_received -= total_message_size;
        call_back(received_serial_number, {received_command_id, std::move(received_command)});
    }
}

RawData FormMessage(const SerialNumber& serial_number, const Command& command) {
    RawData message{};
    Append(message, serial_number.Serialize());
    message.push_back(GetEnumValue(command.command_id));
    const auto message_length = static_cast<std::uint32_t>(
            detail::kSerialNumberLength + detail::kCommandIdLength +
            command.command.size() + detail::kMessageSizeLength + detail::kCrcLength);
    Append(message, PlainUInt32{message_length}.Serialize());
    Append(message, command.command);
    message.push_back(Crc8::Generate(message.data(), static_cast<int>(message.size())));
    return message;
}

}  // namespace api
}  // namespace accerion
