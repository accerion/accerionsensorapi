/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/communication/connection_utils.h"


namespace accerion {
namespace api {

bool SensorDetails::operator==(const SensorDetails& other) const {
    return address == other.address &&
           serial_number == other.serial_number &&
           software_version == other.software_version;
}

void DetectedSensors::Add(const SensorDetails& sensor_details) {
    std::lock_guard<std::mutex> lock{mutex_};
    bool newly_detected = true;
    for (auto &scd : sensors_) {
        if (scd.details.serial_number == sensor_details.serial_number &&
                scd.details.address == sensor_details.address) {
            newly_detected = false;
            scd.last_message_time_point = std::chrono::system_clock::now();
        }
    }
    if (newly_detected) {
        SensorConnectionDetails scd{};
        scd.details = sensor_details;
        scd.last_message_time_point = std::chrono::system_clock::now();
        sensors_.push_back(scd);
    }
}

std::vector<SensorDetails> DetectedSensors::GetAll(const std::chrono::seconds& filter_by_last_detection) const {
    std::lock_guard<std::mutex> lock{mutex_};
    std::vector<SensorDetails> result;
    const auto desired_last_detection = std::chrono::system_clock::now() - filter_by_last_detection;
    for (const auto& s: sensors_) {
        if (s.last_message_time_point > desired_last_detection) {
            result.push_back(s.details);
        }
    }
    return result;
}

}  // namespace api
}  // namespace accerion
