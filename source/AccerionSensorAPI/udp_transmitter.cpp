/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/udp_transmitter.h"

#include <chrono>
#include <iostream>
#include <thread>

#include "AccerionSensorAPI/communication/command_parser.h"

namespace accerion {
namespace api {

constexpr bool kDebug{false};

UdpTransmitter::UdpTransmitter(unsigned int remoteReceivePort) {
    remoteReceivePort_ = static_cast<uint16_t>(remoteReceivePort);;

    socketEndpoint_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socketEndpoint_ < 0) {
        std::cout << "Error while opening transmitting socket" << std::endl;
    }
#ifndef CMAKE_WINDOWS
    int enable = 1;
    if (setsockopt(socketEndpoint_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
        std::cout << "setsockopt(SO_REUSEADDR) failed" << std::endl;
    }
#endif
}

UdpTransmitter::UdpTransmitter(UdpTransmitter &&other) noexcept {
    *this = std::move(other);
}

UdpTransmitter &UdpTransmitter::operator=(UdpTransmitter &&other) noexcept {
    remoteReceivePort_ = other.remoteReceivePort_;
    remoteAddress_ = other.remoteAddress_;
    // To keep socket open, invalidate moved from 'socket_endpoint_'
    CloseSocket();
    socketEndpoint_ = other.socketEndpoint_;
    other.socketEndpoint_ = -1;
    return *this;
}

void UdpTransmitter::SetIpAddress(const Address& address) {
    /*Set the IP addresses for UDP message*/
    remoteAddress_.sin_family = AF_INET;
    remoteAddress_.sin_port = htons(remoteReceivePort_);
    remoteAddress_.sin_addr.s_addr = address.Convert().s_addr;
    memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

    if (kDebug) {
        std::cout << "From UDP Transmitter, setting ip address to := " << inet_ntoa(remoteAddress_.sin_addr) << std::endl;
    }
}

void UdpTransmitter::SetBroadcastIpAddress(const Address& address) {
    int broadcastEnable = 1;

    if (setsockopt(socketEndpoint_, SOL_SOCKET, SO_BROADCAST, (const char *)&broadcastEnable, sizeof(broadcastEnable)) == -1) {
        if (kDebug) {
            perror("Error while setting socket options for UDP transmitter, error is");
        }
    }

    /*Set the IP addresses for broadcast UDP message*/
    remoteAddress_.sin_family = AF_INET;
    remoteAddress_.sin_port = htons(remoteReceivePort_);
    remoteAddress_.sin_addr.s_addr = address.Convert().s_addr;
    memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

    if (kDebug) {
        std::cout << "From UDP Transmitter, setting broadcast address to := " << inet_ntoa(remoteAddress_.sin_addr) << std::endl;
    }
}

 void UdpTransmitter::SetUnicastIpAddress(const Address& address) {
     /*Set the IP addresses for unicast UDP message*/
     remoteAddress_.sin_family = AF_INET;
     remoteAddress_.sin_port = htons(remoteReceivePort_);
     remoteAddress_.sin_addr.s_addr = address.Convert().s_addr;
     memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

     if (kDebug) {
         std::cout << "From UDP Transmitter, setting unicast address to := " << inet_ntoa(remoteAddress_.sin_addr) << std::endl;
     }
 }

bool UdpTransmitter::TransmitMessage(RawData&& message) {
    if (message.size() > bufferSize_) {
        if (kDebug) {
            std::cout << "[UDP] Number of bytes (" << message.size() << ") is larger than maximum message size := "
                      << std::endl;
        }
        return false;
    }
    if (sendto(socketEndpoint_, reinterpret_cast<char*>(message.data()), message.size(), 0,
               reinterpret_cast<sockaddr*>(&remoteAddress_), sizeof(remoteAddress_)) == -1) {
        if (kDebug) {
            perror(" Error multicasting message to port, error is");
        }
        return false;
    }
    return true;
}

UdpTransmitter::~UdpTransmitter() {
    CloseSocket();
}

void UdpTransmitter::TransmitMessages(const std::vector<Command>& commands, const SerialNumber& serial_number) {
    std::size_t total_bytes_sent = 0;
    for (const auto& command : commands) {
        auto message = FormMessage(serial_number, command);

        total_bytes_sent += message.size();
        if (total_bytes_sent >= network_constants::kMaxBytesInUdpMessage) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            total_bytes_sent = message.size();
        }

        TransmitMessage(std::move(message));
    }
}

void UdpTransmitter::CloseSocket() {
#ifdef CMAKE_WINDOWS
    closesocket(socketEndpoint_);
#else
    close(socketEndpoint_);
#endif
    socketEndpoint_ = -1;
}

}  // namespace api
}  // namespace accerion
