/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/udp_receiver.h"

#include <iostream>

namespace accerion {
namespace api {

constexpr bool kDebug{false};

UdpReceiver::UdpReceiver(unsigned int receive_port) {
    receivePort_ = static_cast<uint16_t>(receive_port);
    socketLength_ = sizeof(remote_address_);

#ifdef CMAKE_WINDOWS
    WSADATA wsaData;
    int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
    }
    socketEndpoint_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    unsigned long argp = 1;// 1 to set non-blocking, 0 to set re-usable
    int result = setsockopt(socketEndpoint_, SOL_SOCKET, SO_REUSEADDR, (char*)&argp, sizeof(argp));
    if (result != 0)
    {
        printf("setsockopt() error %d\n", result);
    }

    argp = 1;// 1 to set non-blocking, 0 to set blocking
    if (ioctlsocket(socketEndpoint_, FIONBIO, &argp) == SOCKET_ERROR)
    {
        printf("ioctlsocket() error %d\n", WSAGetLastError());
    }
#else
    socketEndpoint_ = socket(AF_INET, SOCK_DGRAM | SOCK_NONBLOCK, IPPROTO_UDP);
#endif

    if (socketEndpoint_ < 0) {
        std::cout << "Error while opening receiving socket" << std::endl;
    }

#ifndef CMAKE_WINDOWS
    int enable = 1;
    if (setsockopt(socketEndpoint_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        std::cout <<"setsockopt(SO_REUSEADDR) failed" << std::endl;
#endif

    this_address_.sin_family = AF_INET;
    this_address_.sin_port = htons(receivePort_);
    this_address_.sin_addr.s_addr = htonl (INADDR_ANY);

    if (bind(socketEndpoint_, reinterpret_cast<sockaddr*>(&this_address_), sizeof(this_address_)) < 0) {
        perror("Error while binding port, error is := ");
    }
}

UdpReceiver::~UdpReceiver() {
    CloseSocket();
}

UdpReceiver::UdpReceiver(UdpReceiver &&other) noexcept {
    *this = std::move(other);
}

UdpReceiver &UdpReceiver::operator=(UdpReceiver &&other) noexcept {
    this_address_ = other.this_address_;
    remote_address_ = other.remote_address_;
    receivePort_ = other.receivePort_;
    socketLength_ = other.socketLength_;
    // To keep socket open, invalidate moved from 'socket_endpoint_'
    CloseSocket();
    socketEndpoint_ = other.socketEndpoint_;
    other.socketEndpoint_ = -1;
    return *this;
}

bool UdpReceiver::ReceiveMessage() {
    receivedNumOfBytes_ = static_cast<int>(recvfrom(socketEndpoint_, receivedMessage_, sizeof(receivedMessage_), 0,
                                                    reinterpret_cast<sockaddr*>(&remote_address_), &socketLength_));

#ifdef CMAKE_WINDOWS
    if (receivedNumOfBytes_ != SOCKET_ERROR)
#else
    if (receivedNumOfBytes_ != -1)
#endif
    {
        if (receivedNumOfBytes_ > bufferSize_) {
            if (kDebug) {
                std::cout << "Received UDP Message is too big, received num of bytes is := " << receivedNumOfBytes_ << std::endl;
            }
            return false;
        }
        return true;
    }
    else {
#ifdef CMAKE_WINDOWS
        int iError = WSAGetLastError();
        if (iError != WSAEWOULDBLOCK)
        {
            printf("recv failed with error: %ld\n", iError);
        }
#else
        int errsv = errno;
        if (kDebug && (errsv != 11)) {
            perror("Error while receiving messages in UDPReceiver, error is");
        }
#endif
        return false;
    }
}

void UdpReceiver::CloseSocket() {
#ifdef CMAKE_WINDOWS
    closesocket(socketEndpoint_);
#else
    close(socketEndpoint_);
#endif
    socketEndpoint_ = -1;
}

}  // namespace api
}  // namespace accerion
