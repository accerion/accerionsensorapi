/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/connection_manager.h"

#include "AccerionSensorAPI/udp_client.h"

namespace accerion {
namespace api {
namespace detail {

void CheckConnectionSettings(const ConnectionType& connection_type, const Optional<Address>& local_ip) {
    if ((connection_type == ConnectionType::kConnectionUdpUnicast ||
            connection_type == ConnectionType::kConnectionUdpUnicastNoHb) &&
            !local_ip) {
        throw std::runtime_error("local ip address should be provided when connecting via UDP unicast");
    }
}

std::unique_ptr<Sensor> CreateSensor(const SensorDetails& details,
                                     ConnectionType connection_type,
                                     Optional<Address> local_ip) {
    try {
        return MakeUnique<Sensor>(details.address, details.serial_number, connection_type, local_ip);
    } catch (std::system_error& e) {
        std::cerr << "System error exception caught while connecting to sensor "
                  << details.serial_number.value << ":\n    " << e.what() << std::endl;
        return nullptr;
    }
}

std::unique_ptr<UpdateService> CreateService(const SensorDetails& details) {
    try {
        return MakeUnique<UpdateService>(details.address, details.serial_number);
    } catch (std::system_error& e) {
        std::cerr << "System error exception caught while connecting to update service "
                  << details.serial_number.value << ":\n    " << e.what() << std::endl;
        return nullptr;
    }
}

}  // namespace detail

void ConnectionManager::Shutdown() {
    auto& manager = ConnectionManager::GetInstance();

    manager.communication_thread_sensor_.reset();
    manager.communication_thread_service_.reset();

    std::lock_guard<std::mutex> lock{manager.mutex_};

    manager.future_sensor_fill_condition_ = {};
    manager.connect_to_sensor_ = {};

    manager.future_service_fill_condition_ = {};
}

std::vector<SensorDetails> ConnectionManager::GetDetectedSensors(const std::chrono::seconds& period) {
    return GetInstance().detected_sensors_.GetAll(period);
}

Future<std::unique_ptr<Sensor>>& ConnectionManager::GetSensor(Address address,
                                                              ConnectionType connection_type,
                                                              Optional<Address> local_ip) {
    detail::CheckConnectionSettings(connection_type, local_ip);

    auto& manager = ConnectionManager::GetInstance();

    std::lock_guard<std::mutex> lock{manager.mutex_};
    manager.future_sensor_fill_condition_ = [address](const SensorDetails& details) {
        return details.address == address;
    };
    manager.connect_to_sensor_ = [connection_type, local_ip](const SensorDetails& details) {
        return detail::CreateSensor(details, connection_type, local_ip);
    };
    return manager.future_sensor_;
}

Future<std::unique_ptr<Sensor>>& ConnectionManager::GetSensor(SerialNumber serial,
                                                              ConnectionType connection_type,
                                                              Optional<Address> local_ip) {
    detail::CheckConnectionSettings(connection_type, local_ip);

    auto& manager = ConnectionManager::GetInstance();

    std::lock_guard<std::mutex> lock{manager.mutex_};
    manager.future_sensor_fill_condition_ = [serial](const SensorDetails& details) {
        return details.serial_number == serial;
    };
    manager.connect_to_sensor_ = [connection_type, local_ip](const SensorDetails& details) {
        return detail::CreateSensor(details, connection_type, local_ip);
    };
    return manager.future_sensor_;
}

Future<std::unique_ptr<UpdateService>>& ConnectionManager::GetUpdateService(Address address) {
    auto& manager = ConnectionManager::GetInstance();

    std::lock_guard<std::mutex> lock{manager.mutex_};
    manager.future_service_fill_condition_ = [address](const SensorDetails& details) {
        return details.address == address;
    };
    return manager.future_service_;
}

Future<std::unique_ptr<UpdateService>>& ConnectionManager::GetUpdateService(SerialNumber serial) {
    auto& manager = ConnectionManager::GetInstance();

    std::lock_guard<std::mutex> lock{manager.mutex_};
    manager.future_service_fill_condition_ = [serial](const SensorDetails& details) {
        return details.serial_number == serial;
    };
    return manager.future_service_;
}

ConnectionManager& ConnectionManager::GetInstance() {
    static ConnectionManager instance{};
    if (!instance.communication_thread_sensor_ || !instance.communication_thread_service_) {
        throw std::runtime_error("ConnectionManager cannot be used any more when it has been shut down");
    }
    return instance;
}

ConnectionManager::ConnectionManager()
        : detected_sensors_{},
          mutex_{},
          future_sensor_{},
          future_sensor_fill_condition_{},
          connect_to_sensor_{},
          future_service_{},
          future_service_fill_condition_{},
          // Using UdpClient for both communication threads, but only for receiving message, not transmitting them
          // Hence, all transmit parameters are dummy values
          communication_thread_sensor_{MakeUnique<CommunicationThread>(
                  MakeUnique<UdpClient>(0, API_RECEIVE_PORT_UDP),
                  SerialNumber{},
                  [this](SerialNumber sr, Command&& c) { ConnectionManager::ProcessParsedMessage(sr, std::move(c)); })},
          communication_thread_service_{MakeUnique<CommunicationThread>(
                  MakeUnique<UdpClient>(0, UPDATE_SERVICE_TRANSMIT_PORT_UDP),
                  SerialNumber{},
                  [this](SerialNumber sr, Command&& c) { ConnectionManager::ProcessParsedMessage(sr, std::move(c)); })} {
    // At this point, the threads are running as the CommunicationThread constructor waits for that
    // However, still it is good to wait for some time here such that heartbeat messages are received
    // (for example in case of calling GetDetectedSensors)
    // Heartbeat messages are sent every 20 frames, hence wait for some, expecting a nominal frequency of 150 Hz
    std::this_thread::sleep_for(std::chrono::duration<double>{2. * 20. / 150.});
}

ConnectionManager::~ConnectionManager() {
    Shutdown();
}

void ConnectionManager::ProcessParsedMessage(SerialNumber received_serial_number, Command&& command) {
    if (command.command_id != CommandId::PRD_HEARTBEAT_INFO) {
        return;
    }

    const auto heartbeat = HeartBeat::Deserialize(command.command);
    const SensorDetails sensor_details{heartbeat.ip_address, received_serial_number, heartbeat.software_version};

    detected_sensors_.Add(sensor_details);

    std::lock_guard<std::mutex> lock{mutex_};
    if (future_sensor_fill_condition_ && future_sensor_fill_condition_(sensor_details)) {
        auto sensor = connect_to_sensor_(sensor_details);
        if (sensor) {
            future_sensor_fill_condition_ = nullptr;
            connect_to_sensor_ = nullptr;
            future_sensor_.Fill(std::move(sensor));
        }
    }

    if (future_service_fill_condition_ && future_service_fill_condition_(sensor_details)) {
        auto service = detail::CreateService(sensor_details);
        if (service) {
            future_service_fill_condition_ = nullptr;
            future_service_.Fill(std::move(service));
        }
    }
}

}  // namespace api
}  // namespace accerion
