/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/structs/uint64_vector.h"

#include <limits>
#include <sstream>

#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                     | Length (bytes) | Description | Unit |
   |:-------------------------|---------------:|:------------|:-----|
   | `uint32_t`               |              4 | list size   | -    |
   | **for each element in the vector** ||||
   | &emsp; &emsp; `uint64_t` |              8 | element     | -    |
   | **end of the vector** ||||
   For more information, see the [protocol page](docs/protocol.md).

   Throws a runtime exception when the number of elements in the vector exceeds the maximum allowed number of elements.
 */
RawData UInt64Vector::Serialize() const {
    static constexpr auto kMaxSize = std::numeric_limits<std::uint32_t>::max();
    if (values.size() > kMaxSize) {
        std::stringstream ss;
        ss << "Cannot serialize a UInt64Vector with size " << values.size() << " as it exceeds the maximum " << kMaxSize;
        throw std::runtime_error(ss.str());
    }

    RawData data{};
    data.reserve(4 + 8 * values.size());

    Append(data, serialization::Serialize(static_cast<std::uint32_t>(values.size())));

    for (const auto& value: values) {
        Append(data, serialization::Serialize(value));
    }

    return data;
}

UInt64Vector UInt64Vector::Deserialize(const RawData& data) {
    UInt64Vector result{};

    const auto n_elements = serialization::Deserialize<std::uint32_t>({data.cbegin(), data.cbegin() + 4});
    result.values.reserve(n_elements);

    auto start_iter = data.cbegin() + 4;
    for (std::size_t i = 0; i < n_elements; ++i) {
        result.values.push_back(serialization::Deserialize<std::uint64_t>({start_iter, start_iter + 8}));
        start_iter += 8;
    }

    return result;
}

}  // namespace api
}  // namespace accerion

