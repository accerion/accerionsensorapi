/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/map_summary.h"

#include <numeric>

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

std::ostream& operator<<(std::ostream& ss, const MapSummary& summary) {
    ss << "database version: " << summary.database_version << "\n"
       << "total_clusters:   " << summary.CalculateClusterCount() << "\n"
       << "total_signatures: " << summary.CalculateSignatureCount() << "\n";

    ss << std::setw(11) << "cluster_id" << ": " << "size\n";
    for (const auto s : summary.cluster_details) {
        ss << std::setw(11) << std::right << s.first << ": " << s.second << "\n";
    }

    return ss;
}

/**
   \brief Serialize the struct for network communication

   | Type                                               | Length (bytes) | Description      | Unit |
   |:---------------------------------------------------|---------------:|:-----------------|:-----|
   | [Version](@ref accerion::api::Version::Serialize)] |              3 | database version | -    |
   | `uint32_t`                                         |              4 | list size        | -    |
   | **for each relation in the cluster details map** ||||
   | &emsp; &emsp; `uint64_t`                           |              8 | cluster id       | -    |
   | &emsp; &emsp; `uint64_t`                           |              8 | signature count  | -    |
   | **end of the map** ||||
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData MapSummary::Serialize() const {
    auto data = database_version.Serialize();
    Append(data, serialization::Serialize(static_cast<std::uint32_t>(cluster_details.size())));
    for (const auto cluster_detail : cluster_details) {
        Append(data, serialization::Serialize(cluster_detail.first));
        Append(data, serialization::Serialize(cluster_detail.second));
    }
    return data;
}

MapSummary MapSummary::Deserialize(const RawData& data) {
    MapSummary summary;
    constexpr int overview_offset = 7;

    summary.database_version = Version::Deserialize({data.cbegin(), data.cbegin() + 3});
    const auto n_elements = serialization::Deserialize<std::uint32_t>({data.cbegin() + 3, data.cbegin() + 7});
    for (int i = 0; i < n_elements; ++i) {
        const auto iter_offset = i * 16;
        auto first = serialization::Deserialize<ClusterId>({data.cbegin() + overview_offset + iter_offset,
                                                            data.cbegin() + overview_offset + iter_offset + 8});
        auto second = serialization::Deserialize<std::size_t>({data.cbegin() + overview_offset + iter_offset + 8,
                                                               data.cbegin() + overview_offset + iter_offset + 8 + 8});
        summary.cluster_details.emplace(first, second);
    }

    return summary;
}

std::size_t MapSummary::CalculateClusterCount() const {
    return cluster_details.size();
}

std::size_t MapSummary::CalculateSignatureCount() const {
    return std::accumulate(cluster_details.begin(), cluster_details.end(),
                            0UL,  // Initial value of the sum
                            [](std::size_t sum, const std::pair<const accerion::api::ClusterId, std::size_t>& p) {
                                return sum + p.second;
                            });
}

}  // namespace api
}  // namespace accerion
