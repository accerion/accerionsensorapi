/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/loop_closure.h"

#include <fstream>

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/filesystem.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                         | Length (bytes) | Description | Unit        |
   |:---------------------------------------------|---------------:|:------------|:------------|
   | `uint64_t`                                   |              8 | id_a        | SignatureId |
   | `uint64_t`                                   |              8 | id_b        | SignatureId |
   | [Pose](@ref accerion::api::Pose_::Serialize) |             20 | pose        | -           |
   For more information, see the [protocol page](docs/protocol.md).

   All pose information is given in the world coordinate system.
 */
RawData LoopClosure::Serialize() const {
    auto data = serialization::Serialize(id_a);
    Append(data, serialization::Serialize(id_b));
    Append(data, pose.Serialize());
    return data;
}

LoopClosure LoopClosure::Deserialize(const RawData& data) {
    LoopClosure result{};
    result.id_a = serialization::Deserialize<SignatureId>({data.cbegin(), data.cbegin() + 8});
    result.id_b = serialization::Deserialize<SignatureId>({data.cbegin() + 8, data.cbegin() + 16});
    result.pose = Pose::Deserialize({data.begin() + 16, data.begin() + 36});
    return result;
}

bool LoopClosure::WriteLoopClosuresToFile(const std::vector<LoopClosure>& loop_closures, const std::string& path) {
    if (accerion::api::FileExists(path)) {
        if (!accerion::api::RemoveFile(path)) return false;
    }
    std::ofstream out(path.c_str());

    if (!out) return false;

    for (const auto lc : loop_closures) {
        out << "EDGE_SE2 " << lc.id_a << " " << lc.id_b << " " << lc.pose.x << " " << lc.pose.y << " "
            << lc.pose.th * accerion::api::kDegToRad << " 1 0 0 1 0 1" << std::endl;
    }
    return true;
}

}  // namespace api
}  // namespace accerion
