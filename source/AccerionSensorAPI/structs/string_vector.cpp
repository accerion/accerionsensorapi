/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/string_vector.h"

#include "AccerionSensorAPI/structs/plain_string.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                    | Length (bytes) | Description       | Unit |
   |:------------------------|---------------:|:------------------|:-----|
   | uint8_t                 |              1 | amount of strings | -    |
   | **for each string in the vector** ||||
   | &emsp; &emsp; [PlainString](@ref accerion::api::PlainString::Serialize) |              * | character         | -    |
   | **end of the string** ||||
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData StringVector::Serialize() const {
    RawData vec;
    auto amount = std::min(value.size(), {std::numeric_limits<std::uint8_t>::max()});
    vec.push_back(static_cast<std::uint8_t>(amount));
    for (auto i = 0; i < amount; ++i) {
        Append(vec, accerion::api::PlainString{value[i]}.Serialize());
    }
    return vec;
}

StringVector StringVector::Deserialize(RawData raw_data) {
    StringVector vec;
    std::uint8_t number_of_strings = raw_data[0];
    size_t j = 1;
    for (int i = 0; i < number_of_strings; i++) {
        auto plain_string = accerion::api::PlainString::Deserialize({raw_data.cbegin() + j, raw_data.cend()});
        j += plain_string.value.length() + 2;  // plain string serialized is 2 bytes for length + size of message
        vec.value.emplace_back(std::move(plain_string.value));
    }
    return vec;
}

}  // namespace api
}  // namespace accerion

