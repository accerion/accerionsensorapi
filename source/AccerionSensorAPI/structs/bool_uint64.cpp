/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/bool_uint64.h"

#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description   | Unit |
   |:--------------------------------------------------|---------------:|:--------------|:-----|
   | [Boolean](@ref accerion::api::Boolean::Serialize) |              1 | boolean value | -    |
   | `uint64_t`                                        |              8 | uint64 value  | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData BoolUInt64::Serialize() const {
    RawData data{};
    Append(data, Boolean{bool_value}.Serialize());
    Append(data, serialization::Serialize(uint64_value));
    return data;
}

BoolUInt64 BoolUInt64::Deserialize(const RawData& raw_data) {
    BoolUInt64 result{};
    result.bool_value = Boolean::Deserialize({raw_data.front()}).value;
    result.uint64_value = serialization::Deserialize<std::uint64_t>({raw_data.cbegin() + 1, raw_data.cbegin() + 9});
    return result;
}

}  // namespace api
}  // namespace accerion
