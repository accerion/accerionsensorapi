/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/external_reference_pose.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

namespace detail {

const RawData kEmpty8Bytes(8);

}

/**
   \brief Serialize the struct for network communication

   | Type                                         | Length (bytes) | Description          | Unit        |
   |:---------------------------------------------|---------------:|:---------------------|:------------|
   | [Pose](@ref accerion::api::Pose_::Serialize) |             20 | sensor pose          | -           |
   | `uint64_t`                                   |              8 | time offset          | microsecond |
   | [Time](@ref accerion::api::Time::Serialize)  |              8 | timestamp (reserved) | -           |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData ExternalReferencePose::Serialize() const {
    if (!time_offset) {
        throw std::runtime_error("ExternalReferencePose::time_offset should be set");
    }
    RawData data{};
    Append(data, pose.Serialize());
    Append(data, time_offset ? serialization::Serialize(static_cast<std::uint64_t>(time_offset.value().count()))
                             : detail::kEmpty8Bytes);
    Append(data, timestamp ? Time{timestamp.value()}.Serialize() : detail::kEmpty8Bytes);
    return data;
}

ExternalReferencePose ExternalReferencePose::Deserialize(const RawData& raw_data) {
    ExternalReferencePose ext_ref_pose{};

    ext_ref_pose.pose = Pose::Deserialize({raw_data.begin(), raw_data.begin() + 20});

    // At this point, it cannot be known if the time offset was 0 or no data (both would be 8 bytes containing zero) was
    // sent, hence the conditional reset of the time offset when handling the timestamp
    ext_ref_pose.time_offset = std::chrono::microseconds{
        serialization::Deserialize<std::uint64_t>({raw_data.begin() + 20, raw_data.begin() + 28})};

    const RawData timestamp_data{raw_data.begin() + 28, raw_data.begin() + 36};
    if (timestamp_data != detail::kEmpty8Bytes) {
        ext_ref_pose.timestamp = Time::Deserialize(timestamp_data).value;
        ext_ref_pose.time_offset.reset();
    }

    return ext_ref_pose;
}

}  // namespace api
}  // namespace accerion
