/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/corrected_odometry.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                                  | Length (bytes) | Description             | Unit |
   |:----------------------------------------------------------------------|---------------:|:------------------------|:-----|
   | [Time](@ref accerion::api::Time::Serialize)                           |              8 | timestamp               | -    |
   | [Pose](@ref accerion::api::Pose_::Serialize)                          |             20 | pose                    | -    |
   | [StandardDeviation](@ref accerion::api::StandardDeviation::Serialize) |             12 | pose standard deviation | -    |
   For more information, see the [protocol page](docs/protocol.md).

   All pose information is given in the world coordinate system.
 */
RawData CorrectedOdometry::Serialize() const {
    auto data = Time{timestamp}.Serialize();
    Append(data, pose.Serialize());
    Append(data, standard_deviation.Serialize());
    return data;
}

CorrectedOdometry CorrectedOdometry::Deserialize(const RawData& raw_data) {
    CorrectedOdometry corrected_odom{};
    corrected_odom.timestamp = Time::Deserialize({raw_data.cbegin(), raw_data.cbegin() + 8}).value;
    corrected_odom.pose = Pose::Deserialize({raw_data.cbegin() + 8, raw_data.cbegin() + 28});
    corrected_odom.standard_deviation = StandardDeviation::Deserialize({raw_data.cbegin() + 28, raw_data.cbegin() + 40});
    return corrected_odom;
}

}  // namespace api
}  // namespace accerion
