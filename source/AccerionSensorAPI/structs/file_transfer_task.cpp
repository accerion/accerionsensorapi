/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/file_transfer_task.h"

#include "AccerionSensorAPI/structs/plain_string.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                      | Length (bytes) | Description                                                          | Unit    |
   |:----------------------------------------------------------|---------------:|:---------------------------------------------------------------------|:--------|
   | `uint8_t`                                                 |              1 | [FileTransferTask](@ref accerion::api::FileTransferTask)             | -       |
   | `uint8_t`                                                 |              1 | [FileTransferTaskStatus](@ref accerion::api::FileTransferTaskStatus) | -       |
   | `uint8_t`                                                 |              1 | progress if FileTransferTaskStatus is kTaskProgress                  | percent |
   | [PlainString](@ref accerion::api::PlainString::Serialize) |              - | message if FileTransferTaskStatus is kTaskFail                       | -       |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData FileTransferTaskResult::Serialize() const {
    RawData data{};
    data.push_back(GetEnumValue(task));
    data.push_back(GetEnumValue(status));
    data.push_back(progress);
    accerion::api::Append(data, PlainString{message}.Serialize());
    return data;
}

FileTransferTaskResult FileTransferTaskResult::Deserialize(const RawData& data) {
    FileTransferTaskResult result{};
    result.task = static_cast<FileTransferTask>(data[0]);
    result.status = static_cast<FileTransferTaskStatus>(data[1]);
    result.progress = data[2];
    result.message = PlainString::Deserialize({data.cbegin() + 3, data.cend()}).value;
    return result;
}

/**
   \brief Serialize the struct for network communication

   | Type                    | Length (bytes) | Description                                              | Unit |
   |:------------------------|---------------:|:---------------------------------------------------------|:-----|
   | `uint8_t`               |              1 | [FileTransferTask](@ref accerion::api::FileTransferTask) | -    |
   | **for each byte in the list** ||||
   | &emsp; &emsp; `uint8_t` |              1 | byte                                                     | -    |
   | **end of the list** ||||
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData FileTransferTaskRequest::Serialize() const {
    RawData vec;
    vec.push_back(GetEnumValue(task));
    Append(vec, data);
    return vec;
}

FileTransferTaskRequest FileTransferTaskRequest::Deserialize(RawData data) {
    FileTransferTaskRequest req{};
    req.task = static_cast<FileTransferTask>(data[0]);

    // As of 6.1.1, the raw_data.size() > 1 for req.task = FileTransferTask::kGatherRecordings
    if (data.size() > 1) {
        data.erase(data.begin());
        req.data = std::move(data);
    }
    return req;
}

}  // namespace api
}  // namespace accerion

