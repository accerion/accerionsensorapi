/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/signature_vector.h"

#include <limits>
#include <sstream>

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                                | Length (bytes) | Description    | Unit |
   |:--------------------------------------------------------------------|---------------:|:---------------|:-----|
   | `uint32_t`                                                          |              4 | length list    | -    |
   | `uint16_t`                                                          |              2 | list item size | byte |
   | **for each signature in the list** ||||
   | &emsp; &emsp; [Signature](@ref accerion::api::Signature::Serialize) |              - | signature      | -    |
   | **end of the list** ||||
   For more information, see the [protocol page](docs/protocol.md).

   Throws a runtime exception when the number of elements in the vector exceeds the maximum allowed number of elements
   dictated by uint64_t.
 */
RawData SignatureVector::Serialize() const {
    static constexpr auto kMaxSize = std::numeric_limits<std::uint32_t>::max();
    if (value.size() > kMaxSize) {
        std::stringstream ss;
        ss << "Cannot serialize a SignatureVector with size " << value.size() << " as it exceeds the maximum " << kMaxSize;
        throw std::runtime_error(ss.str());
    }
    RawData vec{};
    Append(vec, serialization::Serialize(static_cast<std::uint32_t>(value.size())));
    Append(vec, serialization::Serialize(static_cast<std::uint16_t>(20 + sizeof(ClusterId) + sizeof(SignatureId))));  // 20 = serialization size of Pose
    for (const auto& sig : value) {
        Append(vec, sig.Serialize());
    }
    return vec;
}

SignatureVector SignatureVector::Deserialize(const RawData& data) {
    const auto number_of_signatures =
            serialization::Deserialize<std::uint32_t>({data.cbegin(), data.cbegin() + 4});
    const auto size_of_signature =
            serialization::Deserialize<std::uint16_t>({data.cbegin() + 4, data.cbegin() + 6});

    SignatureVector result{};
    result.value.reserve(number_of_signatures);
    auto start_iter = data.cbegin() + 6;
    for (auto index = 0; index < number_of_signatures; ++index) {
        result.value.push_back(Signature::Deserialize({start_iter, start_iter + size_of_signature}));
        start_iter += size_of_signature;
    }
    return result;
}

}  // namespace api
}  // namespace accerion

