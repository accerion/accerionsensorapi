/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/ram_rom_stats.h"

#include "AccerionSensorAPI/structs/plain_string.h"
#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/enum.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                      | Length (bytes) | Description                            | Unit      |
   |:----------------------------------------------------------|---------------:|:---------------------------------------|:----------|
   | `uint8_t`                                                 |              1 | [DirType](@ref accerion::api::DirType) | -         |
   | `uint32_t`                                                |              4 | size                                   | megabytes |
   | [PlainString](@ref accerion::api::PlainString::Serialize) |              - | name                                   | -         |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData DirSizePart::Serialize() const {
    RawData vec{};
    vec.push_back(GetEnumValue(type));
    Append(vec, serialization::Serialize(size_in_mb));
    Append(vec, PlainString{name}.Serialize());
    return vec;
}

DirSizePart DirSizePart::Deserialize(const RawData& data) {
    DirSizePart result{};
    result.type = static_cast<DirType>(data.front());
    result.size_in_mb = serialization::Deserialize<std::uint32_t>({data.cbegin() + 1, data.cbegin() + 5});
    result.name = PlainString::Deserialize({data.cbegin() + 5, data.cend()}).value;
    return result;
}

/**
   \brief Serialize the struct for network communication

   | Type                                                                    | Length (bytes) | Description   | Unit     |
   |:------------------------------------------------------------------------|---------------:|:--------------|:---------|
   | `uint32_t`                                                              |              4 | ROM available | megabyte |
   | `uint32_t`                                                              |              4 | ROM total     | megabyte |
   | `uint32_t`                                                              |              4 | SD available  | megabyte |
   | `uint32_t`                                                              |              4 | SD total      | megabyte |
   | `uint16_t`                                                              |              2 | RAM used      | megabyte |
   | `uint16_t`                                                              |              2 | RAM total     | megabyte |
   | **for each part in the list** ||||
   | &emsp; &emsp; [DirSizePart](@ref accerion::api::DirSizePart::Serialize) |              - | part          | -        |
   | **end of the list** ||||
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData RamRomStats::Serialize() const {
    auto data = serialization::Serialize(rom_available);
    Append(data, serialization::Serialize(rom_total));
    Append(data, serialization::Serialize(sd_available));
    Append(data, serialization::Serialize(sd_total));
    Append(data, serialization::Serialize(ram_used));
    Append(data, serialization::Serialize(ram_total));
    for (const auto& part : parts) {
        Append(data, part.Serialize());
    }
    return data;
}

RamRomStats RamRomStats::Deserialize(const RawData& data) {
    RamRomStats result{};

    result.rom_available = serialization::Deserialize<std::uint32_t>({data.cbegin(), data.cbegin() + 4});
    result.rom_total = serialization::Deserialize<std::uint32_t>({data.cbegin() + 4, data.cbegin() + 8});
    result.sd_available = serialization::Deserialize<std::uint32_t>({data.cbegin() + 8, data.cbegin() + 12});
    result.sd_total = serialization::Deserialize<std::uint32_t>({data.cbegin() + 12, data.cbegin() + 16});
    result.ram_used = serialization::Deserialize<std::uint16_t>({data.cbegin() + 16, data.cbegin() + 18});
    result.ram_total = serialization::Deserialize<std::uint16_t>({data.cbegin() + 18, data.cbegin() + 20});

    std::int64_t offset = 20;
    while (offset < data.size()) {
        auto part = DirSizePart::Deserialize({data.cbegin() + offset, data.cend()});
        offset += 5 + static_cast<std::int64_t>(part.name.length()) + 2;  // see DirSizePart::Deserialize
        result.parts.push_back(std::move(part));
    }

    return result;
}

}  // namespace api
}  // namespace accerion
