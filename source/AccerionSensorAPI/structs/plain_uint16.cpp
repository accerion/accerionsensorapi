/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/plain_uint16.h"

#include "AccerionSensorAPI/structs/serialization.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description | Unit |
   |:-----------|---------------:|:------------|:-----|
   | `uint16_t` |              2 | value       | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData PlainUInt16::Serialize() const {
    return serialization::Serialize(value);
}

PlainUInt16 PlainUInt16::Deserialize(const RawData& data) {
    return {serialization::Deserialize<std::uint16_t>({data.cbegin(), data.cbegin() + 2})};
}

}  // namespace api
}  // namespace accerion
