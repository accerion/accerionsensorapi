/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/structs/created_signature.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                  | Length (bytes) | Description | Unit |
   |:------------------------------------------------------|---------------:|:------------|:-----|
   | [Time](@ref accerion::api::Time::Serialize)           |              8 | timestamp   | -    |
   | [Signature](@ref accerion::api::Signature::Serialize) |             36 | signature   | -    |
   For more information, see the [protocol page](docs/protocol.md).

   All pose information is given in the world coordinate system.
 */
RawData CreatedSignature::Serialize() const {
    auto data = Time{timestamp}.Serialize();
    Append(data, signature.Serialize());
    return data;
}

CreatedSignature CreatedSignature::Deserialize(const RawData& raw_data) {
    CreatedSignature result{};
    result.timestamp = Time::Deserialize({raw_data.cbegin(), raw_data.cbegin() + 8}).value;
    result.signature = Signature::Deserialize({raw_data.cbegin() + 8, raw_data.cbegin() + 44});
    return result;
}

bool CreatedSignature::operator==(const CreatedSignature& other) const {
    return timestamp == other.timestamp && signature == other.signature;
}

}  // namespace api
}  // namespace accerion
