/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/drift_correction.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                                  | Length (bytes) | Description                   | Unit          |
   |:----------------------------------------------------------------------|---------------:|:------------------------------|:--------------|
   | [Time](@ref accerion::api::Time::Serialize)                           |              8 | timestamp                     | -             |
   | [Pose](@ref accerion::api::Pose_::Serialize)                          |             20 | sensor pose                   | -             |
   | [Pose](@ref accerion::api::Pose_::Serialize)                          |             20 | sensor pose correction        | -             |
   | `uint32_t`                                                            |              4 | cumulative traveled distance  | micrometer    |
   | `uint32_t`                                                            |              4 | cumulative traveled heading   | centidegree   |
   | `uint32_t`                                                            |              4 | error percentage              | 10^-3 percent |
   | `uint64_t`                                                            |              8 | signature id                  | -             |
   | `uint64_t`                                                            |              8 | cluster id                    | -             |
   | `uint8_t`                                                             |              1 | quality estimate              | percent       |
   | [StandardDeviation](@ref accerion::api::StandardDeviation::Serialize) |             12 | correction standard deviation | -             |
   For more information, see the [protocol page](docs/protocol.md).

   All pose information is given in the world coordinate system.
 */
RawData DriftCorrection::Serialize() const {
    auto data = Time{timestamp}.Serialize();
    Append(data, pose.Serialize());
    Append(data, delta.Serialize());
    Append(data, Double<std::uint32_t, std::micro>{cumulative_travelled_distance}.Serialize());
    Append(data, Double<std::uint32_t, std::centi>{cumulative_travelled_heading}.Serialize());
    Append(data, Double<std::uint32_t, std::milli>{error_percentage}.Serialize());
    Append(data, serialization::Serialize(signature_id));
    Append(data, serialization::Serialize(cluster_id));
    data.push_back(quality_estimate);
    Append(data, standard_deviation.Serialize());
    return data;
}

DriftCorrection DriftCorrection::Deserialize(const RawData& data) {
    DriftCorrection dc{};
    dc.timestamp = Time::Deserialize({data.cbegin(), data.cbegin() + 8}).value;
    dc.pose = Pose::Deserialize({data.begin() + 8, data.begin() + 28});
    dc.delta = Pose::Deserialize({data.cbegin() + 28, data.cbegin() + 48});
    dc.cumulative_travelled_distance =
            Double<std::uint32_t, std::micro>::Deserialize({data.cbegin() + 48, data.cbegin() + 52}).value;
    dc.cumulative_travelled_heading =
            Double<std::uint32_t, std::centi>::Deserialize({data.cbegin() + 52, data.cbegin() + 56}).value;
    dc.error_percentage =
            Double<std::uint32_t, std::milli>::Deserialize({data.cbegin() + 56, data.cbegin() + 60}).value;
    dc.signature_id = serialization::Deserialize<SignatureId>({data.cbegin() + 60, data.cbegin() + 68});
    dc.cluster_id = serialization::Deserialize<ClusterId>({data.cbegin() + 68, data.cbegin() + 76});
    dc.quality_estimate = data[76];
    dc.standard_deviation = StandardDeviation::Deserialize({data.begin() + 77, data.begin() + 89});
    return dc;
}

}  // namespace api
}  // namespace accerion
