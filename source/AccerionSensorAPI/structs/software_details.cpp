/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/software_details.h"

#include <ostream>

#include "AccerionSensorAPI/structs/plain_string.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                      | Length (bytes) | Description   | Unit |
   |:----------------------------------------------------------|---------------:|:--------------|:-----|
   | [PlainString](@ref accerion::api::PlainString::Serialize) |             40 | software hash | -    |
   | [PlainString](@ref accerion::api::PlainString::Serialize) |             11 | date          | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData SoftwareDetails::Serialize() const {
    RawData vec;
    Append(vec, PlainString{software_hash}.Serialize());
    Append(vec, PlainString{date}.Serialize());
    return vec;
}

SoftwareDetails SoftwareDetails::Deserialize(RawData raw_data) {
    SoftwareDetails sd;
    sd.software_hash = PlainString::Deserialize(raw_data).value;
    // plain string serialized is 2 bytes for length + size of message
    sd.date = PlainString::Deserialize({raw_data.begin() + sd.software_hash.length() + 2, raw_data.end()}).value;
    return sd;
}

std::ostream& operator<<(std::ostream& os, const SoftwareDetails& version) {
    return os << version.date << ", " << version.software_hash;
}

}  // namespace api
}  // namespace accerion
