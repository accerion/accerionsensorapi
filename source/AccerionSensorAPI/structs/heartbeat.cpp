/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/heartbeat.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description      | Unit |
   |:--------------------------------------------------|---------------:|:-----------------|------|
   | [Address](@ref accerion::api::Address::Serialize) |              4 | ip address       | -    |
   | [Address](@ref accerion::api::Address::Serialize) |              4 | unicast address  | -    |
   | [Version](@ref accerion::api::Version::Serialize) |              3 | software version | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData HeartBeat::Serialize() const {
    auto data = ip_address.Serialize();
    Append(data, unicast_address.Serialize());
    Append(data, software_version.Serialize());
    return data;
}

HeartBeat HeartBeat::Deserialize(const RawData& data) {
    HeartBeat heartbeat{};
    heartbeat.ip_address = Address::Deserialize({data.cbegin(), data.cbegin() + 4});
    heartbeat.unicast_address = Address::Deserialize({data.cbegin() + 4, data.cbegin() + 8});
    heartbeat.software_version = Version::Deserialize({data.cbegin() + 8, data.cbegin() + 11});
    return heartbeat;
}

}  // namespace api
}  // namespace accerion
