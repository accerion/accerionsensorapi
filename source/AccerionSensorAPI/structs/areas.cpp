/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/structs/areas.h"

#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/structs/uint16_vector.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                         | Length (bytes) | Description                                    | Unit |
   |:-------------------------------------------------------------|---------------:|:-----------------------------------------------|:-----|
   | `uint16_t`                                                   |              2 | Active area                                    | -    |
   | [UInt16Vector](@ref accerion::api::UInt16Vector::Serialize)] |              ? | All areas, including active one                | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData Areas::Serialize() const {
    RawData vec;
    Append(vec, serialization::Serialize(active_area));
    Append(vec, UInt16Vector{all_areas}.Serialize());
    return vec;
}

Areas Areas::Deserialize(RawData raw_data) {
    Areas areas{};
    areas.active_area = serialization::Deserialize<std::uint16_t>({raw_data.cbegin(), raw_data.cbegin() + 2});
    areas.all_areas = UInt16Vector::Deserialize({raw_data.cbegin() + 2, raw_data.cend()}).value;
    return areas;
}

}  // namespace api
}  // namespace accerion
