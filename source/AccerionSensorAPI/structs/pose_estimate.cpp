/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/pose_estimate.h"

namespace accerion {
namespace api {

PoseEstimate PoseEstimate::Identity() {
    return {Pose::Identity(), {}};
}

PoseEstimate PoseEstimate::Interpolate(double ratio, const PoseEstimate& p) const {
    return {pose.Interpolate(ratio, p.pose), cov.Interpolate(ratio, p.cov)};
}

}  // namespace api
}  // namespace accerion
