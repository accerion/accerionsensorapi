/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/odometry.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                                  | Length (bytes) | Description                 | Unit |
   |:----------------------------------------------------------------------|---------------:|:----------------------------|:-----|
   | [Time](@ref accerion::api::Time::Serialize)                           |              8 | timestamp                   | -    |
   | [Pose](@ref accerion::api::Pose_::Serialize)                          |             20 | pose                        | -    |
   | [Velocity](@ref accerion::api::Pose_::Serialize)                      |             20 | velocity                    | -    |
   | [StandardDeviation](@ref accerion::api::StandardDeviation::Serialize) |             12 | velocity standard deviation | -    |
   For more information, see the [protocol page](docs/protocol.md).

   All pose information is given in the world coordinate system.
 */
RawData Odometry::Serialize() const {
    RawData data{};
    Append(data, Time{timestamp}.Serialize());
    Append(data, pose.Serialize());
    Append(data, velocity.Serialize());
    Append(data, standard_deviation_velocity.Serialize());
    return data;
}

Odometry Odometry::Deserialize(RawData raw_data) {
    Odometry odometry{};
    odometry.timestamp = Time::Deserialize({raw_data.cbegin(), raw_data.cbegin() + 8}).value;
    odometry.pose = Pose::Deserialize({raw_data.begin() + 8, raw_data.begin() + 28});
    odometry.velocity = Velocity::Deserialize({raw_data.cbegin() + 28, raw_data.cbegin() + 48});
    odometry.standard_deviation_velocity =
            StandardDeviation::Deserialize({raw_data.begin() + 48, raw_data.begin() + 60});
    return odometry;
}

}  // namespace api
}  // namespace accerion
