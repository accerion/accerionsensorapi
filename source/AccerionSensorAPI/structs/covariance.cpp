/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/covariance.h"

#include <cmath>

#include "AccerionSensorAPI/structs/standard_deviation.h"

namespace accerion {
namespace api {

bool Covariance::operator==(const Covariance &other) const {
    return x_x == other.x_x && y_y == other.y_y && th_th == other.th_th;
}

bool Covariance::operator!=(const Covariance &other) const {
    return !(*this == other);
}

Covariance Covariance::Interpolate(double ratio, const Covariance& other) const {
    const StandardDeviation this_std_dev{std::sqrt(x_x), std::sqrt(y_y), std::sqrt(th_th)};
    const StandardDeviation other_std_dev{std::sqrt(other.x_x), std::sqrt(other.y_y), std::sqrt(other.th_th)};
    const StandardDeviation result = this_std_dev.Interpolate(ratio, other_std_dev);
    return {std::pow(result.x, 2), std::pow(result.y, 2), std::pow(result.th, 2)};
}

}  // namespace api
}  // namespace accerion
