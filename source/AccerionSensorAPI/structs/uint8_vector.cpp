/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/uint8_vector.h"

#include <limits>
#include <sstream>

#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                     | Length (bytes) | Description | Unit |
   |:-------------------------|---------------:|:-------------------|
   | `uint32_t`               |              4 | list size   | -    |
   | **for each element in the vector** ||||
   | &emsp; &emsp; `uint8_t` |               1 | element     | -    |
   | **end of the vector** ||||
   For more information, see the [protocol page](docs/protocol.md).

   Throws a runtime exception when the number of elements in the vector exceeds the maximum allowed number of elements.
 */
RawData UInt8Vector::Serialize() const {
    auto max_size = std::numeric_limits<std::uint64_t>::max();
    if (value.size() > max_size) {
        std::stringstream ss;
        ss << "Cannot serialize a UInt8Vector with size " << value.size() << " as it exceeds the maximum " << max_size;
        throw std::runtime_error(ss.str());
    }

    RawData data{};
    data.reserve(4 + value.size());
    Append(data, serialization::Serialize(static_cast<std::uint32_t>(value.size())));
    Append(data, value);
    return data;
}

UInt8Vector UInt8Vector::Deserialize(const RawData& data) {
    const auto n_elements = serialization::Deserialize<std::uint32_t>({data.cbegin(), data.cbegin() + 4});
    return {{data.cbegin() + 4, data.cbegin() + 4 + n_elements}};
}

}  // namespace api
}  // namespace accerion
