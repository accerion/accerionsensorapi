/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/date_time.h"

#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description | Unit |
   |:-----------|---------------:|:------------|:-----|
   | `uint16_t` |              2 | year        | -    |
   | `uint8_t`  |              1 | month       | -    |
   | `uint8_t`  |              1 | day         | -    |
   | `uint8_t`  |              1 | hours       | -    |
   | `uint8_t`  |              1 | minutes     | -    |
   | `uint8_t`  |              1 | seconds     | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData DateTime::Serialize() const {
    RawData vec{};
    Append(vec, serialization::Serialize(year));
    vec.emplace_back(month);
    vec.emplace_back(day);
    vec.emplace_back(hours);
    vec.emplace_back(minutes);
    vec.emplace_back(seconds);
    return vec;
}

DateTime DateTime::Deserialize(const RawData& data) {
    DateTime dt{};
    dt.year = serialization::Deserialize<std::uint16_t>({data.cbegin(), data.cbegin() + 2});
    dt.month = data[2];
    dt.day = data[3];
    dt.hours = data[4];
    dt.minutes = data[5];
    dt.seconds = data[6];
    return dt;
}

bool DateTime::operator==(const DateTime& other) const {
    return day == other.day && month == other.month && year == other.year &&
           hours == other.hours && minutes == other.minutes && seconds == other.seconds;
}

}  // namespace api
}  // namespace accerion
