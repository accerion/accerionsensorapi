/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/uint16_vector.h"

#include <limits>
#include <sstream>

#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                     | Length (bytes) | Description | Unit |
   |:-------------------------|---------------:|:------------|:-----|
   | `uint32_t`               |              4 | element     | -    |
   | **for each element in the vector** ||||
   | &emsp; &emsp; `uint16_t` |              2 | element     | -    |
   | **end of the vector** ||||
   For more information, see the [protocol page](docs/protocol.md).

   Throws a runtime exception when the number of elements in the vector exceeds the maximum allowed number of elements
   dictated by uint64_t.
 */
RawData UInt16Vector::Serialize() const {
    static constexpr auto kMaxSize = std::numeric_limits<std::uint32_t>::max();
    if (value.size() > kMaxSize) {
        std::stringstream ss;
        ss << "Cannot serialize a UInt16Vector with size " << value.size() << " as it exceeds the maximum " << kMaxSize;
        throw std::runtime_error(ss.str());
    }

    RawData data{};
    data.reserve(4 + 2 * value.size());
    Append(data, serialization::Serialize(static_cast<std::uint32_t>(value.size())));
    for (const auto& element: value) {
        Append(data, serialization::Serialize(element));
    }
    return data;
}

UInt16Vector UInt16Vector::Deserialize(const RawData& data) {
    UInt16Vector result{};

    const auto n_elements = serialization::Deserialize<std::uint32_t>({data.cbegin(), data.cbegin() + 4});
    result.value.reserve(n_elements);

    auto start_iter = data.cbegin() + 4;
    for (auto index = 0; index < n_elements; ++index) {
        result.value.push_back(serialization::Deserialize<std::uint16_t>({start_iter, start_iter + 2}));
        start_iter += 2;
    }

    return result;
}

}  // namespace api
}  // namespace accerion
