/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/buffered_recovery.h"

#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/double.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

/**
 * \brief Serialize the struct for network communication
 *
 * | Type       | Length (bytes) | Description | Unit       |
 * |:-----------|---------------:|:------------|:-----------|
 * | `uint32_t` |              4 | length      | micrometer |
 * For more information, see the [protocol page](docs/protocol.md).
 */
RawData BufferLength::Serialize() const {
    return Double<std::uint32_t, std::micro>{buffer_length}.Serialize();
}

BufferLength BufferLength::Deserialize(const RawData& raw_data) {
    return {static_cast<float>(
            Double<std::uint32_t, std::micro>::Deserialize({raw_data.cbegin(), raw_data.cbegin() + 4}).value)};
}

/**
 * \brief Serialize the struct for network communication
 *
 * | Type                                              | Length (bytes) | Description                                                                      | Unit    |
 * |:--------------------------------------------------|---------------:|:---------------------------------------------------------------------------------|:--------|
 * | `uint8_t`                                         |              1 | [BufferedRecoveryMessageType](@ref accerion::api::BufferedRecoveryMessageType)   | -       |
   | [Boolean](@ref accerion::api::Boolean::Serialize) |              1 | indicates if recovery was successful if BufferedRecoveryMessageType is kFinished | -       |
 * | `uint8_t`                                         |              1 | progress if BufferedRecoveryMessageType is kProgress                             | percent |
 * | `uint8_t`                                         |              1 | low overlap ratio if BufferedRecoveryMessageType is kLowOverlap                  | percent |
 * For more information, see the [protocol page](docs/protocol.md).
 */
RawData BufferProgress::Serialize() const {
    return {GetEnumValue(message_type), Boolean{result}.Serialize().front(), progress, percentage_of_low_overlap};
}

BufferProgress BufferProgress::Deserialize(const RawData& raw_data) {
    BufferProgress result{};
    result.message_type = static_cast<BufferedRecoveryMessageType>(raw_data[0]);
    result.result = Boolean::Deserialize({raw_data[1]}).value;
    result.progress = raw_data[2];
    result.percentage_of_low_overlap = raw_data[3];
    return result;
}

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description     | Unit       |
   |:--------------------------------------------------|---------------:|:----------------|:-----------|
   | `int64_t`                                         |              8 | x position      | micrometer |
   | `int64_t`                                         |              8 | y position      | micrometer |
   | `uint32_t`                                        |              4 | search radius   | micrometer |
   | [Boolean](@ref accerion::api::Boolean::Serialize) |              1 | full map search | -          |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData BufferedRecoveryRequest::Serialize() const {
    auto data = Double<std::int64_t, std::micro>{x_pos}.Serialize();
    Append(data, Double<std::int64_t, std::micro>{y_pos}.Serialize());
    Append(data, Double<std::uint32_t, std::micro>{radius}.Serialize());
    Append(data, Boolean{full_map_search}.Serialize());
    return data;
}

BufferedRecoveryRequest BufferedRecoveryRequest::Deserialize(const RawData& data) {
    // Implementation note of Pose_::Deserialize applies here as well
    BufferedRecoveryRequest req{};
    req.x_pos = Double<std::int64_t, std::micro>::Deserialize({data.cbegin(), data.cbegin() + 8}).value;
    req.y_pos = Double<std::int64_t, std::micro>::Deserialize({data.cbegin() + 8, data.cbegin() + 16}).value;
    req.radius = Double<std::uint32_t, std::micro>::Deserialize({data.cbegin() + 16, data.cbegin() + 20}).value;
    req.full_map_search = Boolean::Deserialize({data[20]}).value;
    return req;
}

}  // namespace api
}  // namespace accerion
