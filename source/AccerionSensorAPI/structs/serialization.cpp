/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/serialization.h"

#include <algorithm>

namespace accerion {
namespace api {
namespace serialization {

namespace detail {

std::vector<std::uint8_t> Reverse(std::vector<std::uint8_t>&& data) {
    std::reverse(data.begin(), data.end());
    return std::move(data);
}

}  // namespace detail

}  // namespace serialization
}  // namespace api
}  // namespace accerion
