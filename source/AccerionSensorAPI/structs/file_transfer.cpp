/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/file_transfer.h"

#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

bool IsUpload(const TransferType& type) {
    return type == TransferType::kUploadUpdate ||
            type == TransferType::kUploadLegacyAmf ||
            type == TransferType::kUploadMap;
}

/**
   \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description                                      | Unit |
   |:-----------|---------------:|:-------------------------------------------------|:-----|
   | `uint8_t`  |              1 | [TransferType](@ref accerion::api::TransferType) | -    |
   | `uint64_t` |              8 | file size                                        | byte |
   | `uint64_t` |              8 | chunk size                                       | byte |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData FileUploadDownloadRequest::Serialize() const {
    RawData data{};
    data.push_back(GetEnumValue(transfer_type));
    Append(data, serialization::Serialize(file_size));
    Append(data, serialization::Serialize(chunk_size));
    return data;
}

FileUploadDownloadRequest FileUploadDownloadRequest::Deserialize(const RawData& raw_data) {
    FileUploadDownloadRequest result{};
    result.transfer_type = static_cast<TransferType>(raw_data[0]);
    result.file_size = serialization::Deserialize<std::uint64_t>({raw_data.cbegin() + 1, raw_data.cbegin() + 9});
    result.chunk_size = serialization::Deserialize<std::uint64_t>({raw_data.cbegin() + 9, raw_data.cbegin() + 17});
    return result;
}

/**
   \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description                                          | Unit |
   |:-----------|---------------:|:-----------------------------------------------------|:-----|
   | `uint8_t`  |              1 | [TransferStatus](@ref accerion::api::TransferStatus) | -    |
   | `uint8_t`  |              1 | [TransferType](@ref accerion::api::TransferType)     | -    |
   | `uint16_t` |              2 | tcp port                                             | -    |
   | `uint64_t` |              8 | file size                                            | byte |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData FileTransferHandshake::Serialize() const {
    RawData data{};
    data.push_back(GetEnumValue(transfer_status));
    data.push_back(GetEnumValue(transfer_type));
    Append(data, serialization::Serialize(port_number));
    Append(data, serialization::Serialize(file_size));
    return data;
}

FileTransferHandshake FileTransferHandshake::Deserialize(const RawData& raw_data) {
    FileTransferHandshake result{};
    result.transfer_status = static_cast<TransferStatus>(raw_data[0]);
    result.transfer_type = static_cast<TransferType>(raw_data[1]);
    result.port_number = serialization::Deserialize<std::uint16_t>({raw_data.cbegin() + 2, raw_data.cbegin() + 4});
    result.file_size = serialization::Deserialize<std::uint64_t>({raw_data.cbegin() + 4, raw_data.cbegin() + 12});
    return result;
}

}  // namespace api
}  // namespace accerion
