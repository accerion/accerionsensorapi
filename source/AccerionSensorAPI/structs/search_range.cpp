/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/search_range.h"

#include "AccerionSensorAPI/structs/double.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
 * \brief Serialize the struct for network communication
 *
 * | Type       | Length (bytes) | Description | Unit        |
 * |:-----------|---------------:|:------------|:------------|
 * | `uint32_t` |              4 | radius      | micrometer  |
 * | `uint32_t` |              4 | angle       | centidegree |
 * For more information, see the [protocol page](docs/protocol.md).
 */
RawData SearchRange::Serialize() const {
    if (radius < 0 || angle < 0) {
        throw std::runtime_error("Values are serialized as uint32, thus the values cannot be negative");
    }
    auto data = Double<std::uint32_t, std::micro>{radius}.Serialize();
    Append(data, Double<std::uint32_t, std::centi>{angle}.Serialize());
    return data;
}

SearchRange SearchRange::Deserialize(const RawData& data) {
    SearchRange result{};
    result.radius = Double<std::uint32_t, std::micro>::Deserialize({data.cbegin(), data.cbegin() + 4}).value;
    result.angle = Double<std::uint32_t, std::centi>::Deserialize({data.cbegin() + 4, data.cbegin() + 8}).value;
    return result;
}

}  // namespace api
}  // namespace accerion
