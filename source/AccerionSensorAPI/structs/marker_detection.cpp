/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/marker_detection.h"

#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/plain_string.h"
#include "AccerionSensorAPI/utils/enum.h"
#include "AccerionSensorAPI/utils/algorithm.h"


namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description                                    | Unit |
   |:--------------------------------------------------|---------------:|:-----------------------------------------------|:-----|
   | [Boolean](@ref accerion::api::Boolean::Serialize) |              1 | boolean indicating if mode should be on or off | -    |
   | `uint8_t`                                         |              1 | [MarkerType](@ref accerion::api::MarkerType)   | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData MarkerDetectionMode::Serialize() const {
    auto data = Boolean{mode_state}.Serialize();
    data.push_back(GetEnumValue(type));
    return data;
}

MarkerDetectionMode MarkerDetectionMode::Deserialize(const RawData& data) {
    MarkerDetectionMode result{};
    result.mode_state = Boolean::Deserialize({data[0]}).value;
    result.type = static_cast<MarkerType>(data[1]);
    return result;
}

/**
   \brief Serialize the struct for network communication

   | Type                                                      | Length (bytes) | Description                                  | Unit |
   |:----------------------------------------------------------|---------------:|:---------------------------------------------|:-----|
   | [Time](@ref accerion::api::Time::Serialize)               |              8 | timestamp                                    | -    |
   | `uint8_t`                                                 |              1 | [MarkerType](@ref accerion::api::MarkerType) | -    |
   | [PlainString](@ref accerion::api::PlainString::Serialize) |              - | marker id                                    | -    |
   | [Pose](@ref accerion::api::Pose_::Serialize)              |             20 | marker pose in sensor coordinate system      | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData MarkerDetection::Serialize() const {
    RawData data{};
    Append(data, Time{timestamp}.Serialize());
    data.push_back(GetEnumValue(type));
    Append(data, PlainString{id}.Serialize());
    Append(data, pose.Serialize());
    return data;
}

MarkerDetection MarkerDetection::Deserialize(const RawData& data) {
    MarkerDetection detection{};
    detection.timestamp = Time::Deserialize({data.cbegin(), data.cbegin() + 8}).value;
    detection.type = static_cast<MarkerType>(data[8]);
    detection.id = PlainString::Deserialize({data.cbegin() + 9, data.cend()}).value;
    const auto offset = 8 + 1 + static_cast<std::int64_t>(detection.id.length()) + 2;  // 8 bytes for timestamp, 1 for type, and X + 2 for PlainString
    detection.pose = Pose::Deserialize({data.cbegin() + offset, data.cbegin() + offset + 20});
    return detection;
}

}  // namespace api
}  // namespace accerion
