/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/structs/latest_external_reference.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                                      | Length (bytes) | Description  | Unit |
   |:----------------------------------------------------------|---------------:|:-------------|:-----|
   | [Time](@ref accerion::api::Time::Serialize)               |              8 | timestamp    | -    |
   | [Pose](@ref accerion::api::Pose_::Serialize)              |             20 | sensor pose  | -    |
   | [SearchRange](@ref accerion::api::SearchRange::Serialize) |              8 | search range | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData LatestExternalReference::Serialize() const {
    auto data = Time{timestamp}.Serialize();
    Append(data, pose.Serialize());
    Append(data, search_range.Serialize());
    return data;
}

LatestExternalReference LatestExternalReference::Deserialize(const RawData& data) {
    LatestExternalReference ext_ref{};
    ext_ref.timestamp = Time::Deserialize({data.cbegin(), data.cbegin() + 8}).value;
    ext_ref.pose = Pose::Deserialize({data.begin() + 8, data.begin() + 28});
    ext_ref.search_range = SearchRange::Deserialize({data.begin() + 28, data.begin() + 36});
    return ext_ref;
}

}  // namespace api
}  // namespace accerion
