/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/structs/address.h"

#include <array>
#include <iostream>
#include <sstream>

#include <iomanip>


namespace accerion {
namespace api {

namespace detail {

std::int16_t Signed(const std::uint8_t& x) {
    return static_cast<std::int16_t>(x);
}

}  // namespace detail

Address Address::Parse(const std::string &ip_string) {
    std::stringstream ss(ip_string);
    std::array<std::uint8_t, 4> ip{};

    std::string substr;
    for (std::size_t i = 0; i < 4 && getline(ss, substr, '.'); ++i) {
        ip[i] = static_cast<uint8_t>(std::stoi(substr));
    }
    return Address{ ip[0], ip[1], ip[2], ip[3] };
}

/**
   \brief Serialize the struct for network communication

   | Type      | Length (bytes) | Description  | Unit |
   |:----------|---------------:|:-------------|------|
   | `uint8_t` |              1 | first octet  | -    |
   | `uint8_t` |              1 | second octet | -    |
   | `uint8_t` |              1 | third octet  | -    |
   | `uint8_t` |              1 | fourth octet | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData Address::Serialize() const {
    return {first, second, third, fourth};
}

Address Address::Deserialize(const RawData& data) {
    return {data[0], data[1], data[2], data[3]};
}

Address Address::From(const std::uint32_t& other) {
    return reinterpret_cast<const Address&>(other);
}

Address Address::From(const in_addr& in_addr){
    return *reinterpret_cast<const Address*>(&in_addr.s_addr);
}

in_addr Address::Convert() const {
   in_addr in_addr;
   in_addr.s_addr = *reinterpret_cast<uint32_t*>(Serialize().data());
   return in_addr;
}

bool Address::operator==(const Address &other) const {
    return first == other.first && second == other.second && third == other.third && fourth == other.fourth;
}

bool Address::operator!=(const Address &other) const {
    return !(*this == other);
}

bool Address::operator<(const Address& o) const {
    return first != o.first ? first   < o.first
                            : second != o.second ? second < o.second
                                                 : third != o.third ? third  < o.third
                                                                    : fourth < o.fourth;
}

std::ostream& operator<<(std::ostream& os, const Address& address) {
    return os << detail::Signed(address.first) << "." << detail::Signed(address.second) << "."
              << detail::Signed(address.third) << "." << detail::Signed(address.fourth);
}

std::string Address::AlignedString() const {
    std::stringstream ss;
    ss << std::right << std::setw(3) << std::setfill(' ') << detail::Signed(first) << ".";
    ss << std::right << std::setw(3) << std::setfill(' ') << detail::Signed(second) << ".";
    ss << std::right << std::setw(3) << std::setfill(' ') << detail::Signed(third) << ".";
    ss << std::right << std::setw(3) << std::setfill(' ') << detail::Signed(fourth);
    return ss.str();
}

}  // namespace api
}  // namespace accerion
