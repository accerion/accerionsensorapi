/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/diagnostics.h"

#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/reporting.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                        | Length (bytes) | Description                                      | Unit |
   |:--------------------------------------------|---------------:|:-------------------------------------------------|------|
   | [Time](@ref accerion::api::Time::Serialize) |              8 | timestamp                                        | -    |
   | `uint16_t`                                  |              2 | [modes](@ref accerion::api::kModeDescriptionMap) | -    |
   | `uint16_t`                                  |              2 | [warnings](@ref accerion::api::kWarningMap)      | -    |
   | `uint32_t`                                  |              4 | [errors](@ref accerion::api::kErrorMap)          | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData Diagnostics::Serialize() const {
    auto data = Time{timestamp}.Serialize();
    Append(data, serialization::Serialize(modes));
    Append(data, serialization::Serialize(warning_codes));
    Append(data, serialization::Serialize(error_codes));
    return data;
}

Diagnostics Diagnostics::Deserialize(const RawData& raw_data) {
    Diagnostics diagnostics{};
    diagnostics.timestamp = Time::Deserialize({raw_data.cbegin(), raw_data.cbegin() + 8}).value;
    diagnostics.modes = serialization::Deserialize<std::uint16_t>({raw_data.cbegin() + 8, raw_data.cbegin() + 10});
    diagnostics.warning_codes =
            serialization::Deserialize<std::uint16_t>({raw_data.cbegin() + 10, raw_data.cbegin() + 12});
    diagnostics.error_codes =
            serialization::Deserialize<std::uint32_t>({raw_data.cbegin() + 12, raw_data.cbegin() + 16});
    return diagnostics;
}

std::ostream& operator<<(std::ostream& os, const Diagnostics& diagnostics) {
    using namespace std::chrono;
    return os << "time since boot: "
              << duration_cast<duration<double>>(diagnostics.timestamp.time_since_epoch()).count() << " [s]\n"
              << "  modes: \n" << ReportModes(diagnostics.modes)
              << "  error codes: \n" << ReportErrorCodes(diagnostics.error_codes)
              << "  warning codes: \n" << ReportWarningCodes(diagnostics.warning_codes) << std::endl;
}

}  // namespace api
}  // namespace accerion
