/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/standard_deviation.h"

#include "AccerionSensorAPI/structs/double.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
 * \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description                | Unit        |
   |:-----------|---------------:|:---------------------------|:------------|
   | `uint32_t` |              4 | x standard deviation       | micrometer  |
   | `uint32_t` |              4 | y standard deviation       | micrometer  |
   | `uint32_t` |              4 | heading standard deviation | centidegree |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData StandardDeviation::Serialize() const {
    auto data = Double<std::uint32_t, std::micro>{x}.Serialize();
    Append(data, Double<std::uint32_t, std::micro>{y}.Serialize());
    Append(data, Double<std::uint32_t, std::centi>{th}.Serialize());
    return data;
}

StandardDeviation StandardDeviation::Deserialize(const RawData& raw_data) {
    StandardDeviation dev{};
    dev.x = Double<std::uint32_t, std::micro>::Deserialize({raw_data.cbegin(), raw_data.cbegin() + 4}).value;
    dev.y = Double<std::uint32_t, std::micro>::Deserialize({raw_data.cbegin() + 4, raw_data.cbegin() + 8}).value;
    dev.th = Double<std::uint32_t, std::centi>::Deserialize({raw_data.cbegin() + 8, raw_data.cbegin() + 12}).value;
    return dev;
}

bool StandardDeviation::operator==(const StandardDeviation& other) const {
    return x == other.x && y == other.y && th == other.th;
}

StandardDeviation StandardDeviation::Interpolate(double ratio, const StandardDeviation& s) const {
    // TO DO revisit interpolation
    StandardDeviation result = *this;
    result.x += ratio * (s.x - result.x);
    result.y += ratio * (s.y - result.y);
    result.th += ratio * (s.th - result.th);
    return result;
}

}  // namespace api
}  // namespace accerion

