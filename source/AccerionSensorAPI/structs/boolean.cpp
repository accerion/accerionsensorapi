/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/boolean.h"

#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

Boolean Boolean::Deserialize(RawData raw_data) {
    return {raw_data[0] == std::uint8_t{1}};
}

/**
   \brief Serialize the struct for network communication

   | Type      | Length (bytes) | Description         | Unit |
   |:----------|---------------:|:--------------------|------|
   | `uint8_t` |              1 | 1 = true, 0 = false | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData Boolean::Serialize() const {
    return {static_cast<std::uint8_t>(value)};
}

}  // namespace api
}  // namespace accerion
