/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/calibration_file_request.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {


/**
   \brief Serialize the struct for network communication

   Development only
 */
RawData CalibrationFileRequest::Serialize() const {
    RawData data{};
    data.insert(data.end(), key.c_str(), key.c_str() + 16);
    Append(data, file);
    return data;
}

CalibrationFileRequest CalibrationFileRequest::Deserialize(const RawData& data) {
    CalibrationFileRequest req{};
    std::string key(data.begin(), data.begin() + 16);
    req.key = key;
    req.file.insert(req.file.end(), data.begin() + 16, data.end());
    return std::move(req);
}

}  // namespace api
}  // namespace accerion

