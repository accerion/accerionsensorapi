/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/bool_opt_uint64.h"

#include "AccerionSensorAPI/structs/bool_uint64.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description                                      | Unit |
   |:--------------------------------------------------|---------------:|:-------------------------------------------------|:-----|
   | [Boolean](@ref accerion::api::Boolean::Serialize) |              1 | boolean value                                    | -    |
   | `uint64_t`                                        |              8 | uint64 value, ignore when boolean field is false | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData BoolOptUInt64::Serialize() const {
    return accerion::api::BoolUInt64{bool_value, bool_value && uint64_value ? uint64_value.value() : 0}.Serialize();
}

BoolOptUInt64 BoolOptUInt64::Deserialize(const RawData& raw_data) {
    auto deserialized = BoolUInt64::Deserialize(raw_data);
    Optional<std::uint64_t> optional;
    if (deserialized.bool_value) {
        optional = Optional<std::uint64_t>(deserialized.uint64_value);
    }
    return {deserialized.bool_value, optional};
}

}  // namespace api
}  // namespace accerion
