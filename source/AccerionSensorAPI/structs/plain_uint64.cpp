/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/plain_uint64.h"

#include "AccerionSensorAPI/structs/serialization.h"

namespace accerion {
namespace api {

PlainUInt64 PlainUInt64::Parse(const std::string& s) { return {static_cast<decltype(value)>(std::stoull(s))}; }

/**
   \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description | Unit |
   |:-----------|---------------:|:------------|:-----|
   | `uint64_t` |              8 | value       | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData PlainUInt64::Serialize() const { return serialization::Serialize(value); }

PlainUInt64 PlainUInt64::Deserialize(RawData raw_data) {
    return {serialization::Deserialize<std::uint64_t>(std::move(raw_data))};
}

bool PlainUInt64::operator==(const PlainUInt64& other) const { return value == other.value; }

bool PlainUInt64::operator!=(const PlainUInt64& other) const { return !(*this == other); }

}  // namespace api
}  // namespace accerion
