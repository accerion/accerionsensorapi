/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/acknowledgement.h"

#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/plain_string.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

Acknowledgement Acknowledgement::Deserialize(const RawData& data) {
    Acknowledgement ack;
    ack.accepted = Boolean::Deserialize({data[0]}).value;
    if (ack.accepted) {
        return ack;
    }
    ack.message = PlainString::Deserialize({data.cbegin() + 1, data.cend()}).value;
    return ack;
}

/**
   \brief Serialize the struct for network communication

   | Type                                                      | Length (bytes) | Description                                    | Unit |
   |:----------------------------------------------------------|---------------:|:-----------------------------------------------|------|
   | [Boolean](@ref accerion::api::Boolean::Serialize)         |              1 | indicates if request was accepted or rejected  | -    |
   | [PlainString](@ref accerion::api::PlainString::Serialize) |              ? | error message on rejection                     | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData Acknowledgement::Serialize() const {
    auto data = Boolean{accepted}.Serialize();
    Append(data, PlainString{message}.Serialize());
    return data;
}

}  // namespace api
}  // namespace accerion
