/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/plain_uint32.h"

#include "AccerionSensorAPI/structs/serialization.h"

namespace accerion {
namespace api {

PlainUInt32 PlainUInt32::Parse(const std::string& s) {
    return {static_cast<decltype(value)>(std::stoul(s))};
}

/**
   \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description | Unit |
   |:-----------|---------------:|:------------|:-----|
   | `uint32_t` |              4 | value       | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData PlainUInt32::Serialize() const {
    return serialization::Serialize(value);
}

PlainUInt32 PlainUInt32::Deserialize(RawData raw_data) {
    return {serialization::Deserialize<std::uint32_t>(std::move(raw_data))};
}

bool PlainUInt32::operator==(const PlainUInt32& other) const {
    return value == other.value;
}

bool PlainUInt32::operator!=(const PlainUInt32& other) const {
    return !(*this == other);
}

bool PlainUInt32::operator<(const PlainUInt32& other) const {
   return value < other.value;
}

}  // namespace api
}  // namespace accerion
