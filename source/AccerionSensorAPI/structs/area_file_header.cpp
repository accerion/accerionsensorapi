/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/area_file_header.h"

#include <fstream>
#include <iostream>
#include <sstream>

namespace accerion {
namespace api {
namespace detail {

template <typename T>
static uint32_t MessageSize(T msg) {
    std::string str;
    std::stringstream buffer(str);
    msgpack::pack(buffer, msg);
    return static_cast<uint32_t>(buffer.tellp());
}

void AdjustBytes(std::uint32_t& s) {
    s += (s == 2<<7 || s == 2<<15 || s == (uint32_t) 2<<31) ? 1 : 0;
}

}  // namespace detail

FileSignature FileSignature::Legacy() {
    FileSignature result;
    result.identifier = "";
    result.header_offset = 0;
    return result;
}

std::ostream& operator<<(std::ostream& ss, const AreaOverview& ao) {
    ss << "area_id:          " << ao.area_id << "\n"
       << "total_clusters:   " << ao.total_clusters << "\n"
       << "total_signatures: " << ao.total_signatures << "\n";

    ss << std::setw(11) << "cluster_id" << ": " << "size\n";
    for (const auto s: ao.cluster_details) {
        ss << std::setw(11) << std::right << s.first << ": " << s.second << "\n";
    }
    return ss;
}

std::ostream& operator<<(std::ostream& ss, const FileHeader& fh) {
    ss << "software version: " << fh.software_version << "\n"
       << "database version: " << fh.database_version << "\n"
       << "area overview:    \n" << fh.area_overview << "\n"
       << "chunk size:       " << +fh.chunk_size << "\n";

    return ss;
}

std::string FileHeader::Pack() const {
    FileSignature sig;
    // is there a way to get size without packing?
    sig.header_offset = detail::MessageSize(*this);
    sig.header_offset += detail::MessageSize(sig);

    // sig.headerOffset stores the size of the header and calculated as:
    // sig.headerOffset = msgSize(header) + msgSize(sig)
    // since the value of sig.headerOffset is the function of itself
    // we need to handle the edge-cases
    detail::AdjustBytes(sig.header_offset);

    std::string str;
    std::stringstream buffer(str);
    msgpack::pack(buffer, sig);
    msgpack::pack(buffer, *this);
    return buffer.str();
}

FileHeaderUnpackResult FileHeader::Unpack(const std::string& path) {
    FileHeaderUnpackResult return_value{};

    /// Check validity

    std::size_t offset{0};
    FileSignature file_signature{FileSignature::Legacy()};

    {
        auto file = std::ifstream{path, std::ios_base::binary};
        if (!file.is_open()) {
            return_value.is_valid = false;
            return return_value;
        }

        constexpr size_t sigSize = 2 * sizeof(FileSignature);
        std::array<char, sigSize> buffer{};
        file.read(buffer.data(), sigSize);

        try {
            auto object_handle = msgpack::unpack(buffer.data(), sigSize, offset);
            object_handle.get().convert(file_signature);
        } catch (const std::exception& e) {
            std::cout << "Invalid or old/legacy map file" << std::endl;
        }
    }

    /// Actual unpacking

    if (file_signature.identifier == kFileSignatureIdentifier) {
        auto file = std::ifstream{path, std::ios_base::binary};
        file.seekg(static_cast<std::int64_t>(offset));

        std::vector<char> buffer(file_signature.header_offset);
        file.read(buffer.data(), file_signature.header_offset);

        auto object_handle = msgpack::unpack(buffer.data(), file_signature.header_offset);
        object_handle.get().convert(return_value.header);

        // where the .amf file starts
        return_value.offset = static_cast<int>(file_signature.header_offset);
    }

    /// Post-processing

    if (return_value.offset == 0 || return_value.header.chunk_size == 0) {
        std::cout << "Assuming legacy file; using 1MB chunk size" << std::endl;
        return_value.header.chunk_size = 1000000;
    }

    return return_value;
}

}  // namespace api
}  // namespace accerion

