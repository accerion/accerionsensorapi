/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/udp_settings.h"

#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

namespace detail {

const auto kDefaultUnicastIpAddress         = "127.0.0.1";
const auto kLocalIpClassAStartAddress       = "10.0.0.0";
const auto kLocalIpClassAEndAddress         = "10.255.255.255";
const auto kLocalIpClassBStartAddress       = "172.16.0.0";
const auto kLocalIpClassBEndAddress         = "172.31.255.255";
const auto kLocalIpClassCStartAddress       = "192.168.0.0";
const auto kLocalIpClassCEndAddress         = "192.168.255.255";

const auto kDefaultUnicastIpAddressData = ntohl(inet_addr(kDefaultUnicastIpAddress));
const auto kNetStartClassA = ntohl(inet_addr(kLocalIpClassAStartAddress));
const auto kNetEndClassA   = ntohl(inet_addr(kLocalIpClassAEndAddress));
const auto kNetStartClassB = ntohl(inet_addr(kLocalIpClassBStartAddress));
const auto kNetEndClassB   = ntohl(inet_addr(kLocalIpClassBEndAddress));
const auto kNetStartClassC = ntohl(inet_addr(kLocalIpClassCStartAddress));
const auto kNetEndClassC   = ntohl(inet_addr(kLocalIpClassCEndAddress));

}  // namespace detail

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description                                                  | Unit |
   |:--------------------------------------------------|---------------:|:-------------------------------------------------------------|:-----|
   | `uint8_t`                                         |              1 | [EnabledMessageType](@ref accerion::api::EnabledMessageType) | -    |
   | `uint8_t`                                         |              1 | [UdpStrategy](@ref accerion::api::UdpStrategy)               | -    |
   | [Address](@ref accerion::api::Address::Serialize) |              4 | ip address                                                   | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
 RawData UdpSettings::Serialize() const {
    RawData data{};
    data.push_back(GetEnumValue(message_type));
    data.push_back(static_cast<std::uint8_t>(GetEnumValue(strategy)));
    Append(data, address.Serialize());
    return data;
}

UdpSettings UdpSettings::Deserialize(const RawData& data) {
    UdpSettings ui{};
    ui.message_type = static_cast<EnabledMessageType>(data[0]);
    ui.strategy = static_cast<UdpStrategy>(static_cast<std::uint8_t>(data[1]));
    std::vector<uint8_t> add_vec(data.begin() + 2, data.begin() + 6);
    ui.address = Address::Deserialize(add_vec);
    return ui;
}

bool UdpSettings::IsValid() const {
    bool valid = GetEnumValue(message_type) > 0 && GetEnumValue(message_type) < 5;
    valid &= GetEnumValue(strategy) >= -1 && GetEnumValue(strategy) < 4;

    if (strategy == UdpStrategy::kUnicast || strategy == UdpStrategy::kUnicastNoHB) {
        const auto unicast_ip_s_addr = ntohl(address.Convert().s_addr);
        valid &= unicast_ip_s_addr >= detail::kNetStartClassA && unicast_ip_s_addr <= detail::kNetEndClassA ||
                 unicast_ip_s_addr >= detail::kNetStartClassB && unicast_ip_s_addr <= detail::kNetEndClassB ||
                 unicast_ip_s_addr >= detail::kNetStartClassC && unicast_ip_s_addr <= detail::kNetEndClassC ||
                 unicast_ip_s_addr == detail::kDefaultUnicastIpAddressData;
    }
    return valid;
}

bool UdpSettings::operator==(const UdpSettings &settings) const {
    return message_type == settings.message_type && strategy == settings.strategy && address == settings.address;
}

}  // namespace api
}  // namespace accerion
