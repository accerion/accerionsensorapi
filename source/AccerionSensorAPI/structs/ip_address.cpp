/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/ip_address.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description | Unit |
   |:--------------------------------------------------|---------------:|:------------|------|
   | [Address](@ref accerion::api::Address::Serialize) |              4 | address     | -    |
   | [Address](@ref accerion::api::Address::Serialize) |              4 | netmask     | -    |
   | [Address](@ref accerion::api::Address::Serialize) |              4 | gateway     | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData IpAddress::Serialize() const {
    RawData data{};
    Append(data, address.Serialize());
    Append(data, netmask.Serialize());
    Append(data, gateway.Serialize());
    return data;
}

IpAddress IpAddress::Deserialize(const RawData& data) {
    IpAddress result{};
    result.address = Address::Deserialize({data.cbegin(), data.cbegin() + 4});
    result.netmask = Address::Deserialize({data.cbegin() + 4, data.cbegin() + 8});
    result.gateway = Address::Deserialize({data.cbegin() + 8, data.cbegin() + 12});
    return result;
}

bool IpAddress::operator==(const IpAddress &other) const {
    return address == other.address && netmask == other.netmask && gateway == other.gateway;
}

bool IpAddress::operator!=(const IpAddress &other) const {
    return !(*this == other);
}

}  // namespace api
}  // namespace accerion
