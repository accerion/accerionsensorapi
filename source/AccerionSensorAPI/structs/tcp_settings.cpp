/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/tcp_settings.h"

#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type      | Length (bytes) | Description                                                  | Unit |
   |:----------|---------------:|:-------------------------------------------------------------|:-----|
   | `uint8_t` |              1 | [EnabledMessageType](@ref accerion::api::EnabledMessageType) | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData TcpSettings::Serialize() const {
    return {GetEnumValue(message_type)};
}

TcpSettings TcpSettings::Deserialize(const RawData& data) {
    return {static_cast<EnabledMessageType>(data[0])};
}

bool TcpSettings::operator==(const TcpSettings& other) const { return message_type == other.message_type; }

bool TcpSettings::operator!=(const TcpSettings& other) const { return !(*this == other); }

bool TcpSettings::IsValid() const {
    const auto value = GetEnumValue(message_type);
    return 0 < value && value < 5;
}

}  //  namespace api
}  //  namespace accerion
