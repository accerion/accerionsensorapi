/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/signature.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                         | Length (bytes) | Description    | Unit |
   |:---------------------------------------------|---------------:|:---------------|:-----|
   | [Pose](@ref accerion::api::Pose_::Serialize) |             20 | signature pose | -    |
   | `uint64_t`                                   |              8 | cluster id     | -    |
   | `uint64_t`                                   |              8 | signature id   | -    |
   For more information, see the [protocol page](docs/protocol.md).

   All pose information is given in the world coordinate system.
 */
RawData Signature::Serialize() const {
    RawData vec{};
    Append(vec, pose.Serialize());
    Append(vec, serialization::Serialize(cluster_id));
    Append(vec, serialization::Serialize(signature_id));
    return vec;
}

Signature Signature::Deserialize(const RawData& data) {
    Signature result{};
    result.pose = Pose::Deserialize({data.cbegin(), data.cbegin() + 20});
    result.cluster_id = serialization::Deserialize<ClusterId>({data.cbegin() + 20, data.cbegin() + 28});
    result.signature_id = serialization::Deserialize<SignatureId>({data.cbegin() + 28, data.cbegin() + 36});
    return result;
}

bool Signature::operator==(const Signature& other) const {
    return pose == other.pose && cluster_id == other.cluster_id && signature_id == other.signature_id;
}

}  // namespace api
}  // namespace accerion

