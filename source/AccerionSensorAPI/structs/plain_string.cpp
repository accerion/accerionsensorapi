/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/plain_string.h"

#include <limits>

#include "AccerionSensorAPI/structs/serialization.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                    | Length (bytes) | Description   | Unit |
   |:------------------------|---------------:|:--------------|:-----|
   | `uint16_t`              |              2 | string length | -    |
   | **for each character in the string** ||||
   | &emsp; &emsp; `uint8_t` |              1 | character     | -    |
   | **end of the string** ||||
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData PlainString::Serialize() const {
    auto str_copy = value;
    constexpr auto max_length = std::numeric_limits<std::uint16_t>::max();
    if (str_copy.length() > max_length) {
        str_copy.resize(max_length);
    }
    auto data = serialization::Serialize(static_cast<std::uint16_t>(str_copy.length()));
    data.insert(data.end(), str_copy.c_str(), str_copy.c_str() + str_copy.length());
    return data;
}

PlainString PlainString::Deserialize(const RawData& data) {
    const auto length = serialization::Deserialize<std::uint16_t>({data.cbegin(), data.cbegin() + 2});
    return {std::string(data.cbegin() + 2, data.cbegin() + 2 + length)};
}

}  // namespace api
}  // namespace accerion

