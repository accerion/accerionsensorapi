/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/subset_operation.h"

#include "AccerionSensorAPI/structs/uint64_vector.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                     | Length (bytes) | Description                                                            | Unit |
   |:-------------------------|---------------:|:-----------------------------------------------------------------------|:-----|
   | `uint8_t`                |              1 | [SubsetResultMessageType](@ref accerion::api::SubsetResultMessageType) | -    |
   | `uint8_t`                |              1 | value, result if type is kComplete, percent if type is kProgress       | -    |
   | **for each id in the list** ||||
   | &emsp; &emsp; `uint64_t` |              8 | failed cluster id                                                      | -    |
   | **end of the list** ||||
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData SubsetOperationResult::Serialize() const {
    RawData data{};
    data.push_back(GetEnumValue((message_type)));
    data.push_back(value);
    Append(data, UInt64Vector{failed_ids}.Serialize());
    return data;
}

SubsetOperationResult SubsetOperationResult::Deserialize(const RawData& data) {
    SubsetOperationResult result {};
    result.message_type = static_cast<SubsetResultMessageType>(data[0]);  // 0x00 -> complete, 0x01 -> progress
    result.value = data[1];  // if type 0x00 -> 0x00 success, 0x01 area in use, 0x02 disk usage, etc. if type 0x01 -> progress
    result.failed_ids = UInt64Vector::Deserialize({data.cbegin() + 2, data.cend()}).values;
    return result;
}

/**
   \brief Serialize the struct for network communication

   | Type                     | Length (bytes) | Description | Unit |
   |:-------------------------|---------------:|:------------|:-----|
   | `uint16_t`               |              2 | area id     | -    |
   | **for each id in the list** ||||
   | &emsp; &emsp; `uint64_t` |              8 | cluster id  | -    |
   | **end of the list** ||||
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData SubsetOperationRequest::Serialize() const {
    RawData data{};
    Append(data, serialization::Serialize(target_area_id));
    Append(data, UInt64Vector{cluster_ids}.Serialize());
    return data;
}

SubsetOperationRequest SubsetOperationRequest::Deserialize(const RawData& data) {
    SubsetOperationRequest result{};
    result.target_area_id = serialization::Deserialize<std::uint16_t>({data.cbegin(), data.cbegin() + 2});
    result.cluster_ids = UInt64Vector::Deserialize({data.cbegin() + sizeof(target_area_id), data.cend()}).values;
    return result;
}

}  // namespace api
}  // namespace accerion
