/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/structs/line_follower_data.h"

#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                         | Length (bytes) | Description   | Unit |
   |:---------------------------------------------|---------------:|:--------------|:-----|
   | [Time](@ref accerion::api::Time::Serialize)  |              8 | timestamp     | -    |
   | [Pose](@ref accerion::api::Pose_::Serialize) |             20 | pose          | -    |
   | [Pose](@ref accerion::api::Pose_::Serialize) |             20 | closest point | -    |
   | `uint64_t`                                   |              8 | cluster id    | -    |
   For more information, see the [protocol page](docs/protocol.md).

   All pose information is given in the world coordinate system.
 */
RawData LineFollowerData::Serialize() const {
    RawData data{};
    Append(data, Time{timestamp}.Serialize());
    Append(data, pose.Serialize());
    Append(data, closest_point.Serialize());
    Append(data, serialization::Serialize(cluster_id));
    return data;
}

LineFollowerData LineFollowerData::Deserialize(const RawData& data) {
    LineFollowerData result{};
    result.timestamp = Time::Deserialize({data.cbegin(), data.cbegin() + 8}).value;
    result.pose = Pose::Deserialize({data.cbegin() + 8 , data.cbegin() + 28});
    result.closest_point = Pose::Deserialize({data.cbegin() + 28 , data.cbegin() + 48});
    result.cluster_id = serialization::Deserialize<ClusterId>({data.cbegin() + 48, data.cbegin() + 56});
    return result;
}

}  // namespace api
}  // namespace accerion
