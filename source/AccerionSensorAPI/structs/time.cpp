/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/time.h"

#include "AccerionSensorAPI/structs/serialization.h"

namespace accerion {
namespace api {

namespace detail {

using NanoSecondsUInt64 = std::chrono::duration<std::uint64_t, std::nano>;

}  // namespace detail

/**
   \brief Serialize the struct for network communication

   | Type       | Length (bytes) | Description                                                 | Unit        |
   |:-----------|---------------:|:------------------------------------------------------------|:------------|
   | `uint64_t` |              8 | epoch time based on `std::chrono::system_clock::time_point` | nanoseconds |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData Time::Serialize() const {
    return serialization::Serialize(std::chrono::duration_cast<detail::NanoSecondsUInt64>(value.time_since_epoch()).count());
}

Time Time::Deserialize(const RawData& data) {
    const auto raw_duration_nano_seconds = serialization::Deserialize<std::uint64_t>(data);
    const detail::NanoSecondsUInt64 duration_nano_seconds{raw_duration_nano_seconds};
    const auto time_point_duration = std::chrono::duration_cast<accerion::api::TimePoint::duration>(duration_nano_seconds);
    return {accerion::api::TimePoint{time_point_duration}};
}

}  // namespace api
}  // namespace accerion
