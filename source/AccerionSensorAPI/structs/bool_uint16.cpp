/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/bool_uint16.h"

#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/serialization.h"
#include "AccerionSensorAPI/utils/algorithm.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type                                              | Length (bytes) | Description   | Unit |
   |:--------------------------------------------------|---------------:|:--------------|:-----|
   | [Boolean](@ref accerion::api::Boolean::Serialize) |              1 | boolean value | -    |
   | `uint16_t`                                        |              2 | uint16 value  | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData BoolUInt16::Serialize() const {
    RawData data{};
    Append(data, Boolean{bool_value}.Serialize());
    Append(data, serialization::Serialize(uint16_value));
    return data;
}

BoolUInt16 BoolUInt16::Deserialize(const RawData& data) {
    BoolUInt16 result{};
    result.bool_value = Boolean::Deserialize({data.front()}).value;
    result.uint16_value = serialization::Deserialize<std::uint16_t>({data.cbegin() + 1, data.cbegin() + 3});
    return result;
}

}  // namespace api
}  // namespace accerion
