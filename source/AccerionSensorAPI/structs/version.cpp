/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/structs/version.h"

#include "AccerionSensorAPI/utils/string.h"

namespace accerion {
namespace api {

/**
   \brief Serialize the struct for network communication

   | Type      | Length (bytes) | Description  | Unit |
   |:----------|---------------:|:-------------|------|
   | `uint8_t` |              1 | major        | -    |
   | `uint8_t` |              1 | minor        | -    |
   | `uint8_t` |              1 | patch        | -    |
   For more information, see the [protocol page](docs/protocol.md).
 */
RawData Version::Serialize() const {
    return {major, minor, patch};
}

Version Version::Deserialize(const RawData& data) {
    return {data[0], data[1], data[2]};
}

Version Version::Parse(const std::string& str) {
    Version result{0, 0, 0};
    if (str.empty()) {
        return result;
    }

    auto parts = accerion::api::Split(str, ".");
    if (!parts.empty() && !parts[0].empty()) {
        result.major = static_cast<std::uint8_t>(std::stoi(parts[0]));
    }
    if (parts.size() >= 2 && !parts[1].empty()) {
        result.minor = static_cast<std::uint8_t>(std::stoi(parts[1]));
    }
    if (parts.size() >= 3 && !parts[2].empty()) {
        result.patch = static_cast<std::uint8_t>(std::stoi(parts[2]));
    }
    return result;
}

bool Version::operator==(const Version& other) const {
    return major == other.major && minor == other.minor && patch == other.patch;
}

bool Version::operator!=(const Version& other) const {
    return !(*this == other);
}

bool Version::operator<(const Version& other) const {
    return major != other.major ? major  < other.major
                                : minor != other.minor ? minor < other.minor
                                                       : patch < other.patch;
}

bool Version::operator<=(const Version& other) const {
    return *this == other || *this < other;
}

bool Version::operator>(const Version& other) const {
    return !(*this <= other);
}

bool Version::operator>=(const Version& other) const {
    return *this == other || *this > other;
}

std::string Version::String() const {
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

std::ostream& operator<<(std::ostream& os, const Version& version) {
    const auto conv = [](const std::uint8_t& x) { return static_cast<std::int16_t>(x); };
    return os << conv(version.major) << "." << conv(version.minor) << "." << conv(version.patch);
}

SoftwareVersion GetApiVersion() {
    // The Parse function automatically discards the remainder of the semantic version (major, minor, patch)
    static auto v{Version::Parse(CMAKE_API_FULL_VERSION)};
    return v;
}

}  // namespace api
}  // namespace accerion
