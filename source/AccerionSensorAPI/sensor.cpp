/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/sensor.h"

#include <utility>

#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/bool_uint64.h"
#include "AccerionSensorAPI/structs/plain_uint64.h"
#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/utils/memory.h"
#include "AccerionSensorAPI/utils/sensor.h"
#include "AccerionSensorAPI/utils/string.h"


namespace accerion {
namespace api {

Sensor::Sensor(const Address& address,
               const SerialNumber& serial_number,
               ConnectionType connection_type,
               Optional<Address> local_address)
        : address_{address},
          serial_number_{serial_number},
          process_response_functions_{InitializeProcessResponseFunctionsWithReceivedCommands()},
          future_console_output_{CommandId::INT_CONSOLE_OUTPUT_INFO, process_response_functions_},
          future_corrected_odometry_{CommandId::STR_CORRECTED_ODOMETRY, process_response_functions_},
          future_created_signature_{CommandId::INT_SIGNATURE_CREATED, process_response_functions_},
          future_diagnostics_{CommandId::STR_DIAGNOSTICS, process_response_functions_},
          future_compensated_drift_correction_{CommandId::INT_COMPENSATED_DRIFT_CORRECTION,
                                                process_response_functions_},
          future_heart_beat_{CommandId::PRD_HEARTBEAT_INFO, process_response_functions_},
          future_line_follower_data_{CommandId::STR_LINE_FOLLOWER, process_response_functions_},
          future_marker_detection_{CommandId::INT_MARKER_DETECTION, process_response_functions_},
          future_odometry_{CommandId::STR_ODOMETRY, process_response_functions_},
          future_latest_ext_ref_{CommandId::INT_LATEST_EXTERNAL_REFERENCE, process_response_functions_},
          future_continuous_signature_updating_mode_{CommandId::ACK_CONTINUOUS_SIGNATURE_UPDATING_MODE,
                                                        process_response_functions_},
          future_expert_mode_{CommandId::ACK_EXPERT_MODE, process_response_functions_},
          future_idle_mode_{CommandId::ACK_IDLE_MODE, process_response_functions_},
          future_internal_mode_{CommandId::ACK_INTERNAL_MODE, process_response_functions_},
          future_set_line_following_mode_{CommandId::ACK_SET_LINE_FOLLOWING_MODE, process_response_functions_},
          future_get_line_following_mode_{CommandId::ACK_GET_LINE_FOLLOWING_MODE, process_response_functions_},
          future_set_mapping_mode_{CommandId::ACK_SET_MAPPING_MODE, process_response_functions_},
          future_get_mapping_mode_{CommandId::ACK_GET_MAPPING_MODE, process_response_functions_},
          future_marker_detection_mode_{CommandId::ACK_MARKER_DETECTION_MODE, process_response_functions_},
          future_recording_mode_{CommandId::ACK_RECORDING_MODE, process_response_functions_},
          future_localization_mode_{CommandId::ACK_LOCALIZATION_MODE, process_response_functions_},
          future_corr_odometry_pose_{CommandId::ACK_SET_CORRECTED_ODOMETRY_POSE, process_response_functions_},
          future_save_images_{CommandId::ACK_SET_SAVE_IMAGES, process_response_functions_},
          future_get_save_images_{CommandId::ACK_GET_SAVE_IMAGES, process_response_functions_},
          future_reboot_{CommandId::ACK_REBOOT, process_response_functions_},
          future_set_search_loop_closures_{CommandId::ACK_SEARCH_LOOP_CLOSURES, process_response_functions_},
          signature_information_{},
          future_signature_information_{},
          int_future_signature_information_{CommandId::ACK_SIGNATURE_INFORMATION, process_response_functions_},
          int_future_signature_information_done_{CommandId::ACK_SIGNATURE_INFORMATION_DONE, process_response_functions_},
          future_set_area_{CommandId::ACK_SET_AREA, process_response_functions_},
          future_get_area_ids_{CommandId::ACK_GET_AREA_IDS, process_response_functions_},
          future_set_buffer_length_{CommandId::ACK_SET_BUFFER_LENGTH, process_response_functions_},
          future_get_buffer_length_{CommandId::ACK_GET_BUFFER_LENGTH, process_response_functions_},
          future_buffer_progress_{CommandId::ACK_BUFFER_PROGRESS_INDICATOR, process_response_functions_},
          future_capture_frame_{CommandId::ACK_FRAME, process_response_functions_},
          future_cluster_ids_{CommandId::ACK_GET_CLUSTER_IDS, process_response_functions_},
          future_set_date_time_{CommandId::ACK_SET_DATE_TIME, process_response_functions_},
          future_get_date_time_{CommandId::ACK_GET_DATE_TIME, process_response_functions_},
          future_delete_recordings_{CommandId::ACK_DELETE_RECORDINGS, process_response_functions_},
          future_set_ip_address_{CommandId::ACK_SET_IP_ADDRESS, process_response_functions_},
          future_get_ip_address_{CommandId::ACK_GET_IP_ADDRESS, process_response_functions_},
          future_map_summary_{CommandId::ACK_MAP_SUMMARY, process_response_functions_},
          future_ram_rom_stats_{CommandId::ACK_RAM_ROM_STATS, process_response_functions_},
          future_recording_list_{CommandId::ACK_RECORDINGS_LIST, process_response_functions_},
          future_delete_cluster_{CommandId::ACK_DELETE_CLUSTER, process_response_functions_},
          future_delete_area_{CommandId::ACK_DELETE_AREA, process_response_functions_},
          future_set_sample_rate_{CommandId::ACK_SET_SAMPLE_RATE, process_response_functions_},
          future_get_sample_rate_{CommandId::ACK_GET_SAMPLE_RATE, process_response_functions_},
          future_software_details_{CommandId::ACK_SOFTWARE_DETAILS, process_response_functions_},
          future_software_version_{CommandId::ACK_SOFTWARE_VERSION, process_response_functions_},
          future_set_tcp_settings_{CommandId::ACK_SET_TCP_SETTINGS, process_response_functions_},
          future_get_tcp_settings_{CommandId::ACK_GET_TCP_SETTINGS, process_response_functions_},
          future_set_udp_settings_{CommandId::ACK_SET_UDP_SETTINGS, process_response_functions_},
          future_get_udp_settings_{CommandId::ACK_GET_UDP_SETTINGS, process_response_functions_},
          future_loop_closure_{CommandId::ACK_LOOP_CLOSURE, process_response_functions_},
          future_loop_closures_completed_{CommandId::ACK_SEARCH_LOOP_CLOSURES_COMPLETE, process_response_functions_},
          rc_file_transfer_task_{},
          rc_handshake_{},
          rc_map_subset_{},
          communication_thread_{CreateCommunicationThread(address, serial_number, connection_type,
                                                          process_response_functions_)} {
    UdpSettings udp_settings{};
    TcpSettings tcp_settings{};
    std::tie(udp_settings, tcp_settings) = ConfigureConnection(connection_type, local_address);
    if (connection_type == ConnectionType::kConnectionUdpBroadcast ||
            connection_type == ConnectionType::kConnectionUdpUnicast ||
            connection_type == ConnectionType::kConnectionUdpUnicastNoHb) {
        SetUdpSettings(udp_settings).WaitFor(std::chrono::milliseconds{100});
    } else {
        SetTcpSettings(tcp_settings).WaitFor(std::chrono::milliseconds{100});
    }
}

Sensor::~Sensor() {
    // To be cleared, such that futures will not be used anymore
    process_response_functions_.clear();
}

Future<Acknowledgement>& Sensor::SetLocalizationMode(bool on) {
    return DoRequest(future_localization_mode_, CommandId::CMD_SET_LOCALIZATION_MODE, Boolean{on});
}

Future<Acknowledgement>& Sensor::SetRecordingMode(bool on) {
    return DoRequest(future_recording_mode_, CommandId::CMD_SET_RECORDING_MODE, Boolean{on});
}

Future<Acknowledgement>& Sensor::SetIdleMode(bool on) {
    return DoRequest(future_idle_mode_, CommandId::CMD_SET_IDLE_MODE, Boolean{on});
}

Future<Acknowledgement>& Sensor::Reboot() {
    return DoRequest(future_reboot_, CommandId::CMD_REBOOT);
}

Future<IpAddress>& Sensor::GetIpAddress() {
    return DoRequest(future_get_ip_address_, CommandId::CMD_GET_IP_ADDRESS);
}

Future<PlainUInt16>& Sensor::GetSampleRate() {
    return DoRequest(future_get_sample_rate_, CommandId::CMD_GET_SAMPLE_RATE);
}

SerialNumber Sensor::GetSerialNumber() const {
    return serial_number_;
}

Future<SoftwareVersion>& Sensor::GetSoftwareVersion() {
    return DoRequest(future_software_version_, CommandId::CMD_GET_SOFTWARE_VERSION);
}

Future<TcpSettings>& Sensor::GetTcpSettings() {
    return DoRequest(future_get_tcp_settings_, CommandId::CMD_GET_TCP_SETTINGS);
}

Future<Acknowledgement>& Sensor::SetExpertMode(bool on) {
    return DoRequest(future_expert_mode_, CommandId::CMD_SET_EXPERT_MODE, Boolean{on});
}

Future<Acknowledgement>& Sensor::SetSampleRate(PlainUInt16 rate) {
    return DoRequest(future_set_sample_rate_, CommandId::CMD_SET_SAMPLE_RATE, rate);
}

Future<Acknowledgement>& Sensor::DeleteCluster(ClusterId cluster_id) {
    return DoRequest(future_delete_cluster_, CommandId::CMD_DELETE_CLUSTER, PlainUInt64{cluster_id});
}

Future<Acknowledgement>& Sensor::DeleteArea(Optional<AreaId> area_id) {
    return DoRequest(future_delete_area_,
                     CommandId::CMD_DELETE_AREA,
                     BoolUInt16{!area_id, area_id ? area_id.value() : AreaId{0}});
}

Future<Acknowledgement>& Sensor::SetIpAddress(IpAddress ip) {
    return DoRequest(future_set_ip_address_, CommandId::CMD_SET_IP_ADDRESS, ip);
}

Future<Acknowledgement>& Sensor::SetDateTime(DateTime dt) {
    return DoRequest(future_set_date_time_, CommandId::CMD_SET_DATE_TIME, dt);
}

Future<DateTime>& Sensor::GetDateTime() {
    return DoRequest(future_get_date_time_, CommandId::CMD_GET_DATE_TIME);
}

Future<Acknowledgement>& Sensor::SetCorrectedOdometry(Pose pose) {
    return DoRequest(future_corr_odometry_pose_, CommandId::CMD_SET_CORRECTED_ODOMETRY_POSE, pose);
}

void Sensor::SetExternalReference(ExternalReferencePose external_reference) {
    AddOutgoingMessage(CommandId::CMD_SET_EXTERNAL_REFERENCE_POSE, external_reference);
}

void Sensor::SetSearchRange(SearchRange sr) {
    AddOutgoingMessage(CommandId::CMD_SET_SEARCH_RANGE, sr);
}

Future<Acknowledgement>& Sensor::SetTcpSettings(TcpSettings settings) {
    return DoRequest(future_set_tcp_settings_, CommandId::CMD_SET_TCP_SETTINGS, settings);
}

Future<Acknowledgement>& Sensor::SetMappingMode(bool on, Optional<ClusterId> opt_cluster_id) {
    BoolUInt64 request{on};
    if (on) {
        if (!opt_cluster_id) {
            throw std::logic_error("When turning mapping mode on, the cluster id needs to be supplied.");
        }
        request.uint64_value = opt_cluster_id.value();
    }
    return DoRequest(future_set_mapping_mode_, CommandId::CMD_SET_MAPPING_MODE, request);
}

Future<BoolOptUInt64>& Sensor::GetMappingMode() {
    return DoRequest(future_get_mapping_mode_, CommandId::CMD_GET_MAPPING_MODE);
}

Future<Acknowledgement>& Sensor::SetLineFollowingMode(bool on, Optional<ClusterId> opt_cluster_id) {
    BoolUInt64 request{on};
    if (on) {
        if (!opt_cluster_id) {
            throw std::logic_error("When turning line following mode on, the cluster id needs to be supplied.");
        }
        request.uint64_value = opt_cluster_id.value();
    }
    return DoRequest(future_set_line_following_mode_, CommandId::CMD_SET_LINE_FOLLOWING_MODE, request);
}

Future<BoolOptUInt64>& Sensor::GetLineFollowingMode() {
    return DoRequest(future_get_line_following_mode_, CommandId::CMD_GET_LINE_FOLLOWING_MODE);
}

Future<Acknowledgement>& Sensor::SetUdpSettings(UdpSettings req) {
    return DoRequest(future_set_udp_settings_, CommandId::CMD_SET_UDP_SETTINGS, req);
}

Future<UdpSettings>& Sensor::GetUdpSettings() {
    return DoRequest(future_get_udp_settings_, CommandId::CMD_GET_UDP_SETTINGS);
}

Future<HeartBeat>& Sensor::SubscribeToHeartBeat() {
    future_heart_beat_.ClearData();
    return future_heart_beat_;
}

Future<CorrectedOdometry>& Sensor::SubscribeToCorrectedOdometry() {
    future_corr_odometry_pose_.ClearData();
    return future_corrected_odometry_;
}

Future<Odometry>& Sensor::SubscribeToOdometry() {
    future_odometry_.ClearData();
    return future_odometry_;
}

Future<Diagnostics>& Sensor::SubscribeToDiagnostics() {
    future_diagnostics_.ClearData();
    return future_diagnostics_;
}

Future<DriftCorrection>& Sensor::SubscribeToCompensatedDriftCorrection() {
    future_compensated_drift_correction_.ClearData();
    return future_compensated_drift_correction_;
}

Future<LineFollowerData>& Sensor::SubscribeToLineFollowerData() {
    future_line_follower_data_.ClearData();
    return future_line_follower_data_;
}

Future<CreatedSignature>& Sensor::SubscribeToCreatedSignature() {
    future_created_signature_.ClearData();
    return future_created_signature_;
}

Future<LatestExternalReference>& Sensor::SubscribeToLatestExternalReference() {
    future_latest_ext_ref_.ClearData();
    return future_latest_ext_ref_;
}

Future<SignatureVector>& Sensor::GetSignatureInformation(Optional<accerion::api::ClusterId> cluster_id) {
    future_signature_information_.ClearData();
    int_future_signature_information_.ClearData();
    int_future_signature_information_done_.ClearData();

    signature_information_.value.clear();

    int_future_signature_information_.Callback([this](const SignatureVector& v) {
        Append(signature_information_.value, v.value);
    });
    int_future_signature_information_done_.Callback([this]() {
        future_signature_information_.Fill(std::move(signature_information_));
        int_future_signature_information_.ClearData();
        signature_information_.value.clear();
    });

    AddOutgoingMessage(CommandId::CMD_GET_SIGNATURE_INFORMATION,
                       BoolUInt64{!cluster_id, cluster_id ? cluster_id.value() : ClusterId{0}});
    return future_signature_information_;
}

Future<UInt64Vector>& Sensor::GetClusterIds() {
    return DoRequest(future_cluster_ids_, CommandId::CMD_GET_CLUSTER_IDS);
}

Future<Areas>& Sensor::GetAreaIds() {
    return DoRequest(future_get_area_ids_, CommandId::CMD_GET_AREA_IDS);
}

Future<Acknowledgement>& Sensor::SetContinuousSignatureUpdatingMode(bool on) {
    return DoRequest(future_continuous_signature_updating_mode_,
                     CommandId::CMD_SET_CONTINUOUS_SIGNATURE_UPDATING_MODE,
                     Boolean{on});
}

Future<Acknowledgement>& Sensor::SetArea(AreaId areaId) {
    return DoRequest(future_set_area_, CommandId::CMD_SET_AREA, PlainUInt16{areaId});
}

Future<PlainString>& Sensor::SubscribeToConsoleOutput() {
    future_console_output_.ClearData();
    return future_console_output_;
}

void Sensor::SetCreateMapSubsetCallbacks(SubsetCallback subsetCB, ProgressCallback progressCB) {
    rc_map_subset_.callback.Set(std::move(subsetCB));
    rc_map_subset_.progress_callback.Set(std::move(progressCB));
}

void Sensor::CreateMapSubset(std::vector<ClusterId> clusterIDs, AreaId targetAreaID) {
    SubsetOperationRequest req{targetAreaID, std::move(clusterIDs)};
    AddOutgoingMessage(CommandId::CMD_CREATE_MAP_SUBSET, req);
}

void Sensor::CreateMapSubset(std::string clusterIDs, AreaId targetAreaID) {
    auto ids = StringToIntVector<ClusterId>(std::move(clusterIDs));
    CreateMapSubset(std::move(ids), targetAreaID);
}

Future<SoftwareDetails>& Sensor::GetSoftwareDetails() {
    return DoRequest(future_software_details_, CommandId::CMD_GET_SOFTWARE_DETAILS);
}

Future<MarkerDetection>& Sensor::SubscribeToMarkers() {
    future_marker_detection_.ClearData();
    return future_marker_detection_;
}

Future<Acknowledgement>& Sensor::SetMarkerDetectionMode(const bool on, const Optional<MarkerType> opt_marker_type) {
    MarkerDetectionMode request{on};
    if (on) {
        if (!opt_marker_type) {
            throw std::logic_error("When turning marker detection mode on, the marker type needs to be supplied.");
        }
        request.type = opt_marker_type.value();
    }
    return DoRequest(future_marker_detection_mode_, CommandId::CMD_SET_MARKER_DETECTION_MODE, request);
}

Future<Acknowledgement>& Sensor::SetBufferLength(float bufferLength) {
    return DoRequest(future_set_buffer_length_, CommandId::CMD_SET_BUFFER_LENGTH, BufferLength{bufferLength});
}

Future<BufferLength>& Sensor::GetBufferLength() {
    return DoRequest(future_get_buffer_length_, CommandId::CMD_GET_BUFFER_LENGTH);
}

Future<BufferProgress>& Sensor::StartBufferedRecovery(double xPos, double yPos, double radius, bool fullMapSearch) {
    BufferedRecoveryRequest brr{xPos, yPos, radius, fullMapSearch};
    return DoRequest(future_buffer_progress_, CommandId::CMD_START_PROCESSING_BUFFER, brr);
}

Future<BufferProgress>& Sensor::CancelBufferedRecovery() {
    return DoRequest(future_buffer_progress_, CommandId::CMD_STOP_PROCESSING_BUFFER);
}

Future<StringVector>& Sensor::GetRecordingList() {
    return DoRequest(future_recording_list_, CommandId::CMD_GET_RECORDINGS_LIST);
}

Future<StringVector>& Sensor::DeleteRecordings(std::vector<std::string> vec) {
    return DoRequest(future_delete_recordings_, CommandId::CMD_DELETE_RECORDINGS, StringVector{vec});
}

Future<UInt8Vector>& Sensor::CaptureFrame(std::string key) {
    return DoRequest(future_capture_frame_, CommandId::CMD_CAPTURE_FRAME, PlainString{std::move(key)});
}

Future<LoopClosure>& Sensor::SubscribeToLoopClosures() {
    future_loop_closure_.ClearData();
    return future_loop_closure_;
}

Future<void>& Sensor::SubscribeToLoopClosuresCompleted() {
    future_loop_closures_completed_.ClearData();
    return future_loop_closures_completed_;
}

Future<Acknowledgement>& Sensor::StartSearchForLoopClosures(double search_radius) {
    future_loop_closure_.ClearData();
    future_loop_closures_completed_.ClearData();
    return DoRequest(future_set_search_loop_closures_, CommandId::CMD_START_SEARCH_LOOP_CLOSURES,
                        SearchRadius{search_radius});
}

Future<Acknowledgement>& Sensor::StopSearchForLoopClosures() {
    future_loop_closure_.ClearData();
    future_loop_closures_completed_.ClearData();
    return DoRequest(future_set_search_loop_closures_, CommandId::CMD_STOP_SEARCH_LOOP_CLOSURES);
}

Future<Acknowledgement>& Sensor::SetInternalMode(bool on) {
    return DoRequest(future_internal_mode_, CommandId::CMD_SET_INTERNAL_MODE, Boolean{on});
}

Future<RamRomStats>& Sensor::GetRamRomStats() {
    return DoRequest(future_ram_rom_stats_, CommandId::CMD_GET_RAM_ROM_STATS);
}

bool Sensor::SetFileTransferTaskCallbacks(FileTransferTaskStatusCallback status_cb,
                                          FileTransferTaskProgressCallback progress_cb) {
    if (rc_file_transfer_task_.in_progress) return false;
    rc_file_transfer_task_.callback.Set(std::move(status_cb));
    rc_file_transfer_task_.progress_callback.Set(std::move(progress_cb));
    return true;
}

bool Sensor::ExecuteFileTransferTask(const FileTransferTaskRequest& request) {
    if (rc_file_transfer_task_.in_progress) return false;
    rc_file_transfer_task_.in_progress = true;
    AddOutgoingMessage(CommandId::CMD_FILE_TRANSFER_TASK, request);
    return true;
}

void Sensor::FileTransfer(FileUploadDownloadRequest req, std::string filePath) {
    rc_handshake_.SetFilePath(std::move(filePath));
    rc_handshake_.SetRemoteIpAddress(address_);
    AddOutgoingMessage(CommandId::CMD_FILE_TRANSFER_REQUEST, req);
}

void Sensor::SetFileTransferCallbacks(ProgressCallback progress_cb, DoneCallback done_cb) {
    rc_handshake_.progress_callback.Set(std::move(progress_cb));
    rc_handshake_.done_callback.Set(std::move(done_cb));
}

void Sensor::SendMap(std::string file_path, FileTransferTask task, ProgressCallback transfer_progress_cb,
                     FileTransferTaskProgressCallback processing_progress_cb, DoneCallback done_cb) {
    if (rc_file_transfer_task_.in_progress) {
        done_cb(false, TransferResultMsg.at(TransferResult::kInProgress));
        return;
    }
    if (static_cast<int>(task) > 2) {
        done_cb(false, TransferResultMsg.at(TransferResult::kUnsupported));
        return;
    }
    auto fileSize = GetFileSize(file_path);
    if (fileSize == 0) {
        done_cb(false, TransferResultMsg.at(TransferResult::kFileDoesNotExist));
        return;
    }
    TransferType map_type = DoesStringEndWith(file_path, ".amf") ? TransferType::kUploadLegacyAmf
                                                                 : TransferType::kUploadMap;

    // header is not sent, so calculate size without header
    // and get chunk size if header is available
    std::uint64_t chunk_size = TransferSize.at(TransferType::kDownloadMap);
    if (map_type == TransferType::kUploadLegacyAmf) {
        auto result = FileHeader::Unpack(file_path);
        if (!result.is_valid) {
            done_cb(false, TransferResultMsg.at(TransferResult::kInvalidHeader));
            return;
        }
        fileSize -= static_cast<std::uint64_t>(result.offset);
        chunk_size = result.header.chunk_size;
    }
    FileUploadDownloadRequest req{map_type, fileSize, chunk_size};
    auto done_lambda = [this, task, processing_progress_cb, done_cb](bool success, const std::string &message) {
        if (!success) {
            done_cb(false, message);
            return;
        }

        auto ftt_done_lambda = [done_cb](FileTransferTask task, FileTransferTaskStatus status, std::string message) {
                if (status == FileTransferTaskStatus::kTaskSuccess) {
                    done_cb(true, TransferResultMsg.at(TransferResult::kSuccessfulSendMap));
                }
                if (status == FileTransferTaskStatus::kTaskFail) {
                    done_cb(false, std::move(message));
                }
            };
        SetFileTransferTaskCallbacks(std::move(ftt_done_lambda), processing_progress_cb);
        ExecuteFileTransferTask(FileTransferTaskRequest{task});
    };
    SetFileTransferCallbacks(std::move(transfer_progress_cb), std::move(done_lambda));
    FileTransfer(req, std::move(file_path));
}

void Sensor::GetMap(std::string filepath, ProgressCallback transfer_progress_cb, DoneCallback done_cb) {
    FileUploadDownloadRequest req{TransferType::kDownloadMap, 0, 0};
    SetFileTransferCallbacks(std::move(transfer_progress_cb), std::move(done_cb));
    FileTransfer(req, std::move(filepath));
}

void Sensor::GetRecordings(std::vector<std::string> names, std::string file_path, ProgressCallback transfer_progress_cb,
                           FileTransferTaskProgressCallback processing_progress_cb, DoneCallback done_cb) {
    if (rc_file_transfer_task_.in_progress) {
        done_cb(false, TransferResultMsg.at(TransferResult::kInProgress));
        return;
    }
    auto names_vec = accerion::api::StringVector{std::move(names)};
    FileTransferTaskRequest ftt_request{FileTransferTask::kGatherRecordings, names_vec.Serialize()};
    // Can be captured by move in C++ 14, instead of using std::bind
    auto ftt_done_lambda = std::bind(
        [this](FileTransferTask task, FileTransferTaskStatus status, std::string &file_path,
               ProgressCallback &transfer_progress_cb, DoneCallback &done_cb) {
            if (status == FileTransferTaskStatus::kTaskFail)
                done_cb(false, TransferResultMsg.at(TransferResult::kFailedGetRecordings));
            if (status == FileTransferTaskStatus::kTaskSuccess) {
                // Recordings gathered, ready for download:
                FileUploadDownloadRequest req{TransferType::kDownloadRecordings, 0, 0};
                SetFileTransferCallbacks(transfer_progress_cb, done_cb);
                FileTransfer(req, file_path);
            }
        },
        std::placeholders::_1, std::placeholders::_2, std::move(file_path), std::move(transfer_progress_cb),
        std::move(done_cb));
    SetFileTransferTaskCallbacks(std::move(ftt_done_lambda), std::move(processing_progress_cb));
    ExecuteFileTransferTask(std::move(ftt_request));
}

Future<MapSummary>& Sensor::GetMapSummary() {
    return DoRequest(future_map_summary_, CommandId::CMD_GET_MAP_SUMMARY);
}

void Sensor::LogEvent(const std::string& message) {
    AddOutgoingMessage(CommandId::CMD_LOG_EVENT, accerion::api::PlainString{message});
}

Future<Acknowledgement>& Sensor::SetSaveImages(bool value) {
    return DoRequest(future_save_images_, CommandId::CMD_SET_SAVE_IMAGES, Boolean{value});
}

Future<Boolean>& Sensor::GetSaveImages() {
    return DoRequest(future_get_save_images_, CommandId::CMD_GET_SAVE_IMAGES);
}

ProcessResponseFunctions Sensor::InitializeProcessResponseFunctionsWithReceivedCommands() {
    return {
            {CommandId::ACK_MAP_SUBSET,
                    [this](RawData d) { rc_map_subset_.ProcessAck(std::move(d)); }},
            {CommandId::ACK_FILE_TRANSFER_HANDSHAKE,
                    [this](RawData d) { rc_handshake_.ProcessAck(std::move(d)); }},
            {CommandId::ACK_FILE_TRANSFER_STATUS,
                    [this](RawData d) { rc_file_transfer_task_.ProcessAck(std::move(d)); }},
    };
}

CommunicationThread Sensor::CreateCommunicationThread(const Address& address,
                                                      const SerialNumber& serial_number,
                                                      ConnectionType connection_type,
                                                      const ProcessResponseFunctions& process_response_functions) {

    if (connection_type == ConnectionType::kConnectionUdpBroadcast ||
            connection_type == ConnectionType::kConnectionUdpUnicast ||
            connection_type == ConnectionType::kConnectionUdpUnicastNoHb) {
        auto udp_client = MakeUnique<UdpClient>(SENSOR_RECEIVE_PORT_UDP, API_RECEIVE_PORT_UDP);
        udp_client->SetIpAddress(address);

        return CommunicationThread{std::move(udp_client),
                                   serial_number,
                                   CreateDefaultParsedMessageCallback(serial_number, process_response_functions)};
    } else {
        auto tcp_client = MakeUnique<TcpClient>(address, TCP_PORT);

        std::system_error error = tcp_client->ConnectToServer();
#ifdef CMAKE_WINDOWS
        if (error.code().value() == WSAECONNREFUSED)
#else
        while (error.code().value() == EINPROGRESS) {
            error = tcp_client->ConnectToServer();
        }
        if (error.code().value() == ECONNREFUSED)
#endif
        {
            throw error;
        }

        return CommunicationThread{std::move(tcp_client),
                                   serial_number,
                                   CreateDefaultParsedMessageCallback(serial_number, process_response_functions)};
    }
}

}  // namespace api
}  // namespace accerion
