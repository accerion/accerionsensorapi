/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/crc8.h"

#include "AccerionSensorAPI/utils/memory.h"

namespace accerion {
namespace api {

namespace detail {

constexpr auto kWidth = 8 * sizeof(Crc8::Crc);  // defining the width of the CRC
constexpr auto kPolynomial = 0xD8;  // 11011 followed by 0's
constexpr auto kTopbit = 1 << (kWidth - 1);  // defining the top bit based on the width

}  // namespace detail

Crc8::Crc Crc8::Generate(const uint8_t* message, int nBytes) {
    const auto& instance = GetInstance();

    Crc remainder = 0;

    // Divide the message by the polynomial, a byte at a time
    for (auto byte = 0; byte < nBytes; ++byte) {
        std::uint8_t data = message[byte] ^ (remainder >> (detail::kWidth - 8));
        remainder = static_cast<Crc>(instance.table_[data] ^ (remainder << 8));
    }

    // The final remainder is the CRC
    return remainder;
}

const Crc8& Crc8::GetInstance() {
    static Crc8 instance{};
    return instance;
}

std::array<Crc8::Crc, 256> Crc8::CreateTable() {
    std::array<Crc, 256> result{};

	Crc remainder;

	// Compute the remainder of each possible dividend
	for (std::uint16_t dividend = 0; dividend < 256; ++dividend) {
		// Start with the dividend followed by zeros
		remainder = static_cast<uint8_t>(dividend << (detail::kWidth - 8));

		// Perform modulo-2 division, a bit at a time
		for (std::uint8_t bit = 8; bit > 0; --bit) {
			// Try to divide the current data bit
			if (remainder & detail::kTopbit) {
				remainder = static_cast<Crc>((remainder << 1) ^ detail::kPolynomial);
			}
			else {
				remainder = static_cast<Crc>(remainder << 1);
			}
		}

		// Store the result into the table.
        result[dividend] = remainder;
	}

    return result;
}

}  // namespace api
}  // namespace accerion
