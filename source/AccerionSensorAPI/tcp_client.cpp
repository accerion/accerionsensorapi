/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/tcp_client.h"

#include <cstring>
#include <iostream>

#include "AccerionSensorAPI/communication/command_parser.h"
#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

constexpr bool kDebug{false};

TcpClient::TcpClient(Address address, unsigned int remoteReceivePort) {
    open_ = false;

    remote_receive_port_ = static_cast<uint16_t>(remoteReceivePort);
    remote_address_.sin_addr.s_addr = address.Convert().s_addr;

    connected_ = false;
    tcpSettings_ = EnabledMessageType::kBoth;
    OpenSocket();
}

TcpClient::~TcpClient() {
    closeSocket();
}

TcpClient::TcpClient(TcpClient &&other) noexcept {
    *this = std::move(other);
}

TcpClient& TcpClient::operator=(TcpClient &&other) noexcept {
    remote_receive_port_ = other.remote_receive_port_;
    remote_address_ = other.remote_address_;
    open_ = other.open_;
    connected_ = other.connected_;
    tcpSettings_ = other.tcpSettings_;
    // To keep socket open, invalidate moved from 'socket_'
    closeSocket();
    socket_ = other.socket_;
    other.socket_ = -1;
    return *this;
}

bool TcpClient::OpenSocket() {
#ifdef CMAKE_WINDOWS
    WSADATA wsaData;
    int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        std::cout << "WSAStartup failed: " << iResult << std::endl;
        return 1;
    }
    socket_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    unsigned long argp = 1;// 1 to set non-blocking, 0 to set re-usable
    int result = setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, (char *) &argp, sizeof(argp));
    if (result != 0) {
        std::cout << "setsockopt() error " << result << std::endl;
        return EXIT_FAILURE;
    }

    argp = 1;// 1 to set non-blocking, 0 to set blocking
    if (ioctlsocket(socket_, FIONBIO, &argp) == SOCKET_ERROR) {
        std::cout << "ioctlsocket() error " << WSAGetLastError() << std::endl;
        return EXIT_FAILURE;
    }

    // Disable Nagle's algorithm (TCP_NODELAY) for Low-latency connection
    int no_delay = 0; // Note: Windows 'no_delay' parameter seems to be reversed
    int rc = setsockopt(socket_, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<const char *>(&no_delay),
                        sizeof(no_delay));
    if (rc != 0) {
        std::cout << "Set TCP_NODELAY socket option error: " << rc << std::endl;
        return EXIT_FAILURE;
    }

    tcp_keepalive alive_struct;
    DWORD ret;

    alive_struct.onoff = true;           //keepalive enabled
    alive_struct.keepaliveinterval = 100;//Interval between keepalive probes is set to 100 ms
    alive_struct.keepalivetime = 500;    //Keep alive time set to 500 ms

    if (WSAIoctl(socket_, SIO_KEEPALIVE_VALS, &alive_struct, sizeof alive_struct, NULL, 0, &ret, NULL, NULL)
        != 0) {
        std::cout << "Set keep alive config error: " << WSAGetLastError() << std::endl;
    }
#else
    socket_ = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP);
    // Network Programming use KEEPALIVE TCP mechanism; https://www.programmersought.com/article/4161587163/
    /* Set: use keepalive on fd */
    int alive = 1;
    if (setsockopt(socket_, SOL_SOCKET, SO_KEEPALIVE, &alive, sizeof alive) != 0) {
        std::cout << "Set keepalive error: " << strerror(errno) << std::endl;
    }

    /* 1 seconds without data, trigger keep-alive mechanism, send keep-alive package */
    int idle = 1;
    if (setsockopt(socket_, SOL_TCP, TCP_KEEPIDLE, &idle, sizeof idle) != 0) {
        std::cout << "Set keepalive idle error: " << strerror(errno) << std::endl;
    }

    /* If no response is received, re-send the keep-alive pack; after 1 seconds */
    int intv = 1;
    if (setsockopt(socket_, SOL_TCP, TCP_KEEPINTVL, &intv, sizeof intv) != 0) {
        std::cout << "Set keepalive intv Error: " << strerror(errno) << std::endl;
    }

    /* has not received the keep-alive package for 2 consecutive times, as Connection invalidation */
    int cnt = 2;
    if (setsockopt(socket_, SOL_TCP, TCP_KEEPCNT, &cnt, sizeof cnt) != 0) {
        std::cout << "Set keepalive cnt error: " << strerror(errno) << std::endl;
    }

    // Disable Nagle's algorithm (TCP_NODELAY) for Low-latency connection
    int no_delay = 1;
    if (setsockopt(socket_, SOL_TCP, TCP_NODELAY, &no_delay, sizeof(no_delay)) != 0) {
        std::cout << "Set TCP_NODELAY socket option error: " << strerror(errno) << std::endl;
    }
#endif

    if (socket_ < 0) {
        std::cout << "Error while opening transmitting TCP socket" << std::endl;
        open_ = false;
        return false;
    }

    // Set the IP addresses for TCP message
    remote_address_.sin_family = AF_INET;
    remote_address_.sin_port = htons(remote_receive_port_);

    std::memset(remote_address_.sin_zero, '\0', sizeof(remote_address_.sin_zero));

    open_ = true;
    if (kDebug) { std::cout << "TCP socket opened" << std::endl; }
    return true;
}

std::system_error TcpClient::ConnectToServer() {
    const std::system_error kNoError{0, std::generic_category()};

    if (connected_) {
        if (kDebug) { std::cout << "already connected" << std::endl; }
        return kNoError;
    }

    if (!open_ && !OpenSocket()) {
        if (kDebug) { std::cout << "socket not open" << std::endl; }
        return std::system_error(-1, std::generic_category());  // Socket error
    }

    if (kDebug) {
        std::cout << "TcpClient: setting server IP address to := " << inet_ntoa(remote_address_.sin_addr) << std::endl;
    }
    int connection = connect(socket_, reinterpret_cast<sockaddr*>(&remote_address_), sizeof(remote_address_));

    if (connection >= 0) {
        connected_ = true;
        return kNoError;
    }

#ifdef CMAKE_WINDOWS
    auto WSALastError = GETSOCKETERRNO();
    if (WSALastError == WSAEISCONN) {
        if (kDebug) { std::cout << "Is connected error: " << WSALastError << std::endl; }
        connected_ = true;
        return std::system_error(WSALastError, std::generic_category());
    }

    // Properly handle errors as suggested https://docs.microsoft.com/en-us/windows/win32/api/winsock2/nf-winsock2-connect
    else if (WSALastError != WSAEINVAL && WSALastError != WSAEWOULDBLOCK && WSALastError != WSAEALREADY) {
        if (kDebug) { std::cout << "Socket error: " << WSALastError << std::endl; }
        connected_ = false;
        return std::system_error(WSALastError, std::generic_category());
    }
#else
    if ((GETSOCKETERRNO() != EINPROGRESS) && (GETSOCKETERRNO() != EALREADY) && (GETSOCKETERRNO() != ECONNREFUSED)) {
        if (kDebug) { perror("Error connecting TCP port, error is := "); }
        connected_ = false;
        return std::system_error(GETSOCKETERRNO(), std::generic_category());
    }
    else if (GETSOCKETERRNO() == ECONNREFUSED) {
        if (kDebug) {
            std::cout << "Connection refused, maybe reached limit number of connected client(s)." << std::endl;
        }
        return std::system_error(ECONNREFUSED, std::generic_category());
    }
#endif

    // Check socket for errors or timeout
    fd_set writeFD, exceptFD;

    FD_ZERO(&writeFD);
    FD_SET(socket_, &writeFD);

    FD_ZERO(&exceptFD);
    FD_SET(socket_, &exceptFD);

    timeval tv;
    tv.tv_sec = 5;
    tv.tv_usec = 0;

    int result = select(socket_ + 1, nullptr, &writeFD, &exceptFD, &tv);
    if (result <= 0 || FD_ISSET(socket_, &exceptFD)) {
        int socket_error;
        int error_len = sizeof(socket_error);

        getsockopt(socket_, SOL_SOCKET, SO_ERROR, (char*) &socket_error, (socklen_t*) &error_len);

        if (kDebug) { std::cout << "select error on TCP port, error is := " << socket_error << std::endl; }
        connected_ = false;
        return std::system_error(socket_error, std::generic_category());
    }

    if (!connected_) {
        if (kDebug) { std::cout << "still no connection possible..." << std::endl; }
        return std::system_error(GETSOCKETERRNO(), std::generic_category());
    }

    return kNoError;
}

int TcpClient::TransmitMessage(const uint8_t *const transmittedMessage, const std::uint64_t n_bytes_message) {
    if (!GetConnected() || !open_) {
        return -1;
    }

    std::uint64_t n_bytes_send_so_far = 0;
    while (n_bytes_send_so_far != n_bytes_message) {
#ifdef CMAKE_WINDOWS
        const auto n_bytes_send =
            sendto(socket_, reinterpret_cast<const char *>(&transmittedMessage[n_bytes_send_so_far]),
                   n_bytes_message - n_bytes_send_so_far, 0, reinterpret_cast<sockaddr*>(&remote_address_),
                   sizeof(remote_address_));
        if (n_bytes_send == SOCKET_ERROR) {
            const auto error_code = GETSOCKETERRNO();
            if (error_code == WSAEWOULDBLOCK) {
                continue;
            } else if (error_code == WSAECONNRESET) {
                connected_ = false;
                closeSocket();
            }
            return error_code;
        }
#else
        const auto n_bytes_send =
            sendto(socket_, &transmittedMessage[n_bytes_send_so_far], n_bytes_message - n_bytes_send_so_far,
                   MSG_NOSIGNAL, reinterpret_cast<sockaddr*>(&remote_address_), sizeof(remote_address_));
        if (n_bytes_send == -1) {
            const auto error_code = GETSOCKETERRNO();
            if (error_code == EAGAIN) {
                continue;
            } else if (error_code == EPIPE) {
                if (kDebug) {
                    perror(" Error sending message to port, error is := ");
                }
                connected_ = false;
                closeSocket();
            }
            return error_code;
        }
#endif
        n_bytes_send_so_far += n_bytes_send;
    }
    return 0;
}

bool TcpClient::ReceiveMessage() {
    if (!open_ || !GetConnected()) {
        return false;
    }

#ifdef CMAKE_WINDOWS
    receivedNumOfBytes_ = recv(socket_, receivedMessage_, sizeof(receivedMessage_), 0);
    if (receivedNumOfBytes_ <= 0) {
        auto error = WSAGetLastError();
        // Either socket closed by peer or TCP_KEEPALIVE mechanism
        if (error == NULL || error == WSAECONNRESET) {
            std::cout << "Received connection reset, closing socket" << std::endl;
            closeSocket();
            connected_ = false;
        }
        else if (error != WSAEWOULDBLOCK) {
            std::cout << "Error while receiving messages in TCPReceiver, error is: " << error << std::endl;
        }

        return false;
    }
#else
    receivedNumOfBytes_ = static_cast<int>(read(socket_, receivedMessage_, sizeof(receivedMessage_)));
    if (receivedNumOfBytes_ <= 0) {
        int errsv = errno;
        // Either socket reset by peer (104), TCP_KEEPALIVE mechanism or connection closed EOF
        if (errsv == ECONNRESET || errsv == ETIMEDOUT || receivedNumOfBytes_ == 0) {
            if (kDebug) {
                perror("Socket timeout, either closed by peer or TCP_KEEPALIVE mechanism");
            }

            closeSocket();
            connected_ = false;
        }
        else if (errsv != EAGAIN) {
            perror("Error while receiving messages in TcpClient, error is");
        }

        return false;
    }
#endif
    else {
        if (receivedNumOfBytes_ > bufferSize_) {
            if (kDebug) {
                std::cout << "Received TCP Message is too big, received num of bytes is := " << receivedNumOfBytes_
                          << std::endl;
            }
            return false;
        }

        if (kDebug) {
            std::cout << "received num of bytes := " << receivedNumOfBytes_ << std::endl;
        }
        return true;
    }
}

bool TcpClient::SendMessage(const CommandId& id, RawData&& message) {
    auto message_type{MessageType::kAcknowledgement};
    const auto iter = kCommandMessageTypes.find(id);
    if (iter != kCommandMessageTypes.end()) {
        message_type = iter->second;
    }

    if (message_type == MessageType::kAcknowledgement) {
        return TransmitMessage(message.data(), message.size());
    }

    if (tcpSettings_ == EnabledMessageType::kInactive) {
        return true;
    }

    int error_number = 0;
    if (message_type == MessageType::kStreaming) {
        if (tcpSettings_ == EnabledMessageType::kBoth || tcpSettings_ == EnabledMessageType::kStreaming) {
            error_number = TransmitMessage(message.data(), message.size());
        }
    }
    else if (message_type == MessageType::kIntermittent || message_type == MessageType::kCommand) {
        if (tcpSettings_ == EnabledMessageType::kBoth || tcpSettings_ == EnabledMessageType::kIntermittent) {
            error_number = TransmitMessage(message.data(), message.size());
        }
    }

    if (error_number == GetEnumValue(SocketError::kBrokenPipe)) {
        tcpSettings_ = EnabledMessageType::kInactive;
        remote_address_.sin_addr.s_addr = 0;
    }

    return error_number == 0;
}

void TcpClient::TransmitMessages(const std::vector<Command>& commands, const SerialNumber& serial_number) {
    for (const auto& command : commands) {
        auto message = FormMessage(serial_number, command);
        SendMessage(command.command_id, std::move(message));
    }
}

void TcpClient::closeSocket() {
    open_ = false;
#ifdef CMAKE_WINDOWS
    closesocket(socket_);
#else
    close(socket_);
#endif
    socket_ = -1;
}

}  // namespace api
}  // namespace accerion
