/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/received_command/file_transfer_task.h"

namespace accerion {
namespace api {

void ReceivedCommandFileTransferTask::ProcessAck(RawData raw_data) {
    FileTransferTaskResult result = FileTransferTaskResult::Deserialize(raw_data);
    if (result.status == FileTransferTaskStatus::kTaskProgress) {
        progress_callback.Do(result.task, result.progress);
        return;
    }
    callback.Do(result.task, result.status, result.message);
    in_progress = false;
}

}  // namespace api
}  // namespace accerion
