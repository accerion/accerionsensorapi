/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "AccerionSensorAPI/received_command/map_subset.h"

namespace accerion {
namespace api {

void ReceivedCommandMapSubset::ProcessAck(RawData raw_data) {
    if (!callback) {
        return;
    }
    SubsetOperationResult result = SubsetOperationResult::Deserialize(raw_data);

    if (result.message_type == SubsetResultMessageType::kProgress)
    {
        progress_callback.Do(result.value);
        return;
    }
    callback.Do(static_cast<SubsetResult>(result.value), result.failed_ids);
}

}  // namespace api
}  // namespace accerion
