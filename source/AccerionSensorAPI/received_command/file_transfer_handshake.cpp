/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionSensorAPI/received_command/file_transfer_handshake.h"

#include <fstream>
#include <memory>

#include "AccerionSensorAPI/structs/area_file_header.h"
#include "AccerionSensorAPI/tcp_client.h"
#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/utils/memory.h"
#include "AccerionSensorAPI/utils/time.h"

namespace accerion {
namespace api {

ReceivedCommandFileTransferHandshake::~ReceivedCommandFileTransferHandshake() {
    JoinFileTransferThreads();
};

void ReceivedCommandFileTransferHandshake::JoinFileTransferThreads() {
    // Join file transfer threads
    for (std::uint8_t i = 0; i < kMaxFileTransferThreads; i++) {
        if (file_transfer_threads_[i].joinable()) {
            file_transfer_threads_[i].join();
            file_transfer_threads_running_[i].store(false);
            std::cout << "Closed down i = " << i << " file transfer thread" << std::endl;
        }
    }
}
void ReceivedCommandFileTransferHandshake::ProcessAck(RawData raw_data) {
    const auto handshake = FileTransferHandshake::Deserialize(raw_data);

    data.Set(handshake);

    switch (handshake.transfer_status) {
        case TransferStatus::kAvailable:
            OpenFileTransferThread(handshake);
            break;
        case TransferStatus::kTransferFinished:
            done_callback.Do(true, TransferStatusMessage.at(handshake.transfer_status));
            break;
        case TransferStatus::kTransferAlreadyInProgress:
        case TransferStatus::kFileDoesNotExist:
        case TransferStatus::kFailed:
        case TransferStatus::kUnsupported:
            done_callback.Do(false, TransferStatusMessage.at(handshake.transfer_status));
            break;
        default:
            throw std::runtime_error("Implementation error: TransferStatus not handled by ReceivedCommandFileTransferHandshake::ProcessAck.");
    }
}

void ReceivedCommandFileTransferHandshake::OpenFileTransferThread(const FileTransferHandshake &handshake) {
    JoinFileTransferThreads();

    // Run file transfer threads
    for (std::uint8_t i = 0; i < kMaxFileTransferThreads; ++i) {
        if (!file_transfer_threads_running_[i].load()) {
            file_transfer_threads_[i] =
                std::thread([this, handshake]() {
                    RunFileTransfer(handshake, progress_callback, done_callback, client_ip_, file_path_);
                });
            file_transfer_threads_running_[i].store(true);
            std::cout << "Opened up i = " << i << " file transfer thread" << std::endl;
            break;
        }
    }
}

void ReceivedCommandFileTransferHandshake::RunFileTransfer(
        FileTransferHandshake handshake,
        const ReceivedCommandCallback<ProgressCallback> &progress_cb,
        const ReceivedCommandCallback<DoneCallback> &done_cb,
        const accerion::api::Address& client_address,
        const std::string &file_path) {
    std::cout << "Port number: " << handshake.port_number << std::endl;

    uint64_t total_transferred_bytes = 0;
    auto tcp_client = MakeUnique<TcpClient>(client_address, handshake.port_number);
    while (!tcp_client->GetConnected()) { tcp_client->ConnectToServer(); }

    const auto start_time = std::chrono::steady_clock::now();

    if (IsUpload(handshake.transfer_type)) {  // Upload
        uint64_t file_size = GetFileSize(file_path);
        if (file_size == 0) {
            done_cb.Do(false, "Could not read file size..");
            return;
        }

        std::ifstream file_handle{file_path, std::ios::in | std::ios::binary};
        uint64_t maximum_tcp_message_length{};
        const auto transfer_size_it = TransferSize.find(handshake.transfer_type);
        if (transfer_size_it != TransferSize.end()) {
            maximum_tcp_message_length = transfer_size_it->second;
        }

        if (handshake.transfer_type == TransferType::kUploadLegacyAmf) {
            auto result = FileHeader::Unpack(file_path);
            if (!result.is_valid) {
                done_cb.Do(false, "Could not read file header");
                return;
            }
            file_size -= result.offset;
            file_handle.ignore(result.offset);
            if (result.header.chunk_size != 0) { maximum_tcp_message_length = result.header.chunk_size; }
        }

        auto file_buffer =
            std::unique_ptr<std::uint8_t[]>{new uint8_t[maximum_tcp_message_length]};

        while (tcp_client->GetConnected()) {
            file_handle.read(reinterpret_cast<char *>(file_buffer.get()), maximum_tcp_message_length);
            auto bytes_read = static_cast<uint32_t>(file_handle.gcount());
            std::cout << "bytesRead = " << bytes_read << std::endl;

            if (tcp_client->TransmitMessage(file_buffer.get(), bytes_read) == 0) {
                total_transferred_bytes += bytes_read;
                auto progress_percentage = static_cast<int>(static_cast<double>(total_transferred_bytes)
                                                            / static_cast<double>(file_size) * 100);
                progress_cb.Do(progress_percentage);
                // TO DO: THIS DOESN'T WORK (YET), DUE TO IT NOT BEING BLOCKING IT SEEMS
            } else {
                std::cout << "Couldn't send.." << std::endl;
            }

            if (total_transferred_bytes == file_size) { break; }
        }
    } else {  // Download
        std::ofstream file_handle{file_path, std::ios::binary | std::ios::out};

        while (tcp_client->GetConnected()) {
            if (tcp_client->ReceiveMessage()) {
                total_transferred_bytes += tcp_client->GetReceivedNumOfBytes();
                file_handle.write(
                    reinterpret_cast<char *>(tcp_client->GetReceivedMessage()),
                    tcp_client->GetReceivedNumOfBytes());
                auto progress_percentage = static_cast<int>(static_cast<double>(total_transferred_bytes)
                                                            / static_cast<double>(handshake.file_size) * 100);
                progress_cb.Do(progress_percentage);
            }
            if (total_transferred_bytes == handshake.file_size) { break; }
        }
    }
    std::cout << "MB/s: "
              << MbPerSecond(
                     std::chrono::steady_clock::now() - start_time,
                     total_transferred_bytes)
              << std::endl;
}

}  // namespace api
}  // namespace accerion
