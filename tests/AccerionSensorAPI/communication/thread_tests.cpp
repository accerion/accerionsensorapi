/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/communication/thread.h"


const accerion::api::SerialNumber kDummySerialNumber{};


/// Simple communication-like client that just loops back the outgoing commands as incoming ones
class DummyClient {
public:
    void ConnectToServer() const {};
    void StayAlive() const {};

    void TransmitMessages(const std::vector<accerion::api::Command>& commands,
                          const accerion::api::SerialNumber& serial_number) {
        for (const auto& command: commands) {
            accerion::api::Append(incoming_messages_buffer_, accerion::api::FormMessage(serial_number, command));
        }
    }

    bool ReceiveMessage() {
        incoming_messages_.clear();
        std::swap(incoming_messages_, incoming_messages_buffer_);
        return !incoming_messages_.empty();
    }
    const std::uint8_t* GetReceivedMessage() const { return incoming_messages_.data(); }
    std::size_t GetReceivedNumOfBytes() const { return incoming_messages_.size(); }

private:
    std::vector<std::uint8_t> incoming_messages_;
    std::vector<std::uint8_t> incoming_messages_buffer_;
};

constexpr std::chrono::duration<double> kWaitForCommunicationThread{
    10. / accerion::api::network_constants::kApiThroughput};  // 5 iterations


TEST(CommunicationThread, Movable) {
    std::atomic<accerion::api::CommandId> last_received_command_id{accerion::api::CommandId{}};  // invalid id

    accerion::api::CommunicationThread thread{
        accerion::api::MakeUnique<DummyClient>(),
        kDummySerialNumber,
        [&last_received_command_id](accerion::api::SerialNumber, accerion::api::Command&& command) {
            last_received_command_id = command.command_id;
        }};

    // Baseline check, everything works
    thread.AddOutgoingMessage(accerion::api::CommandId::CMD_GET_IP_ADDRESS);
    std::this_thread::sleep_for(kWaitForCommunicationThread);
    ASSERT_EQ(last_received_command_id.load(), accerion::api::CommandId::CMD_GET_IP_ADDRESS);

    // Actual test, everything still works when thread is moved
    last_received_command_id = accerion::api::CommandId{};  // reset to invalid id
    accerion::api::CommunicationThread other_thread = {std::move(thread)};

    other_thread.AddOutgoingMessage(accerion::api::CommandId::CMD_GET_IP_ADDRESS);
    std::this_thread::sleep_for(kWaitForCommunicationThread);
    ASSERT_EQ(last_received_command_id.load(), accerion::api::CommandId::CMD_GET_IP_ADDRESS);
}
