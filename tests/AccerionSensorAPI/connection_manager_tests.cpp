/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/connection_manager.h"


/// Kind of dummy tests, because there are no sensors available for this test and so no connections can be made
/// Still that can be tested, plus the error handling
/// There is also another purpose of this test. Because it includes the ConnectionManager, it will include a lot of the
/// API. This will help in get a better overview of the test coverage that is calculated.


TEST(ConnectionManager, NoSensorConnection) {
    using namespace accerion::api;
    const std::chrono::seconds time_out{3};

    ASSERT_FALSE(ConnectionManager::GetSensor(Address{}, ConnectionType::kConnectionTcp).WaitFor(time_out));
    ASSERT_FALSE(ConnectionManager::GetSensor(SerialNumber{}, ConnectionType::kConnectionTcp).WaitFor(time_out));
}

TEST(ConnectionManager, NoUpdateServiceConnection) {
    using namespace accerion::api;
    const std::chrono::seconds time_out{3};

    ASSERT_FALSE(ConnectionManager::GetUpdateService(Address{}).WaitFor(time_out));
    ASSERT_FALSE(ConnectionManager::GetUpdateService(SerialNumber{}).WaitFor(time_out));
}

TEST(ConnectionManager, InvalidConnection) {
    using namespace accerion::api;
    ASSERT_ANY_THROW(ConnectionManager::GetSensor(Address{}, ConnectionType::kConnectionUdpUnicast));
    ASSERT_ANY_THROW(ConnectionManager::GetSensor(Address{}, ConnectionType::kConnectionUdpUnicastNoHb));
}
