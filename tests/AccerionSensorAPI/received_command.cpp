/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <future>
#include "gtest/gtest.h"

#include "AccerionSensorAPI/received_command/received_command.h"


using DataType = std::vector<double>;
using TestReceivedCommandData = accerion::api::ReceivedCommandData<DataType>;


// Roughly the code that is also in the api::Sensor object, can be improved in the future perhaps
struct BlockingResult {
    bool time_out = false;
    DataType data{0};
};

auto CreateThread(TestReceivedCommandData& rc_data) {
    return std::async(std::launch::async, [&rc_data]() {
        constexpr std::chrono::seconds kTimeOut{1};
        BlockingResult result;
        result.data = rc_data.Get(kTimeOut, result.time_out);
        return result;
    });
}


TEST(ReceivedCommandTests, DataNoTimeOut) {
    const DataType kValue{1.5, 3.5, 5.5};

    TestReceivedCommandData rc_data{};

    auto future = CreateThread(rc_data);

    std::this_thread::sleep_for(std::chrono::milliseconds{10});
    rc_data.Set(kValue);

    const auto result = future.get();

    EXPECT_FALSE(result.time_out);
    EXPECT_EQ(result.data, kValue);
}

TEST(ReceivedCommandTests, DataTimeOut) {
    TestReceivedCommandData rc_data{};

    const auto result = CreateThread(rc_data).get();

    EXPECT_TRUE(result.time_out);
}
