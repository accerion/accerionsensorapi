/* Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <fstream>
#include <numeric>
#include <vector>

#include "gtest/gtest.h"

#include "AccerionSensorAPI/utils/algorithm.h"

struct MoveableBool {
    MoveableBool() {
        is_moved = false;
    }

    MoveableBool(const MoveableBool&) = delete;
    MoveableBool& operator=(const MoveableBool&) noexcept = delete;
    MoveableBool(MoveableBool&& other) noexcept : MoveableBool{} {
        *this = std::move(other);
    }
    MoveableBool& operator=(MoveableBool&&) noexcept {
        is_moved = true;
        return *this;
    }

    ~MoveableBool() = default;

    bool is_moved;
};

// Moving the second vector is possible when the first vector is empty
TEST(Append, AppendByMovingVector) {
    std::vector<MoveableBool> vector_a(5), vector_b;
    for (const MoveableBool& a : vector_a) {
        ASSERT_EQ(a.is_moved, false);
    }
    ASSERT_EQ(vector_a.size(), 5);
    ASSERT_EQ(vector_b.size(), 0);
    accerion::api::Append(vector_b, std::move(vector_a));
    ASSERT_EQ(vector_b.size(), 5);
    for (const MoveableBool& b : vector_b) {
        ASSERT_EQ(b.is_moved, false);
    }
}

// Moving the elements from second vector is required when the first vector is not empty
TEST(Append, AppendByMovingComponents) {
    std::vector<MoveableBool> vector_a(5), vector_b(1);
    for (const MoveableBool& a : vector_a) {
        ASSERT_EQ(a.is_moved, false);
    }
    ASSERT_EQ(vector_a.size(), 5);
    ASSERT_EQ(vector_b.size(), 1);
    accerion::api::Append(vector_b, std::move(vector_a));
    ASSERT_EQ(vector_b.size(), 6);
    for (const MoveableBool& b : vector_b) {
        ASSERT_EQ(b.is_moved, true);
    }
}

TEST(Erase, Generic) {
    std::size_t kSize = 30;
    std::vector<int> v(kSize);
    std::iota(v.begin(), v.end(), 3);
    accerion::api::Erase(v, [](auto i) { return i % 2 == 0; });

    ASSERT_EQ(v.size(), kSize / 2);
}

template <typename T>
class EraseTest : public testing::Test {
protected:
    using Container = T;
};

using EraseTestTypes = ::testing::Types<std::set<int>, std::unordered_set<int>>;
TYPED_TEST_SUITE(EraseTest, EraseTestTypes);

TYPED_TEST(EraseTest, Generic) {
    using Container = typename TestFixture::Container;

    std::size_t kSize = 30;
    Container s;
    for (int i = 0; i < kSize; ++i) {
        s.insert(i);
    }
    accerion::api::Erase(s, [](auto i) { return i % 2 == 0; });

    ASSERT_EQ(s.size(), kSize / 2);
}
