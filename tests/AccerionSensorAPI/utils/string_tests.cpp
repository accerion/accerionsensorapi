/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <list>
#include <string>
#include <vector>

#include "gtest/gtest.h"

#include "AccerionSensorAPI/utils/string.h"


TEST(String, ToLower) {
    const std::list<std::pair<std::string, std::string>> tests = {
        {"1 abc % -", "1 abc % -"},
        {"aBcD", "abcd"},
    };
    for (const auto& test : tests) {
        auto input = test.first;
        const auto& expectation = test.second;

        accerion::api::ToLower(input);
        ASSERT_EQ(input, expectation) << "String '" << input << "' should become '" << expectation << "'";
    }
}

TEST(String, JoinCharPtr) {
    const std::string del = " ";
    const char* parts[] = {"1", "2 3"};
    ASSERT_EQ(accerion::api::Join(parts, parts + 2, del), "1 2 3");
}

TEST(String, JoinString) {
    const std::string del = ", ";
    std::vector<int> ints;
    ASSERT_EQ(accerion::api::Join(ints.cbegin(), ints.cend(), del), "");

    ints = std::vector<int>{1, 2, 3, 4, 5};
    ASSERT_EQ(accerion::api::Join(ints.cbegin(), ints.cbegin(), del), "");
    ASSERT_EQ(accerion::api::Join(ints.cbegin(), ints.cbegin() + 2, del), "1, 2");
    ASSERT_EQ(accerion::api::Join(ints.cbegin(), ints.cend(), del), "1, 2, 3, 4, 5");
}

TEST(String, Split) {
    const std::vector<std::tuple<std::string, std::string, std::vector<std::string>>> tests = {
            {"", " ", {""}},
            {" ", " ", {"", ""}},
            {"123", " ", {"123"}},
            {"1 2 3", " ", {"1", "2", "3"}},
            {" 1 2 ", " ", {"", "1", "2", ""}},
            {"1  2  3", " ", {"1", "", "2", "", "3"}},
            {",,1,,2,,", ",", {"", "", "1", "", "2", "", ""}},
            {"  1  2  ", "  ", {"", "1", "2", ""}},
            {" , 1, 2 , 3 ,", ", ", {" ", "1", "2 ", "3 ,"}},
            {"....1...2..3.", "..", {"", "", "1", ".2", "3."}}
    };

    for (const auto& test: tests) {
        const auto& str = std::get<0>(test);
        const auto& delimiter = std::get<1>(test);
        const auto& expect = std::get<2>(test);

        std::stringstream ss;
        ss << "String '" << str << "' with delimiter '" << delimiter << "' should be parsed as '"
           << accerion::api::Join(expect.cbegin(), expect.cend(), "', '") << "'.";

        const auto actual = accerion::api::Split(str, delimiter);
        ASSERT_EQ(actual.size(), expect.size()) << ss.str();
        for (std::size_t i = 0; i < expect.size(); ++i) {
            ASSERT_EQ(actual[i], expect[i]) << ss.str();
        }
    }
}

TEST(String, DoesStringStartEndWith) {
    const std::vector<std::tuple<std::string, std::string, bool>> tests = {
            {"", " ", false},
            {" ", "", true},
            {" ", " ", true},
            {" ", "  ", false},
            {"123", "1", true},
            {"123", "2", false},
            {"123", "12", true},
            {"123", "11", false}
    };

    for (const auto& test: tests) {
        auto str = std::get<0>(test);
        auto search_str = std::get<1>(test);
        const auto& expect = std::get<2>(test);

        auto result = accerion::api::DoesStringStartWith(str, search_str);
        ASSERT_EQ(result, expect)
                << "String '" << str << "' should " << (expect ? "" : "not ") << "start with '" << search_str << "'";

        std::reverse(str.begin(), str.end());
        std::reverse(search_str.begin(), search_str.end());
        result = accerion::api::DoesStringEndWith(str, search_str);
        ASSERT_EQ(result, expect)
                << "String '" << str << "' should " << (expect ? "" : "not ") << "end with '" << search_str << "'";
    }
}

TEST(String, DoesStringContain) {
    const std::vector<std::tuple<std::string, std::string, bool>> tests = {
            {"", " ", false},
            {" ", "", true},
            {" ", " ", true},
            {" ", "  ", false},
            {"123", "1", true},
            {"123", "2", true},
            {"123", "3", true},
            {"123", "12", true},
            {"123", "123", true},
            {"123", "4", false}
    };

    for (const auto& test: tests) {
        const auto& str = std::get<0>(test);
        const auto& search_str = std::get<1>(test);
        const auto& expect = std::get<2>(test);

        const auto result = accerion::api::DoesStringContain(str, search_str);
        ASSERT_EQ(result, expect) << "String " << std::quoted(str) << " should " << (expect ? "" : "not ")
                                  << "contain " << std::quoted(search_str);
    }
}

TEST(String, StringToIntVector) {
    const std::string input_string("1-3, 44, 55, 999");
    const std::vector<int> expected_output = {1, 2, 3, 44, 55, 999};
    auto results = accerion::api::StringToIntVector<int>(input_string);
    ASSERT_EQ(results, expected_output);
}

TEST(String, ToString) {
    ASSERT_EQ(accerion::api::ToString(1.0,  0), "1");
    ASSERT_EQ(accerion::api::ToString(0.1,  1), "0.1");
    ASSERT_EQ(accerion::api::ToString(0.15, 1), "0.2");
    ASSERT_EQ(accerion::api::ToString(0.01, 1), "0.0");

    ASSERT_EQ(accerion::api::ToString(-1.0,  0), "-1");
    ASSERT_EQ(accerion::api::ToString(-0.15, 1), "-0.2");
}
