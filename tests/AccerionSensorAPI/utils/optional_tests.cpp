/* Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/utils/optional.h"


TEST(Optional, Equality) {
    // Tests both the equal and non-equal operators
    const accerion::api::Optional<int> empty{};
    const accerion::api::Optional<int> value_1 = 1;
    const accerion::api::Optional<int> value_2 = 2;

    ASSERT_EQ(empty, empty);
    ASSERT_EQ(value_1, value_1);
    ASSERT_EQ(value_2, value_2);

    ASSERT_NE(empty, value_1);
    ASSERT_NE(value_1, value_2);
    ASSERT_NE(value_2, empty);
}

TEST(Optional, AssignWithDefaultInitializer) {
    // Tests both the equal and non-equal operators
    accerion::api::Optional<int> optional{};
    optional = accerion::api::Optional<int>{};
    EXPECT_FALSE(optional);

    optional = accerion::api::Optional<int>{1};
    EXPECT_TRUE(optional);

    accerion::api::Optional<std::string> optional_str{};

    optional_str = "test";
    EXPECT_TRUE(optional_str);

    optional_str = std::string{};
    EXPECT_TRUE(optional_str);

    optional_str = accerion::api::Optional<std::string>{};
    EXPECT_FALSE(optional_str);
}