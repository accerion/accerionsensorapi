/* Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "gtest/gtest.h"

#include "AccerionSensorAPI/utils/filesystem.h"
#include "AccerionSensorAPI/utils/units.h"


TEST(FileSystem, GetFileSize) {
    const std::string test_file_path_ = "./dummy.amf";
    constexpr auto expected_file_size{5 * accerion::api::kMegaByte};

    const std::vector<char> kilobyte_zeros(accerion::api::kKiloByte, 0);
    std::ofstream ofs(test_file_path_, std::ios::binary | std::ios::out);
    for (auto i = 0ull; i < expected_file_size; i += kilobyte_zeros.size()) {
        if (!ofs.write(kilobyte_zeros.data(), accerion::api::kKiloByte)) {
            std::cout << "problem writing to file" << std::endl;
            throw std::runtime_error("Couldn't make dummy file");
        }
    }
    ofs.close();

    EXPECT_TRUE(accerion::api::FileExists(test_file_path_)) << "Dummy file does not exist";

    EXPECT_EQ(expected_file_size, accerion::api::GetFileSize(test_file_path_)) << "Calculated size does not match";

    accerion::api::RemoveFile(test_file_path_);
}
