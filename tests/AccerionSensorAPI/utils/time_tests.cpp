/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "gtest/gtest.h"

#include "AccerionSensorAPI/utils/time.h"

TEST(Time, MbPerSecond) {
    const uint64_t bytes = 2097152;
    const std::chrono::seconds duration(2);
    ASSERT_DOUBLE_EQ(1, accerion::api::MbPerSecond(duration, bytes));
}
