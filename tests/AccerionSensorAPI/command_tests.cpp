/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <chrono>
#include <limits>

#include "gtest/gtest.h"

#include "AccerionSensorAPI/structs/acknowledgement.h"
#include "AccerionSensorAPI/structs/area_file_header.h"
#include "AccerionSensorAPI/structs/areas.h"
#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/bool_opt_uint64.h"
#include "AccerionSensorAPI/structs/bool_uint16.h"
#include "AccerionSensorAPI/structs/bool_uint64.h"
#include "AccerionSensorAPI/structs/buffered_recovery.h"
#include "AccerionSensorAPI/structs/calibration_file_request.h"
#include "AccerionSensorAPI/structs/corrected_odometry.h"
#include "AccerionSensorAPI/structs/created_signature.h"
#include "AccerionSensorAPI/structs/date_time.h"
#include "AccerionSensorAPI/structs/diagnostics.h"
#include "AccerionSensorAPI/structs/double.h"
#include "AccerionSensorAPI/structs/drift_correction.h"
#include "AccerionSensorAPI/structs/external_reference_pose.h"
#include "AccerionSensorAPI/structs/file_transfer.h"
#include "AccerionSensorAPI/structs/file_transfer_task.h"
#include "AccerionSensorAPI/structs/heartbeat.h"
#include "AccerionSensorAPI/structs/ip_address.h"
#include "AccerionSensorAPI/structs/latest_external_reference.h"
#include "AccerionSensorAPI/structs/line_follower_data.h"
#include "AccerionSensorAPI/structs/loop_closure.h"
#include "AccerionSensorAPI/structs/map_summary.h"
#include "AccerionSensorAPI/structs/odometry.h"
#include "AccerionSensorAPI/structs/plain_uint16.h"
#include "AccerionSensorAPI/structs/plain_uint32.h"
#include "AccerionSensorAPI/structs/plain_uint64.h"
#include "AccerionSensorAPI/structs/ram_rom_stats.h"
#include "AccerionSensorAPI/structs/search_range.h"
#include "AccerionSensorAPI/structs/signature_vector.h"
#include "AccerionSensorAPI/structs/software_details.h"
#include "AccerionSensorAPI/structs/standard_deviation.h"
#include "AccerionSensorAPI/structs/string_vector.h"
#include "AccerionSensorAPI/structs/subset_operation.h"
#include "AccerionSensorAPI/structs/tcp_settings.h"
#include "AccerionSensorAPI/structs/udp_settings.h"
#include "AccerionSensorAPI/structs/uint16_vector.h"
#include "AccerionSensorAPI/structs/uint64_vector.h"
#include "AccerionSensorAPI/structs/uint8_vector.h"

namespace detail {

template<typename TypeFrom, typename TypeTo = TypeFrom>
constexpr auto kMin = static_cast<TypeTo>(std::numeric_limits<TypeFrom>::min());
template<typename TypeFrom, typename TypeTo = TypeFrom>
constexpr auto kMax = static_cast<TypeTo>(std::numeric_limits<TypeFrom>::max());

template<typename TypeFrom, typename TypeTo>
constexpr auto kMinCenti = kMin<TypeFrom, TypeTo> / TypeTo{1e2};
template<typename TypeFrom, typename TypeTo>
constexpr auto kMaxCenti = kMax<TypeFrom, TypeTo> / TypeTo{1e2};

template<typename TypeFrom, typename TypeTo>
constexpr auto kMinMicro = kMin<TypeFrom, TypeTo> / TypeTo{1e6};
template<typename TypeFrom, typename TypeTo>
constexpr auto kMaxMicro = kMax<TypeFrom, TypeTo> / TypeTo{1e6};

constexpr double kMinPoseAngle = -180;
constexpr double kMaxPoseAngle = 179.99;

// The min and max values are offset to prevent overflow issues with (de)serialization
const accerion::api::Pose kMinPose{kMinMicro<std::int64_t, double> + 1,
                                   kMinMicro<std::int64_t, double> + 1,
                                   kMinPoseAngle};
const accerion::api::Pose kMaxPose{kMaxMicro<std::int64_t, double> - 1,
                                   kMaxMicro<std::int64_t, double> - 1,
                                   kMaxPoseAngle};

accerion::api::TimePoint CreateTime(
        std::uint16_t year, std::uint8_t month, std::uint8_t day,
        std::uint8_t hours = 0, std::uint8_t minutes = 0, std::uint8_t seconds = 0) {
    std::tm tm = {
        /* .tm_sec  = */ seconds,
        /* .tm_min  = */ minutes,
        /* .tm_hour = */ hours,
        /* .tm_mday = */ day,
        /* .tm_mon  = */ month - 1,
        /* .tm_year = */ year - 1900,
    };
    tm.tm_isdst = -1; // Use DST value from local time zone
    return std::chrono::system_clock::from_time_t(std::mktime(&tm));
}

// Not really minimum and maximum time, but should be sufficient
const accerion::api::TimePoint kMinTimePoint{CreateTime(2000, 1, 1)};
const accerion::api::TimePoint kMaxTimePoint{CreateTime(2100, 12, 31, 23, 59, 59)};

}  // namespace detail


TEST(CommandTests, Heartbeat) {
    using namespace accerion::api;
    // Setting the in-values
    const HeartBeat hb_in{{192,168,1,117}, {192,168,1,117}, {5,5,0}};

    // Serialize
    const auto serialized_data = hb_in.Serialize();

    // Deserialize
    const HeartBeat hb_out = HeartBeat::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(hb_in.ip_address, hb_out.ip_address);
    ASSERT_EQ(hb_in.unicast_address, hb_out.unicast_address);
    ASSERT_EQ(hb_in.software_version, hb_out.software_version);
}

void AcknowledgementTest(bool value, std::string message) {
    using namespace accerion::api;
    const Acknowledgement ack_in{value, std::move(message)};
    const auto serialized_data = ack_in.Serialize();
    const auto ack_out = Acknowledgement::Deserialize(serialized_data);
    ASSERT_EQ(ack_in.accepted, ack_out.accepted);
    ASSERT_EQ(ack_in.message, ack_out.message);
}

TEST(CommandTests, Acknowledgement) {
    AcknowledgementTest(true, "");
    AcknowledgementTest(false, "test");
}

TEST(CommandTests, Boolean) {
    for (auto input_value: {false, true}) {
        const accerion::api::Boolean input{input_value};
        const auto data = input.Serialize();
        const auto output = accerion::api::Boolean::Deserialize(data);
        ASSERT_EQ(input.value, output.value);
    }
}

TEST(CommandTests, SearchRadius) {
    using namespace accerion::api;
    const SearchRadius radius_value{1.141516};
    const auto serialized_data = radius_value.Serialize();
    const auto radius_value_out = SearchRadius::Deserialize(serialized_data);
    ASSERT_EQ(radius_value.value, radius_value_out.value);
}

TEST(CommandTests, Pose) {
    using namespace accerion::api;
    const std::vector<accerion::api::Pose> inputs {
        {0.0, 0.0, 0.0},
        ::detail::kMinPose,
        ::detail::kMaxPose,
        {::detail::kMinMicro<std::int64_t, double> + 1, 0.0, ::detail::kMinPoseAngle},
        {0.0, ::detail::kMinMicro<std::int64_t, double> + 1, 1.0},
        {::detail::kMaxMicro<std::int64_t, double> - 1, 33.5523, ::detail::kMaxPoseAngle},
        {76.823, ::detail::kMaxMicro<std::int64_t, double> - 1, -80.12},
        {-10.0, -20.0, -30.0},
    };

    for (const auto& input: inputs) {
        const auto data = input.Serialize();
        const auto output = accerion::api::Pose::Deserialize(data);
        ASSERT_TRUE(input.IsApprox(output, 1e-6, 1e-2)) << input << " vs. " << output;
    }
}

TEST(CommandTests, StdDev) {
    using namespace accerion::api;

    const auto test = [](double x, double y, double heading) {
        const StandardDeviation input{x, y, heading};
        const auto data = input.Serialize();
        const auto output = StandardDeviation::Deserialize(data);

        // Compare outputs to inputs
        ASSERT_NEAR(input.x, output.x, 1e-6);
        ASSERT_NEAR(input.y, output.y, 1e-6);
        ASSERT_NEAR(input.th, output.th, 1e-2);
    };

    test(::detail::kMinMicro<std::uint32_t, double>, 10.0, ::detail::kMinCenti<std::uint32_t, double>);
    test(::detail::kMaxMicro<std::uint32_t, double> / 2, 10.0, ::detail::kMinCenti<std::uint32_t, double> + 0.01);
    test(::detail::kMaxMicro<std::uint32_t, double>, 10.0, 0);
    test(0.0, 0.0, 0.0);
    test(10.0, ::detail::kMinMicro<std::uint32_t, double>, 0.01);
    test(10.0, ::detail::kMaxMicro<std::uint32_t, double> / 2, ::detail::kMaxCenti<std::uint32_t, double> - 0.01);
    test(10.0, ::detail::kMaxMicro<std::uint32_t, double>, ::detail::kMaxCenti<std::uint32_t, double>);
}

TEST(CommandTests, Odometry) {
    using namespace accerion::api;

    const auto test = [](const Odometry& input) {
        const auto data = input.Serialize();
        const auto output = Odometry::Deserialize(data);

        ASSERT_EQ(input.timestamp, output.timestamp);
        ASSERT_EQ(input.pose, output.pose);
        ASSERT_EQ(input.velocity, output.velocity);
        ASSERT_EQ(input.standard_deviation_velocity, output.standard_deviation_velocity);
    };

    // Maximum/positive values
    Odometry input{::detail::kMaxTimePoint,
                   Pose{10.0, ::detail::kMaxMicro<std::int64_t, double>  - 1, 30.0},
                   Velocity{::detail::kMaxMicro<std::int32_t, double>, 2147.48, ::detail::kMaxPoseAngle},
                   StandardDeviation{.22, ::detail::kMaxMicro<std::uint32_t, double>, 33.44}};
    test(input);

    // Minimum/negative values
    input = {::detail::kMinTimePoint,
             Pose{-10.0, ::detail::kMinMicro<std::int64_t, double> + 1, -30.0},
             Velocity{::detail::kMaxMicro<std::int32_t, double>, -2147.48, ::detail::kMinPoseAngle},
             StandardDeviation{.22, ::detail::kMinMicro<std::uint32_t, double>, 33.44}};
    test(input);
}

TEST(CommandTests, CorrectedOdometry) {
    using namespace accerion::api;

    const auto test = [](const CorrectedOdometry& input) {
        const auto data = input.Serialize();
        const auto output = CorrectedOdometry::Deserialize(data);

        ASSERT_EQ(input.timestamp, output.timestamp);
        ASSERT_TRUE(input.pose.IsApprox(output.pose));
        ASSERT_EQ(input.standard_deviation, output.standard_deviation);
    };

    // Maximum/positive values
    CorrectedOdometry input{::detail::kMaxTimePoint,
                            Pose{::detail::kMaxMicro<std::int64_t, double> - 1, 20.0, 30.0},
                            StandardDeviation{::detail::kMaxMicro<std::uint32_t, double>, 22.33, 33.44}};
    test(input);

    // Minimum/negative values
    input = {::detail::kMinTimePoint,
             Pose{::detail::kMinMicro<std::int64_t, double> + 1, -20.0, -30.0},
             StandardDeviation{::detail::kMinMicro<std::uint32_t, double>, 22.33, 33.44}};
    test(input);
}

TEST(CommandTests, DriftCorrection) {
    using namespace accerion::api;

    const auto test = [](const auto& input) {
        const auto data = input.Serialize();
        const auto output = DriftCorrection::Deserialize(data);

        ASSERT_EQ(input.timestamp, output.timestamp);
        ASSERT_EQ(input.pose, output.pose);
        ASSERT_EQ(input.delta, output.delta);
        ASSERT_EQ(input.cumulative_travelled_distance, output.cumulative_travelled_distance);
        ASSERT_EQ(input.cumulative_travelled_heading, output.cumulative_travelled_heading);
        ASSERT_EQ(input.error_percentage, output.error_percentage);
        ASSERT_EQ(input.cluster_id, output.cluster_id);
        ASSERT_EQ(input.quality_estimate, output.quality_estimate);
        ASSERT_EQ(input.standard_deviation, output.standard_deviation);
    };

    // Maximum/positive values
    DriftCorrection input{::detail::kMaxTimePoint,
                          Pose{::detail::kMaxMicro<std::int64_t, double> - 1, 20.0, 30.0},
                          11.22, 22.33, 33.44,
                          44.55, 55.66,
                          66, 10000, 999, 78,
                          StandardDeviation{0.02, ::detail::kMaxMicro<std::uint32_t, double>, 10.01}};
    test(input);

    // Minimum/negative values
    input = {::detail::kMinTimePoint,
             Pose{10.0, ::detail::kMinMicro<std::int64_t, double> + 1, 30.0},
             11.22, 22.33, 33.44,
             44.55, 55.66,
             66, 10000, 999, 78,
             StandardDeviation{::detail::kMinMicro<std::uint32_t, double>, 0.02, 10.01}};
    test(input);
}

TEST(CommandTests, DriftCorrectionNegativeValues) {
    using namespace accerion::api;
    // Setting the in-values
    const DriftCorrection driftcorrection_in{
        ::detail::kMaxTimePoint,
        -10.0, -20, -30.0,
        -11.22, -22, -33.44,
        44.55, 55.66,
        66, 10000, 999, 78,
        {0.001, 0.02, 10.01}};

    // Serialize
    const auto serialized_data = driftcorrection_in.Serialize();

    // Deserialize
    const DriftCorrection driftcorrection_out = DriftCorrection::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(driftcorrection_in.timestamp, driftcorrection_out.timestamp);
    ASSERT_EQ(driftcorrection_in.pose, driftcorrection_out.pose);
    ASSERT_EQ(driftcorrection_in.delta, driftcorrection_out.delta);
    ASSERT_EQ(driftcorrection_in.cumulative_travelled_distance, driftcorrection_out.cumulative_travelled_distance);
    ASSERT_EQ(driftcorrection_in.cumulative_travelled_heading, driftcorrection_out.cumulative_travelled_heading);
    ASSERT_EQ(driftcorrection_in.error_percentage, driftcorrection_out.error_percentage);
    ASSERT_EQ(driftcorrection_in.cluster_id, driftcorrection_out.cluster_id);
    ASSERT_EQ(driftcorrection_in.quality_estimate, driftcorrection_out.quality_estimate);
    ASSERT_EQ(driftcorrection_in.standard_deviation, driftcorrection_out.standard_deviation);
}

TEST(CommandTests, SoftwareDetails) {
    using namespace accerion::api;
    // Setting the in-values
    std::string random_generated_string = "5Obqq8YOTAEkL2ACWtGhCcY52SUuBTfPZdNVrNiE";
    std::string a_date = "Jul 21 2021";

    const SoftwareDetails sd_in{random_generated_string, a_date};

    // Serialize
    const auto serialized_data = sd_in.Serialize();

    // Deserialize
    const SoftwareDetails sd_out = SoftwareDetails::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(sd_out.software_hash.length(), random_generated_string.length());
    ASSERT_EQ(sd_out.software_hash, random_generated_string);
    ASSERT_EQ(sd_out.date.length(), a_date.length());
    ASSERT_EQ(sd_out.date, a_date);
}

TEST(CommandTests, Diagnostics) {
    using namespace accerion::api;
    // Setting the in-values
    const Diagnostics diag_in{accerion::api::TimePoint(accerion::api::TimePoint::duration{123456}), 1234, 1234, 12345};

    // Serialize
    const auto serialized_data = diag_in.Serialize();

    // Deserialize
    const Diagnostics diag_out = Diagnostics::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(diag_in.timestamp, diag_out.timestamp);
    ASSERT_EQ(diag_in.modes, diag_out.modes);
    ASSERT_EQ(diag_in.warning_codes, diag_out.warning_codes);
    ASSERT_EQ(diag_in.error_codes, diag_out.error_codes);
}

TEST(CommandTests, Address) {
    using namespace accerion::api;
    // Setting the in-values
    const Address address_in{192,168,0,123};

    // Serialize
    const auto serialized_data = address_in.Serialize();

    // Deserialize
    const Address address_out = Address::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(address_in.first, address_out.first);
    ASSERT_EQ(address_in.second, address_out.second);
    ASSERT_EQ(address_in.third, address_out.third);
    ASSERT_EQ(address_in.fourth, address_out.fourth);

    ASSERT_EQ(address_in, address_out);
}

TEST(CommandTests, IpAddressExtended) {
    using namespace accerion::api;
    // Setting the in-values
    const IpAddress ad{192,168,0,123,
                       255, 254, 253, 0,
                       193, 169, 2, 1};

    // Serialize
    const auto serialized_data = ad.Serialize();

    // Deserialize
    const auto address_out = IpAddress::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(ad, address_out);
}

TEST(CommandTests, IpAddress) {
    using namespace accerion::api;
    // Setting the in-values
    const IpAddress ad{192,168,0,123,
                        255, 254, 253, 0,
                        193, 169, 2, 1};

    // Serialize
    const auto serialized_data = ad.Serialize();

    // Deserialize
    const auto address_out = IpAddress::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(ad, address_out);
}

TEST(CommandTests, DateTime) {
    using namespace accerion::api;
    // Setting the in-values
    const DateTime dt{2, 1, 1995, 12, 15, 55};

    // Serialize
    const auto serialized_data = dt.Serialize();

    // Deserialize
    const auto dt_out = DateTime::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(dt.day, dt_out.day);
    ASSERT_EQ(dt.month, dt_out.month);
    ASSERT_EQ(dt.year, dt_out.year);
    ASSERT_EQ(dt.hours, dt_out.hours);
    ASSERT_EQ(dt.minutes, dt_out.minutes);
    ASSERT_EQ(dt.seconds, dt_out.seconds);
}

TEST(CommandTests, PlainUInt16) {
    using namespace accerion::api;
    // Setting the in-values
    const PlainUInt16 pu{1234};

    // Serialize
    const auto serialized_data = pu.Serialize();

    // Deserialize
    const auto pu_out = PlainUInt16::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(pu.value, pu_out.value);
}

TEST(CommandTests, PlainUInt32) {
    using namespace accerion::api;
    // Setting the in-values
    const PlainUInt32 pu{1234567};

    // Serialize
    const auto serialized_data = pu.Serialize();

    // Deserialize
    const auto pu_out = PlainUInt32::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(pu.value, pu_out.value);
}

TEST(CommandTests, PlainUInt64) {
    for (const auto value : {static_cast<std::uint64_t>(1234567),
                             ::detail::kMin<std::uint64_t>,
                             ::detail::kMax<std::uint64_t> / 2,
                             ::detail::kMax<std::uint64_t>}) {

        const accerion::api::PlainUInt64 data_in {value};
        const auto data_serialized = data_in.Serialize();
        const auto data_out = accerion::api::PlainUInt64::Deserialize(data_serialized);
        ASSERT_EQ(data_in.value, data_out.value);
    }
}


TEST(CommandTests, PlainString) {
    using namespace accerion::api;
    // Setting the in-values
    const PlainString ps{"The Quick Brown Fox Jumped Over The Lazy Dog"};

    // Serialize
    const auto serialized_data = ps.Serialize();

    // Deserialize
    const PlainString ps_out = PlainString::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(ps.value.length(), ps_out.value.length());
    ASSERT_EQ(ps.value, ps_out.value);
}

TEST(CommandTests, StringVector) {
    using namespace accerion::api;
    // Setting the in-values
    const std::string string1{"The Quick Brown Fox Jumped Over The Lazy Dog"};
    const std::string string2{"This is a test string!"};
    const StringVector vec {{string1, string2}};

    // Serialize
    const auto serialized_data = vec.Serialize();

    // Deserialize
    const StringVector output_vec = StringVector::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(vec.value.size(), output_vec.value.size());
    ASSERT_TRUE(std::equal(vec.value.begin(), vec.value.end(), output_vec.value.begin()));
}

TEST(CommandTests, SoftwareVersion) {
    using namespace accerion::api;
    // Setting the in-values
    SoftwareVersion sv{12, 34, 56};

    // Serialize
    const auto serialized_data = sv.Serialize();

    // Deserialize
    const SoftwareVersion sv_out = SoftwareVersion::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(sv, sv_out);
}

TEST(CommandTests, Signature) {
    const std::vector<accerion::api::Signature> inputs = {
        {::detail::kMinMicro<std::int64_t, double> + 1, ::detail::kMinMicro<std::int64_t, double> + 1, ::detail::kMinPoseAngle, 999, 888},
        {-0.0123, -20.34, 100.77, ::detail::kMin<accerion::api::ClusterId>, ::detail::kMin<accerion::api::SignatureId>},
        {-10.23, 0.0234, 0.08, ::detail::kMax<accerion::api::ClusterId>, ::detail::kMax<accerion::api::SignatureId>},
        {::detail::kMaxMicro<std::int64_t, double> - 1, ::detail::kMaxMicro<std::int64_t, double> - 1, ::detail::kMaxPoseAngle, 999, 888}
    };

    for (const auto& input : inputs) {
        const auto data = input.Serialize();
        const auto output = accerion::api::Signature::Deserialize(data);
        ASSERT_EQ(input, output);
    }
}

TEST(CommandTests, SignaturePositionPacket) {
    using namespace accerion::api;

    const SignatureVector input{{
        {Pose{1.23, 2.34, 7.88}, 777, 555},
        {Pose{2.23, 3.34, 8.88}, 888, 666},
        {Pose{3.23, 4.34, 9.88}, 999, 777}
    }};

    const auto serialized_data = input.Serialize();
    const auto output = SignatureVector::Deserialize(serialized_data);

    ASSERT_EQ(input.value.size(), output.value.size());
    ASSERT_EQ(input.value, output.value);
    ASSERT_TRUE(std::equal(input.value.cbegin(), input.value.cend(), output.value.cbegin()));
}

TEST(CommandTests, TcpSettings) {
    using namespace accerion::api;
    // Setting the in-values
    TcpSettings settings{EnabledMessageType::kBoth};

    // Serialize
    const auto serialized_data = settings.Serialize();

    // Deserialize
    const auto settings_out = TcpSettings::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(settings.message_type, settings_out.message_type);
}

TEST(CommandTests, LineFollowerData) {
    using namespace accerion::api;

    const auto test = [](const auto& input) {
        const auto data = input.Serialize();
        const auto output = LineFollowerData::Deserialize(data);

        ASSERT_EQ(input.timestamp, output.timestamp);
        ASSERT_EQ(input.pose, output.pose);
        ASSERT_EQ(input.closest_point, output.closest_point);
        ASSERT_EQ(input.cluster_id, output.cluster_id);
    };

    // Maximum/positive values
    LineFollowerData input{::detail::kMaxTimePoint,
                           Pose{::detail::kMaxMicro<std::int64_t, double> - 1, 10.10, 12.12},
                           13.13, ::detail::kMaxMicro<std::int32_t, double>, 15.15,
                           ::detail::kMax<std::uint16_t>};
    test(input);

    // Minimum/negative values
    input = {::detail::kMinTimePoint,
             Pose{-10.10, ::detail::kMinMicro<std::int64_t, double> + 1, -12.12},
             -13.13, ::detail::kMinMicro<std::int32_t, double>, -15.15,
             ::detail::kMin<std::uint16_t>};
    test(input);
}

TEST(CommandTests, LoopClosure) {
    using namespace accerion::api;

    LoopClosure lc{123, 9999, {1.234, 5.678, 9.19}};

    // Serialize
    const auto serialized_data = lc.Serialize();

    // Deserialize
    const auto lc_out = LoopClosure::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(lc.id_a, lc_out.id_a);
    ASSERT_EQ(lc.id_b, lc_out.id_b);
    ASSERT_EQ(lc.pose, lc_out.pose);
}

TEST(CommandTests, UdpSettings) {
    using namespace accerion::api;
    // Setting the in-values
    UdpSettings settings{Address{192, 168, 0, 117}, EnabledMessageType::kBoth, UdpStrategy::kBroadcast};

    // Serialize
    const auto serialized_data = settings.Serialize();

    // Deserialize
    const auto settings_out = UdpSettings::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(settings, settings_out);
}

TEST(CommandTests, UInt64Vector) {
    accerion::api::UInt64Vector data_in;

    data_in.values.push_back(::detail::kMin<std::uint64_t>);
    data_in.values.push_back(123);
    data_in.values.push_back(234);
    data_in.values.push_back(::detail::kMax<std::uint64_t> / 2);
    data_in.values.push_back(::detail::kMax<std::uint64_t>);

    const auto data_serialized = data_in.Serialize();

    const auto data_out = accerion::api::UInt64Vector::Deserialize(data_serialized);

    ASSERT_EQ(data_in.values.size(), data_out.values.size());
    ASSERT_EQ(data_in.values, data_out.values);
}

TEST(CommandTests, UInt16Vector) {
    using namespace accerion::api;
    // Setting the in-values
    UInt16Vector uv;
    uv.value.push_back(::detail::kMin<std::uint16_t>);
    uv.value.push_back(123);
    uv.value.push_back(234);
    uv.value.push_back(::detail::kMax<std::uint16_t> / 2);
    uv.value.push_back(::detail::kMax<std::uint16_t>);

    // Serialize
    const auto serialized_data = uv.Serialize();

    // Deserialize
    const UInt16Vector uv_out = UInt16Vector::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(uv.value.size(), uv_out.value.size());
    ASSERT_TRUE(std::equal(uv.value.begin(), uv.value.end(), uv_out.value.begin()));
}

TEST(CommandTests, UInt8Vector) {
    using namespace accerion::api;
    // Setting the in-values
    UInt8Vector uv;
    uv.value.push_back(123);
    uv.value.push_back(234);
    uv.value.push_back(254);

    // Serialize
    const auto serialized_data = uv.Serialize();

    // Deserialize
    const UInt8Vector uv_out = UInt8Vector::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(uv.value.size(), uv_out.value.size());
    ASSERT_TRUE(std::equal(uv.value.begin(), uv.value.end(), uv_out.value.begin()));
}

void testSubsetResultCommand(const accerion::api::SubsetOperationResult& sr) {
    using namespace accerion::api;
    // Serialize
    const auto serialized_data = sr.Serialize();
    // Deserialize
    const auto sr_out = SubsetOperationResult::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(sr.failed_ids.size(), sr_out.failed_ids.size());
    ASSERT_EQ(sr.failed_ids, sr_out.failed_ids);
    ASSERT_EQ(sr.message_type, sr_out.message_type);
    ASSERT_EQ(sr.value, sr_out.value);
}

TEST(CommandTests, SubsetOperationResult) {
    using namespace accerion::api;
    // Setting the in-values
    SubsetOperationResult sr;
    sr.message_type = SubsetResultMessageType::kProgress;
    sr.value = 99;

    testSubsetResultCommand(sr);

    sr.message_type = SubsetResultMessageType::kComplete;
    sr.value = 0;
    sr.failed_ids.push_back(0);
    sr.failed_ids.push_back(1);
    sr.failed_ids.push_back(2);

    testSubsetResultCommand(sr);
}

TEST(CommandTests, SubsetOperationRequest) {
    using namespace accerion::api;
    // Setting the in-values
    SubsetOperationRequest request;
    request.target_area_id = 999;
    request.cluster_ids.push_back(0);
    request.cluster_ids.push_back(1);
    request.cluster_ids.push_back(2);

    // Serialize
    const auto serialized_data = request.Serialize();

    // Deserialize
    const SubsetOperationRequest sor_out = SubsetOperationRequest::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(request.target_area_id, sor_out.target_area_id);
    ASSERT_EQ(request.cluster_ids.size(), sor_out.cluster_ids.size());
    ASSERT_EQ(request.cluster_ids, sor_out.cluster_ids);
}

TEST(CommandTests, BufferLength) {
    using namespace accerion::api;
    // Setting the in-values
    BufferLength sblr{};
    sblr.buffer_length = ::detail::kMaxMicro<std::uint32_t, float>;

    // Serialize
    const auto serialized_data = sblr.Serialize();

    // Deserialize
    const auto sblr_out = BufferLength::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(sblr.buffer_length, sblr_out.buffer_length);
}

TEST(CommandTests, BufferProgress) {
    using namespace accerion::api;
    // Setting the in-values
    BufferProgress bp{BufferedRecoveryMessageType::kLowOverlap, true, 95, 20};

    // Serialize
    const auto serialized_data = bp.Serialize();

    // Deserialize
    const BufferProgress bp_out = BufferProgress::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(bp.message_type , bp_out.message_type);
    ASSERT_EQ(bp_out.message_type, BufferedRecoveryMessageType::kLowOverlap);
    ASSERT_EQ(bp.result , bp_out.result);
    ASSERT_EQ(bp.progress , bp_out.progress);
    ASSERT_EQ(bp.percentage_of_low_overlap , bp_out.percentage_of_low_overlap);
}

TEST(CommandTests, BufferedRecoveryRequest) {
    using namespace accerion::api;

    const auto test = [](const BufferedRecoveryRequest& input) {
        const auto data = input.Serialize();
        const auto output = BufferedRecoveryRequest::Deserialize(data);

        ASSERT_EQ(input.x_pos , output.x_pos);
        ASSERT_EQ(input.y_pos , output.y_pos);
        ASSERT_EQ(input.radius , output.radius);
        ASSERT_EQ(input.full_map_search , output.full_map_search);
        };

    // Maximum/positive values
    BufferedRecoveryRequest input{::detail::kMaxMicro<std::int64_t, double> - 1,
                                  ::detail::kMaxMicro<std::int64_t, double> - 1,
                                  ::detail::kMaxMicro<std::uint32_t, double>, true};
    test(input);

    // Minimum/negative values
    input = {::detail::kMinMicro<std::int64_t, double> + 1,
             ::detail::kMinMicro<std::int64_t, double> + 1,
             ::detail::kMinMicro<std::uint32_t, double>, false};
    test(input);
}

TEST(CommandTests, DirSizePart) {
    using namespace accerion::api;
    // Setting the in-values
    DirSizePart dsp{DirType::kRecording, 2300, "test"};

    // Serialize
    const auto serialized_data = dsp.Serialize();

    // Deserialize
    const DirSizePart dsp_out = DirSizePart::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(dsp.type , dsp_out.type);
    ASSERT_EQ(dsp_out.type, DirType::kRecording);
    ASSERT_EQ(dsp.size_in_mb , dsp_out.size_in_mb);
    ASSERT_EQ(dsp.name , dsp_out.name);
}

TEST(CommandTests, RamRomStats) {
    using namespace accerion::api;
    // Setting the in-values
    DirSizePart dsp1{DirType::kRecording, 2300, "test"};
    DirSizePart dsp2{DirType::kLogs, 9999, "hello"};
    DirSizePart dsp3{DirType::kSdOther, 8888, "Some long name"};

    RamRomStats rrs;
    rrs.rom_available = 1234;
    rrs.rom_total = 2345;
    rrs.sd_available = 3456;
    rrs.sd_total = 4567;
    rrs.ram_used = 5678;
    rrs.ram_total = 6789;
    rrs.parts.push_back(dsp1);
    rrs.parts.push_back(dsp2);
    rrs.parts.push_back(dsp3);

    // Serialize
    const auto serialized_data = rrs.Serialize();

    // Deserialize
    const RamRomStats rrs_out = RamRomStats::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(rrs.rom_available, rrs_out.rom_available);
    ASSERT_EQ(rrs.rom_total, rrs_out.rom_total);
    ASSERT_EQ(rrs.sd_available, rrs_out.sd_available);
    ASSERT_EQ(rrs.sd_total, rrs_out.sd_total);
    ASSERT_EQ(rrs.ram_used, rrs_out.ram_used);
    ASSERT_EQ(rrs.ram_total, rrs_out.ram_total);
    ASSERT_EQ(rrs.parts.size(), rrs_out.parts.size());

    for (int i = 0; i < rrs.parts.size(); i++) {
        ASSERT_EQ(rrs.parts[i].type, rrs_out.parts[i].type) << "part type does not match for index " << i;
        ASSERT_EQ(rrs.parts[i].size_in_mb, rrs_out.parts[i].size_in_mb) << "part size_in_mb does not match for index " << i;
        ASSERT_EQ(rrs.parts[i].name, rrs_out.parts[i].name) << "part name does not match for index " << i;
    }
}

TEST(CommandTests, BoolUInt64) {
    accerion::api::BoolUInt64 data_input {true, std::numeric_limits<std::uint64_t>::max()};

    const auto data_serialized = data_input.Serialize();

    // Deserialize
    const auto data_output = accerion::api::BoolUInt64::Deserialize(data_serialized);

    // Compare outputs to inputs
    ASSERT_EQ(data_input.bool_value, data_output.bool_value);
    ASSERT_EQ(data_input.uint64_value, data_output.uint64_value);
}

TEST(CommandTests, BoolOptUInt64) {
    // Only when boolean is true, then the integer value should be set
    const std::vector<accerion::api::BoolOptUInt64> inputs{
        {true, std::numeric_limits<std::uint64_t>::min()},
        {true, std::numeric_limits<std::uint64_t>::max()},
        {false, {}},
        {false, std::numeric_limits<std::uint64_t>::min()},
        {false, std::numeric_limits<std::uint64_t>::max()}
    };


    for (const auto& input: inputs) {
        const auto data = input.Serialize();
        const auto output = accerion::api::BoolOptUInt64::Deserialize(data);

        ASSERT_EQ(input.bool_value, output.bool_value);
        if (input.bool_value) {
            ASSERT_TRUE(output.uint64_value);
            ASSERT_EQ(input.uint64_value.value(), output.uint64_value.value());
        } else {
            ASSERT_FALSE(output.uint64_value);
        }
    }
}

TEST(CommandTests, BoolUInt16) {
    using namespace accerion::api;
    // Setting the in-values
    BoolUInt16 bu{true, 12345};

    // Serialize
    const auto serialized_data = bu.Serialize();

    // Deserialize
    const auto bu_out = BoolUInt16::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(bu.bool_value, bu_out.bool_value);
    ASSERT_EQ(bu.uint16_value, bu_out.uint16_value);
}

TEST(CommandTests, ExternalReferencePose) {
    const std::vector<accerion::api::ExternalReferencePose> inputs{
            {accerion::api::Pose::Identity(), std::chrono::microseconds{3}, {}},
            {accerion::api::Pose{11.11, 22.22, 33.33}, std::chrono::microseconds{12345678}, {}},
            {::detail::kMinPose, std::chrono::microseconds::min(), {}},
            {::detail::kMaxPose, std::chrono::microseconds::max(), {}}
    };

    for (const auto& input: inputs) {
        const auto data = input.Serialize();
        const auto output = accerion::api::ExternalReferencePose::Deserialize(data);

        // Compare outputs to inputs
        ASSERT_EQ(input.pose, output.pose);
        if (input.time_offset) {
            ASSERT_TRUE(output.time_offset);
            ASSERT_EQ(input.time_offset.value(), output.time_offset.value());
        }
        if (input.timestamp) {
            ASSERT_TRUE(output.timestamp);
            ASSERT_EQ(input.timestamp.value(), output.timestamp.value());
        }
    }
}

TEST(CommandTests, LatestExternalReference) {
    const accerion::api::LatestExternalReference input_data{
            accerion::api::TimePoint(accerion::api::TimePoint::duration{12345}), {1., 2., 3.}, {4., 5.}};

    const auto serialized_data = input_data.Serialize();

    const auto output_data = accerion::api::LatestExternalReference::Deserialize(serialized_data);

    ASSERT_EQ(input_data.timestamp, output_data.timestamp);
    ASSERT_EQ(input_data.pose, output_data.pose);
    ASSERT_EQ(input_data.search_range.radius, output_data.search_range.radius);
    ASSERT_EQ(input_data.search_range.angle, output_data.search_range.angle);
}

TEST(CommandTests, SearchRange) {
    const std::vector<accerion::api::SearchRange> inputs{
        {1., 2.},
        {::detail::kMinMicro<uint32_t, double>, ::detail::kMinCenti<uint32_t, double>},
        {::detail::kMaxMicro<uint32_t, double>, ::detail::kMaxCenti<uint32_t, double>}
    };
    for (const auto input: inputs) {
        const auto data = input.Serialize();
        const auto output = accerion::api::SearchRange::Deserialize(data);

        ASSERT_EQ(input.radius, output.radius);
        ASSERT_EQ(input.angle, output.angle);
    }
}

TEST(CommandTests, CalibrationFileRequest) {
    using namespace accerion::api;
    // Setting the in-values
    CalibrationFileRequest cfr;
    cfr.key = "1234567898765432";
    for(int i = 0; i < (std::rand() % 200 + 100); i++) {
        cfr.file.push_back((std::rand() % 254 + 1));
    }

    // Serialize
    const auto serialized_data = cfr.Serialize();

    // Deserialize
    const CalibrationFileRequest cfr_out = CalibrationFileRequest::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(cfr.key, cfr_out.key);
    ASSERT_EQ(cfr.file.size(), cfr_out.file.size());
    for(int i = 0; i < cfr.file.size(); i++) {
        ASSERT_EQ(cfr.file[i], cfr_out.file[i])   << "file_[i] do not match at " << i;
    }
}

TEST(CommandTests, FileTransferTaskResult) {
    using namespace accerion::api;

    const std::vector<FileTransferTaskResult> inputs {
            {FileTransferTask::kGatherLogs, FileTransferTaskStatus::kTaskFail, 0, "blah 'blah' \"blah\""},
            {FileTransferTask::kMergeMap, FileTransferTaskStatus::kTaskProgress, 50, "blah"},
            {FileTransferTask::kUpdateSensor, FileTransferTaskStatus::kTaskSuccess, 100, ""}
    };

    for (const auto& input: inputs) {
        const auto serialized_data = input.Serialize();
        const auto output = FileTransferTaskResult::Deserialize(serialized_data);
        ASSERT_EQ(input.task, output.task);
        ASSERT_EQ(input.status, output.status);
        ASSERT_EQ(input.progress, output.progress);
        ASSERT_EQ(input.message, output.message);
    }
}

TEST(CommandTests, FileUploadDownloadRequest) {
    using namespace accerion::api;
    // Setting the in-values
    FileUploadDownloadRequest req{};

    req.transfer_type = TransferType::kUploadLegacyAmf;
    req.file_size = 123456789;
    req.chunk_size = 987654321;

    // Serialize
    const auto serialized_data = req.Serialize();

    // Deserialize
    const FileUploadDownloadRequest req_out = FileUploadDownloadRequest::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(req.transfer_type, req_out.transfer_type);
    ASSERT_EQ(req.file_size, req_out.file_size);
    ASSERT_EQ(req.chunk_size, req_out.chunk_size);
}

TEST(CommandTests, FileTransferHandshake) {
    using namespace accerion::api;
    // Setting the in-values
    FileTransferHandshake hs{};
    hs.transfer_status = TransferStatus::kTransferFinished;
    hs.transfer_type = TransferType::kUploadLegacyAmf;
    hs.port_number = std::numeric_limits<std::uint16_t>::max();
    hs.file_size = 123456789;

    // Serialize
    const auto serialized_data = hs.Serialize();

    // Deserialize
    const FileTransferHandshake hs_out = FileTransferHandshake::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(hs.transfer_status, hs_out.transfer_status);
    ASSERT_EQ(hs.transfer_type, hs_out.transfer_type);
    ASSERT_EQ(hs.port_number, hs_out.port_number);
    ASSERT_EQ(hs.file_size, hs_out.file_size);
}

TEST(CommandTests, MapSummary) {
    using namespace accerion::api;
    const std::size_t cluster_amount = 10;
    const std::size_t total_signature_amount = 1500;
    // Setting the in-values
    MapSummary map_summary;
    map_summary.database_version.major = 1;
    map_summary.database_version.minor = 3;
    map_summary.database_version.patch = 4;
    for (int i = 0; i < cluster_amount; i++) {
        std::uint64_t first = i;
        std::uint64_t second = total_signature_amount / cluster_amount;

        map_summary.cluster_details.insert({first, second});
    }

    // Serialize
    const auto serialized_data = map_summary.Serialize();

    // Deserialize
    const MapSummary map_summary_out = MapSummary::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(map_summary.database_version, map_summary_out.database_version);
    ASSERT_EQ(map_summary.CalculateSignatureCount(), map_summary_out.CalculateSignatureCount());
    ASSERT_EQ(map_summary.CalculateClusterCount(), map_summary_out.CalculateClusterCount());
    ASSERT_EQ(map_summary.cluster_details, map_summary_out.cluster_details);
}

TEST(CommandTests, CreatedSignature) {
    using namespace accerion::api;

    const Pose pose{123.123, 456.456, 123.45};
    const std::vector<CreatedSignature> inputs{
        {::detail::kMinTimePoint,
         {pose, ::detail::kMin<ClusterId>, ::detail::kMin<SignatureId>}},
        {::detail::kMinTimePoint + (::detail::kMaxTimePoint - ::detail::kMinTimePoint) / 2,
         {pose, ::detail::kMax<ClusterId> / 2, ::detail::kMax<SignatureId> / 2}},
        {::detail::kMaxTimePoint,
         {pose, ::detail::kMax<ClusterId>, ::detail::kMax<SignatureId>}}
    };

    for (const auto& input: inputs) {
        const auto serialized_data = input.Serialize();
        const auto output = CreatedSignature::Deserialize(serialized_data);
        ASSERT_EQ(input, output);
    }
}

TEST(CommandTests, Areas) {
    using namespace accerion::api;
    // Setting the in-values
    Areas areas;
    areas.active_area = 22;
    areas.all_areas = {0, 1, 2, 3, 22};

    // Serialize
    const auto serialized_data = areas.Serialize();

    // Deserialize
    const Areas areas_out = Areas::Deserialize(serialized_data);

    // Compare outputs to inputs
    ASSERT_EQ(areas_out.active_area, areas.active_area);
    ASSERT_EQ(areas_out.all_areas, areas.all_areas);
}

TEST(CommandTests, Time) {
    using namespace accerion::api;
    // Setting the in-values
    const std::vector<accerion::api::TimePoint> inputs{
        ::detail::kMinTimePoint,
        ::detail::kMinTimePoint + (::detail::kMaxTimePoint - ::detail::kMinTimePoint) / 2,
        ::detail::kMaxTimePoint
    };

    for (const auto& input : inputs) {
        const auto data = Time{ input }.Serialize();
        const auto output = Time::Deserialize(data).value;
        ASSERT_EQ(input, output);
    }
}
