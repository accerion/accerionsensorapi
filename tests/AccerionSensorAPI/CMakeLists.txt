cmake_minimum_required(VERSION 3.5)

project(UnitTestsAccerionSensorAPI)

if(NOT TARGET ${PROJECT_NAME})
    add_unit_tests(${PROJECT_NAME} "AccerionSensorAPI")
endif()
