/* Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/structs/version.h"


TEST(Version, Parse) {
    using namespace accerion::api;
    const std::vector<std::pair<std::string, Version>> tests = {
            {"", {0, 0, 0}},
            {"3", {3, 0, 0}},
            {"3.2", {3, 2, 0}},
            {"3.2.10", {3, 2, 10}},
            {"3.2.10.50", {3, 2, 10}}
    };

    for (const auto& test: tests) {
        ASSERT_EQ(Version::Parse(test.first), test.second)
                << "String '" << test.first << "' should be parsed as version '" << test.second << "'." << std::endl;
    }
}

TEST(Version, Less) {
    struct Test {
        accerion::api::Version a;
        accerion::api::Version b;
        bool result_less;
        bool result_less_equal;
    };
    const std::vector<Test> tests = {
            {{0, 0, 0}, {0, 0, 0}, false, true},
            {{2, 0, 0}, {3, 0, 0}, true, true},
            {{3, 0, 0}, {3, 0, 0}, false, true},
            {{4, 0, 0}, {3, 0, 0}, false, false},
            {{3, 1, 0}, {3, 2, 0}, true, true},
            {{3, 2, 0}, {3, 2, 0}, false, true},
            {{3, 3, 0}, {3, 2, 0}, false, false},
            {{3, 2, 5}, {3, 2, 10}, true, true},
            {{3, 2, 10}, {3, 2, 10}, false, true},
            {{3, 2, 15}, {3, 2, 10}, false, false}
    };

    for (const auto& test: tests) {
        ASSERT_EQ(test.a < test.b, test.result_less)
                << "Version '" << test.a << "' should " << (test.result_less ? "" : "not ") << "be smaller than '"
                << test.b << "'.";
    }

    for (const auto& test: tests) {
        ASSERT_EQ(test.a <= test.b, test.result_less_equal)
                << "Version '" << test.a << "' should " << (test.result_less_equal ? "" : "not ")
                << "be smaller or equal than '" << test.b << "'.";
    }
}
