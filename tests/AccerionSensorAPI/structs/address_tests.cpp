/* Copyright (c) 2017-2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/structs/address.h"


TEST(Address, Less) {
    struct Test {
        accerion::api::Address a;
        accerion::api::Address b;
        bool result_less;
    };
    const std::vector<Test> tests = {
            {{0, 0, 0, 0}, {0, 0, 0, 0}, false},
            {{2, 0, 0, 0}, {3, 0, 0, 0}, true},
            {{3, 0, 0, 0}, {3, 0, 0, 0}, false},
            {{4, 0, 0, 0}, {3, 0, 0, 0}, false},
            {{3, 1, 0, 0}, {3, 2, 0, 0}, true},
            {{3, 2, 0, 0}, {3, 2, 0, 0}, false},
            {{3, 3, 0, 0}, {3, 2, 0, 0}, false},
            {{3, 2, 5, 0}, {3, 2, 10, 0}, true},
            {{3, 2, 10, 0}, {3, 2, 10, 0}, false},
            {{3, 2, 15, 0}, {3, 2, 10, 0}, false},
            {{3, 2, 10, 4}, {3, 2, 10, 5}, true},
            {{3, 2, 10, 5}, {3, 2, 10, 5}, false},
            {{3, 2, 10, 6}, {3, 2, 10, 5}, false}
    };

    for (const auto& test: tests) {
        ASSERT_EQ(test.a < test.b, test.result_less)
                << "Address '" << test.a << "' should " << (test.result_less ? "" : "not ") << "be smaller than '"
                << test.b << "'.";
    }
}

TEST(Address, Parse) {
    const auto address = accerion::api::Address::Parse("192.168.178.110");
    EXPECT_EQ(address.first, 192);
    EXPECT_EQ(address.second, 168);
    EXPECT_EQ(address.third, 178);
    EXPECT_EQ(address.fourth, 110);
}

TEST(Address, Conversion) {
    const auto kExpectedAddress = accerion::api::Address::Parse("192.168.178.110");
    const auto in_addr = kExpectedAddress.Convert();
    EXPECT_EQ(kExpectedAddress, accerion::api::Address::From(in_addr));
}
