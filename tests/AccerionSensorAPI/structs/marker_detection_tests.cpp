/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/structs/marker_detection.h"

bool CompareMarkerDetectionModes(const accerion::api::MarkerDetectionMode& a,
                                 const accerion::api::MarkerDetectionMode& b) {
    return a.mode_state == b.mode_state && a.type == b.type;
}

TEST(MarkerDetectionMode, SerializationRoundTrip) {
    const std::vector<accerion::api::MarkerDetectionMode> test_cases{
            {false, accerion::api::MarkerType::kDataMatrixMultiCode},
            {true, accerion::api::MarkerType::kAruco}
    };

    for (const auto& test_case: test_cases) {
        const auto result = accerion::api::MarkerDetectionMode::Deserialize(test_case.Serialize());
        ASSERT_PRED2(CompareMarkerDetectionModes, result, test_case);
    }
}


bool CompareMarkerDetections(const accerion::api::MarkerDetection& a, const accerion::api::MarkerDetection& b) {
    return a.timestamp == b.timestamp && a.type == b.type && a.id == b.id && a.pose == b.pose;
}

TEST(MarkerDetection, SerializationRoundTrip) {
    const std::vector<accerion::api::MarkerDetection> test_cases{
            {accerion::api::TimePoint(accerion::api::TimePoint::duration{0})},
            {accerion::api::TimePoint(accerion::api::TimePoint::duration{639182}),
             accerion::api::MarkerType::kAruco, "aruco marker id", {-.684, -156., -179.12}},
            {accerion::api::TimePoint(accerion::api::TimePoint::duration{3124509864}),
             accerion::api::MarkerType::kDataMatrixMultiCode, "multi code marker id", {.498, 168., 25.72}}
    };

    for (const auto& test_case: test_cases) {
        const auto result = accerion::api::MarkerDetection::Deserialize(test_case.Serialize());
        ASSERT_PRED2(CompareMarkerDetections, result, test_case)
                << "Failed for test case with timestamp " << test_case.timestamp.time_since_epoch().count();
    }
}
