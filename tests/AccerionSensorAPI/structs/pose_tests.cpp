/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <unordered_set>

#include "gtest/gtest.h"

#include "AccerionSensorAPI/structs/pose.h"


bool PosesAreApproximatelyEqual(const accerion::api::Pose& a, const accerion::api::Pose& b) {
    return a.IsApprox(b);
}

bool IsValueAllowed(double value, const std::unordered_set<double>& allowed_values) {
    return std::any_of(allowed_values.cbegin(),
                       allowed_values.cend(),
                       [value](double allowed_value) {
                           return std::abs(value - allowed_value) < 1e-13;
                       });
}


const accerion::api::Pose kPoseIdentity = accerion::api::Pose::Identity();

TEST(NormalizeAngle, NormalizeAngle) {
    struct TestCase {
        double value;
        double expected;
    };

    // The test values are given in half circles, so in those units, the normalization function is expected to map
    // everything to the -1, 1 interval by add or subtracting 2
    const std::vector<TestCase> test_cases {
            {-3.9,  0.1},
            {-2.9, -0.9},
            {-2.0,  0.0},
            {-1.9,  0.1},
            {-0.9, -0.9},
            { 0.0,  0.0},
            { 0.1,  0.1},
            { 1.1, -0.9},
            { 2.0,  0.0},
            { 2.1,  0.1},
            { 3.1, -0.9},
    };

    for (const auto& test_case: test_cases) {
        const auto norm_angle_rad = accerion::api::NormalizeAngleRad(test_case.value * accerion::api::kPi);
        ASSERT_NEAR(norm_angle_rad / accerion::api::kPi, test_case.expected, std::numeric_limits<double>::epsilon())
                << "Expected the value " << test_case.value << " to become " << test_case.expected << ".";

        const auto norm_angle_deg = accerion::api::NormalizeAngleDeg(test_case.value * 180);
        ASSERT_NEAR(norm_angle_deg / 180, test_case.expected, std::numeric_limits<double>::epsilon())
                << "Expected the value " << test_case.value << " to become " << test_case.expected << ".";
    }
}

TEST(Pose, Cast) {
    const accerion::api::Pose_<int, true> pid{3, 5, static_cast<int>(1 * accerion::api::kRadToDeg)};
    const accerion::api::Pose_<int, false> pir{3, 5, 1};
    const accerion::api::Pose_<float, true> pfd{3, 5, 1 * accerion::api::kRadToDeg};
    const accerion::api::Pose_<float, false> pfr{3, 5, 1};
    const accerion::api::Pose_<double, true> pdd{3, 5, 1 * accerion::api::kRadToDeg};
    const accerion::api::Pose_<double, false> pdr{3, 5, 1};

    // Due to the rounding of values, some pose instances are approximately equal
    ASSERT_TRUE((pid.Cast<double, false>()).IsApprox(pdr, 1e-10, .5 * accerion::api::kDegToRad));
    ASSERT_EQ((pir.Cast<double, false>()), pdr);
    ASSERT_TRUE((pfd.Cast<double, false>()).IsApprox(pdr, 1e-10, std::numeric_limits<float>::epsilon()));
    ASSERT_EQ((pfr.Cast<double, false>()), pdr);
    ASSERT_EQ((pdd.Cast<double, false>()), pdr);

    ASSERT_EQ(pid, (pir.Cast<int, true>()));
    ASSERT_EQ(pid, (pfd.Cast<int, true>()));
    ASSERT_EQ(pid, (pfr.Cast<int, true>()));
    ASSERT_EQ(pid, (pdd.Cast<int, true>()));
    ASSERT_EQ(pid, (pdr.Cast<int, true>()));

    // Specifically test the rounding functionality
    ASSERT_EQ((accerion::api::Pose_<float, true>{0, 0, 0.49}.Cast<int, true>()),
              (accerion::api::Pose_<int, true>{0, 0, 0}));
    ASSERT_EQ((accerion::api::Pose_<float, true>{0, 0, 0.50}.Cast<int, true>()),
              (accerion::api::Pose_<int, true>{0, 0, 1}));
}

TEST(Pose, Parse) {
    ASSERT_FALSE(accerion::api::Pose::Parse(""));
    ASSERT_FALSE(accerion::api::Pose::Parse("1"));
    ASSERT_FALSE(accerion::api::Pose::Parse("1 2"));

    ASSERT_EQ(accerion::api::Pose::Parse("1 2 3").value(), (accerion::api::Pose{1, 2, 3}));
    ASSERT_EQ(accerion::api::Pose::Parse("1.1 2.2 3.3").value(), (accerion::api::Pose{1.1, 2.2, 3.3}));
}

TEST(Pose, Norm) {
    for (double n : {-2.2, -1.1, 0., 1.1, 2.2}) {
        const accerion::api::Pose result{n, n, n * 75.0};
        ASSERT_DOUBLE_EQ(result.NormSq(), 2. * n * n) << "Test with n = " << n << " failed.";
        ASSERT_DOUBLE_EQ(result.Norm(), std::sqrt(2.) * std::abs(n)) << "Test with n = " << n << " failed.";
    }
}

TEST(Pose, NormalizeRotation) {
    for (double n : {-2, -1, 0, 1, 2}) {
        accerion::api::Pose_<double, true> result_deg{0.0, 0.0, n * 360.0};
        result_deg.NormalizeRotation();
        ASSERT_EQ(result_deg, kPoseIdentity) << "Test with n = " << n << " failed.";

        accerion::api::Pose_<double, false> result_rad{0.0, 0.0, n * 2 * accerion::api::kPi};
        result_rad.NormalizeRotation();
        ASSERT_EQ(result_rad, (kPoseIdentity.Cast<double, false>())) << "Test with n = " << n << " failed.";
    }
}

TEST(Pose, InterpolatePosition) {
    const accerion::api::Pose p_a{ 1.,  3., 0.};
    const accerion::api::Pose p_b{-1., 13., 0.};

    struct TestCase{
        double ratio;
        accerion::api::Pose p_expect;
    };
    const std::vector<TestCase> tests = {
            {0.0, p_a},
            {0.2, {0.6, 5., 0.}},
            {0.5, {0.0, 8., 0.}}
    };

    for (const auto& test: tests) {
        std::stringstream test_case_ss;
        test_case_ss << "test with input poses [" << p_a << "] and [" << p_b << "]"
                     << " and ratio " << test.ratio << " failed";
        const auto test_case_str = test_case_ss.str();

        // Test interpolated pose
        auto p_actual = p_a.Interpolate(test.ratio, p_b);
        ASSERT_PRED2(PosesAreApproximatelyEqual, p_actual, test.p_expect) << test_case_str;

        // Test inversely interpolated pose
        p_actual = p_b.Interpolate(1.0 - test.ratio, p_a);
        ASSERT_PRED2(PosesAreApproximatelyEqual, p_actual, test.p_expect) << "inverse " << test_case_str;
    }
}

TEST(Pose, InterpolateRotation) {
    struct TestCase {
        double a;
        double b;
        double ratio;
        // In some cases, multiple values can be expected due to ambiguous test cases and implementation specifics
        std::unordered_set<double> expectations;
    };
    const std::vector<TestCase> tests = {
            {- 50.,  50., 0.0, {-50}},
            {- 50.,  50., 0.2, {-30}},
            {- 50.,  50., 0.5, {0}},
            {- 90.,  90., 0.2, {-126, -54}},
            {- 90.,  90., 0.5, {-180, 0, 180}},
            {-100.,  80., 0.2, {-136, -64}},
            {-100.,  80., 0.5, {-10, 170}},
            {-130., 130., 0.2, {-150}},
            {-130., 130., 0.5, {-180, 180}}
    };

    const auto create_pose = [](double angle) { return accerion::api::Pose{0, 0, angle}; };

    for (const auto& test: tests) {
        std::stringstream test_case_ss;
        test_case_ss << "test with input angles " << test.a << " and " << test.b << " and ratio " << test.ratio
                     << " failed";
        const auto test_case_str = test_case_ss.str();

        const auto p_a = create_pose(test.a);
        const auto p_b = create_pose(test.b);

        // Test interpolated pose
        auto actual = p_a.Interpolate(test.ratio, p_b).th;
        ASSERT_PRED2(IsValueAllowed, actual, test.expectations) << test_case_str;

        // Test inversely interpolated pose
        actual = p_b.Interpolate(1.0 - test.ratio, p_a).th;
        ASSERT_PRED2(IsValueAllowed, actual, test.expectations) << "inverse " << test_case_str;
    }
}

TEST(Pose, InverseMult) {
    accerion::api::Pose const p{1.0, 3.0, 0.5};
    accerion::api::Pose const p_inv = p.Inverse();

    ASSERT_PRED2(PosesAreApproximatelyEqual, p_inv * p, kPoseIdentity);
    ASSERT_PRED2(PosesAreApproximatelyEqual, p * p_inv, kPoseIdentity);
}

TEST(Pose, IsApproxWithNegativeHeading) {
    accerion::api::Pose const p{0., 0., -180.};

    ASSERT_TRUE(p.IsApprox({0., 0., 180.}));
}
