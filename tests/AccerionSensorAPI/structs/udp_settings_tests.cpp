/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "gtest/gtest.h"

#include "AccerionSensorAPI/structs/udp_settings.h"
#include "AccerionSensorAPI/utils/enum.h"


TEST(UdpSettings, IsValid) {
    using namespace accerion::api;
    struct Test {
        UdpSettings settings;
        bool result;
    };
    const std::vector<Test> tests = {
            {UdpSettings{Address{}, EnabledMessageType::kBoth, UdpStrategy::kBroadcast}, true},
            // Invalid enumeration values
            {UdpSettings{Address{}, EnabledMessageType(0), UdpStrategy::kBroadcast}, false},
            {UdpSettings{Address{}, EnabledMessageType(5), UdpStrategy::kBroadcast}, false},
            {UdpSettings{Address{}, EnabledMessageType::kBoth, UdpStrategy(-2)}, false},
            {UdpSettings{Address{}, EnabledMessageType::kBoth, UdpStrategy(4)}, false},
            // Valid and invalid address values in case of unicast
            {UdpSettings{Address{}, EnabledMessageType::kBoth, UdpStrategy::kUnicast}, false},
            {UdpSettings{Address{127, 0, 0, 1}, EnabledMessageType::kBoth, UdpStrategy::kUnicast}, true},
            {UdpSettings{Address{127, 0, 0, 2}, EnabledMessageType::kBoth, UdpStrategy::kUnicast}, false},
            {UdpSettings{Address{192, 168, 0, 0}, EnabledMessageType::kBoth, UdpStrategy::kUnicastNoHB}, true},
            {UdpSettings{Address{192, 167, 0, 0}, EnabledMessageType::kBoth, UdpStrategy::kUnicastNoHB}, false}
    };
    for (const auto& test: tests) {
        ASSERT_EQ(test.settings.IsValid(), test.result) << "Entry with (" << test.settings.address
                << ", " << +GetEnumValue(test.settings.message_type)
                << ", " << +GetEnumValue(test.settings.strategy) << ")";
    }
}
