set(CMAKE_TEST_DIR "${CMAKE_BINARY_DIR}/tests/ctest/cmake")

# Cleanup test files from previous run
file(REMOVE_RECURSE ${CMAKE_TEST_DIR})

# Install the library
execute_process(
    COMMAND ${CMAKE_COMMAND} --install ${CMAKE_BINARY_DIR} --prefix ${CMAKE_TEST_DIR}/install
    RESULT_VARIABLE generate_result)

if(NOT generate_result EQUAL 0)
    message(FATAL_ERROR "CMake install command failed with result ${generate_result}.")
endif()

# Configure the test project
file(WRITE ${CMAKE_TEST_DIR}/CMakeLists.txt
[[
cmake_minimum_required(VERSION 3.5)
project(TestCMakePackage)

# Required for compatibility with 'Visual Studio', 
# where binaries are placed in subdirectories based on build configuration
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/Release/bin")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE "${CMAKE_BINARY_DIR}/Release/bin")

# Find the installed package
find_package(AccerionSensorAPI REQUIRED)

# Add a simple executable that links against the installed library
add_executable(test_target ${CMAKE_CURRENT_SOURCE_DIR}/test_main.cpp)
target_link_libraries(test_target PRIVATE AccerionSensorAPI)
]])

# Create a test main file
file(WRITE ${CMAKE_TEST_DIR}/test_main.cpp
[[
#include <AccerionSensorAPI/sensor.h>

int main() {
    return 0;
}
]])

# Generate build instructions for the test target
execute_process(
    COMMAND ${CMAKE_COMMAND} -S ${CMAKE_TEST_DIR} -B ${CMAKE_TEST_DIR}/build -DCMAKE_PREFIX_PATH=${CMAKE_TEST_DIR}/install
    RESULT_VARIABLE generate_result)

if(NOT generate_result EQUAL 0)
    message(FATAL_ERROR "Generating buid instructions for test target failed")
endif()

# Build the test target
execute_process(
    COMMAND ${CMAKE_COMMAND} --build ${CMAKE_TEST_DIR}/build --config Release --parallel
    RESULT_VARIABLE build_result)

if(NOT build_result EQUAL 0)
    message(FATAL_ERROR "Building test target command failed")
endif()

# Run the test target
execute_process(
    COMMAND ${CMAKE_TEST_DIR}/build/Release/bin/test_target
    RESULT_VARIABLE run_result)

if(NOT run_result EQUAL 0)
    message(FATAL_ERROR "Running test target failed: ${run_result}")
endif()
