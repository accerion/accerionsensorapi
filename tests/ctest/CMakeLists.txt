add_test(
    NAME TestCMakePackaging
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_SOURCE_DIR}/TestCMakePackaging.cmake
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)
