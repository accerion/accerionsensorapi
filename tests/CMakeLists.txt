cmake_minimum_required(VERSION 3.5)

include_directories(
        ${gtest_SOURCE_DIR}/include
        ${gmock_SOURCE_DIR}/include)

function(add_unit_tests TEST_NAME LIBS)
    file(GLOB_RECURSE SOURCES
            "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

    # Append the test library
    set(LIBS "${LIBS}" gtest_main)

    add_executable(${TEST_NAME} ${SOURCES})
    target_link_libraries(${TEST_NAME} ${LIBS})
endfunction()

add_subdirectory(AccerionSensorAPI)
add_subdirectory(ctest)
