# Protocol changelog

## UNRELEASED
* -
## 7.0.0
* Added upload database option for file transfers(0x03)
* MapHeader no longer retrievable through 0xA8, it now returns a MapSummary
* Removed send, retrieve and import g2o

## 6.1.5
* Added ’Marker Detection’ commands (mode request 0xAE, -response 0xAF and intermittent 0xB0)
* Mark ’Aruco Marker Detection’ commands as deprecated (0x5B, 0x2B and 0x18)
* Added 2 new error flags in diagnostics: SD card not mounted and Kernel patch error. See Diagnostics Errors
* Documented datatype of Signature ID in Signature Created message was incorrect and did not reflect real datatype uint32_t
* Added missing ports for filetransfers in Protocol Details

## 6.1.4
* Removed second line following feature (command ids 0x4B and 0x75)

## 6.1.3
None

## 6.1.2
* Added Intermittent 0xAD: Signature Created

## 6.1.1
* Added search radius value to the payload part of File Transfer Task with type: SearchLoop-
Closures

## 6.1.0
None

## 6.0.0
* Added ”Message length” field to all the commands that were missing it
* Changed order in UDP Settings message Commands 0x89: Set UDP Settings
* Removed high and low brightness warnings for camera 2
* Added messages:
  * Command 0x5E: Upload/Download File
  * Acknowledgement 0x07: File Transfer Handshake
  * Acknowledgement 0x46: Sensor Started
  * Acknowledgement 0x47: Sensor Stopped
  * Command 0x61: Get All IP Addresses
  * Command 0xA6: File Transfer Task
  * Acknowledgement 0xA7: File Transfer Status
  * Command 0xA8: Get Map Header
  * Acknowledgement 0xA9: Map Header
  * Commands 0xA4: Remove Area
  * Acknowledgement 0xA5: RemoveArea Result
  * Commands 0xA2: Get RAM ROM stats
  * Acknowledgement 0xA3: RAMROM stats
* Added the field Full Map Search to (Commands 0x8B: Start Processing Buffer).
* Removed messages:
  * Command 0x02: Get File
  * Acknowledgement 0x03: File
  * Acknowledgement 0x04: Progress
  * Command 0x05: Upload File
  * Acknowledgement 0x06: Upload File
  * Streaming 0x15: Quality Estimate
  * Acknowledgement 0x27: Calibration Mode
  * Acknowledgement 0x2A: Recovery Mode
  * Acknowledgement 0x2F: Replace Cluster G2O Result
  * Acknowledgement 0x35: Cluster Map
  * Acknowledgement 0x36: Logs
  * Acknowledgement 0x37: Update Result
  * Acknowledgement 0x38: Mapsharing Place Map (Import) Result
  * Acknowledgement 0x3E: Record Recovery Buffer
  * Acknowledgement 0x44: Serial Number
  * Acknowledgement 0x49: Set Sensor Mount Position Acknowledgement
  * Acknowledgement 0x4F: Find Loop Closures G2O Format
  * Command 0x56: Set Calibration Mode
  * Command 0x65: Get All Serial Numbers
  * Command 0x6A: Get Sensor Mount Position
  * Command 0x72: Set Recovery Mode
  * Command 0x84: Set Sensor Mount Position
  * Command 0x90: Find Loop Closures In G2O Format
  * Command 0x91: Replace Cluster With G2O Format
  * Command 0x93: Get Logs
  * Acknowledgement 0x92: Mapsharing Get Mapfile (Export)
  * Command 0x94: Accerion Reserved
  * Command 0x95: Mapsharing Place Mapfile (Import)
  * Acknowledgement 0x96: Get Backup Logs
* Modified messages:
  * Periodic 0x01: Heartbeat
  * Acknowledgement 0x07: File Transfer Handshake
  * Streaming 0x13: Diagnostics
  * Streaming 0x17: Signature Position Packet
  * Acknowledgement 0x40: IP Address
  * Acknowledgement 0x41: TCP Information
  * Acknowledgement 0x45: Console Output Info
  * Acknowledgement 0x4C: Frame

Older versions are documented in the old protocol documents.
