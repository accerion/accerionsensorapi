# Protocol
In case you do not want to use the API but are just interested in the raw communication protocol, this page will explain how to implement it.

> <br> &emsp; **WARNING**: This page is still under development. It is not complete and some sections still need attention. <br> <br>

Sections in this document:
* [Connection types](#Connection types)
* [Generic message structure](#Generic message structure)
* [Sensor usage](#Sensor usage)
* [Other features](#Other features)

The changelog with an overview of the history can be found [on a separate page](docs/protocol_changelog.md).


<a name="Connection types"></a>
## Connection types

UDP connection types put less stress on the network but have no guarantee of reaching their destination. Therefore, TCP
is much more stable. Accerion offers the option to enable/disable streaming and/or intermittent messages on both UDP and
TCP. This enables you as a user to for instance enable the streaming messages on UDP and get the intermittent messages
over TCP. As of yet periodic messages are present by default, due to the heartbeat messages. If needed, it is possible
to disable the heartbeat messages.

For UDP, both unicast and broadcast are supported.

Below the port details are given.

### UDP

| Purpose                | Port  | Description                                 |
|:-----------------------|:------|:--------------------------------------------|
| Host to sensor         | 13360 | Sensor listens to the client                |
| Sensor to host         | 13359 | Sensor communicates to the client           |
| Update service to host | 13362 | UpdateService publishes heartbeat to client |

### TCP

| Purpose                           | Port  | Description                                       |
|:----------------------------------|:------|:--------------------------------------------------|
| Host/sensor                       | 13361 | Sensor bi-directional communication               |
| Host/update service               | 1989  | Update service bi-directional communication       |
| Host/sensor file transfer         | 13370 | Sensor file transfers, excluding logs and updates |
| Host/update service file transfer | 13380 | Update service file transfers (logs and updates)  |


<a name="Generic message structure"></a>
## Message structure
A message always has the following header and footer:

| Type                                     | Length | Description     |
|:-----------------------------------------|-------:|:----------------|
| `uint32_t`                               |      4 | Serial number   |
| `uint8_t`                                |      1 | Command id      |
| `uint32_t`                               |      4 | Message lengths |
| Body containing the serialized structure |||
| `uint8_t`                                |      1 | CRC8 value      |

The first 4 bytes encode the sensor [serial number](@ref accerion::api::SerialNumber::Serialize) to/from which the message is send/received.
It is followed by the byte indicating the [message/command id](@ref accerion::api::CommandId) (follow the link for an overview of all ids)
and the total length of the message in 4 bytes (including the header and footer). This is the header for every message.
Then the actual message content comes, which depends on the message/command id.
For every message there is C++ struct that has the (de)serialization functionality in it.
The documentation of the `Serialize` method of each struct will explain how the body is created.
All references on this page will refer to that function, where applicable.
The message finishes with a footer of 1 byte that holds the CRC-8 value.
If you want to validate a message, please check the [Crc8 class](@ref accerion::api::Crc8).

In the serialization, every field is always converted to an integer type, if needed, for example for double.
This is done to guarantee that any system knows what to expect, this would not be the case when sending raw double data, for example.
Every integer type sent in the protocol is with the most significant byte first (big endian).
Note that most systems actually store the integer data in reverse order, so with the least significant byte first (little endian).
For a message containing multiple fields, every field is added to the message in order.
So, there is no reversing of message content on any system, but if the system is little endian, then the underlying serialized integer data needs to be reversed.

Most messages have a fixed length. In case of variable length messages, for example vectors or strings, then the length will be part of the message.
Some examples to clarify:
- Fixed object (like [Signature](@ref accerion::api::Signature::Serialize)): fixed length, so no length included (besides the overall message length).
- Variable object (like [PlainString](@ref accerion::api::PlainString::Serialize)): variable length, so the length is included.
- Variable object of fixed objects (like [std::vector<Signature>](@ref accerion::api::SignatureVector::Serialize)): variable length, so length is included, but also the object size.
- Variable object of variable objects (like [std::vector<std::string>](@ref accerion::api::StringVector::Serialize)): variable length, so length is included, but also every object is prepended with the object size.

This way of (de)serializing ensures that field can be added at the end of any object or message, without breaking compatibility.

There are different [types of messages](@ref accerion::api::MessageType):
* Streaming messages are constantly being sent.
* Periodic messages are sent at intervals.
* Intermittent messages happen irregularly and are mainly intended for requests/commands and their acknowledgement/callback messages.

More details about specific message will be outlined below when discussing different features.

<a name="Sensor usage"></a>
## Sensor usage

### Configuration and monitoring

Normally, there is no initialization needed to connect to the sensor.

| Message type | Message                                                                                                                                      |
|:-------------|:---------------------------------------------------------------------------------------------------------------------------------------------|
| Periodic     | Heart beat ([0x01](@ref accerion::api::CommandId::PRD_HEARTBEAT_INFO)) as [HeartBeat](@ref accerion::api::HeartBeat::Serialize)              |
| Streaming    | Diagnostics ([0x13](@ref accerion::api::CommandId::STR_DIAGNOSTICS)) as [Diagnostics](@ref accerion::api::Diagnostics::Serialize)            |
| Intermittent | Console output ([0x45](@ref accerion::api::CommandId::INT_CONSOLE_OUTPUT_INFO)) as [PlainString](@ref accerion::api::PlainString::Serialize) |

| Requests                                                                                                                      | Responses                                                                                                                                           |
|:------------------------------------------------------------------------------------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------|
| Get software version ([0x68](@ref accerion::api::CommandId::CMD_GET_SOFTWARE_VERSION)) as empty-body request                  | Software version ([0x4D](@ref accerion::api::CommandId::ACK_SOFTWARE_VERSION)) as [SoftwareVersion](@ref accerion::api::SoftwareVersion::Serialize) |
| Get software details ([0x97](@ref accerion::api::CommandId::CMD_GET_SOFTWARE_DETAILS)) as empty-body request                  | Software details ([0x3A](@ref accerion::api::CommandId::ACK_SOFTWARE_DETAILS)) as [SoftwareDetails](@ref accerion::api::SoftwareDetails::Serialize) |
| Get memory statistics ([0xA2](@ref accerion::api::CommandId::CMD_GET_RAM_ROM_STATS)) as empty-body request                    | Memory statistics ([0xA3](@ref accerion::api::CommandId::ACK_RAM_ROM_STATS)) as [RamRomStats](@ref accerion::api::RamRomStats::Serialize)           |
| Log event ([0xB1](@ref accerion::api::CommandId::CMD_LOG_EVENT)) as [PlainString](@ref accerion::api::PlainString::Serialize) |                                                                                                                                                     |

A sensor produces a periodic heartbeat message. This message is broadcast over UDP and makes sure the sensor is always retrievable.
The heartbeat message contains information like its IP settings. If you only retrieve the heartbeat message,
chances are likely that you have disabled both UDP and TCP streaming and intermittent messages.

Diagnostics is a streaming message that contains valuable information regarding the state of the sensor. As seen in the
protocol the message contains possible errors and warnings messages, but every message also shows the current modes
that are activated. 16 bits are available, where a 1 indicates that the mode is on, and a 0 means it is off. Each mode is allocated
a bit in this 16-bit field. Bits are read from right to left. So index 0, which is allocated for idle mode, is the bit on the far right.

The logging of events can be used to inject information in the ARAMS user logs for future reference.

| Requests                                                                                                                                                                                                                                              | Responses                                                                                                                           |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------|
| Get sample rate ([0x62](@ref accerion::api::CommandId::CMD_GET_SAMPLE_RATE)) as empty-body request | Sample rate ([0x33](@ref accerion::api::CommandId::ACK_GET_SAMPLE_RATE)) as [PlainUInt16](@ref accerion::api::PlainUInt16::Serialize)   |
| Set sample rate ([0x70](@ref accerion::api::CommandId::CMD_SET_SAMPLE_RATE)) as [PlainUInt16](@ref accerion::api::PlainUInt16::Serialize) | ([0x32](@ref accerion::api::CommandId::ACK_SET_SAMPLE_RATE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Get IP address ([0x60](@ref accerion::api::CommandId::CMD_GET_IP_ADDRESS)) as empty-body request | IP address ([0x49](@ref accerion::api::CommandId::ACK_GET_IP_ADDRESS)) as [IpAddress](@ref accerion::api::IpAddress::Serialize)         |
| Set IP address ([0x80](@ref accerion::api::CommandId::CMD_SET_IP_ADDRESS)) as [IpAddress](@ref accerion::api::IpAddress::Serialize) | ([0x40](@ref accerion::api::CommandId::ACK_SET_IP_ADDRESS)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |          
| Get UDP settings ([0x46](@ref accerion::api::CommandId::CMD_GET_UDP_SETTINGS)) as empty-body request  | UDP settings ([0x4F](@ref accerion::api::CommandId::ACK_GET_UDP_SETTINGS)) as [UdpSettings](@ref accerion::api::UdpSettings::Serialize) |
| Set UDP settings ([0x89](@ref accerion::api::CommandId::CMD_SET_UDP_SETTINGS)) as [UdpSettings](@ref accerion::api::UdpSettings::Serialize) | ([0x4E](@ref accerion::api::CommandId::ACK_SET_UDP_SETTINGS)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Get TCP settings ([0x69](@ref accerion::api::CommandId::CMD_GET_TCP_SETTINGS)) as empty-body request  | TCP settings ([0x47](@ref accerion::api::CommandId::ACK_GET_TCP_SETTINGS)) as [TcpSettings](@ref accerion::api::TcpSettings::Serialize) |
| Set TCP settings ([0x87](@ref accerion::api::CommandId::CMD_SET_TCP_SETTINGS)) as [TcpSettings](@ref accerion::api::TcpSettings::Serialize) | ([0x41](@ref accerion::api::CommandId::ACK_SET_TCP_SETTINGS)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |

The sample rate is the rate at which streaming messages are sent out. Default value is 150Hz.
The default IP address of a sensor is 192.168.0.1xx, where xx are the last two digits of the serial number which you can find on the label of the sensor.
When the IP address is changed, the sensor needs to be rebooted and any connection needs to be reestablished.
When using UDP, the following settings are available:
* Which messages are send: only streaming, only intermittent, none or both
* Which strategy: broadcast or unicast


### Start-up sequence

| Requests                                                                                                                                                                         | Responses |
|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:----------|
| Set external reference ([0x85](@ref accerion::api::CommandId::CMD_SET_EXTERNAL_REFERENCE_POSE)) as [ExternalReferencePose](@ref accerion::api::ExternalReferencePose::Serialize) |           |
| Set correction search range ([0xB2](@ref accerion::api::CommandId::CMD_SET_SEARCH_RANGE)) as [SearchRange](@ref accerion::api::SearchRange::Serialize)                    |           |

The [SetExternalReferencePose](@ref accerion::api::Sensor::SetExternalReference) function informs the sensor of its current location. The sensor expects to receive this message at a frequency of at least 5 Hz; otherwise, a [warning](@ref accerion::api::kWarningMap) is issued in the [diagnostics](@ref accerion::api::Diagnostics) message.
During mapping, the external reference is used to assign poses to signatures, and during localization, it searches the signature map for matches.
To monitor the most recent external reference being processed by the sensor, use the [SubscribeToLatestExternalReference](@ref accerion::api::Sensor::SubscribeToLatestExternalReference) function.

When internal mode is used, setting an external reference pose and -search range are ignored. Instead, use the functions below:

| Requests                                                                                                                                            | Responses                                                                                                                                                     |
|:----------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Set internal mode ([0x50](@ref accerion::api::CommandId::CMD_SET_INTERNAL_MODE)) as [Boolean](@ref accerion::api::Boolean::Serialize)               | Acknowledgement ([0x28](@ref accerion::api::CommandId::ACK_INTERNAL_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)               |
| Set corrected odometry pose ([0x81](@ref accerion::api::CommandId::CMD_SET_CORRECTED_ODOMETRY_POSE)) as [Pose](@ref accerion::api::Pose::Serialize) | Acknowledgement ([0x42](@ref accerion::api::CommandId::ACK_SET_CORRECTED_ODOMETRY_POSE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
 

### Commissioning

| Message type | Message                                                                                                                                                 |
|:-------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
| Intermittent | Created signature ([0xAD](@ref accerion::api::CommandId::INT_SIGNATURE_CREATED)) as [CreatedSignature](@ref accerion::api::CreatedSignature::Serialize) |

| Requests                                                                                                                                                                                                                   | Responses                                                                                                                                                                                                                                                                                               |
|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Set area ([0x9D](@ref accerion::api::CommandId::CMD_SET_AREA)) as [PlainUInt16](@ref accerion::api::PlainUInt16::Serialize) | Acknowledgement ([0xB7](@ref accerion::api::CommandId::ACK_SET_AREA)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Get areas ([0x9B](@ref accerion::api::CommandId::CMD_GET_AREA_IDS)) as empty-body request | Areas ([0x9C](@ref accerion::api::CommandId::ACK_GET_AREA_IDS)) as [Areas](@ref accerion::api::Areas::Serialize) |
| Set mapping mode ([0x86](@ref accerion::api::CommandId::CMD_SET_MAPPING_MODE)) as [BoolUInt64](@ref accerion::api::BoolUInt64::Serialize)                                                                                  | Acknowledgement ([0x20](@ref accerion::api::CommandId::ACK_SET_MAPPING_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)                                                                                                                                                          |
| Get clusters ([0x99](@ref accerion::api::CommandId::CMD_GET_CLUSTER_IDS)) as empty-body request                                                                                                                            | Clusters ([0x9A](@ref accerion::api::CommandId::ACK_GET_CLUSTER_IDS)) as [UInt64Vector](@ref accerion::api::UInt64Vector::Serialize)                                                                                                                                                                    |
| Get signature positions ([0x5A](@ref accerion::api::CommandId::CMD_GET_SIGNATURE_INFORMATION)) as [BoolUInt64](@ref accerion::api::BoolUInt64::Serialize)                                                                  | Signatures positions start/stop ([0x2E](@ref accerion::api::CommandId::ACK_SIGNATURE_INFORMATION_DONE)) as empty-body command <br> Signatures positions package ([0x17](@ref accerion::api::CommandId::ACK_SIGNATURE_INFORMATION)) as [SignatureVector](@ref accerion::api::SignatureVector::Serialize) |

The first real step of working with Accerion sensors is to do the mapping.
During mapping, the sensor will see what the surface looks like and it will create signatures from it.
To start mapping, a cluster id must be provided, which will unique indentify a line/group of signatures.

During mapping, the sensor takes images of the floor and creates a unique signature out it.
This signature is stored in its database together with the sensor pose known at that time.
These signatures form a cluster (with a unique ID). Multiple clusters form an area (also referred to as ‘map’).


#### Map management

| Requests                                                                                                                                                             | Responses                                                                                                                                                  |
|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| Remove cluster ([0x74](@ref accerion::api::CommandId::CMD_DELETE_CLUSTER)) as [PlainUInt64](@ref accerion::api::PlainUInt64::Serialize)                              | Cluster removed ([0x34](@ref accerion::api::CommandId::ACK_DELETE_CLUSTER)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)                  |
| Remove area ([0xA4](@ref accerion::api::CommandId::CMD_DELETE_AREA)) as [BoolUInt16](@ref accerion::api::BoolUInt16::Serialize)                                      | Remove area result ([0xA5](@ref accerion::api::CommandId::ACK_DELETE_AREA)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)                     |
| Perform subsetting ([0xA0](@ref accerion::api::CommandId::CMD_CREATE_MAP_SUBSET)) as [SubsetOperationRequest](@ref accerion::api::SubsetOperationRequest::Serialize) | Subsetting result ([0xA1](@ref accerion::api::CommandId::ACK_MAP_SUBSET)) as [SubsetOperationResult](@ref accerion::api::SubsetOperationResult::Serialize) |

All file transfer related functionality is covered by the section on [file transfer](#File transfer) further down this page.


### In operation

| Message type | Message                                                                                                                                                          |
|:-------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Streaming    | Odometry pose ([0x12](@ref accerion::api::CommandId::STR_ODOMETRY)) as [Odometry](@ref accerion::api::Odometry::Serialize)                                       |
| Streaming    | Corrected odometry pose ([0x11](@ref accerion::api::CommandId::STR_CORRECTED_ODOMETRY)) as [CorrectedOdometry](@ref accerion::api::CorrectedOdometry::Serialize) |
<!---| Intermittent | Raw drift correction ([0x15](@ref accerion::api::CommandId::INT_DRIFT_CORRECTION)) as [DriftCorrection](@ref accerion::api::DriftCorrection::Serialize) where the correction is not interally corrected, but instead the timestamp contains the timestamp of when the frame was made.                                                                                 |-->
| Intermittent | Compensated drift correction ([0x14](@ref accerion::api::CommandId::INT_COMPENSATED_DRIFT_CORRECTION)) as [DriftCorrection](@ref accerion::api::DriftCorrection::Serialize) where the correction is internally corrected for latency.                                                                                                                                   |
| Streaming    | Line follower data ([0x16](@ref accerion::api::CommandId::STR_LINE_FOLLOWER)) as [LineFollowerData](@ref accerion::api::LineFollowerData::Serialize)             |

| Requests                                                                                                                                               | Responses                                                                                                                                            |
|:-------------------------------------------------------------------------------------------------------------------------------------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| Set localization mode ([0x52](@ref accerion::api::CommandId::CMD_SET_LOCALIZATION_MODE)) as [Boolean](@ref accerion::api::Boolean::Serialize)          | Acknowledgement ([0x22](@ref accerion::api::CommandId::ACK_LOCALIZATION_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)  |
| Set line following mode ([0x88](@ref accerion::api::CommandId::CMD_SET_LINE_FOLLOWING_MODE)) as [BoolUInt64](@ref accerion::api::BoolUInt64::Serialize) | Acknowledgement ([0x2D](@ref accerion::api::CommandId::ACK_SET_LINE_FOLLOWING_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Get line following mode ([0x1A](@ref accerion::api::CommandId::CMD_GET_LINE_FOLLOWING_MODE)) as empty-body request | Bool with optional UInt64 ([0x1B](@ref accerion::api::CommandId::ACK_GET_LINE_FOLLOWING_MODE)) as [BoolOptUInt64](@ref accerion::api::BoolOptUInt64::Serialize) |

There are two positioning types that an Accerion sensor offers: relative and absolute.
Relative positioning, also called odometry, looks for translation and rotation, like a computer mouse.
Relative positioning will accumulate drift.
(Note that relative positioning is available at all times, including while mapping.)
With localization mode on, the sensor should search the floor for matches.
In case a signature of the floor is recognized, a drift correction is output. 
Also, the corrected odometry pose will have incorporated the correction.

On top of localization mode, there is a specific mode called line following.
This is a special use case an additional message with line follower data is sent, which provides the closest point on the cluster for the current sensor pose.


#### Continuous signature updating

| Requests                                                                                                                                                                        | Responses                                                                                                                                                            |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Set continuous signature updating mode ([0x9E](@ref accerion::api::CommandId::CMD_SET_CONTINUOUS_SIGNATURE_UPDATING_MODE)) as [Boolean](@ref accerion::api::Boolean::Serialize) | Acknowledgement ([0x9F](@ref accerion::api::CommandId::ACK_CONTINUOUS_SIGNATURE_UPDATING_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |

Continuous signature updating mode is a mode that can be naturally only be activated during localization mode.
In this mode, for every drift correction, the sensor updates the map to keep it up to date with the most recent state of the floor.
It is advised to enable this mode in operation.
Be aware that the mode must be switched off in case of map updating operations (for example while mapping or during map management actions).

<a name="Other features"></a>
## Other features

### Other modes and settings

| Requests                                                                                                                                                                                                                                                               | Responses                                                                                                                                         |
|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------|
| Set idle mode ([0x54](@ref accerion::api::CommandId::CMD_SET_IDLE_MODE)) as [Boolean](@ref accerion::api::Boolean::Serialize)                                                                                                                                          | Acknowledgement ([0x24](@ref accerion::api::CommandId::ACK_IDLE_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)       |
| Set expert mode ([0x6B](@ref accerion::api::CommandId::CMD_SET_EXPERT_MODE)) as [Boolean](@ref accerion::api::Boolean::Serialize)                                                                                                                                      | Acknowledgement ([0x39](@ref accerion::api::CommandId::ACK_EXPERT_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)     |
| Reboot ([0x55](@ref accerion::api::CommandId::CMD_REBOOT)) as empty-body request                                                                                                                                                        | Acknowledgement ([0x25](@ref accerion::api::CommandId::ACK_REBOOT)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)          |
| Set save image while mapping setting ([0xB3](@ref accerion::api::CommandId::CMD_SET_SAVE_IMAGES)) as [Boolean](@ref accerion::api::Boolean::Serialize)                                                                                                                 | Acknowledgement ([0xB5](@ref accerion::api::CommandId::ACK_SET_SAVE_IMAGES)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Get save images setting ([0xB4](@ref accerion::api::CommandId::CMD_GET_SAVE_IMAGES)) as empty-body request                                                                                                                                                             | Boolean         ([0xB6](@ref accerion::api::CommandId::ACK_GET_SAVE_IMAGES)) as [Boolean](@ref accerion::api::Boolean::Serialize)                 |

<a name="File transfer"></a>
### File transfer

| Requests                                                                                                                                                                          | Responses                                                                                                                                                                     |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Get map summary ([0xA8](@ref accerion::api::CommandId::CMD_GET_MAP_SUMMARY)) as empty-body request                                                                                  | Map Summary ([0xA9](@ref accerion::api::CommandId::ACK_MAP_SUMMARY)) as [MapSummary](@ref accerion::api::MapSummary::Serialize)                                                 |
| File transfer request ([0x5E](@ref accerion::api::CommandId::CMD_FILE_TRANSFER_REQUEST)) as [FileUploadDownloadRequest](@ref accerion::api::FileUploadDownloadRequest::Serialize) | File transfer handshake ([0x07](@ref accerion::api::CommandId::ACK_FILE_TRANSFER_HANDSHAKE)) as [FileTransferHandshake](@ref accerion::api::FileTransferHandshake::Serialize) |
| File transfer task ([0xA6](@ref accerion::api::CommandId::CMD_FILE_TRANSFER_TASK)) as [FileTransferTaskRequest](@ref accerion::api::FileTransferTaskRequest::Serialize)           | File transfer status ([0xA7](@ref accerion::api::CommandId::ACK_FILE_TRANSFER_STATUS)) as [FileTransferTaskResult](@ref accerion::api::FileTransferTaskResult::Serialize)     |

There are different types of files and related actions. For a kind of overview, check the pages on [file transfer](@ref file_transfer.h) and [file transfer task](@ref file_transfer_task.h).


### Buffered recovery

| Requests                                                                                                                                                                                                                                                                                                | Responses                                                                                                                                                           |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Get buffer length ([0x6C](@ref accerion::api::CommandId::CMD_GET_BUFFER_LENGTH)) as empty-body request | Buffer length ([0x3E](@ref accerion::api::CommandId::ACK_GET_BUFFER_LENGTH)) as [BufferLength](@ref accerion::api::BufferLength::Serialize)                 |
| Set buffer length ([0x8A](@ref accerion::api::CommandId::CMD_SET_BUFFER_LENGTH)) as [BufferLength](@ref accerion::api::BufferLength::Serialize)   | Acknowledgement ([0x3C](@ref accerion::api::CommandId::ACK_SET_BUFFER_LENGTH)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize)                 |     
| Start processing buffer ([0x8B](@ref accerion::api::CommandId::CMD_START_PROCESSING_BUFFER)) as [BufferedRecoveryRequest](@ref accerion::api::BufferedRecoveryRequest::Serialize) <br> Stop processing buffer ([0x6D](@ref accerion::api::CommandId::CMD_STOP_PROCESSING_BUFFER)) as empty-body request | Buffer progress indicator ([0x3D](@ref accerion::api::CommandId::ACK_BUFFER_PROGRESS_INDICATOR)) as [BufferProgress](@ref accerion::api::BufferProgress::Serialize) |


### Marker detection

| Message type | Message                                                                                                                                             |
|:-------------|:----------------------------------------------------------------------------------------------------------------------------------------------------|
| Intermittent | Marker detection ([0xB0](@ref accerion::api::CommandId::INT_MARKER_DETECTION)) as [MarkerDetection](@ref accerion::api::MarkerDetection::Serialize) |

| Requests                                                                                                                                                                      | Responses                                                                                                                                               |
|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
| Set marker detection mode ([0xAE](@ref accerion::api::CommandId::CMD_SET_MARKER_DETECTION_MODE)) as [MarkerDetectionMode](@ref accerion::api::MarkerDetectionMode::Serialize) | Acknowledgement ([0xAF](@ref accerion::api::CommandId::ACK_MARKER_DETECTION_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |


### Recordings

| Requests                                                                                                                                      | Responses                                                                                                                                        |
|:----------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------|
| Set recording mode ([0x53](@ref accerion::api::CommandId::CMD_SET_RECORDING_MODE)) as [Boolean](@ref accerion::api::Boolean::Serialize)       | Acknowledgement ([0x23](@ref accerion::api::CommandId::ACK_RECORDING_MODE)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Get list of recordings ([0x5D](@ref accerion::api::CommandId::CMD_GET_RECORDINGS_LIST)) as empty-body request | Recordings list ([0x3F](@ref accerion::api::CommandId::ACK_RECORDINGS_LIST)) as [StringVector](@ref accerion::api::StringVector::Serialize)        |
| Delete recordings ([0x5F](@ref accerion::api::CommandId::CMD_DELETE_RECORDINGS)) as [StringVector](@ref accerion::api::StringVector::Serialize) | List of recordings that could not be deleted ([0x43](@ref accerion::api::CommandId::ACK_DELETE_RECORDINGS)) as [StringVector](@ref accerion::api::StringVector::Serialize)        |

Retrieving recordings is also covered in the section on [file transfer](#File transfer).


### Time handling

| Requests                                                                                                                            | Responses                                                                                                                       |
|:------------------------------------------------------------------------------------------------------------------------------------|:--------------------------------------------------------------------------------------------------------------------------------|
| Set date and time ([0x83](@ref accerion::api::CommandId::CMD_SET_DATE_TIME)) as [DateTime](@ref accerion::api::DateTime::Serialize) | Acknowledgement ([0x48](@ref accerion::api::CommandId::ACK_SET_DATE_TIME)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Get date and time ([0x4A](@ref accerion::api::CommandId::CMD_GET_DATE_TIME)) as empty-body request | Date and time ([0x4B](@ref accerion::api::CommandId::ACK_GET_DATE_TIME)) as [DateTime](@ref accerion::api::DateTime::Serialize) |


### Searching for loop closures
| Message type | Message                                                                                                                             |
|:-------------|:------------------------------------------------------------------------------------------------------------------------------------|
| Intermittent | Loop Closure ([0x66](@ref accerion::api::CommandId::ACK_LOOP_CLOSURE)) as [LoopClosure](@ref accerion::api::LoopClosure::Serialize) |
| Intermittent | Search Complete ([0x67](@ref accerion::api::CommandId::ACK_SEARCH_LOOP_CLOSURES_COMPLETE)) as empty body                            |

| Requests                                                                                                                                            | Responses                                                                                                                                              |
|:----------------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------|
| Start Search ([0x63](@ref accerion::api::CommandId::CMD_START_SEARCH_LOOP_CLOSURES)) as [SearchRadius](@ref accerion::api::SearchRadius::Serialize) | Acknowledgement ([0x65](@ref accerion::api::CommandId::ACK_SEARCH_LOOP_CLOSURES)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
| Stop Search ([0x64](@ref accerion::api::CommandId::CMD_STOP_SEARCH_LOOP_CLOSURES)) as empty-body request                                            | Acknowledgement ([0x65](@ref accerion::api::CommandId::ACK_SEARCH_LOOP_CLOSURES)) as [Acknowledgement](@ref accerion::api::Acknowledgement::Serialize) |
