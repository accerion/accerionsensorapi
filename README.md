# AccerionSensorAPI
This C++ wrapper/API can be used for communicating with Accerion sensors. It is designed to ease the integration and usage of the sensors.
With only minimal lines of code you can already start receiving messages from a sensor.

This document explains how to:
- [Build the Doxygen Documentation] (#documentation)
- [What is the structure of the repository](#structure)
- [Build/compile (and install) the API library](#build)
- [Use the API library in your C++ project](#usage)
- [Threading Behaviour](#threading)

More information on sensor communication using the raw protocol can be found on the dedicated [Protocol page](docs/protocol.md).

<a name="documentation"></a>
## Building the Doxygen Documentation

This repository is documented using Doxygen comments to provide clear and structured documentation for the codebase. To generate the documentation, follow these steps:

1. **Install Doxygen**  
   You can download and install Doxygen from the official website:  
   [https://www.doxygen.nl/download.html](https://www.doxygen.nl/download.html)

   - On Windows, you can download the installer and follow the installation instructions.
   - On Linux, you can install it via your package manager (e.g., `sudo apt install doxygen` for Debian-based systems).
2. **Install Graphviz**  
   Doxygen supports generating visual diagrams (e.g., call graphs, dependency graphs) using **Graphviz**. To enable this feature, you must install Graphviz on your system.

   - **Official Website**: [https://graphviz.gitlab.io/download/](https://graphviz.gitlab.io/download/)
   - **Linux (Debian/Ubuntu)**: Run the following command to install Graphviz:
     ```bash
     sudo apt install graphviz
     ```
3. **Generate the Documentation**  
   Once Doxygen is installed, navigate to the root directory of this repository. You should find a `Doxyfile` configuration file. This file contains the necessary settings to generate the documentation.

   Run the following command in your terminal:

   ```bash
   doxygen Doxyfile
   ```
  
   Doxygen will then process the source code and create the documentation, which can be found in the `~/docs/generated/html` directory, where `~` indicates the root of this repository.
4. **View the Documentation**  
After Doxygen completes the process, open the `index.html` file found in the folder mentioned above (in a web browser of your choice) to view the generated documentation.

<a name="structure"></a>
## Repository structure
### Dependencies
- **MessagePack (`3rdparty/msgpack-c`)** - Used by the AccerionSensorAPI. Is part of this repository as a git submodule.
  It will be built as part of the API, so no additional actions are required, other than explained in the [build](#build) process.
- **GoogleTest** - Used by the unit tests only. Will be downloaded and built as part of the unit test builds.
  No additional actions are required.

### Branches
The repository has two main branches:
- master: The latest released code. Releases and tags can be found in the repository corresponding to the software releases.
- development: The development branch on which continuous development is done by Accerion and other contributors.

It should be clear that the master branch should be used by customers. More specifically, the release version of the API
should match that of the sensor firmware version. The development branch should only be used for future feature testing
and in close collaboration with Accerion.


<a name="build"></a>
## Building and installing the API
The API can be compiled on the following OSs:

- Ubuntu 16.04 (amd64 architecture)
- Ubuntu 18.04 (amd64 architecture)
- Ubuntu 20.04 (amd64 architecture)
- (not preferred) Windows 10

Other operating systems, distributions, and architectures are not officially tested or supported. However, the API is designed to be lightweight, 
relying on minimal external libraries. As a result, it is likely to work on other platforms, 
provided you can compile C++ and have access to either `arpa/inet` or `winsock/ws2tcpip` libraries.

To compile the AccerionSensorAPI, follow these steps
```bash
git submodule update --init
mkdir build && cd build
cmake ..
make AccerionSensorAPI -j
```

Enable the `UNIT_TESTS` option in order to build `AccerionSensorAPIUnitTests`:
```bash
cmake .. -DUNIT_TESTS=ON
make AccerionSensorAPIUnitTests -j
```
All executables are located in the `bin` folder within the build directory.

### Creating a package
In order to create a '.deb' package run the `cpack` command from the build directory.

### Installing the AccerionSensorAPI library
To install the API library directly from the built targets, in addition to the build steps, also the following step needs to be executed
```bash
sudo make install
```
Note that only the AccerionSensorAPI library and its required dependencies are installed, 
as the installation rules are only defined for this target.

<a name="usage"></a>
## How to use the API
### Add AccerionSensorAPI into a project with CMake

**To build the AccerionSensorAPI as part of your project**, first clone the repository, 
then add the following to your `CMakeLists.txt`:
```cmake
add_subdirectory(accerionsensorapi)
target_link_libraries(YourApplication PRIVATE AccerionSensorAPI)
```

Afterward, you can include the necessary header files in your source code as follows:
```cpp
#include "AccerionSensorAPI/sensor.h"
#include "AccerionSensorAPI/update_service.h"
#include "AccerionSensorAPI/connection_manager.h"
```

**If the AccerionSensorAPI is already installed as a system library** add the following to your `CMakeList.txt`:
```cmake
find_package(AccerionSensorAPI 7.0.0 REQUIRED)
target_link_libraries(YourApplication PRIVATE AccerionSensorAPI)
```

Then, include the header files in your code like this:
```cpp
#include <AccerionSensorAPI/sensor.h>
#include <AccerionSensorAPI/update_service.h>
#include <AccerionSensorAPI/connection_manager.h>
```

All classes, functions and variables are in the namespace `accerion::api`, but it will be omitted in the rest of this document for readability.

### Connect to the sensor
When you know that a sensor with a certain ip address and serial number is available, then you can directly create a connection to it via:
```cpp
std::unique_ptr<Sensor> sensor = MakeUnique<Sensor>(ip_address, serial_number, connection_type, local_ip);
std::unique_ptr<UpdateService> update_service = MakeUnique<UpdateService>(ip_address, serial_number);
``` 
Note that `std::make_unique` can be used instead of `MakeUnique` in C++14 and higher.

The last two connection parameters to create a Sensor can be used to configure the preferred connection method and optionally the local ip address in case of UDP communication.

Because the Sensor and UpdateService are not copyable or movable, the sensor/update service instance is stored as a unique pointer.
In that way the instance can still be moved between components in your code.
In case multiple components need to access an instance, then the unique pointer can easily be converted to a shared pointer, by doing:
```cpp
std::shared_ptr<Sensor> shared_ptr_sensor = MakeUnique<Sensor>(...);
```
or:
```cpp
auto unique_ptr_sensor = MakeUnique<Sensor>(...);
std::shared_ptr<Sensor> shared_ptr_sensor = std::move(unique_ptr_sensor);
```
Note that direct instantiation might crash if some input arguments are wrong, or when the sensor is not available.
With the `ConnectionManager` it is will be more convenient to connect with a sensor:
```cpp
const std::chrono::seconds time_to_wait_for_connection{3};

auto& future_sensor = ConnectionManager::GetSensor(ip_address, connection_type, local_ip);
std::unique_ptr<Sensor> sensor = future_sensor.WaitFor(time_to_wait_for_connection);

auto& future_sensor = ConnectionManager::GetSensor(serial_number, connection_type, local_ip);
std::unique_ptr<Sensor> sensor = future_sensor.WaitFor(time_to_wait_for_connection);

auto& future_update_service = ConnectionManager::GetUpdateService(Address ip);
std::unique_ptr<UpdateService> update_service = future_update_service.WaitFor(time_to_wait_for_connection);

auto& future_update_service = ConnectionManager::GetUpdateService(SerialNumber serial);
std::unique_ptr<UpdateService> update_service = future_update_service.WaitFor(time_to_wait_for_connection);
```
In this way, the manager will retry connecting with a sensor, if creating a connection fails for some reason.
Only when the manager succeeds, then the future will be filled.
The future object can be used to wait for the connection (`WaitFor`) or get notified when the object becomes available (`Callback`).
Note that when using `WaitFor`, the user will need to check that the received pointer is not the `nullptr` in case of a time out.
This does not hold when registering a callback via `Callback`, because it will not be called when there is no connection.

More information on the `Future` class is given in the [threading section](#threading).

Lastly, when you do not know which sensors are available, then the following can be used:
```cpp
std::vector<SensorDetails> detected_sensors = GetDetectedSensors();
```
It will return a list of sensors seen in the last 2 seconds.
Using this information, a sensor can be selected and a connection can be made using one of the methods discussed so far.

The `ConnectionManager` contains two threads listening to sensor and update service heartbeat messages.
In case the manager is not needed any more, then it can be stopped by executing: 
```cpp
ConnectionManager::Shutdown();
```
Note that the ConnectionManager becomes unusable after this action and only direct connections can be created (directly creating `Sensor` and `UpdateService` instances).


### Communicate with the sensor

The API features two main classes for sensor communication:
 * `Sensor`: Primarily used for data inputs and outputs, facilitating core communication with the sensor.
 * `UpdateService`: Designed for maintenance tasks such as software/firmware updates and log retrieval.

With a `Sensor` (or `UpdateService`) instance you can start communicating with the sensor, listening to messages,
sending requests and await responses.
Except for file transfer, all actions that can be done with a `Sensor`/`UpdateService` instance will use a `Future` object.
For more information on the Future object, check the next section.

Regarding thread-safety, in principle the `Sensor`, `UpdateService` and `ConnectionManager` objects are not thread-safe. 
Although features involving `Future`s are fairly isolated, so different features could be used in a multithreaded way.
(For example, it would be fine to request the areas overview, while turning on localization mode.)

In case of multiple connections (`Sensor` instances) with the same physical sensor, performing naturally conflicting
requests simultaneously could lead to undefined behaviour, resulting in a sensor with an unknown state.


<a name="threading"></a>
## Threading Behaviour

### Threads
The AccerionSensorAPI employs multiple instance of the generic `CommunicationThread`.
This thread does all communication using a given client (UDP or TCP), so receiving and sending messages from/to a given port.
It runs at a maximum of 300 Hz.

The following classes are responsible for spawning threads:
 * `ConnectionManager`:
    * Spawns two communication threads, each containing a UDP client, only listening to `HeartBeat` messages of the sensor and the update service, respectively.
    * Communication threads run until the `Shutdown` method is invoked or the instance is destructed.
 * `Sensor`:
    * Spawns a communication thread, receiving and sending messages using a TCP client.
    * Only during file transfers, one additional thread (using a TCP client) is started and stopped when finished the transfer is done.
      This thread is unlimited in throughput and processes the file transfer messages as fast as possible. 
    * The communication thread runs until the instance is destructed.
 * `UpdateService`:
    * Has exactly the same threading behaviour as a `Sensor` instance.
    

### Response handling of API requests
When a request is made using the API, a `accerion::api::Future` instance is returned. For example
```cpp
Future<Acknowledgement>& future = sensor.SetMappingMode(true, 5);
```
This line of code will:
- Clear the data potentially already stored internally in the Future object, that is related to the request made (more on this later)
- Send the request to start mapping
- Return a reference to the aforementioned Future object

With this `Future`, the user can basically do three things:
- _Ignore the `future`._ <br>
  In this case, only the request will be sent and the response is not of interest to the user.
- _The `WaitFor` method of the future is called._ <br>
  The implementation mimics the usage of the `std::future::wait_for` and so it has a custom timeout (3 seconds by default) and a condition function as parameters.
  It will block the caller code, which is useful when the result is needed immediately.
  The returned object is an `accerion::api::Optional` of the data of interest.
  The optional is filled if a response is received in time, but empty if the timeout duration has passed without receiving any response.
  Lastly, if desired, a data condition function can be supplied.
  In that case, all responses that do not satisfy the condition function are ignored.
- _The `Callback` method of the future is called with the desired callback._ <br>
  By registering a callback, the user will be notified through that function when a response is received.
  Hence, the caller code will not be blocked in this case.
  This is useful for sending a request without requiring the result immediately.

In summary, with a `Future` instance the user can control the program's execution flow.
`WaitFor` calls will pause the execution until completion.
Not calling any future method or using the `Callback` method allows the program to continue.

The future is implemented in such a way that if the response was received before a callback was set or the data is requested,
then still the data is saved internally. When `WaitFor` or `Callback` is called afterwards, then immediately the data is returned or the callback is called.
In that sense, there cannot be a race condition, as in, it is not required to call `WaitFor` or `Callback` before making a request.
However, in a future, responses are not queued internally, only the last one is kept.
When multiple responses are received and afterwards a call to `WaitFor` or `Callback` is made, then the latest response will be used.

In the following some more details will be outlined on how a `Future` works and how it should be used.

Imagine internal mode is on and you do the following:
```cpp
sensor.SetCorrectedOdometry(pose_1);
auto& future = sensor.SubscribeToCorrectedOdometry();
sensor.SetCorrectedOdometry(pose_2);
const auto pose = future.WaitFor().value();  // Assuming no timeout occurred
```
What will be content of `pose`? Will it contain `pose_1` or `pose_2`? Answer is that it could be either of the two.
If the response for the first pose request is already received, but the second not yet, then `pose` will be equal to `pose_1`.
If the response for the second pose request is also already received, it would overwrite the first and `pose` would be equal to `pose_2`.
The future is only reset when it is returned to the user, which deletes any data that was received already.
To avoid this race condition, the following will work:
```cpp
sensor.SetCorrectedOdometry(pose_1);
auto pose = sensor.SubscribeToCorrectedOdometry().WaitFor().value();  // Assuming no timeout occurred
sensor.SetCorrectedOdometry(pose_2);
pose = sensor.SubscribeToCorrectedOdometry().WaitFor().value();  // Assuming no timeout occurred
```
Now the two `pose` instances will be equal to `pose_1` and `pose_2` respectively.
So, everytime a `Future` instance is returned by the API, its state is reset, deleting any response that was already received.

Note that callbacks are not reset and should be explicitly unset when appropriate.
To clarify, in the following snippet the callback is set until the end of the code is reached, resulting in a crash:
```cpp
{
    std::atomic_int counter{0};
    auto &future = sensor.SetLocalizationMode(true);
    future.Callback([&counter](const auto&) { ++counter; });
    sensor.SetLocalizationMode(false);  // does not reset the callback
}  // counter gets destroyed as it gets out of scope
sensor.SetLocalizationMode(true);  // crash
```
When the response from the last set mode action is received, the callback is still set and called, resulting in a crash, as the atomic counter is deleted at that point.
To avoid this (as in general with callbacks), make sure that the lifetime of variables used by callbacks are guaranteed or unset the callback by calling `Callback({})` or `Callback(nullptr)` on the relevant future.

## Backwards Compatibility

We adhere to Semantic Versioning for our software releases. Our version numbers follow the pattern: `MAJOR.MINOR.PATCH`.

With semantic versioning, the following can be expected:
- **MAJOR**: Incremented when we introduce backwards-incompatible changes. This may require you to make changes to your code. Some examples are;
    - A function signature was changed.
    - A struct's member has been removed
- **MINOR**: Incremented when we add new features or functionality in a backwards-compatible manner. For instance:
    - A new feature is released with completely new functions and structs
    - A member is added to a struct, a default will be provided to maintain compatibility
- **PATCH**: Incremented when we make backwards-compatible bug fixes or minor improvements. An example:
    - A line in a .cpp file is changed in order to fix a bug

Behavioural changes of the Triton software would always be backwards incompatible.
Therefore only with a major increment behaviour is allowed to change as it could require changes on the user side.
An example would be a situation in which certain modes are no longer compatible with each other.
An important detail is that we limit the definitions above to the **public interface**.

### Public Interface

The public interface is defined by the following header files:

- `connection_manager.h`
- `sensor.h`
- `update_service.h`
- `utils/sensor.h`
- All types referred to in the interfaces of the classes listed above

Anything defined within these files is considered part of the interface. This means structs and their members as well as classes and their methods and public members.
This means that for **MINOR** and **PATCH** increments, these things are unchanged. Only when there is a **MAJOR** increment, the interface might be affected resulting in you having to update parts of your code.
