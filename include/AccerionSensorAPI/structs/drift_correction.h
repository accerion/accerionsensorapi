/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/signature.h"
#include "AccerionSensorAPI/structs/standard_deviation.h"
#include "AccerionSensorAPI/structs/time.h"

namespace accerion {
namespace api {

struct DriftCorrection {
    static DriftCorrection Deserialize(const RawData& data);
    RawData Serialize() const;

    TimePoint timestamp;
    Pose pose;
    Pose delta;
    double cumulative_travelled_distance;  //!< meter
    double cumulative_travelled_heading;   //!< degree
    double error_percentage;
    SignatureId signature_id;
    ClusterId cluster_id;
    std::uint8_t quality_estimate;
    StandardDeviation standard_deviation;
};

}  // namespace api
}  // namespace accerion
