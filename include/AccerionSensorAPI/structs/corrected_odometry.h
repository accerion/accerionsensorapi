/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/standard_deviation.h"
#include "AccerionSensorAPI/structs/time.h"

namespace accerion {
namespace api {

struct CorrectedOdometry {
    static CorrectedOdometry Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    TimePoint timestamp;
    Pose pose;
    StandardDeviation standard_deviation;
};

}  // namespace api
}  // namespace accerion
