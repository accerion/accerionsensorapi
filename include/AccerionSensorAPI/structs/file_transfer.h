/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <map>
#include <string>

#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/utils/enum.h"

namespace accerion {
namespace api {

enum class TransferType : std::uint8_t {
    // 0 - 128 upload
    kUploadLegacyAmf    = 0x01,  //!< 0x01
    kUploadUpdate       = 0x02,  //!< 0x02
    kUploadMap          = 0x03,  //!< 0x03

    // 129 - 256 download
    kDownloadMap        = 0x82,  //!< 0x82
    kDownloadRecordings = 0x83,  //!< 0x83
    kDownloadLogs       = 0x84   //!< 0x84
};

const std::map<TransferType, uint64_t> TransferSize {
    {TransferType::kUploadLegacyAmf, 1e6},
    {TransferType::kUploadUpdate, 1e6},
    {TransferType::kDownloadMap, 50e6},
    {TransferType::kUploadMap, 50e6}
};

// Return true if type is upload, otherwise return false for download
bool IsUpload(const TransferType& type);

enum class TransferStatus : uint8_t {
    kAvailable                 = 0x00,  //!< 0x00
    kTransferAlreadyInProgress = 0x01,  //!< 0x01
    kTransferFinished          = 0x02,  //!< 0x02
    kFileDoesNotExist          = 0x03,  //!< 0x03
    kFailed                    = 0x04,  //!< 0x04
    kUnsupported               = 0x05   //!< 0x05
};

const std::map<TransferStatus, std::string> TransferStatusMessage {
        {TransferStatus::kAvailable, ""},
        {TransferStatus::kTransferAlreadyInProgress, "File transfer not available as there is one already in progress"},
        {TransferStatus::kTransferFinished, ""},
        {TransferStatus::kFileDoesNotExist, "File does not exist"},
        {TransferStatus::kFailed, "File transfer failed"},
        {TransferStatus::kUnsupported, "File transfer is not supported, i.e. because writing operations are performed on the map"}
};

// Defines the different possible transfer errors
enum class TransferResult : uint8_t {
    kSuccessfulSendMap   = 0x00,  //!< 0x00
    kUnsupported         = 0x01,  //!< 0x01
    kInProgress          = 0x02,  //!< 0x02, another task is already in progress
    kFileDoesNotExist    = 0x03,  //!< 0x03
    kInvalidHeader       = 0x04,  //!< 0x04
    kProcessingError     = 0x05,  //!< 0x05
    kTransferError       = 0x06,  //!< 0x06
    kFailedGetRecordings = 0x07  //!< 0x07
};

const std::map<TransferResult, std::string> TransferResultMsg {
    {TransferResult::kSuccessfulSendMap, "Map was successfully sent and processed"},
    {TransferResult::kUnsupported, "Unsupported task was selected"},
    {TransferResult::kInProgress, "A task was already in progress"},
    {TransferResult::kFileDoesNotExist, "File does not seem to exist or is empty"},
    {TransferResult::kInvalidHeader, "Could not read file header"},
    {TransferResult::kProcessingError, "An error occurred while processing the file"},
    {TransferResult::kTransferError, "An error occurred while transferring the file"},
    {TransferResult::kFailedGetRecordings, "An error occurred while gathering the recordings"}
};

struct FileUploadDownloadRequest {
    static FileUploadDownloadRequest Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    TransferType transfer_type;
    std::uint64_t file_size;
    std::uint64_t chunk_size;  // UPLOAD ONLY
};

struct FileTransferHandshake {
    static FileTransferHandshake Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    TransferStatus transfer_status;  // available or not
    TransferType transfer_type;
    std::uint16_t port_number;
    std::uint64_t file_size;
};

}  // namespace api
}  // namespace accerion
