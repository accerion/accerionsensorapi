/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>
#include <vector>

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/signature.h"

namespace accerion {
namespace api {

struct LoopClosure {
    static LoopClosure Deserialize(const RawData& data);
    RawData Serialize() const;

    /**
     \brief All loop closures will be written to a file in g2o format
    */
    static bool WriteLoopClosuresToFile(const std::vector<LoopClosure>& loop_closures, const std::string& path);

    SignatureId id_a;
    SignatureId id_b;
    Pose pose;
};

}  // namespace api
}  // namespace accerion
