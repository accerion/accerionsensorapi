/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/covariance.h"
#include "AccerionSensorAPI/structs/pose.h"

namespace accerion {
namespace api {

class PoseEstimate {
 public:
    static PoseEstimate Identity();

    bool operator==(const PoseEstimate& other) const {
        return pose == other.pose && cov == other.cov;
    }

    PoseEstimate Interpolate(double ratio, const PoseEstimate& p) const;

    accerion::api::Pose pose;
    accerion::api::Covariance cov;
};

}  // namespace api
}  // namespace accerion
