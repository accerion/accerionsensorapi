/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <chrono>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

using Clock = std::chrono::system_clock;
using TimePoint = Clock::time_point;

struct Time {
    static Time Deserialize(const RawData& data);
    RawData Serialize() const;

    TimePoint value;
};

}  // namespace api
}  // namespace accerion
