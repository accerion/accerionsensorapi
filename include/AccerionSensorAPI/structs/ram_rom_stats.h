/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>
#include <vector>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

enum class DirType : std::uint8_t {
    kCompleteMap = 0,  //!< 0x00
    kArea = 1,         //!< 0x01
    kRecordings = 2,   //!< 0x02
    kRecording = 3,    //!< 0x03
    kSdOther = 4,      //!< 0x04
    kLogs = 5          //!< 0x05
};

struct DirSizePart {
    static DirSizePart Deserialize(const RawData& data);
    RawData Serialize() const;

    DirType type;
    std::uint32_t size_in_mb;
    std::string name;
};

struct RamRomStats {
    static RamRomStats Deserialize(const RawData& data);
    RawData Serialize() const;

    std::uint32_t rom_available;
    std::uint32_t rom_total;
    std::uint32_t sd_available;
    std::uint32_t sd_total;
    std::uint16_t ram_used;
    std::uint16_t ram_total;
    std::vector<DirSizePart> parts;
};

}  // namespace api
}  // namespace accerion

