/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/address.h"

namespace accerion {
namespace api {

enum class UdpStrategy : std::int8_t {
    kUntouched      = -1,  //!< invalid value in protocol, as enumeration value is encoded as std::uint8_t
    kDisabled       =  0,  //!< 0x00
    kBroadcast      =  1,  //!< 0x01
    kUnicast        =  2,  //!< 0x02
    kUnicastNoHB    =  3   //!< 0x03
};

struct UdpSettings {
    RawData Serialize() const;
    static UdpSettings Deserialize(const RawData& data);

    bool IsValid() const;

    bool operator==(const UdpSettings &settings) const;

    Address address;
    EnabledMessageType message_type;
    UdpStrategy strategy;
};

}  // namespace api
}  // namespace accerion
