/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

struct DateTime {
    static DateTime Deserialize(const RawData& data);
    RawData Serialize() const;

    bool operator==(const DateTime& other) const;

    std::uint8_t day;
    std::uint8_t month;
    std::uint16_t year;
    std::uint8_t hours;
    std::uint8_t minutes;
    std::uint8_t seconds;
};

}  // namespace api
}  // namespace accerion
