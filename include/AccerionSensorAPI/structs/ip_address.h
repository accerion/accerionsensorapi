/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/address.h"

namespace accerion {
namespace api {

struct IpAddress {
    static IpAddress Deserialize(const RawData& data);
    RawData Serialize() const;

    bool operator==(const IpAddress& other) const;
    bool operator!=(const IpAddress& other) const;

    Address address;
    Address netmask;
    Address gateway;
};

}  // namespace api
}  // namespace accerion
