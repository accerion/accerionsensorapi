/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

struct Acknowledgement {
    static Acknowledgement Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    bool accepted;
    std::string message;
};

}  // namespace api
}  // namespace accerion
