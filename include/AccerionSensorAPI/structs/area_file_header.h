/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>
#include <map>

#include "msgpack.hpp"

#include "AccerionSensorAPI/structs/signature.h"
#include "AccerionSensorAPI/structs/version.h"


namespace accerion {
namespace api {

constexpr auto kFileSignatureIdentifier = "ACCAF";

struct [[deprecated]] AreaOverview {
    std::uint32_t area_id;
    std::uint32_t total_clusters;
    std::uint32_t total_signatures;
    std::map<accerion::api::ClusterId, std::size_t> cluster_details;  //!< Cluster id and signature count per cluster relation

    MSGPACK_DEFINE_MAP(
        // Use custom map keys for backwards-compatability with
        // previous defined mapping of member variables
        // (https://github.com/msgpack/msgpack-c/wiki/v2_0_cpp_adaptor#since-210)
        MSGPACK_NVP("areaId", area_id),
        MSGPACK_NVP("totalClusters", total_clusters),
        MSGPACK_NVP("totalSignatures", total_signatures),
        MSGPACK_NVP("clusterDetails", cluster_details));

    friend std::ostream& operator<<(std::ostream& ss, const AreaOverview& ao);
};

struct [[deprecated]] FileSignature {
    std::string identifier = kFileSignatureIdentifier;
    uint32_t header_offset = 1000000;
    MSGPACK_DEFINE_ARRAY(identifier, header_offset);

    static FileSignature Legacy();
};

struct FileHeaderUnpackResult;  // Forward declaration
struct [[deprecated]] FileHeader {
    Version software_version;
    Version database_version;
    AreaOverview area_overview;
    uint64_t chunk_size = 0;
    MSGPACK_DEFINE_MAP(
        MSGPACK_NVP("softwareVersion", software_version),
        MSGPACK_NVP("databaseVersion", database_version),
        MSGPACK_NVP("areaOverview", area_overview),
        MSGPACK_NVP("chunkSize", chunk_size));

    // msgpack library variants of (de)serializing
    std::string Pack() const;
    static FileHeaderUnpackResult Unpack(const std::string& path);

    friend std::ostream& operator<<(std::ostream& ss, const FileHeader& fh);
};

struct [[deprecated]] FileHeaderUnpackResult {
    bool is_valid{true};
    int offset{0};
    FileHeader header{};
};

}  // namespace api
}  // namespace accerion
