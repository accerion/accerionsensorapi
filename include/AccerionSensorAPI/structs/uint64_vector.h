/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <vector>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

struct UInt64Vector {
    static UInt64Vector Deserialize(const RawData& data);
    RawData Serialize() const;

    std::vector<std::uint64_t> values;
};

}  // namespace api
}  // namespace accerion

