/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>

#include "AccerionSensorAPI/structs/pose.h"
#include "AccerionSensorAPI/structs/time.h"

namespace accerion {
namespace api {

enum class MarkerType : std::uint8_t {
    kAruco,               //!< 0x00
    kDataMatrixMultiCode  //!< 0x01, not yet implemented on sensor side
};

struct MarkerDetectionMode{
    static MarkerDetectionMode Deserialize(const RawData& data);
    RawData Serialize() const;

    bool mode_state;
    MarkerType type;
};

struct MarkerDetection {
    static MarkerDetection Deserialize(const RawData& data);
    RawData Serialize() const;

    TimePoint timestamp;
    MarkerType type;
    std::string id;
    Pose pose;  // pose of marker detected, in Triton coordinate system
};

}  // namespace api
}  // namespace accerion
