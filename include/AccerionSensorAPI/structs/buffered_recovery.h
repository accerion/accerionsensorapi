/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

enum class BufferedRecoveryMessageType : uint8_t {
    kProgress       = 1,  //!< 0x01
    kFinished       = 2,  //!< 0x02
    kCanceled       = 3,  //!< 0x03
    kLowOverlap     = 4,  //!< 0x04
    kNoMap          = 5,  //!< 0x05
    kNoSignatures   = 6,  //!< 0x06
    kEmpty          = 7   //!< 0x07
};

struct BufferLength {
 public:
    static BufferLength Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    float buffer_length;
};

struct BufferProgress {
    static BufferProgress Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    BufferedRecoveryMessageType message_type;
    bool result;  // value valid if messageType = 0x02
    std::uint8_t progress;   // valid if messageType = 0x01
    std::uint8_t percentage_of_low_overlap;  // valid if messageType = 0x04
};

struct BufferedRecoveryRequest {
    static BufferedRecoveryRequest Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    double x_pos;
    double y_pos;
    double radius;
    bool full_map_search;
};

}  // namespace api
}  // namespace accerion
