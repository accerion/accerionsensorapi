/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

struct PlainUInt64 {
    static PlainUInt64 Parse(const std::string& s);

    static PlainUInt64 Deserialize(RawData raw_data);

    RawData Serialize() const;

    bool operator==(const PlainUInt64& other) const;
    bool operator!=(const PlainUInt64& other) const;

    std::uint64_t value;
};

}  // namespace api
}  // namespace accerion
