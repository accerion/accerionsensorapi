/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstdint>
#include <string>

#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/type_def.h"

namespace accerion {
namespace api {

struct Address {
    // Parse an ip address from a string formatted like "192.168.2.123"
    static Address Parse(const std::string &ip_string);

    static Address Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    in_addr Convert() const;
    static Address From(const std::uint32_t& other);
    static Address From(const in_addr& in_addr);

    bool operator==(const Address& other) const;
    bool operator!=(const Address& other) const;
    bool operator<(const Address& other) const;

    friend std::ostream& operator<<(std::ostream& os, const Address& address);
    // Get string of address formatted like "192.168.  0.  1"
    std::string AlignedString() const;

    std::uint8_t first;
    std::uint8_t second;
    std::uint8_t third;
    std::uint8_t fourth;
};

}  // namespace api
}  // namespace accerion
