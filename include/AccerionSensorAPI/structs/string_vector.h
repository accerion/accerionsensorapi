/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <limits>
#include <string>
#include <vector>

#include "AccerionSensorAPI/structs/plain_string.h"

namespace accerion {
namespace api {

struct StringVector {
    static StringVector Deserialize(RawData raw_data);
    RawData Serialize() const;

    std::vector<std::string> value;
};

}  // namespace api
}  // namespace accerion

