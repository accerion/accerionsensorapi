/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

struct PlainUInt32 {
    static PlainUInt32 Parse(const std::string& s);

    static PlainUInt32 Deserialize(RawData raw_data);
    RawData Serialize() const;

    bool operator==(const PlainUInt32& other) const;
    bool operator!=(const PlainUInt32& other) const;
    bool operator<(const PlainUInt32& other) const;

    std::uint32_t value;
};

using SerialNumber = PlainUInt32;

}  // namespace api
}  // namespace accerion
