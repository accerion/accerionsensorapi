/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/pose.h"

namespace accerion {
namespace api {

struct SearchRange {
    static SearchRange Deserialize(const RawData& data);
    RawData Serialize() const;

    double radius;  // in meter (value should be positive
    double angle;  // in degree (value should be positive
};

}  // namespace api
}  // namespace accerion
