/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <ostream>
#include <map>

#include "AccerionSensorAPI/structs/version.h"
#include "AccerionSensorAPI/structs/signature.h"

namespace accerion {
namespace api {

struct MapSummary {
    static MapSummary Deserialize(const RawData& data);
    RawData Serialize() const;

    std::size_t CalculateClusterCount() const;
    std::size_t CalculateSignatureCount() const;

    friend std::ostream& operator<<(std::ostream& ss, const MapSummary& fh);

    Version database_version;
    std::map<accerion::api::ClusterId, std::size_t> cluster_details;  //!< Cluster id and respective signature count
};

}  // namespace api
}  // namespace accerion
