/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <map>
#include <string>

#include "AccerionSensorAPI/structs/time.h"

namespace accerion {
namespace api {

struct Diagnostics {
    static Diagnostics Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    friend std::ostream& operator<<(std::ostream& os, const Diagnostics& diagnostics);

    TimePoint timestamp;
    std::uint16_t modes;
    std::uint16_t warning_codes;
    std::uint32_t error_codes;
};

/**
 * @brief Enum values for different possible errors encountered during sensor operation
 */
enum class Errors : std::uint8_t {
    kErrorLicense               = 0,
    kErrorNoSdCard              = 1,
    kErrorSdCardNotMounted      = 2,
    kErrorKernelPatchNotApplied = 3,
    kErrorCorruptedMap          = 4,
    kMaxNumberOfErrors,
};

/**
 * @brief Enum values for different possible warnings 
 */
enum class Warnings : std::uint8_t {
    kWarningTooFastMapping                  = 0,
    kWarningLowThroughputRead               = 1,
    kWarningLowThroughputTrack              = 2,
    kWarningLowThroughputMain               = 3,
    kWarningLowMemory                       = 4,
    kWarningLowBrightness                   = 5,
    kWarningHighBrightness                  = 6,
    kWarningTrackingFailed                  = 7,
    kWarningInfrequentOrNoExternalReference = 8,
    kMaxNumberOfWarnings,
};

/**
 * @brief Enum values to represent different modes of operation of the sensor
 */
enum class Modes : std::uint8_t {
    kModeIdle                           = 0,
    kModeMapping                        = 1,
    kModeInternal                       = 2,
    kModeLocalization                   = 3,
    kModeRecording                      = 4,
    kModeAruco                          = 5,
    kModeExpert                         = 6,
    kModeLineFollowing                  = 7,
    kModeBufferedRecovery               = 8,
    kModeContinuousSignatureUpdating    = 9,
    kModeDataMatrixMultiCode            = 10,
    kMaxNumberOfModes,
};

/// Map relating the bit set in the modes field in the diagnostics with the mode name
const std::map<std::uint8_t, std::string> kModeDescriptionMap {
    {0, "idle"},
    {1, "mapping"},
    {2, "internal"},
    {3, "localization"},
    {4, "recording raw data"},
    {5, "aruco marker detection"},
    {6, "expert"},
    {7, "line following"},
    {8, "buffered pose recovery"},
    {9, "continuous signature updating"},
    {10, "multi code detection"},
};

/// Map relating the bit set in the warnings field in the diagnostics with the warning message
const std::map<std::uint8_t, std::string> kWarningMap{
    {0, "Too Fast For Mapping"},
    {1, "Low Throughput Read"},
    {2, "Low Throughput Track"},
    {3, "Low Throughput Main"},
    {4, "Low Memory"},
    {5, "Low Brightness Cam"},
    {6, "High Brightness Cam"},
    {7, "Tracking Failed"},
    {8, "Infrequent or no external reference input"}
};

/// Map relating the bit set in the errors field in the diagnostics with the error message
const std::map<std::uint8_t, std::string> kErrorMap{
    {0, "License error"},
    {1, "No SD card error"},
    {2, "SD card not mounted error"},
    {3, "Kernel patch error"},
    {4, "Corrupted map error"},
};

}  // namespace api
}  // namespace accerion
