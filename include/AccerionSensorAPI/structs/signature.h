/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/pose.h"

namespace accerion {
namespace api {

using SignatureId = std::uint64_t;
using ClusterId = std::uint64_t;

struct Signature {
    static Signature Deserialize(const RawData& data);
    RawData Serialize() const;

    bool operator==(const Signature& other) const;

    Pose pose;
    ClusterId cluster_id;
    SignatureId signature_id;
};

}  // namespace api
}  // namespace accerion
