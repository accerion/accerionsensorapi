/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

enum class FileTransferTask : std::uint8_t {
    kReplaceMap             = 0,  //!< 0x00
    kMergeMap               = 1,  //!< 0x01
    kUpdateMap              = 2,  //!< 0x02
    kUpdateSensor           = 3,  //!< 0x03
    kGatherLogs             = 4,  //!< 0x04
    kGatherRecordings       = 6  //!< 0x06
};

enum class FileTransferTaskStatus : std::uint8_t {
    kTaskProgress   = 0,  //!< 0x00
    kTaskSuccess    = 1,  //!< 0x01
    kTaskFail       = 2   //!< 0x02
};

struct FileTransferTaskResult {
    static FileTransferTaskResult Deserialize(const RawData& data);
    RawData Serialize() const;

    FileTransferTask task;
    FileTransferTaskStatus status;
    std::uint8_t progress;  // Only filled in case of status == kTaskProgress
    std::string message;  // Only filled in case of status == kTaskFail
};

struct FileTransferTaskRequest {
    static FileTransferTaskRequest Deserialize(RawData data);
    RawData Serialize() const;

    FileTransferTask task;
    RawData data;
};

}  // namespace api
}  // namespace accerion

