/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/version.h"

namespace accerion {
namespace api {

/**
 * \brief HeartBeat struct as received through the HeartBeat message.
 *  The HeartBeat is sent by the sensor or updateservice to indicate it is live.
 *  It contains the IP and unicast address
 */
struct HeartBeat {
    static HeartBeat Deserialize(const RawData& data);
    RawData Serialize() const;
    
    Address ip_address;
    Address unicast_address;
    Version software_version;
};

}  // namespace api
}  // namespace accerion
