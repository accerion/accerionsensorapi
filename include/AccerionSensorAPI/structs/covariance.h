/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

namespace accerion {
namespace api {

struct Covariance {
    bool operator==(const Covariance &other) const;
    bool operator!=(const Covariance &other) const;

    Covariance Interpolate(double ratio, const Covariance& other) const;

    double x_x;
    double y_y;
    double th_th;
};

}  // namespace api
}  // namespace accerion
