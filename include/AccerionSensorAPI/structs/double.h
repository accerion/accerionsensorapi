/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <ratio>

#include "AccerionSensorAPI/structs/serialization.h"

namespace accerion {
namespace api {

// The template parameters represent
//   - Type: the type which the double should be serialized as
//   - Ratio: the std::ratio specialization in which the value should be stored
template <class Type, typename Ratio>
struct Double {
    static_assert(Ratio::num == 1, "Expecting a std::ratio specialization with a numerator equal to 1");

    static Double Deserialize(const RawData& data) {
        return {static_cast<double>(
                serialization::Deserialize<Type>({data.cbegin(), data.cbegin() + sizeof(Type)})) / Ratio::den};
    }

    /**
       \brief Serialize the struct for network communication
    
       | Type                       | Length (bytes) | Description                                                                    | Unit |
       |:---------------------------|---------------:|:-------------------------------------------------------------------------------|:-----|
       | `template parameter Type`  |              ? | double value, serialized as the unit expressed by the Ratio template parameter | ?    |
       For more information, see the [protocol page](docs/protocol.md).
     */
    RawData Serialize() const {
        return serialization::Serialize(static_cast<Type>(value * Ratio::den));
    }

    double value;
};

using SearchRadius = Double<std::uint64_t, std::micro>;

}  // namespace api
}  // namespace accerion
