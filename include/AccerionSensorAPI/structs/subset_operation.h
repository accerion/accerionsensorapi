/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstdint>
#include <vector>

#include "AccerionSensorAPI/structs/areas.h"
#include "AccerionSensorAPI/structs/signature.h"

namespace accerion {
namespace api {

enum class SubsetResultMessageType : std::uint8_t {
    kComplete        = 0x0,  //!< 0x0
    kProgress        = 0x1   //!< 0x1
};

enum class SubsetResult : std::uint8_t {
    kSuccess        = 0x0,  //!< 0x0
    kAreaIdInUse    = 0x1,  //!< 0x1
    kDiskFull       = 0x2,  //!< 0x2
    kNoInput        = 0x3   //!< 0x3
};

struct SubsetOperationResult {
    static SubsetOperationResult Deserialize(const RawData& data);
    RawData Serialize() const;

    SubsetResultMessageType message_type;
    /**
       This value can contain the following information:
         - value of #accerion::api::SubsetResult if #message_type is #accerion::api::SubsetResultMessageType::kComplete
         - progress percentage, if #message_type is #accerion::api::SubsetResultMessageType::kProgress
     */
    std::uint8_t value;
    std::vector<ClusterId> failed_ids;
};

struct SubsetOperationRequest {
    static SubsetOperationRequest Deserialize(const RawData& data);
    RawData Serialize() const;

    AreaId target_area_id;
    std::vector<ClusterId> cluster_ids;
};

}  // namespace api
}  // namespace accerion

