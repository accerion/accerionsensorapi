/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <cstdint>
#include <vector>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

using AreaId = std::uint16_t;

struct Areas {
    static Areas Deserialize(RawData raw_data);
    RawData Serialize() const;

    AreaId active_area;
    std::vector<AreaId> all_areas;
};

}  // namespace api
}  // namespace accerion
