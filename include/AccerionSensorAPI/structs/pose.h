/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cmath>
#include <cassert>
#include <ostream>
#include <string>
#include <sstream>

#include "AccerionSensorAPI/structs/double.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/optional.h"

namespace accerion {
namespace api {

// Constants
constexpr double kPi = 3.141592653589793238462643383279502884197169399;
constexpr double kDegToRad = kPi / 180.0;
constexpr double kRadToDeg = 180.0 / kPi;


template <typename T>
T NormalizeAngleDeg(const T& angle) {
    using namespace std;
    return angle - 360. * floor((angle + 180.) / 360.);
}

template <typename T>
T NormalizeAngleRad(const T& angle) {
    using namespace std;
    static const T k2Pi{2. * kPi};
    return angle - k2Pi * floor((angle + kPi) / k2Pi);
}


template <typename T, bool Degrees>
class Pose_ {
 public:
    static Pose_ Identity() {
        return {T{0}, T{0}, T{0}};
    }

    static accerion::api::Optional<Pose_> Parse(const std::string& s) {
        std::stringstream ss{s};
        const auto extract = [&ss](double& value) -> bool {
            ss >> value;
            return !ss.fail();
        };
        Pose_ pose{};
        if (extract(pose.x) && extract(pose.y) && extract(pose.th)) {
            return pose;
        } else {
            return {};
        }
    }

    template <typename NewType, bool NewDegrees = Degrees>
    Pose_<NewType, NewDegrees> Cast() const {
        double new_th = th;
        if (Degrees != NewDegrees) {
            new_th *= NewDegrees ? kRadToDeg : kDegToRad;
        }
        return Caster<NewType, NewDegrees>{}(x, y, new_th);
    }

    T NormSq() const {
        return x * x + y * y;
    }

    T Norm() const {
        return std::sqrt(NormSq());
    }

    bool operator==(const Pose_ &o) const {
        return x == o.x && y == o.y && th == o.th;
    }
    bool operator!=(const Pose_ &o) const {
        return !(*this == o);
    }
    bool IsApprox(const Pose_ &o, const double eps = 1.0e-14, Optional<double> opt_eps_rot = Optional<double>{}) const {
        static_assert(std::is_same<T, double>::value, "Can only use IsApprox for type double");
        if (!opt_eps_rot) {
            opt_eps_rot = Optional<double>{eps};
        }
        return std::abs(o.x - x) < eps && std::abs(o.y - y) < eps &&
               NormalizedAngle(std::abs(o.th - th)) < opt_eps_rot.value();
    }

    Pose_ Inverse() const {
        using namespace std;
        // For Pose_ P with rotation R and translation t, the inverse P_i is given by
        // P = [R|t] -> P_i = [R_t | - R_t t] where R_t is the rotation matrix transposed
        const auto th_rad = Degrees ? th * kDegToRad : th;
        const auto cos_h = cos(th_rad);
        const auto sin_h = sin(th_rad);
        return {
            static_cast<T>(-(cos_h * x + sin_h * y)),
            static_cast<T>(-(-sin_h * x + cos_h * y)),
            static_cast<T>(-th)};
    }
    Pose_ operator*(const Pose_& o) const {
        Pose_ result = {*this};
        result *= o;
        return result;
    }
    Pose_& operator*=(const Pose_& o) {
        using namespace std;
        const auto th_rad = Degrees ? th * kDegToRad : th;
        const auto cos_h = cos(th_rad);
        const auto sin_h = sin(th_rad);
        x += cos_h * o.x - sin_h * o.y;
        y += sin_h * o.x + cos_h * o.y;
        th += o.th;
        NormalizeRotation();
        return *this;
    }

    // Make sure the heading angle value is in the specified range
    void NormalizeRotation() {
        th = NormalizedAngle(th);
    }

    // Interpolate this Pose_ and another one based on given ratio, where ratio <= 0.0 returns this, and ratio >= 1.0
    // returns the other.
    Pose_ Interpolate(double ratio, const Pose_& o) const {
        static_assert(std::is_same<T, double>::value, "Can only interpolate for type double");
        assert(0.0 <= ratio && ratio <= 1.0);

        // Early out when there is no interpolation needed
        if (ratio <= 0.0) {
            return *this;
        } else if (ratio >= 1.0) {
            return o;
        }

        Pose_ result = *this;
        result.x += ratio * (o.x - result.x);
        result.y += ratio * (o.y - result.y);
        result.th += ratio * NormalizedAngle(o.th - result.th);
        result.NormalizeRotation();

        return result;
    }

    /**
       \brief Serialize the struct for network communication

       | Type      | Length (bytes) | Description | Unit        |
       |:----------|---------------:|:------------|:------------|
       | `int64_t` |              8 | x position  | micrometer  |
       | `int64_t` |              8 | y position  | micrometer  |
       | `int32_t` |              4 | heading     | centidegree |
       For x and y, the approximate allowed range is between -922337203 and 922337203 in meter
       (This ensures that the double epsilon is below 1 micrometer).
       For more information, see the [protocol page](docs/protocol.md).
     */
    RawData Serialize() const {
        static_assert(std::is_same<T, double>::value && Degrees, "Can only serialize for type double and degrees");

        auto data = Double<std::int64_t, std::micro>{x}.Serialize();
        Append(data, Double<std::int64_t, std::micro>{y}.Serialize());
        Append(data, Double<std::int32_t, std::centi>{NormalizedAngle(th)}.Serialize());
        return data;
    }

    static Pose_ Deserialize(const RawData& data) {
        static_assert(std::is_same<T, double>::value && Degrees, "Can only deserialize for type double and degrees");

        Pose_ p{};
        p.x = Double<std::int64_t, std::micro>::Deserialize({data.cbegin(), data.cbegin() + 8}).value;
        p.y = Double<std::int64_t, std::micro>::Deserialize({data.cbegin() + 8, data.cbegin() + 16}).value;
        p.th = Double<std::int32_t, std::centi>::Deserialize({data.cbegin() + 16, data.cbegin() + 20}).value;
        p.NormalizeRotation();
        return p;
    }

    T x;  // x position in meter
    T y;  // y position in meter
    T th;  // heading in units as per template parameter, range should be within -180, 180 degrees or radians equivalent

 private:
    // The following boiler-plating Caster structs enable the generic Cast function.
    // The partial class specialization mechanism is used as a similar mechanism does not exist for functions.
    template <typename NewType, bool NewDegrees, typename = void>
    struct Caster {
        Pose_<NewType, NewDegrees> operator()(T new_x, T new_y, double new_th) const {
            return {static_cast<NewType>(new_x), static_cast<NewType>(new_y), static_cast<NewType>(new_th)};
        }
    };
    template <typename NewType, bool NewDegrees>
    struct Caster<NewType, NewDegrees,
                  typename std::enable_if<std::is_floating_point<T>::value && std::is_integral<NewType>::value>::type> {
        Pose_<NewType, NewDegrees> operator()(T new_x, T new_y, double new_th) const {
            return {static_cast<NewType>(std::round(new_x)),
                    static_cast<NewType>(std::round(new_y)),
                    static_cast<NewType>(std::round(new_th))};
        }
    };

    static T NormalizedAngle(const T& angle) {
        return Degrees ? NormalizeAngleDeg(angle) : NormalizeAngleRad(angle);
    }

    friend std::ostream& operator<<(std::ostream& os, const Pose_& p) {
        return os << "(" << p.x << "," << p.y << ")" << " " << p.th;
    }
};

using Pose = Pose_<double, true>;
using Velocity = Pose_<double, true>;

}  // namespace api
}  // namespace accerion
