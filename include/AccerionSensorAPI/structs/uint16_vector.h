/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <vector>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

class UInt16Vector {
 public:
    static UInt16Vector Deserialize(const RawData& data);
    RawData Serialize() const;

    std::vector<uint16_t> value;
};

}  // namespace api
}  // namespace accerion

