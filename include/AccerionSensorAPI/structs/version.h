/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <string>

#include "msgpack.hpp"

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

struct Version {
    static Version Deserialize(const RawData& data);
    RawData Serialize() const;

    static Version Parse(const std::string& str);

    bool operator==(const Version& other) const;
    bool operator!=(const Version& other) const;
    bool operator<(const Version& other) const;
    bool operator<=(const Version& other) const;
    bool operator>(const Version& other) const;
    bool operator>=(const Version& other) const;

    std::string String() const;
    friend std::ostream& operator<<(std::ostream& os, const Version& version);

    std::uint8_t major;
    std::uint8_t minor;
    std::uint8_t patch;
    MSGPACK_DEFINE_MAP(major, minor, patch);
};
using SoftwareVersion = Version;

// The API version is made available using this function and is coming from CMake
SoftwareVersion GetApiVersion();

}  // namespace api
}  // namespace accerion
