/* Copyright (c) 2023-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/signature.h"
#include "AccerionSensorAPI/structs/time.h"

namespace accerion {
namespace api {

struct CreatedSignature {
    static CreatedSignature Deserialize(const RawData& raw_data);
    RawData Serialize() const;

    bool operator==(const CreatedSignature& other) const;

    TimePoint timestamp;
    Signature signature;
};

}  // namespace api
}  // namespace accerion
