/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstdint>
#include <vector>
#include <cstring>

#include "AccerionSensorAPI/commands.h"


namespace accerion {
namespace api {
namespace serialization {

namespace detail {

/// \brief Method to reverse the content of a vector of bytes
std::vector<std::uint8_t> Reverse(std::vector<std::uint8_t>&& data);

}  // namespace

/// \brief Method to serialize an integral type into a vector of uint8_t
template <typename Integer, typename = typename std::enable_if<std::is_integral<Integer>::value>::type>
std::vector<uint8_t> Serialize(const Integer& value) {
    constexpr std::size_t size = sizeof(value);
    std::vector<std::uint8_t> data(size);
    memcpy(data.data(), &value, size);
    return detail::Reverse(std::move(data));
}
template <>
inline std::vector<uint8_t> Serialize(const std::uint8_t& value) {
    return {value};
}

/// \brief Method to deserialize a vector of uint8_t into an integral type
template <typename T, typename = typename std::enable_if<std::is_integral<T>::value>::type>
T Deserialize(std::vector<std::uint8_t> data) {
    data = detail::Reverse(std::move(data));
    // Return a copy (unavoidable) of the reinterpreted data
    return *reinterpret_cast<const T*>(data.data());
}

template <>
inline std::uint8_t Deserialize(std::vector<std::uint8_t> data) {
    return data.front();
}

}  // namespace serialization
}  // namespace api
}  // namespace accerion
