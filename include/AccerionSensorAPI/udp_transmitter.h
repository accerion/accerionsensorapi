/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstring>
#include <vector>
#include <sys/types.h>

#include "AccerionSensorAPI/communication/networking.h"
#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/plain_uint32.h"  // SerialNumber

#ifdef CMAKE_WINDOWS
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#else
    #include <unistd.h>
#endif

namespace accerion {
namespace api {

/**
 * @brief      Wrapper class that holds a UDP socket for transmitting messages.
 */
class UdpTransmitter {
public:
    /**
     * @brief      Constructs a UDPTransmitter object
     *
     * @param[in]  remoteReceivePort  The remote receive port to which the messages are sent
     */
    explicit UdpTransmitter(unsigned int remoteReceivePort);

    ~UdpTransmitter();

    UdpTransmitter(const UdpTransmitter&) = delete;
    UdpTransmitter& operator=(const UdpTransmitter&) = delete;
    UdpTransmitter(UdpTransmitter &&other) noexcept;
    UdpTransmitter& operator=(UdpTransmitter &&other) noexcept;

    /**
     * @brief      Transmits a message over UDP
     *
     * @param[in]  message  The message to be transmitted
     *
     * @return     True if message is sent successfully, false otherwise
     */
    bool TransmitMessage(RawData&& message);

    /**
     * @brief      Method to set the IP address that is to be transmitted to
     *
     * @param      address     The address that is to be transmitted to
     */
    void SetIpAddress(const Address& address);
    void SetUnicastIpAddress(const Address& address);
    void SetBroadcastIpAddress(const Address& address);

    /**
     * @brief      Method to transmit a vector of commands.
     *
     * @param      commands         A vector of Command objects
     * @param      serial_number    Serial number of target sensor
     */
    void TransmitMessages(const std::vector<Command>& commands, const SerialNumber& serial_number);

private:
    void CloseSocket();

    std::uint16_t remoteReceivePort_{}; //!< Holds the remote receive port to which the outgoing UDP messages are sent.
    int socketEndpoint_{}; //!< Holds the file descriptor for UDP socket
    sockaddr_in remoteAddress_{}; //!< Holds the remote address to which outgoing UDP messages are sent (now set to Broadcast)

    static constexpr unsigned int bufferSize_ = network_constants::kMaxBytesInUdpMessage;
};

}  // namespace api
}  // namespace accerion
