/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <vector>
#include <memory>
#include <mutex>

#include "AccerionSensorAPI/callbacks.h"
#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/communication/thread.h"
#include "AccerionSensorAPI/future.h"
#include "AccerionSensorAPI/received_command/file_transfer_handshake.h"
#include "AccerionSensorAPI/received_command/file_transfer_task.h"
#include "AccerionSensorAPI/received_command/map_subset.h"
#include "AccerionSensorAPI/structs/acknowledgement.h"
#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/area_file_header.h"
#include "AccerionSensorAPI/structs/areas.h"
#include "AccerionSensorAPI/structs/boolean.h"
#include "AccerionSensorAPI/structs/bool_uint16.h"
#include "AccerionSensorAPI/structs/bool_opt_uint64.h"
#include "AccerionSensorAPI/structs/buffered_recovery.h"
#include "AccerionSensorAPI/structs/corrected_odometry.h"
#include "AccerionSensorAPI/structs/created_signature.h"
#include "AccerionSensorAPI/structs/date_time.h"
#include "AccerionSensorAPI/structs/diagnostics.h"
#include "AccerionSensorAPI/structs/double.h"
#include "AccerionSensorAPI/structs/drift_correction.h"
#include "AccerionSensorAPI/structs/external_reference_pose.h"
#include "AccerionSensorAPI/structs/heartbeat.h"
#include "AccerionSensorAPI/structs/ip_address.h"
#include "AccerionSensorAPI/structs/latest_external_reference.h"
#include "AccerionSensorAPI/structs/line_follower_data.h"
#include "AccerionSensorAPI/structs/loop_closure.h"
#include "AccerionSensorAPI/structs/map_summary.h"
#include "AccerionSensorAPI/structs/marker_detection.h"
#include "AccerionSensorAPI/structs/odometry.h"
#include "AccerionSensorAPI/structs/plain_uint16.h"
#include "AccerionSensorAPI/structs/ram_rom_stats.h"
#include "AccerionSensorAPI/structs/search_range.h"
#include "AccerionSensorAPI/structs/signature_vector.h"
#include "AccerionSensorAPI/structs/software_details.h"
#include "AccerionSensorAPI/structs/string_vector.h"
#include "AccerionSensorAPI/structs/tcp_settings.h"
#include "AccerionSensorAPI/structs/udp_settings.h"
#include "AccerionSensorAPI/structs/uint64_vector.h"
#include "AccerionSensorAPI/structs/uint8_vector.h"
#include "AccerionSensorAPI/tcp_client.h"
#include "AccerionSensorAPI/type_def.h"
#include "AccerionSensorAPI/udp_client.h"

#ifdef CMAKE_WINDOWS
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

namespace accerion {
namespace api {

/// Sensor provides an object-oriented "interface". Methods invoked on this object are relayed to the sensor over the
/// network and received messages are forwarded to the registered callbacks
class Sensor {
public:
    /**
     * \brief Constructor for Sensor object, can be invoked by you, but recommended use is through SensorManager
     * \param address ip address of the sensor that is to be connected to
     * \param serial_number of the sensor that is to be connected to
     * \param connection_type to provide the preferred method of connection
     * \param local_address is the optional ip address of the local machine, only required in case unicasting is used
     **/
    Sensor(const Address& address,
           const SerialNumber& serial_number,
           ConnectionType connection_type,
           Optional<Address> local_address = {});

    ~Sensor();

    Sensor(Sensor&&) = delete;
    Sensor& operator=(Sensor&&) = delete;
    Sensor(const Sensor&) = delete;
    Sensor& operator=(const Sensor&) = delete;

    // STREAMING MSGS
    /**
     * \brief Method to register a callback to the HeartBeat message
     * \warning This callback only works when connecting using UDP
     **/
    Future<HeartBeat>& SubscribeToHeartBeat();

    /// \brief Method to register a callback to the Corrected Odometry message
    Future<CorrectedOdometry>& SubscribeToCorrectedOdometry();

    /// \brief Method to register a callback to the Uncorrected Pose Odometry message
    Future<Odometry>& SubscribeToOdometry();

    /// \brief Method to register a callback to the Diagnostics message
    Future<Diagnostics>& SubscribeToDiagnostics();

    /// \brief Method to register a callback to the Compensated Drift Correction message
    Future<DriftCorrection>& SubscribeToCompensatedDriftCorrection();

    /// \brief Method to register a callback to the Line Follower message
    Future<LineFollowerData>& SubscribeToLineFollowerData();

    /// \brief Method to register a callback to the Console Output message
    Future<PlainString>& SubscribeToConsoleOutput();

    /// \brief Method to register a callback to the CreatedSignature message
    Future<CreatedSignature>& SubscribeToCreatedSignature();

    /// \brief Method to register a callback to a LatestExternalReference message
    Future<LatestExternalReference>& SubscribeToLatestExternalReference();

    // CMD - ACK MSGS
    /**
     * \brief Method to set (Absolute) Localization Mode
     * \param on bool holding true/false whether to turn this on/off
     **/
    Future<Acknowledgement>& SetLocalizationMode(bool on);

    /**
     * \brief Method to set Recording Mode
     * \param on bool holding true/false whether to turn this on/off
     **/
    Future<Acknowledgement>& SetRecordingMode(bool on);

    /**
     * \brief Method to set Idle Mode
     * \param on bool holding true/false whether to turn this on/off
     **/
    Future<Acknowledgement>& SetIdleMode(bool on);

    /// \brief Method to Reboot
    Future<Acknowledgement>& Reboot();

    /**
     * \brief Method to request the signature information for a certain cluster of the whole map
     * \param cluster_id   send the cluster details for this cluster, when empty send whole map
     **/
    Future<SignatureVector>& GetSignatureInformation(Optional<ClusterId> cluster_id = {});

    /// \brief Method to get the list of cluster ids
    Future<UInt64Vector>& GetClusterIds();

    /// \brief Method to get the IP Address
    Future<IpAddress>& GetIpAddress();

    /// \brief Method to get the Sample Rate
    Future<PlainUInt16>& GetSampleRate();

    /// \brief Method to get the Serial Number
    SerialNumber GetSerialNumber() const;

    /**
     * \brief Method to set marker detection mode
     * \param on bool holding true/false whether to turn this on/off
     * \param marker_type which marker should be detected, to be supplied when enabling mode
     **/
    Future<Acknowledgement>& SetMarkerDetectionMode(bool on, Optional<MarkerType> marker_type);

    /// \brief Method to get the Software Version
    Future<SoftwareVersion>& GetSoftwareVersion();

    /// \brief Method to get the TCP settings
    Future<TcpSettings>& GetTcpSettings();

    /**
     * \brief Method to set Expert Mode
     * \param on bool holding true/false whether to turn this on/off
     **/
    Future<Acknowledgement>& SetExpertMode(bool on);

    /**
     * \brief Method to set the Sample Rate
     * \param rate SampleRate struct holding the sample rate value that is to be set
     **/
    Future<Acknowledgement>& SetSampleRate(PlainUInt16 rate);

    /**
     * \brief Method to delete a Cluster from the current area
     * \param cluster_id ClusterId ID of the Cluster that has to be deleted
     **/
    Future<Acknowledgement>& DeleteCluster(ClusterId cluster_id);

    /**
     * \brief Method to delete an Area
     * \param area_id Optional<AreaId> ID of the Area to be deleted, all are deleted if optional is empty
     * \return future with a boolean if deletion was accepted & successful
     **/
    Future<Acknowledgement>& DeleteArea(Optional<AreaId> area_id);

    /**
     * \brief Method to set the IP Address
     * The sensor will require a reboot when the IP address is changed
     * Also, any connection with the sensor (Sensor instance) will need to be recreated
     * \param ip IpAddress struct containing the IP address that is to be set
     **/
    Future<Acknowledgement>& SetIpAddress(IpAddress ip);

    /**
     * \brief Method to set the Date/Time
     * \param dt DateTime struct containing the Date and Time to be set
     **/
    Future<Acknowledgement>& SetDateTime(DateTime dt);

    /**
     * \brief Method to get the Date/Time
     * \return Optional potentially containing DateTime struct with the Date and Time
     **/
    Future<DateTime>& GetDateTime();

    /**
     * \brief Method to set the Mapping Mode
     * \param on bool value to set it either on or off
     * \param opt_cluster_id ClusterId ID of the cluster to be mapped, to be provided when turning mode on
     **/
    Future<Acknowledgement>& SetMappingMode(bool on, Optional<ClusterId> opt_cluster_id = {});

    /**
     * \brief Method to get the Mapping Mode
     * \return BoolOptUInt64 which holds the state of the mode and optionally the cluster id
     **/
    Future<BoolOptUInt64>& GetMappingMode();

    /**
     * \brief Method to set the Sensor Pose
     * \param pose Pose struct that holds the position to be set
     **/
    Future<Acknowledgement>& SetCorrectedOdometry(Pose pose);

    /**
     * \brief Method to provide external position input to the sensor
     * \param pose ExternalReferencePose struct that holds time offset, pose and a timestamp
     **/
    void SetExternalReference(ExternalReferencePose pose);

    /**
     * \brief Method to provide a search range to the sensor
     * \param sr SearchRange struct that holds radius and angle
     **/
    void SetSearchRange(SearchRange sr);

    /**
     * \brief Method to set the TCP settings
     * \param settings indicate which messages should be sent by TCP
     **/
    Future<Acknowledgement>& SetTcpSettings(TcpSettings settings);

    /**
     * \brief Method to set the Line Following Mode
     * \param on bool indicating whether to turn the mode on/off
     * \param opt_cluster_id ClusterId ID of the cluster to follow, to be provided when turning mode on
     **/
    Future<Acknowledgement>& SetLineFollowingMode(bool on, Optional<ClusterId> opt_cluster_id = {});

    /**
     * \brief Method to get the Line Following Mode
     * \return BoolOptUInt64 holding the state of the mode and optionally the cluster id
     **/
    Future<BoolOptUInt64>& GetLineFollowingMode();

    /**
     * \brief Method to set the UDP settings
     * \param req UdpInformation struct containing the UDP settings to be set
     **/
    Future<Acknowledgement>& SetUdpSettings(UdpSettings req);

    /// \brief Method to get the UDP settings
    Future<UdpSettings>& GetUdpSettings();

    /**
     * \brief Method to set callbacks for map subset creation
     * \param subsetCB callback method to be invoked to inform about result
     * \param progressCB callback method to be invoked to inform about progress
     **/
    void SetCreateMapSubsetCallbacks(SubsetCallback subsetCB, ProgressCallback progressCB);
    /**
     * \brief Method for map subset creation
     * \param clusterIDs vector containing the ID's of the clusters that should be in the subset
     * \param targetAreaID the ID's of the area where the subset should be moved to
     **/
    void CreateMapSubset(std::vector<ClusterId> clusterIDs, AreaId targetAreaID);
    /**
     * \brief Method for map subset creation
     * \param clusterIDs string containing the ID's of the clusters that should be in the subset
     * \param targetAreaID the ID's of the area where the subset should be moved to
     **/
    void CreateMapSubset(std::string clusterIDs, AreaId targetAreaID);

    /// \brief Method to get the Software Details
    Future<SoftwareDetails>& GetSoftwareDetails();

    /// \brief Method to register a callback to the marker detection message
    Future<MarkerDetection>& SubscribeToMarkers();

    /**
     * \brief Method to set the Buffer Length of the Buffered Recovery Mode
     * \param bufferLength float length in meters
     **/
    Future<Acknowledgement>& SetBufferLength(float bufferLength);
    /// \brief Method to get the Buffer Length of the Buffered Recovery Mode
    Future<BufferLength>& GetBufferLength();

    /**
     * \brief Method to start the Buffered Recovery Mode
     * \param xPos double x position in meters
     * \param yPos double y position in meters
     * \param radius double radius in meters
     * \param fullMapSearch bool full map search if true
     **/
    Future<BufferProgress>& StartBufferedRecovery(double xPos, double yPos, double radius, bool fullMapSearch);
    /// \brief Method to stop the Buffered Recovery Mode
    Future<BufferProgress>& CancelBufferedRecovery();

    /// \brief Method to get a list of all the recordings
    Future<StringVector>& GetRecordingList();

    /**
     * \brief Method to delete recordings, names should match the recordings on the sensor.
     * \param names vector of names to be removed
     * \return Future containing a StringVector. Contains names of recordings that could NOT be removed
     **/
    Future<StringVector>& DeleteRecordings(std::vector<std::string> names);

    /**
     * \brief Method to retrieve a frame [INTERNAL USE ONLY]
     * \param key that gives you access to this functionality
     **/
    Future<UInt8Vector>& CaptureFrame(std::string key);

    /// \brief Method to register a callback to a loop closure message
    Future<LoopClosure>& SubscribeToLoopClosures();
    /// \brief Method to register a callback to loop closures completed message
    Future<void>& SubscribeToLoopClosuresCompleted();
    /**
     * \brief Method to start searching for loop clsoures
     * \param search_radius radius in which signatures should be matched to each other
     **/
    Future<Acknowledgement>& StartSearchForLoopClosures(double search_radius);
    /**
     * \brief Method to stop searching for loop closures
     * Stopping the search will not invoke the search for loop closures complete callback
     **/
    Future<Acknowledgement>& StopSearchForLoopClosures();

    /**
     * \brief Method to set the internal mode
     * \param on boolean whether it should be on or off
     **/
    Future<Acknowledgement>& SetInternalMode(bool on);

    /**
     * \brief Method to set Continuous Signature Updating Mode
     * \param on true for on, false for off
     **/
    Future<Acknowledgement>& SetContinuousSignatureUpdatingMode(bool on);

    /// \brief Method to get the list of area ids
    Future<Areas>& GetAreaIds();

    /// \brief Method to set the area id
    Future<Acknowledgement>& SetArea(AreaId areaId);

    /// \brief Method to get ram rom usage statistics
    Future<RamRomStats>& GetRamRomStats();

    /// \brief Methods to do pre or post file transfer actions, such as replacing the map or gathering a bunch of recordings
    bool SetFileTransferTaskCallbacks(FileTransferTaskStatusCallback status_cb, FileTransferTaskProgressCallback progress_cb);
    bool ExecuteFileTransferTask(const FileTransferTaskRequest& request);

    /// \brief Methods to do file transfers
    void SetFileTransferCallbacks(ProgressCallback progress_cb, DoneCallback done_cb);
    void FileTransfer(FileUploadDownloadRequest req, std::string filePath);

    /**
     * \brief Method to upload and process a map
     * \param file_path string full path to where the file is stored including name (ending on .amf)
     * \param task indicating whether the map should be replaced, updated or merged.
     * \param transfer_progress_cb callback method to be invoked to inform about transfer progress
     * \param processing_progress_cb callback method to be invoked to inform about processing progress
     * \param done_cb callback method to be invoked after a success or failure
     **/
    void SendMap(std::string file_path,
                 FileTransferTask task,
                 ProgressCallback transfer_progress_cb,
                 FileTransferTaskProgressCallback processing_progress_cb,
                 DoneCallback done_cb);
    /**
     * \brief Method to download a map
     * \param filepath string full path to where the file should be stored including name (ending on .amf)
     * \param transfer_progress_cb callback method to be invoked to inform about progress
     * \param done_cb callback method to be invoked after a success or failure
     **/
    void GetMap(std::string filepath, ProgressCallback transfer_progress_cb, DoneCallback done_cb);

    /**
     * \brief Method to download recordings
     * \param names vector of names to be downloaded
     * \param file_path string full path to where the file should be stored including name (ending on .amf)
     * \param transfer_progress_cb callback method to be invoked to inform about transfer progress
     * \param processing_progress_cb callback method to be invoked to inform about processing progress
     * \param done_cb callback method to be invoked after a success or failure
     **/
    void GetRecordings(std::vector<std::string> names,
                       std::string file_path,
                       ProgressCallback transfer_progress_cb,
                       FileTransferTaskProgressCallback processing_progress_cb,
                       DoneCallback done_cb);

    /// \brief Method to download a map summary
    Future<MapSummary>& GetMapSummary();

    /**
     * @brief Method to log events on sensor with a custom message. Sensor will write away that message with
     * a timestamp of when the event was received. Can be used to mark certain important moments in the logs etc.
     *
     * @param message event will store a timestamp and the provided message in a separate log file
     */
    void LogEvent(const std::string& message);

    Future<Acknowledgement>& SetSaveImages(bool value);

    Future<Boolean>& GetSaveImages();

private:
    template <typename... AddOutgoingCommandArgs>
    void AddOutgoingMessage(AddOutgoingCommandArgs... args) {
        communication_thread_.AddOutgoingMessage(std::forward<AddOutgoingCommandArgs>(args)...);
    }

    template <typename Data, typename... AddOutgoingCommandArgs>
    Future<Data>& DoRequest(Future<Data>& future, AddOutgoingCommandArgs... args) {
        future.ClearData();
        AddOutgoingMessage(std::forward<AddOutgoingCommandArgs>(args)...);
        return future;
    }

    /// \brief Method creates a map relating the command ids to the corresponding processing functions
    ProcessResponseFunctions InitializeProcessResponseFunctionsWithReceivedCommands();

    /// \brief Method to create the communication thread, which include creating the communication client
    static CommunicationThread CreateCommunicationThread(const Address& address,
                                                         const SerialNumber& serial_number,
                                                         ConnectionType connection_type,
                                                         const ProcessResponseFunctions& process_response_functions);

    const Address address_;
    const SerialNumber serial_number_;

    ProcessResponseFunctions process_response_functions_;

    // Streaming message handling
    Future<PlainString> future_console_output_;
    Future<CorrectedOdometry> future_corrected_odometry_;
    Future<CreatedSignature> future_created_signature_;
    Future<Diagnostics> future_diagnostics_;
    Future<DriftCorrection> future_compensated_drift_correction_;
    Future<HeartBeat> future_heart_beat_;
    Future<LineFollowerData> future_line_follower_data_;
    Future<MarkerDetection> future_marker_detection_;
    Future<Odometry> future_odometry_;
    Future<LatestExternalReference> future_latest_ext_ref_;

    // Acknowledgement message handling
    Future<Acknowledgement> future_continuous_signature_updating_mode_;
    Future<Acknowledgement> future_expert_mode_;
    Future<Acknowledgement> future_idle_mode_;
    Future<Acknowledgement> future_internal_mode_;
    Future<Acknowledgement> future_set_line_following_mode_;
    Future<BoolOptUInt64>   future_get_line_following_mode_;
    Future<Acknowledgement> future_set_mapping_mode_;
    Future<BoolOptUInt64>   future_get_mapping_mode_;
    Future<Acknowledgement> future_marker_detection_mode_;
    Future<Acknowledgement> future_recording_mode_;
    Future<Acknowledgement> future_localization_mode_;

    Future<Acknowledgement> future_corr_odometry_pose_;
    Future<Acknowledgement> future_save_images_;
    Future<Boolean>         future_get_save_images_;
    Future<Acknowledgement> future_reboot_;
    Future<Acknowledgement> future_set_search_loop_closures_;

    SignatureVector signature_information_;
    Future<SignatureVector> future_signature_information_;
    Future<SignatureVector> int_future_signature_information_;
    Future<void> int_future_signature_information_done_;

    // Remaining message handling
    Future<Acknowledgement> future_set_area_;
    Future<Areas> future_get_area_ids_;
    Future<Acknowledgement> future_set_buffer_length_;
    Future<BufferLength> future_get_buffer_length_;
    Future<BufferProgress> future_buffer_progress_;
    Future<UInt8Vector> future_capture_frame_;
    Future<UInt64Vector> future_cluster_ids_;
    Future<Acknowledgement> future_set_date_time_;
    Future<DateTime> future_get_date_time_;
    Future<StringVector> future_delete_recordings_;
    Future<Acknowledgement> future_set_ip_address_;
    Future<IpAddress> future_get_ip_address_;
    Future<MapSummary> future_map_summary_;
    Future<RamRomStats> future_ram_rom_stats_;
    Future<StringVector> future_recording_list_;
    Future<Acknowledgement> future_delete_cluster_;
    Future<Acknowledgement> future_delete_area_;
    Future<Acknowledgement> future_set_sample_rate_;
    Future<PlainUInt16> future_get_sample_rate_;
    Future<SoftwareDetails> future_software_details_;
    Future<SoftwareVersion > future_software_version_;
    Future<Acknowledgement> future_set_tcp_settings_;
    Future<TcpSettings> future_get_tcp_settings_;
    Future<Acknowledgement> future_set_udp_settings_;
    Future<UdpSettings> future_get_udp_settings_;
    Future<LoopClosure> future_loop_closure_;
    Future<void> future_loop_closures_completed_;

    // Legacy received command handling
    ReceivedCommandFileTransferTask rc_file_transfer_task_;
    ReceivedCommandFileTransferHandshake rc_handshake_;
    ReceivedCommandMapSubset rc_map_subset_;

    CommunicationThread communication_thread_;
};

}  // namespace api
}  // namespace accerion
