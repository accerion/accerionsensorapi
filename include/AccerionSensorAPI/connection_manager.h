/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <memory>
#include <vector>

#include "AccerionSensorAPI/communication/connection_utils.h"
#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/sensor.h"
#include "AccerionSensorAPI/type_def.h"
#include "AccerionSensorAPI/update_service.h"

#ifdef CMAKE_WINDOWS
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

namespace accerion {
namespace api {

/// Can be used for detecting sensors in the local subnet and to connect to a sensor or update service
class ConnectionManager {
 public:
    ConnectionManager(ConnectionManager&&) = delete;
    ConnectionManager& operator=(ConnectionManager&&) = delete;
    ConnectionManager(const ConnectionManager&) = delete;
    ConnectionManager& operator=(const ConnectionManager&) = delete;

    /**
     * \brief Stop the internal communication threads
     * \note After this action, the ConnectionManager is not unusable, so calling any method afterwards will result in
     * an exception
     */
    static void Shutdown();

    /**
     * \brief Get all sensors detected
     * \param period indicates when the sensor should have been seen last, default 2 seconds
     *               (so a sensor returned by the function has been seen in the last 2 seconds)
     * The result is in arbitrary order
     */
    static std::vector<SensorDetails> GetDetectedSensors(const std::chrono::seconds& period = std::chrono::seconds{2});

    /**
     * \brief method to get Sensor object based on its IP address
     * \param address struct that contains the IP address of the sensor you'd like to connect to
     * \param connection_type to provide the preferred connection method
     * \param local_ip the optional local IP address, use an address the sensor can access, required in case of UDP
     * \return Sensor object which allows you to interact with that sensor.
     **/
    static Future<std::unique_ptr<Sensor>>& GetSensor(Address address,
                                                      ConnectionType connection_type,
                                                      Optional<Address> local_ip = {});

    /**
     * \brief method to get Sensor object based on its serial number
     * \param serial contains the serial of the sensor you'd like to connect to
     * \param connection_type to provide the preferred connection method
     * \param local_ip struct containing the local IP address, use an address the sensor can access
     * \return Sensor object which allows you to interact with that sensor.
     * 
     **/
    static Future<std::unique_ptr<Sensor>>& GetSensor(SerialNumber serial,
                                                      ConnectionType connection_type,
                                                      Optional<Address> local_ip = {});

    /**
     * \brief method to get UpdateService object based on its IP address
     * \param address struct that contains the IP address of the UpdateServices you'd like to connect to
     * \return UpdateService object which allows you to interact with that UpdateService.
     *
     **/
    static Future<std::unique_ptr<UpdateService>>& GetUpdateService(Address address);

    /**
     * \brief method to get UpdateService object based on its serial number
     * \param serial contains the serial of the UpdateService you'd like to connect to
     * \return UpdateService object which allows you to interact with that UpdateService.
     *
     **/
    static Future<std::unique_ptr<UpdateService>>& GetUpdateService(SerialNumber serial);

 private:
    /// Singleton
    static ConnectionManager& GetInstance();
    ConnectionManager();
    ~ConnectionManager();

    void ProcessParsedMessage(SerialNumber received_serial_number, Command&& command);

    DetectedSensors detected_sensors_;

    // Mutex required to access the following attributes, except the futures and threads
    std::mutex mutex_;

    Future<std::unique_ptr<Sensor>> future_sensor_;
    std::function<bool(const SensorDetails&)> future_sensor_fill_condition_;
    std::function<std::unique_ptr<Sensor>(const SensorDetails&)> connect_to_sensor_;

    Future<std::unique_ptr<UpdateService>> future_service_;
    std::function<bool(const SensorDetails&)> future_service_fill_condition_;

    // Unique pointers because it can be shutdown
    std::unique_ptr<CommunicationThread> communication_thread_sensor_;
    std::unique_ptr<CommunicationThread> communication_thread_service_;
};

}  // namespace api
}  // namespace accerion
