/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/udp_receiver.h"
#include "AccerionSensorAPI/udp_transmitter.h"

namespace accerion {
namespace api {

/// @brief      Wrapper class that combines a UDP transmitter and receiver.
class UdpClient : public UdpTransmitter, public UdpReceiver {
public:
    /**
     * @brief       Constructs a UdpClient object
     *
     * @param[in]   transmitPort    The port over which UDP messages are transmitted
     * @param[in]   receivePort     The port over which UDP messages are received
     */
    UdpClient(unsigned int transmitPort, unsigned int receivePort)
            : UdpTransmitter(transmitPort), UdpReceiver(receivePort) {};

    /**
     * \brief       Method used for connecting to the sensor (dummy)
     */
    std::system_error ConnectToServer() {
        return std::system_error(0, std::generic_category()); // No error
    }

    /**
     * @brief      Function that reconnects to the server, when required (dummy)
     */
    void StayAlive() {}
};

}  // namespace api
}  // namespace accerion
