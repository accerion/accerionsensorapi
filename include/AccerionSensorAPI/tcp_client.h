/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <vector>
#include <sys/types.h>
#include <system_error>

#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/communication/networking.h"
#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/plain_uint32.h"

#ifdef CMAKE_WINDOWS
    #define WINVER 0x0A00
    #define _WIN32_WINNT 0x0A00
    #include <mstcpip.h>
    #include <ws2tcpip.h>
    #include <stdio.h>
    #include <fcntl.h>
    #if defined(_MSC_VER)
        typedef SSIZE_T ssize_t;
    #endif
    #ifndef MSG_NOSIGNAL
        #define MSG_NOSIGNAL 0
    #endif
    #pragma comment(lib, "Ws2_32.lib")
#else
    #include <unistd.h>
    #include <netinet/tcp.h>
#endif

// Cross-platform error code
#ifdef CMAKE_WINDOWS
    #define GETSOCKETERRNO() (WSAGetLastError())
#else
    #define GETSOCKETERRNO() (errno)
#endif

namespace accerion {
namespace api {

class TcpClient {
public:
    /**
     * @brief      Constructs a TcpClient object
     * @param[in]  address the address to connect to
     * @param[in]  receivePort  The receive port
     */
    TcpClient(Address address, unsigned int receivePort);

    ~TcpClient();

    TcpClient(const TcpClient&) = delete;
    TcpClient& operator=(const TcpClient&) = delete;
    TcpClient(TcpClient &&other) noexcept;
    TcpClient& operator=(TcpClient &&other) noexcept;

    /**
     * \brief Internal Method used for connecting to the sensor
     * 
     */
    std::system_error ConnectToServer();

    /**
     * \brief Internal Method used for reading messages.
     * 
     * \return bool when message has been received
     */
    bool ReceiveMessage();

    /**
     * @brief      Transmits a TCP Message, pointed by transmittedMessage and
     *             has byte size transmittedNumOfBytes
     *
     * @param      transmittedMessage     The transmitted message
     * @param[in]  n_bytes_message  Number of bytes to be transmitted
     *
     * @return     Errno set by Linux system calls, 0 if no error
     */
    int TransmitMessage(const uint8_t *const transmittedMessage, const std::uint64_t n_bytes_message);

    /**
     * @brief      Method to transmit a vector of commands.
     *
     * @param      commands         A vector of Command objects
     * @param      serial_number    Serial number of target sensor
     */
    void TransmitMessages(const std::vector<Command>& commands, const SerialNumber& serial_number);

    /**
     * @brief      Gets a uint8_t pointer received message.
     *
     * @return     Pointer to the received message.
     */
#ifdef CMAKE_WINDOWS
        char* GetReceivedMessage()
    {
        return &receivedMessage_[0];
    }
#else
    uint8_t* GetReceivedMessage() {
        return &receivedMessage_[0];
    }
#endif

    /**
     * @brief      Gets the received number of bytes.
     *
     * @return     The received number of bytes.
     */
    int GetReceivedNumOfBytes() const {
      return receivedNumOfBytes_;
    }

    /**
     * @brief      get the value of the connected variable
     *
     * @return connected_ : boolean value
     */
    bool GetConnected() const { return connected_; };

    /**
     * @brief      Function that reconnects to the server, when required
     */
    void StayAlive() {
        if (!GetConnected()) {
            ConnectToServer();
        }
    }
    
private:
    /**
     * \brief Internal Method used for opening the socket.
     * 
     * \return bool for success/failure
     */
    bool OpenSocket();

    /**
     * \brief Internal Method used for closing the socket.
     */
    void closeSocket();

    /// Method to send a single message
    bool SendMessage(const CommandId& id, RawData&& message);

    uint16_t remote_receive_port_;    //!< Holds the remote receive port to which the outgoing TCP messages are sent
#ifdef CMAKE_WINDOWS
    SOCKET socket_;             //!< Holds the file descriptor for TCP socket
#else
    int socket_;                //!< Holds the file descriptor for TCP socket
#endif

    sockaddr_in remote_address_;

    //!< Holds the remote address to which outgoing TCP messages are sent

    bool open_; //!< Holds whether the socket is open or not
    bool connected_; //!< Holds whether there is a connection or not

    static constexpr unsigned int bufferSize_ = network_constants::kTcpBufferSize; //!< holds the buffer size which indicates the maximumnuber of bytes in a message

#ifdef CMAKE_WINDOWS
    char receivedMessage_[bufferSize_];          //!< Holds received message as a byte array
#else
    uint8_t receivedMessage_[bufferSize_];          //!< Holds received message as a byte array
#endif
    int receivedNumOfBytes_;               //!< Holds number of bytes in received TCP message

    EnabledMessageType tcpSettings_;

    std::vector<uint8_t> transmittedData_;    //!< Holds outgoing UDP data (i.e. part of the message excluding serial number, command ID and CRC code)
};

}  // namespace api
}  // namespace accerion
