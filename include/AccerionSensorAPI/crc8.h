/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <array>
#include <cstdint>

#include "AccerionSensorAPI/type_def.h"

namespace accerion {
namespace api {

/// CRC8 class that is intended for message verification
class Crc8 {
public:
    using Crc = std::uint8_t;

    Crc8(Crc8&&) noexcept = delete;
    Crc8& operator=(Crc8&&) noexcept = delete;
    Crc8(const Crc8&) noexcept = delete;
    Crc8& operator=(const Crc8&) noexcept = delete;

	/**
     * @brief     Method to generate a CRC8 based on input
     *
     * @param[in] message : pointer to array data
	 * @param[in] nBytes : total size of the provided data
	 * @return crc : returns the calculated crc8 over the inputted data
     */
    static Crc Generate(const std::uint8_t* message, int nBytes);

private:
    static const Crc8& GetInstance();

    static std::array<Crc, 256> CreateTable();

    Crc8() = default;
    ~Crc8() = default;

    const std::array<Crc, 256> table_{CreateTable()};
};

}  // namespace api
}  // namespace accerion
