/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstdint>
#include <map>
#include <vector>


namespace accerion {
namespace api {

#define API_RECEIVE_PORT_UDP    13359  //!< The UDP port the API has to LISTEN to, e.g. the sensor talks over this port
#define SENSOR_RECEIVE_PORT_UDP 13360  //!< The UDP port the API has to SEND over, e.g. the sensor listens to this port
#define TCP_PORT                13361  //!< The port the API communicates over with the sensor when using TCP

#define UPDATE_SERVICE_TRANSMIT_PORT_UDP 13362  //!< The UDP port the API has to LISTEN to, e.g. the UpdateService talks over this port
#define UPDATE_SERVICE_PORT_TCP           1989  //!< The port the API communicates over with the UpdateService when using TCP

constexpr auto kTcpPortFileTransfer = 13370;  //!< File transfer communication is using this port on TCP for any files except updates and logs
constexpr auto kTcpPortFileTransferUpdateService = 13380;  //!< File transfer communication is using this port on TCP for updates and logs

/**
 * Enum containing all the commands that go to and from a sensor with their respective hex values.
 * In the second column you can see the value of the ID and the structure that in the body of the message.
 */
enum class CommandId : std::uint8_t {
    PRD_HEARTBEAT_INFO                          = 0x01,  //!< 0x01 with #accerion::api::HeartBeat
    ACK_FILE_TRANSFER_HANDSHAKE                 = 0x07,  //!< 0x07 with #accerion::api::FileTransferHandshake
    STR_CORRECTED_ODOMETRY                      = 0x11,  //!< 0x11 with #accerion::api::CorrectedOdometry
    STR_ODOMETRY                                = 0x12,  //!< 0x12 with #accerion::api::Odometry
    STR_DIAGNOSTICS                             = 0x13,  //!< 0x13 with #accerion::api::Diagnostics
    INT_COMPENSATED_DRIFT_CORRECTION            = 0x14,  //!< 0x14 with #accerion::api::DriftCorrection
    INT_DRIFT_CORRECTION                        = 0x15,  //!< 0x15 with #accerion::api::DriftCorrection RESERVED
    STR_LINE_FOLLOWER                           = 0x16,  //!< 0x16 with #accerion::api::LineFollowerData
    ACK_SIGNATURE_INFORMATION                   = 0x17,  //!< 0x17 with #accerion::api::SignatureVector
    CMD_GET_MAPPING_MODE                        = 0x18,  //!< 0x18 without body
    ACK_GET_MAPPING_MODE                        = 0x19,  //!< 0x20 with #accerion::api::BoolUInt64
    CMD_GET_LINE_FOLLOWING_MODE                 = 0x1A,  //!< 0x1A without body
    ACK_GET_LINE_FOLLOWING_MODE                 = 0x1B,  //!< 0x1B with #accerion::api::BoolUInt64
    ACK_SET_MAPPING_MODE                        = 0x20,  //!< 0x20 with #accerion::api::Acknowledgement
    ACK_LOCALIZATION_MODE                       = 0x22,  //!< 0x22 with #accerion::api::Acknowledgement
    ACK_RECORDING_MODE                          = 0x23,  //!< 0x23 with #accerion::api::Acknowledgement
    ACK_IDLE_MODE                               = 0x24,  //!< 0x24 with #accerion::api::Acknowledgement
    ACK_REBOOT                                  = 0x25,  //!< 0x25 with #accerion::api::Acknowledgement
    ACK_PLACE_CALIB                             = 0x26,  //!< 0x26 with #accerion::api::Acknowledgement
    ACK_INTERNAL_MODE                           = 0x28,  //!< 0x28 with #accerion::api::Acknowledgement
    ACK_SET_LINE_FOLLOWING_MODE                 = 0x2D,  //!< 0x2D with #accerion::api::Acknowledgement
    ACK_SIGNATURE_INFORMATION_DONE              = 0x2E,  //!< 0x2E without body
    ACK_SET_SAMPLE_RATE                         = 0x32,  //!< 0x32 with #accerion::api::Acknowledgement
    ACK_GET_SAMPLE_RATE                         = 0x33,  //!< 0x32 with #accerion::api::PlainUInt16
    ACK_DELETE_CLUSTER                          = 0x34,  //!< 0x34 with #accerion::api::Acknowledgement
    ACK_EXPERT_MODE                             = 0x39,  //!< 0x39 with #accerion::api::Acknowledgement
    ACK_SOFTWARE_DETAILS                        = 0x3A,  //!< 0x3A with #accerion::api::SoftwareDetails
    ACK_SET_BUFFER_LENGTH                       = 0x3C,  //!< 0x3C with #accerion::api::Acknowledgement
    ACK_BUFFER_PROGRESS_INDICATOR               = 0x3D,  //!< 0x3D with #accerion::api::BufferProgress
    ACK_GET_BUFFER_LENGTH                       = 0x3E,  //!< 0x3C with #accerion::api::BufferLength
    ACK_RECORDINGS_LIST                         = 0x3F,  //!< 0x3F with #accerion::api::StringVector
    ACK_SET_IP_ADDRESS                          = 0x40,  //!< 0x40 with #accerion::api::Acknowledgement
    ACK_SET_TCP_SETTINGS                        = 0x41,  //!< 0x41 with #accerion::api::Acknowledgement
    ACK_SET_CORRECTED_ODOMETRY_POSE             = 0x42,  //!< 0x42 with #accerion::api::Acknowledgement
    ACK_DELETE_RECORDINGS                       = 0x43,  //!< 0x43 with #accerion::api::StringVector
    INT_CONSOLE_OUTPUT_INFO                     = 0x45,  //!< 0x45 with #accerion::api::PlainString
    CMD_GET_UDP_SETTINGS                        = 0x46,  //!< 0x46 without body
    ACK_GET_TCP_SETTINGS                        = 0x47,  //!< 0x47 with #accerion::api::TcpSettings
    ACK_SET_DATE_TIME                           = 0x48,  //!< 0x48 with #accerion::api::Acknowledgement
    ACK_GET_IP_ADDRESS                          = 0x49,  //!< 0x49 with #accerion::api::IpAddress
    CMD_GET_DATE_TIME                           = 0x4A,  //<< 0x4A without body
    ACK_GET_DATE_TIME                           = 0x4B,  //!< 0x48 with #accerion::api::DateTime
    ACK_FRAME                                   = 0x4C,  //!< 0x4C with #accerion::api::UInt8Vector
    ACK_SOFTWARE_VERSION                        = 0x4D,  //!< 0x4D with #accerion::api::SoftwareVersion
    ACK_SET_UDP_SETTINGS                        = 0x4E,  //!< 0x4E with #accerion::api::Acknowledgement
    ACK_GET_UDP_SETTINGS                        = 0x4F,  //!< 0x4E with #accerion::api::UdpSettings
    CMD_SET_INTERNAL_MODE                       = 0x50,  //!< 0x50 with #accerion::api::Boolean
    CMD_SET_LOCALIZATION_MODE                   = 0x52,  //!< 0x52 with #accerion::api::Boolean
    CMD_SET_RECORDING_MODE                      = 0x53,  //!< 0x53 with #accerion::api::Boolean
    CMD_SET_IDLE_MODE                           = 0x54,  //!< 0x54 with #accerion::api::Boolean
    CMD_REBOOT                                  = 0x55,  //!< 0x55 with #accerion::api::Boolean
    CMD_GET_SIGNATURE_INFORMATION               = 0x5A,  //!< 0x5A with #accerion::api::BoolUInt64
    CMD_GET_RECORDINGS_LIST                     = 0x5D,  //!< 0x5D without body
    CMD_FILE_TRANSFER_REQUEST                   = 0x5E,  //!< 0x5E with #accerion::api::FileUploadDownloadRequest
    CMD_DELETE_RECORDINGS                       = 0x5F,  //!< 0x5F with #accerion::api::StringVector
    CMD_GET_IP_ADDRESS                          = 0x60,  //!< 0x60 without body
    CMD_GET_SAMPLE_RATE                         = 0x62,  //!< 0x62 without body
    CMD_START_SEARCH_LOOP_CLOSURES              = 0x63,  //!< 0x63 with #accerion::api::SearchRadius
    CMD_STOP_SEARCH_LOOP_CLOSURES               = 0x64,  //!< 0x64 without body
    ACK_SEARCH_LOOP_CLOSURES                    = 0x65,  //!< 0x65 with #accerion::api::Acknowledgement
    ACK_LOOP_CLOSURE                            = 0x66,  //!< 0x66 with #accerion::api::LoopClosure
    ACK_SEARCH_LOOP_CLOSURES_COMPLETE           = 0x67,  //!< 0x67 without body
    CMD_GET_SOFTWARE_VERSION                    = 0x68,  //!< 0x68 without body
    CMD_GET_TCP_SETTINGS                        = 0x69,  //!< 0x69 without body
    CMD_SET_EXPERT_MODE                         = 0x6B,  //!< 0x6B with #accerion::api::Boolean
    CMD_GET_BUFFER_LENGTH                       = 0x6C,  //!< 0x6C without body
    CMD_STOP_PROCESSING_BUFFER                  = 0x6D,  //!< 0x6D without body
    CMD_SET_SAMPLE_RATE                         = 0x70,  //!< 0x70 with #accerion::api::PlainUInt16
    CMD_DELETE_CLUSTER                          = 0x74,  //!< 0x74 with #accerion::api::PlainUInt64
    CMD_CAPTURE_FRAME                           = 0x76,  //!< 0x76 with #accerion::api::PlainString
    CMD_SET_IP_ADDRESS                          = 0x80,  //!< 0x80 with #accerion::api::IpAddress
    CMD_SET_CORRECTED_ODOMETRY_POSE             = 0x81,  //!< 0x81 with #accerion::api::Pose
    CMD_SET_DATE_TIME                           = 0x83,  //!< 0x83 with #accerion::api::DateTime
    INT_LATEST_EXTERNAL_REFERENCE               = 0x84,  //!< 0x84 with #accerion::api::LatestExternalReference
    CMD_SET_EXTERNAL_REFERENCE_POSE             = 0x85,  //!< 0x85 with #accerion::api::ExternalReferencePose
    CMD_SET_MAPPING_MODE                        = 0x86,  //!< 0x86 with #accerion::api::BoolUInt64
    CMD_SET_TCP_SETTINGS                        = 0x87,  //!< 0x87 with #accerion::api::TcpSettings
    CMD_SET_LINE_FOLLOWING_MODE                 = 0x88,  //!< 0x88 with #accerion::api::BoolUInt64
    CMD_SET_UDP_SETTINGS                        = 0x89,  //!< 0x89 with #accerion::api::UdpSettings
    CMD_SET_BUFFER_LENGTH                       = 0x8A,  //!< 0x8A with #accerion::api::PlainUInt32
    CMD_START_PROCESSING_BUFFER                 = 0x8B,  //!< 0x8B with #accerion::api::BufferedRecoveryRequest
    CMD_GET_SOFTWARE_DETAILS                    = 0x97,  //!< 0x97 without body
    CMD_PLACE_CALIB                             = 0x98,  //!< 0x98 with #accerion::api::CalibrationFileRequest
    CMD_GET_CLUSTER_IDS                         = 0x99,  //!< 0x99 without body
    ACK_GET_CLUSTER_IDS                         = 0x9A,  //!< 0x9A with #accerion::api::UInt64Vector
    CMD_GET_AREA_IDS                            = 0x9B,  //!< 0x9B without body
    ACK_GET_AREA_IDS                            = 0x9C,  //!< 0x9C with #accerion::api::Areas
    CMD_SET_AREA                                = 0x9D,  //!< 0x9D with #accerion::api::PlainUInt16
    CMD_SET_CONTINUOUS_SIGNATURE_UPDATING_MODE  = 0x9E,  //!< 0x9E with #accerion::api::Boolean
    ACK_CONTINUOUS_SIGNATURE_UPDATING_MODE      = 0x9F,  //!< 0x9F with #accerion::api::Acknowledgement
    CMD_CREATE_MAP_SUBSET                       = 0xA0,  //!< 0xA0 with #accerion::api::SubsetOperationRequest
    ACK_MAP_SUBSET                              = 0xA1,  //!< 0xA1 with #accerion::api::SubsetOperationResult
    CMD_GET_RAM_ROM_STATS                       = 0xA2,  //!< 0xA2 without body
    ACK_RAM_ROM_STATS                           = 0XA3,  //!< 0xA3 with #accerion::api::RamRomStats
    CMD_DELETE_AREA                             = 0xA4,  //!< 0xA4 with #accerion::api::BoolUInt16
    ACK_DELETE_AREA                             = 0xA5,  //!< 0xA5 with #accerion::api::Acknowledgement
    CMD_FILE_TRANSFER_TASK                      = 0xA6,  //!< 0xA6 with #accerion::api::FileTransferTaskRequest
    ACK_FILE_TRANSFER_STATUS                    = 0xA7,  //!< 0xA7 with #accerion::api::FileTransferTaskResult
    CMD_GET_MAP_SUMMARY                         = 0xA8,  //!< 0xA8 without body
    ACK_MAP_SUMMARY                             = 0xA9,  //!< 0xA9 with #accerion::api::MapSummary
    INT_SIGNATURE_CREATED                       = 0xAD,  //!< 0xAD with #accerion::api::CreatedSignature
    CMD_SET_MARKER_DETECTION_MODE               = 0xAE,  //!< 0xAE with #accerion::api::MarkerDetectionMode
    ACK_MARKER_DETECTION_MODE                   = 0xAF,  //!< 0xAF with #accerion::api::Acknowledgement
    INT_MARKER_DETECTION                        = 0xB0,  //!< 0xB0 with #accerion::api::MarkerDetection
    CMD_LOG_EVENT                               = 0xB1,  //!< 0xB1 with #accerion::api::PlainString
    CMD_SET_SEARCH_RANGE                        = 0xB2,  //!< 0xB2 with #accerion::api::SearchRange
    CMD_SET_SAVE_IMAGES                         = 0xB3,  //!< 0xB3 see #accerion::api::Boolean
    CMD_GET_SAVE_IMAGES                         = 0xB4,  //!< 0xB4 without body
    ACK_SET_SAVE_IMAGES                         = 0xB5,  //!< 0xB5 see #accerion::api::Acknowledgement
    ACK_GET_SAVE_IMAGES                         = 0xB6,  //!< 0xB5 see #accerion::api::Boolean
    ACK_SET_AREA                                = 0xB7,  //!< 0xB7 see #accerion::api::Acknowledgement
};

/// Enum containing the various types of message types, each command belongs to a certain type and might have an
/// influence on the way they are sent/received
enum class MessageType : std::uint8_t {
    kPeriodic        = 0x01,  //!< 0x01 sent at intervals
    kStreaming       = 0x02,  //!< 0x02 sent continuously
    kIntermittent    = 0x03,  //!< 0x03 sent irregularly, mainly intended for commands and their callback messages
    kAcknowledgement = 0x04,  //!< 0x04 callback messages
    kCommand         = 0x05   //!< 0x05 request messages
};

/// Enum containing the various types of connection message type filter settings
enum class EnabledMessageType : std::uint8_t {
    kInactive        = 0x01,  //!< 0x01
    kStreaming       = 0x02,  //!< 0x02
    kIntermittent    = 0x03,  //!< 0x03
    kBoth            = 0x04   //!< 0x04
};

/// Enum containing socket errors
enum class SocketError : uint8_t {
    kBrokenPipe = 32
};

/// Map containing all the in and outgoing messages and their corresponding message type
const std::map<CommandId, MessageType> kCommandMessageTypes {
    {CommandId::PRD_HEARTBEAT_INFO                          , MessageType::kPeriodic},
    {CommandId::ACK_FILE_TRANSFER_HANDSHAKE                 , MessageType::kAcknowledgement},
    {CommandId::STR_CORRECTED_ODOMETRY                      , MessageType::kStreaming},
    {CommandId::STR_ODOMETRY                                , MessageType::kStreaming},
    {CommandId::STR_DIAGNOSTICS                             , MessageType::kStreaming},
    {CommandId::INT_COMPENSATED_DRIFT_CORRECTION            , MessageType::kIntermittent},
    {CommandId::INT_DRIFT_CORRECTION                        , MessageType::kIntermittent},
    {CommandId::STR_LINE_FOLLOWER                           , MessageType::kStreaming},
    {CommandId::ACK_SIGNATURE_INFORMATION                   , MessageType::kAcknowledgement},
    {CommandId::CMD_GET_MAPPING_MODE                        , MessageType::kCommand},
    {CommandId::ACK_GET_MAPPING_MODE                        , MessageType::kAcknowledgement},
    {CommandId::CMD_GET_LINE_FOLLOWING_MODE                 , MessageType::kCommand},
    {CommandId::ACK_GET_LINE_FOLLOWING_MODE                 , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_MAPPING_MODE                        , MessageType::kAcknowledgement},
    {CommandId::ACK_LOCALIZATION_MODE                       , MessageType::kAcknowledgement},
    {CommandId::ACK_RECORDING_MODE                          , MessageType::kAcknowledgement},
    {CommandId::ACK_IDLE_MODE                               , MessageType::kAcknowledgement},
    {CommandId::ACK_REBOOT                                  , MessageType::kAcknowledgement},
    {CommandId::ACK_INTERNAL_MODE                           , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_LINE_FOLLOWING_MODE                 , MessageType::kAcknowledgement},
    {CommandId::ACK_SIGNATURE_INFORMATION_DONE              , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_SAMPLE_RATE                         , MessageType::kAcknowledgement},
    {CommandId::ACK_GET_SAMPLE_RATE                         , MessageType::kAcknowledgement},
    {CommandId::ACK_DELETE_CLUSTER                          , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_BUFFER_LENGTH                       , MessageType::kAcknowledgement},
    {CommandId::ACK_BUFFER_PROGRESS_INDICATOR               , MessageType::kAcknowledgement},
    {CommandId::ACK_GET_BUFFER_LENGTH                       , MessageType::kAcknowledgement},
    {CommandId::ACK_RECORDINGS_LIST                         , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_IP_ADDRESS                          , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_TCP_SETTINGS                        , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_CORRECTED_ODOMETRY_POSE             , MessageType::kAcknowledgement},
    {CommandId::ACK_DELETE_RECORDINGS                       , MessageType::kAcknowledgement},
    {CommandId::INT_CONSOLE_OUTPUT_INFO                     , MessageType::kIntermittent},
    {CommandId::CMD_GET_UDP_SETTINGS                        , MessageType::kCommand},
    {CommandId::ACK_GET_TCP_SETTINGS                        , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_DATE_TIME                           , MessageType::kAcknowledgement},
    {CommandId::ACK_GET_IP_ADDRESS                          , MessageType::kAcknowledgement},
    {CommandId::CMD_GET_DATE_TIME                           , MessageType::kCommand},
    {CommandId::ACK_GET_DATE_TIME                           , MessageType::kAcknowledgement},
    {CommandId::ACK_FRAME                                   , MessageType::kAcknowledgement},
    {CommandId::ACK_SOFTWARE_VERSION                        , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_UDP_SETTINGS                        , MessageType::kAcknowledgement},
    {CommandId::ACK_GET_UDP_SETTINGS                        , MessageType::kAcknowledgement},
    {CommandId::ACK_PLACE_CALIB                             , MessageType::kAcknowledgement},
    {CommandId::ACK_EXPERT_MODE                             , MessageType::kAcknowledgement},
    {CommandId::ACK_SOFTWARE_DETAILS                        , MessageType::kAcknowledgement},
    {CommandId::CMD_SET_INTERNAL_MODE                       , MessageType::kCommand},
    {CommandId::CMD_SET_LOCALIZATION_MODE                   , MessageType::kCommand},
    {CommandId::CMD_SET_RECORDING_MODE                      , MessageType::kCommand},
    {CommandId::CMD_SET_IDLE_MODE                           , MessageType::kCommand},
    {CommandId::CMD_REBOOT                                  , MessageType::kCommand},
    {CommandId::CMD_GET_SIGNATURE_INFORMATION               , MessageType::kCommand},
    {CommandId::CMD_GET_RECORDINGS_LIST                     , MessageType::kCommand},
    {CommandId::CMD_FILE_TRANSFER_REQUEST                   , MessageType::kCommand},
    {CommandId::CMD_GET_IP_ADDRESS                          , MessageType::kCommand},
    {CommandId::CMD_GET_SAMPLE_RATE                         , MessageType::kCommand},
    {CommandId::CMD_START_SEARCH_LOOP_CLOSURES              , MessageType::kCommand},
    {CommandId::CMD_STOP_SEARCH_LOOP_CLOSURES               , MessageType::kCommand},
    {CommandId::ACK_SEARCH_LOOP_CLOSURES                    , MessageType::kAcknowledgement},
    {CommandId::ACK_LOOP_CLOSURE                            , MessageType::kAcknowledgement},
    {CommandId::ACK_SEARCH_LOOP_CLOSURES_COMPLETE           , MessageType::kAcknowledgement},
    {CommandId::CMD_GET_SOFTWARE_VERSION                    , MessageType::kCommand},
    {CommandId::CMD_GET_TCP_SETTINGS                        , MessageType::kCommand},
    {CommandId::CMD_SET_EXPERT_MODE                         , MessageType::kCommand},
    {CommandId::CMD_GET_BUFFER_LENGTH                       , MessageType::kCommand},
    {CommandId::CMD_STOP_PROCESSING_BUFFER                  , MessageType::kCommand},
    {CommandId::CMD_SET_SAMPLE_RATE                         , MessageType::kCommand},
    {CommandId::CMD_DELETE_CLUSTER                          , MessageType::kCommand},
    {CommandId::CMD_CAPTURE_FRAME                           , MessageType::kCommand},
    {CommandId::CMD_SET_IP_ADDRESS                          , MessageType::kCommand},
    {CommandId::CMD_SET_CORRECTED_ODOMETRY_POSE             , MessageType::kCommand},
    {CommandId::ACK_DELETE_RECORDINGS                       , MessageType::kCommand},
    {CommandId::CMD_SET_DATE_TIME                           , MessageType::kCommand},
    {CommandId::INT_LATEST_EXTERNAL_REFERENCE               , MessageType::kCommand},
    {CommandId::CMD_SET_EXTERNAL_REFERENCE_POSE             , MessageType::kCommand},
    {CommandId::CMD_SET_MAPPING_MODE                        , MessageType::kCommand},
    {CommandId::CMD_SET_TCP_SETTINGS                        , MessageType::kCommand},
    {CommandId::CMD_SET_LINE_FOLLOWING_MODE                 , MessageType::kCommand},
    {CommandId::CMD_SET_UDP_SETTINGS                        , MessageType::kCommand},
    {CommandId::CMD_SET_BUFFER_LENGTH                       , MessageType::kCommand},
    {CommandId::CMD_START_PROCESSING_BUFFER                 , MessageType::kCommand},
    {CommandId::CMD_PLACE_CALIB                             , MessageType::kCommand},
    {CommandId::CMD_GET_SOFTWARE_DETAILS                    , MessageType::kCommand},
    {CommandId::CMD_GET_CLUSTER_IDS                         , MessageType::kCommand},
    {CommandId::ACK_GET_CLUSTER_IDS                         , MessageType::kAcknowledgement},
    {CommandId::CMD_GET_AREA_IDS                            , MessageType::kCommand},
    {CommandId::ACK_GET_AREA_IDS                            , MessageType::kAcknowledgement},
    {CommandId::CMD_SET_AREA                                , MessageType::kCommand},
    {CommandId::CMD_SET_CONTINUOUS_SIGNATURE_UPDATING_MODE  , MessageType::kCommand},
    {CommandId::ACK_CONTINUOUS_SIGNATURE_UPDATING_MODE      , MessageType::kAcknowledgement},
    {CommandId::CMD_CREATE_MAP_SUBSET                       , MessageType::kCommand},
    {CommandId::ACK_MAP_SUBSET                              , MessageType::kAcknowledgement},
    {CommandId::CMD_GET_RAM_ROM_STATS                       , MessageType::kCommand},
    {CommandId::ACK_RAM_ROM_STATS                           , MessageType::kAcknowledgement},
    {CommandId::CMD_DELETE_AREA                             , MessageType::kCommand},
    {CommandId::ACK_DELETE_AREA                             , MessageType::kAcknowledgement},
    {CommandId::CMD_FILE_TRANSFER_TASK                      , MessageType::kCommand},
    {CommandId::ACK_FILE_TRANSFER_STATUS                    , MessageType::kAcknowledgement},
    {CommandId::CMD_GET_MAP_SUMMARY                         , MessageType::kCommand},
    {CommandId::ACK_MAP_SUMMARY                             , MessageType::kAcknowledgement},
    {CommandId::INT_SIGNATURE_CREATED                       , MessageType::kIntermittent},
    {CommandId::CMD_SET_MARKER_DETECTION_MODE               , MessageType::kCommand},
    {CommandId::ACK_MARKER_DETECTION_MODE                   , MessageType::kAcknowledgement},
    {CommandId::INT_MARKER_DETECTION                        , MessageType::kIntermittent},
    {CommandId::CMD_LOG_EVENT                               , MessageType::kCommand},
    {CommandId::CMD_SET_SEARCH_RANGE                        , MessageType::kCommand},
    {CommandId::CMD_SET_SAVE_IMAGES                         , MessageType::kCommand},
    {CommandId::CMD_GET_SAVE_IMAGES                         , MessageType::kCommand},
    {CommandId::ACK_SET_SAVE_IMAGES                         , MessageType::kAcknowledgement},
    {CommandId::ACK_GET_SAVE_IMAGES                         , MessageType::kAcknowledgement},
    {CommandId::ACK_SET_AREA                                , MessageType::kAcknowledgement},
};

using RawData = std::vector<std::uint8_t>;

/// Default Command structure, a message container, basically consisting of an CommandId and the data
struct Command {
    Command() = default;
    Command(CommandId id, RawData data);

    virtual ~Command() = default;

    Command(const Command&) = default;
    Command& operator=(const Command&) = default;
    Command(Command&&) noexcept = default;
    Command& operator=(Command&&) noexcept = default;

    bool operator==(const Command &rhs) const;
    bool operator!=(const Command &rhs) const;

    CommandId command_id{};
    RawData command{};
};

}  // namespace api
}  // namespace accerion
