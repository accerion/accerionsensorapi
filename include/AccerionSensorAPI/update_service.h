/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <iostream>
#include <exception>
#include <fstream>
#include <sstream>
#include <string>
#include <functional>
#include <vector>
#include <ctime>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <queue>

#include "AccerionSensorAPI/callbacks.h"
#include "AccerionSensorAPI/communication/thread.h"
#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/crc8.h"
#include "AccerionSensorAPI/future.h"
#include "AccerionSensorAPI/received_command/file_transfer_handshake.h"
#include "AccerionSensorAPI/received_command/file_transfer_task.h"
#include "AccerionSensorAPI/structs/diagnostics.h"
#include "AccerionSensorAPI/structs/heartbeat.h"
#include "AccerionSensorAPI/tcp_client.h"
#include "AccerionSensorAPI/type_def.h"
#include "AccerionSensorAPI/udp_receiver.h"

#ifdef CMAKE_WINDOWS
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

namespace accerion {
namespace api {

/// UpdateService provides an object-oriented "interface". Methods invoked on this object are relayed to the
/// sensor over the network and received messages are forwarded to the registered callbacks.
/// Note that an UpdateService instance should be released when not used any more as only one connection per
/// sensor can exist (decided configuration of an Accerion sensor).
class UpdateService {
public:
    /**
    * \brief Constructor for UpdateService object, can be invoked by you, but recommended use is through SensorManager
    * \param address address of the sensor that is to be connected to
    * \param serial_number of the sensor that is to be connected to
    **/
    UpdateService(const Address& address, const SerialNumber& serial_number);

    ~UpdateService();

    UpdateService(UpdateService&&) = delete;
    UpdateService& operator=(UpdateService&&) = delete;
    UpdateService(const UpdateService&) = default;
    UpdateService& operator=(const UpdateService&) = default;

    /// \brief Method to register a callback to the Heartbeat message
    Future<HeartBeat>& SubscribeToHeartBeat();

    /// \brief Method to register a callback to the Diagnostics message
    Future<Diagnostics>& SubscribeToDiagnostics();

    /// \brief Method to retrieve the serial number that belongs to the sensor
    SerialNumber GetSerialNumber() const;

    /**
    * \brief Method to import/upload a calibration file [INTERNAL USE ONLY]
    * \param sourcePath string full path to where the file that is to be uploaded is located
    * \param doneCB callback method to be invoked after a success or failure
    * \param key that gives you access to this functionality
    **/
    bool SendCalibration(std::string sourcePath, DoneCallback doneCB, std::string key);

    void SetFileTransferTaskCallbacks(FileTransferTaskStatusCallback status_cb,
                                      FileTransferTaskProgressCallback progress_cb);
    void ExecuteFileTransferTask(FileTransferTaskRequest request);

    void FileTransfer(FileUploadDownloadRequest req, std::string filePath);
    void SetFileTransferCallbacks(ProgressCallback progress_cb, DoneCallback done_cb);

    /**
     * \brief Method to send an update
     * \param filepath string full path to where the file that is to be uploaded is located
     * \param transfer_progress_cb callback method to be invoked to inform about transfer progress
     * \param processing_progress_cb callback method to be invoked to inform about processing progress
     * \param done_cb callback method to be invoked to inform about completion
     **/
    void UpdateSensor(std::string filepath,
                      ProgressCallback transfer_progress_cb,
                      FileTransferTaskProgressCallback processing_progress_cb,
                      DoneCallback done_cb);

    /**
     * \brief Method to retrieve logs
     * \param file_path string full path to where the file should be stored
     * \param transfer_progress_cb callback method to be invoked to inform about transfer progress
     * \param processing_progress_cb callback method to be invoked to inform about processing progress
     * \param done_cb callback method to be invoked to inform about completion
     **/
    void GetLogs(const std::string& file_path,
                 ProgressCallback transfer_progress_cb,
                 FileTransferTaskProgressCallback processing_progress_cb,
                 DoneCallback done_cb);

private:
    void retrievedCalibAck(std::vector<uint8_t> received_command_);

    /// \brief   Method creates a map relating the command ids to the corresponding processing functions.
    ProcessResponseFunctions InitializeProcessResponseFunctionsWithReceivedCommands();

    static std::unique_ptr<TcpClient> CreateTcpClient(const Address& address);

    const Address address_;
    const SerialNumber serial_number_;

    ProcessResponseFunctions process_response_functions_;

    Future<HeartBeat> future_heart_beat_;
    Future<Diagnostics> future_diagnostics_;

    // Legacy received command handling
    ReceivedCommandFileTransferHandshake rc_handshake_;
    ReceivedCommandFileTransferTask rc_file_transfer_task_;

    DoneCallback calibDoneCallBack;

    CommunicationThread communication_thread_;
};

}  // namespace api
}  // namespace accerion
