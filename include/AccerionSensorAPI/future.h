/* Copyright (c) 2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>

#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/utils/optional.h"


namespace accerion {
namespace api {

// Forward declaration
class Sensor;
class UpdateService;

const std::chrono::seconds kDefaultTimeout{3};

/**
 * In general, the Future object is public and that will be returned by Sensor, UpdateService and ConnectionManager
 * when something is requested. The user can use an instance to choose:
 *   - wait for the requested data to become available (WaitFor), or
 *   - register a call back that will be called when the data becomes available (Callback)
 * Note that it is not possible to do both.
 *
 * Future object is thread-safe.
 */

/**
 * @tparam Data  The expected data type returned after a request
 */
template<typename Data>
class Future {
public:
    ~Future() = default;

    // No move or copy
    Future(Future &&) = delete;
    Future &operator=(Future &&) = delete;
    Future(const Future &) = delete;
    Future &operator=(const Future &) = delete;

    /**
     * \brief Wait for a some period for data to become available, where the data satisfies given condition function
     * @tparam Duration  The duration type
     * @tparam Predicate The function definition of the condition to be checked
     * @param  time_out  The duration indicating the time the function will wait for the desired data
     * @param  condition The function implementation of the condition that the data should obey
     * @return           The optional data that is retrieved
     */
    template<typename Duration = decltype(kDefaultTimeout)>
    Optional<Data> WaitFor(const Duration& time_out = kDefaultTimeout,
                           const std::function<bool(Data)>& condition = nullptr) {
        Optional<Data> result{};
        std::unique_lock<std::mutex> lock(mutex_);
        const auto data_valid = [this, &condition]() -> bool {
            // Data is present and optionally, if there is a condition function, the data satisfies condition
            return data_ && (!condition || condition(data_.value()));
        };
        if (cv_.wait_for(lock, time_out, data_valid)) {
            std::swap(result, data_);
            data_.reset();
        }
        return result;
    }

    /// Register a function that will be called with the data, when it becomes available
    void Callback(std::function<void(Data)> callback) {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_ && callback != nullptr) {
            callback(std::move(data_.value()));
            data_.reset();
        }
        callback_ = std::move(callback);
    }

private:
    friend class Sensor;
    friend class UpdateService;

    Future() = default;
    Future(CommandId id, ProcessResponseFunctions& container) {
        container.emplace(id, [this](RawData raw_data) {
            auto data = Data::Deserialize(std::move(raw_data));
            Fill(std::move(data));
        });
    }

    // Register a function that will be called with the data as its argument when the data becomes available
    void Fill(Data &&data) {
        {
            std::lock_guard<std::mutex> lock(mutex_);
            if (callback_) {
                callback_(data);
            }
            data_ = std::move(data);
        }  // lock scope
        cv_.notify_all();
    }

    void ClearData() {
        std::lock_guard<std::mutex> lock(mutex_);
        data_.reset();
    }

    Optional<Data> data_{};
    std::function<void(Data)> callback_{nullptr};

    // Attributes required for a thread-safe updating the attributes above
    std::mutex mutex_{};
    std::condition_variable cv_{};
};

/**
 * Specialization of Future when there is no data:
 *   - WaitFor will return a boolean whether the Future is filled (true) or timed out (false)
 *   - Callback will call the registered callback when the Future is filled
 */
template <>
class Future<void> {
public:
    ~Future() = default;

    // No move or copy
    Future(Future &&) = delete;
    Future &operator=(Future &&) = delete;
    Future(const Future &) = delete;
    Future &operator=(const Future &) = delete;

    /**
     * \brief Wait for a some period for the future to be filled
     * @tparam Duration  The duration type
     * @param  time_out  The duration indicating the time the function will wait for the desired data
     * @return           A boolean if the future is filled in time
     */
    template<typename Duration = decltype(kDefaultTimeout)>
    bool WaitFor(const Duration& time_out = kDefaultTimeout) {
        std::unique_lock<std::mutex> lock(mutex_);
        const auto data_valid = [this]() -> bool { return data_received_; };
        if (cv_.wait_for(lock, time_out, data_valid)) {
            data_received_ = false;
            return true;
        }
        return false;
    }

    /// Register a function that will be called when the future is filled
    void Callback(std::function<void()> callback) {
        std::lock_guard<std::mutex> lock(mutex_);
        if (data_received_ && callback != nullptr) {
            callback();
            data_received_ = false;
        }
        callback_ = std::move(callback);
    }

private:
    friend class Sensor;
    friend class UpdateService;

    Future(CommandId id, ProcessResponseFunctions& container) {
        container.emplace(id, [this](const RawData&) {
            Fill();
        });
    }

    // Register a function that will be called with the data as its argument when the data becomes available
    void Fill() {
        {
            std::lock_guard<std::mutex> lock(mutex_);
            if (callback_) {
                callback_();
            }
            data_received_ = true;
        }  // lock scope
        cv_.notify_all();
    }

    void ClearData() {
        std::lock_guard<std::mutex> lock(mutex_);
        data_received_ = false;
    }

    bool data_received_{false};
    std::function<void()> callback_{nullptr};

    // Attributes required for a thread-safe updating the attributes above
    std::mutex mutex_{};
    std::condition_variable cv_{};
};

/**
 * Specialization of Future for a unique pointer with data:
 *   - WaitFor will return a potentially empty unique pointer
 *   - Callback will call the registered callback with the unique pointer
 *
 * This specialization is used for non-movable and non-copyable objects
 */
template<typename Object>
class Future<std::unique_ptr<Object>> {
private:
    using ObjectPtr = std::unique_ptr<Object>;

public:
    ~Future() = default;

    // No move or copy
    Future(Future &&) = delete;
    Future &operator=(Future &&) = delete;
    Future(const Future &) = delete;
    Future &operator=(const Future &) = delete;

    /**
     * \brief Wait for a some period for pointer to become available
     * @tparam Duration  The duration type
     * @param  time_out  The duration indicating the time the function will wait for the desired data
     * @return           The pointer that is retrieved which can be empty (if the data retrieval failed or timed out
     */
    template<typename Duration>
    ObjectPtr WaitFor(const Duration& time_out) {
        ObjectPtr result{nullptr};
        std::unique_lock<std::mutex> lock(mutex_);
        if (callback_) {
            throw std::runtime_error("A callback is already set in this Future, so WaitFor cannot be used");
        }
        const auto data_valid = [this]() -> bool { return static_cast<bool>(instance_); };
        if (cv_.wait_for(lock, time_out, data_valid)) {
            result = std::move(instance_);
        }
        return result;
    }

    /// Register a function that will be called with the data pointer, when it becomes available
    void Callback(std::function<void(ObjectPtr)> callback) {
        std::lock_guard<std::mutex> lock(mutex_);
        if (instance_ && callback != nullptr) {
            callback(std::move(instance_));
            instance_.reset();
        }
        callback_ = std::move(callback);
    }

private:
    friend class ConnectionManager;

    Future() = default;

    // Register a function that will be called with the instance as its argument when it becomes available
    void Fill(ObjectPtr instance) {
        bool notify = false;
        {
            std::lock_guard<std::mutex> lock(mutex_);
            instance_.reset();
            if (callback_) {
                callback_(std::move(instance));
            } else {
                instance_ = std::move(instance);
                notify = true;
            }
        }  // lock scope
        if (notify) {
            cv_.notify_all();
        }
    }

    ObjectPtr instance_{nullptr};
    std::function<void(ObjectPtr)> callback_{nullptr};

    // Attributes required for a thread-safe updating the attributes above
    std::mutex mutex_{};
    std::condition_variable cv_{};
};

}  // namespace api
}  // namespace accerion
