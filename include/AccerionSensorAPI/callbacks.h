/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <functional>
#include <string>
#include <vector>

#include "AccerionSensorAPI/structs/file_transfer_task.h"
#include "AccerionSensorAPI/structs/signature.h"
#include "AccerionSensorAPI/structs/subset_operation.h"

namespace accerion {
namespace api {

/*
    Callback type definitions for various message handling functions,
    each accepting a specific struct or data type as a parameter.
*/

using DoneCallback = std::function<void(bool, std::string)>;
using FileTransferTaskStatusCallback = std::function<void(FileTransferTask, FileTransferTaskStatus, std::string)>;
using FileTransferTaskProgressCallback = std::function<void(FileTransferTask, int)>;
using ProgressCallback = std::function<void(int)>;
using SubsetCallback = std::function<void(SubsetResult, std::vector<ClusterId>)>;

}  // namespace api
}  // namespace accerion
