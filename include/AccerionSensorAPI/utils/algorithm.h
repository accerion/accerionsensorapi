/* Copyright (c) 2023, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <algorithm>
#include <iterator>
#include <set>
#include <type_traits>
#include <unordered_set>
#include <vector>

namespace accerion {
namespace api {

namespace detail {

template <typename Container, typename ValueType = typename Container::value_type>
struct IsSet {
    static constexpr bool value = std::is_same<Container, std::set<ValueType>>::value ||
                                  std::is_same<Container, std::unordered_set<ValueType>>::value;
};

}  // namespace detail

template <typename Type>
void Append(std::vector<Type>& a, const std::vector<Type>& b) {
    if (a.empty()) {
        a = b;
    } else {
        a.insert(std::end(a), std::begin(b), std::end(b));
    }
}

template <typename Type>
void Append(std::vector<Type>& a, std::vector<Type>&& b) {
    if (a.empty()) {
        a = std::move(b);
    } else {
        a.insert(std::end(a), std::make_move_iterator(b.begin()),
                 std::make_move_iterator(b.end()));
    }
}

// Wrapper around the erase-remove-if idiom
template <typename Container, typename Predicate, typename std::enable_if<!detail::IsSet<Container>::value, bool>::type = true>
void Erase(Container& container, const Predicate& predicate) {
    container.erase(
            std::remove_if(std::begin(container), std::end(container), predicate),
            std::end(container));
}
template <typename Container, typename Predicate, typename std::enable_if<detail::IsSet<Container>::value, bool>::type = true>
void Erase(Container& container, const Predicate& predicate) {
    // Specific implementation of erase-remove-if idiom for set-like containers
    for (auto it = container.begin(); it != container.end(); ) {
        if (predicate(*it)) {
            container.erase(it++);
        } else {
            ++it;
        }
    }
}

}  // namespace api

}  // namespace accerion
