/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <stdexcept>


namespace accerion {
namespace api {

// Should be replaced by std::optional in C++17, therefore also mimicking that interface.
template <typename Object>
class Optional {
public:
    Optional() = default;

    Optional(Object object) :
        empty_{false}, object_{std::move(object)}{
    }

    explicit operator bool() const {
        return !empty_;
    }

    bool operator==(const Optional& other) const {
        return (empty_ && other.empty_) || (!empty_ && !other.empty_ && object_ == other.object_);
    }

    bool operator!=(const Optional& other) const {
        return !(*this == other);
    }

    //  For scalar types assign explicitly created new optional object
    Optional& operator=(Object&& object) {
        static_assert(!std::is_scalar<Object>::value, "Cannot use assignment operator with scalar type");
        empty_ = false;
        object_ = std::move(object);
        return *this;
    }

    //  For scalar types assign explicitly created new optional object
    Optional& operator=(const Object& object) {
        static_assert(!std::is_scalar<Object>::value, "Cannot use assignment operator with scalar type");
        empty_ = false;
        object_ = object;
        return *this;
    }

    Object& value() {
        if (empty_) {
            throw std::runtime_error("Optional object is empty.");
        }
        return object_;
    }
    const Object& value() const {
        if (empty_) {
            throw std::runtime_error("Optional object is empty.");
        }
        return object_;
    }

    void reset() {
        empty_ = true;
        object_ = {};
    }

private:
    bool empty_{true};
    Object object_{};
};

}  // namespace api
}  // namespace accerion
