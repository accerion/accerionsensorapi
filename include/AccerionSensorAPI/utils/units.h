/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
* All rights reserved.
*
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this
* file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <stdint.h>

namespace accerion {
namespace api {

static constexpr int16_t kKiloByte = 1024;
static constexpr int32_t kMegaByte = 1024 * kKiloByte;
static constexpr int64_t kGigaByte = 1024 * kMegaByte;
// Make sure file size constants fit within assigned integer types
static_assert(1024 * kKiloByte == 1048576, "Potential integer overflow");
static_assert(1024 * kMegaByte == 1073741824, "Potential integer overflow");
static_assert(1024 * kGigaByte == 1099511627776, "Potential integer overflow");

}  // namespace api
}  // namespace accerion
