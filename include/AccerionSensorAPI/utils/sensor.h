/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <memory>

#include "AccerionSensorAPI/sensor.h"
#include "AccerionSensorAPI/update_service.h"
#include "AccerionSensorAPI/utils/optional.h"

namespace accerion {
namespace api {

/**
 * @brief Based on connection type provided by the user, find and connect to the first Accerion sensor found on the network.
 * The returned sensor (i.e. first found sensor) depends on the GetDetectedSensors function output and logic for handling the 
 * found sensors (for example, whether there would be sorting of the sensors or not in case multiple sensors are found).
 * 
 * @param connection_type the connection type requested by the user
 * @param opt_local_ip the local ip address used for UDP connection
 * @return std::shared_ptr<accerion::api::Sensor> pointer to the sensor object
 */
std::unique_ptr<Sensor> GetAnySensor(ConnectionType connection_type = ConnectionType::kConnectionTcp,
                                     Optional<Address> opt_local_ip = {});

std::pair<UdpSettings, TcpSettings> ConfigureConnection(const ConnectionType& connection_type,
                                                        const Optional<Address>& local_ip = {});

// All file types for up- and downloading
enum class FileType : std::uint8_t {
    Logs,
    Map,
    Coordinates,
    LoopClosures,
    Recording,
    Update
};

const std::unordered_map<FileType, std::string> kFileExtensions = {
    {FileType::Coordinates, "csv"},
    {FileType::LoopClosures, "g2o"},
    {FileType::Logs, "zip"},
    {FileType::Map, "db"},
    {FileType::Recording, "arf"},
    {FileType::Update, "asu"}
};

std::string GenerateFileName(const FileType& file_type,
                             const accerion::api::SerialNumber& serial_number);

}  // namespace api
}  // namespace accerion
