/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstdint>
#include <regex>
#include <string>
#include <sstream>
#include <vector>

namespace accerion {
namespace api {

// Convert all characters in the string to lower case, inplace
void ToLower(std::string& s);

// Generate a string with the iterated values with the supplied delimiter
// For example
//     std::vector<int> ints{1, 2, 3, 4, 5};
//     std::cout << Join(ints.cbegin(), ints.cend(), ", ");
// will result in string
//     "1, 2, 3, 4, 5"
template <typename Iter>
std::string Join(Iter iter, const Iter end, const std::string& delimiter) {
    std::ostringstream ss;
    while (iter != end) {
        ss << *iter;
        ++iter;
        if (iter != end) {
            ss << delimiter;
        }
    }
    return ss.str();
}

// Split provided string using the supplied character. Delimiters are not aggregated and so also empty strings can be
// returned, like in the following example:
//     ',1,,2,,' split on ',' returns ['', '1', '', '2', '', '']
std::vector<std::string> Split(const std::string& s, const std::string& delimiter);

/**
 * @brief Method to check whether a string starts with a certain substring
 * @param string the complete string that is to be checked
 * @param search_string the potential start of the string
 * @return bool true if the string starts with the search_string
 */
bool DoesStringStartWith(const std::string& string, const std::string& search_string);

/**
 * @brief Method to check whether a string ends with a certain substring
 * @param string the complete string that is to be checked
 * @param search_string the potential end of the string
 * @return bool true if the string ends with the search_string
 */
bool DoesStringEndWith(const std::string& string, const std::string& search_string);

/**
 * @brief Method to check whether a certain substring is present in a string
 * @param string the complete string that is to be checked
 * @param search_string the potential part of the string
 * @return bool true if the substring is present in a string
 */
bool DoesStringContain(const std::string& string, const std::string& search_string);

template<typename T>
std::vector<T> StringToIntVector(const std::string& input) {
    const std::regex reg{ R"(((\d+)-(\d+))|(\d+))" };
    std::smatch sm{};
    std::vector<T> ints;
    for (std::string s{input}; std::regex_search(s, sm, reg); s = sm.suffix()) {
        if (sm[1].str().length()) {
            for (int i{std::stoi(sm[2])}; i <= std::stoi(sm[3]); ++i) ints.push_back(i);
        }
        else {
            ints.push_back(std::stoi(sm[0]));
        }
    }
    return ints;
}

std::string ToString(double value, std::uint8_t digits);

}  // namespace api
}  // namespace accerion
