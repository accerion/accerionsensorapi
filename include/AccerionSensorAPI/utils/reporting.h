/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <bitset>
#include <climits>
#include <ostream>
#include <set>
#include <string>

#include "AccerionSensorAPI/structs/acknowledgement.h"
#include "AccerionSensorAPI/structs/diagnostics.h"
#include "AccerionSensorAPI/structs/buffered_recovery.h"
#include "AccerionSensorAPI/utils/optional.h"


namespace accerion {
namespace api {

void Report(const BufferProgress& bp);

// Collection of frequently used call backs
void ReportProgress(int progress);
void ReportDone(const std::string& action, bool success, const std::string& message);
void ReportMode(const std::string& mode, Acknowledgement result, bool setting);
void ReportMode(const std::string& mode, accerion::api::Optional<accerion::api::Acknowledgement> ack);

namespace detail {

template <typename Type>
constexpr size_t BitsPerType() {
    return sizeof(Type) * CHAR_BIT;
}

// Helper object for printing Diagnostics
template <typename Type>
class ReportBase {
protected:
    using Id = std::uint8_t;
    using NameMap = std::map<Id, std::string>;

    explicit ReportBase(Type value, const NameMap& name_map)
        : name_map_{name_map}, ids_{CreateIds(name_map)}, bits_{value} {}

    std::set<Id> GetIds() const {
        return ids_;
    }

    std::ostream& Print(std::ostream&os) const {
        if (bits_.none()) {
            os << "    none\n";
        } else {
            for (const auto& id: ids_) {
                if (Value(id)) {
                    os << "    " + Name(id) + "\n";
                }
            }
        }
        return os;
    }

    virtual std::string Name(Id id) const {
        return name_map_.at(id);
    }

    bool Value(Id id) const {
        return bits_.test(static_cast<std::size_t>(id));
    }

private:
    static std::set<Id> CreateIds(const NameMap& name_map) {
        std::set<Id> ids;
        for (const auto& id_name_pair: name_map) {
            ids.emplace_hint(ids.end(), id_name_pair.first);
        }
        return ids;
    }

    const std::set<Id> ids_;
    const NameMap& name_map_;
    const std::bitset<BitsPerType<Type>()> bits_;
};

}  // namespace detail

using ModesType = decltype(Diagnostics::modes);
class ReportModes : private detail::ReportBase<ModesType> {
public:
    explicit ReportModes(ModesType modes, bool all = true);

private:
    static std::size_t MaxSize(const NameMap& name_map);

    friend std::ostream& operator<<(std::ostream& os, const ReportModes& modes);

    std::string Name(ReportBase::Id i) const override;

    const std::size_t name_max_size_;
    const bool all_;
};

using ErrorCodeType = decltype(Diagnostics::error_codes);
class ReportErrorCodes : private detail::ReportBase<ErrorCodeType> {
public:
    explicit ReportErrorCodes(ErrorCodeType codes) : ReportBase(codes, kErrorMap) {}

private:
    friend std::ostream& operator<<(std::ostream& os, const ReportErrorCodes& codes);
};

using WarningCodeType = decltype(Diagnostics::warning_codes);
class ReportWarningCodes : private detail::ReportBase<WarningCodeType> {
public:
    explicit ReportWarningCodes(WarningCodeType codes) : ReportBase(codes, kWarningMap) {}

private:
    friend std::ostream& operator<<(std::ostream& os, const ReportWarningCodes& codes);
};

}  // namespace api
}  // namespace accerion
