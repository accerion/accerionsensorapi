/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstdint>
#include <cstdio>
#include <string>

namespace accerion {
namespace api {

/// As the C++17 library filesystem is not available and to not add a dependency
/// with boost, these utility functions are custom made, but could be replaced
/// in the future.

/**
 * Extract the extension of the given path, without the dot, otherwise return
 * empty string. Effectively, get the part after the last dot.
 */
std::string GetExtension(const std::string& path);

/**
 * Get file size
 *
 * Returns 0 is the file could not be read (and logically when the file has that
 * size).
 */
std::uint64_t GetFileSize(const std::string& path);

/**
 * Check if file exists
 *
 * Returns true if the file exists at the corresponding path.
 */
bool FileExists(const std::string& path);

/**
 * Remove file
 *
 * Returns true if the file at corresponding path is removed successfully.
 */
bool RemoveFile(const std::string& path);

}  // namespace api
}  // namespace accerion
