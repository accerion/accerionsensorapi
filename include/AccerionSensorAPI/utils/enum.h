/* Copyright (c) 2022-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <type_traits>


namespace accerion {
namespace api {

template <typename Enum,
          typename Type = typename std::underlying_type<Enum>::type,
          typename = typename std::enable_if<std::is_enum<Enum>::value>::type>
constexpr Type GetEnumValue(const Enum& e) {
    return static_cast<Type>(e);
}

}  // namespace api
}  // namespace accerion
