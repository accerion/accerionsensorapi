/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <chrono>
#include <future>

#include "AccerionSensorAPI/structs/acknowledgement.h"
#include "AccerionSensorAPI/utils/units.h"

namespace accerion {
namespace api {

// Function that will wait till a done call back is performed and return the function was successful or not and the
// message of the done callback
template <typename Function>
accerion::api::Acknowledgement WaitForDoneCallbackMsg(Function function) {
    std::promise<accerion::api::Acknowledgement> result;
    std::atomic_flag satisfied{false};
    // Trigger the execution of the function
    function([&result, &satisfied](bool success, std::string message) {
        if (!satisfied.test_and_set()) {
            result.set_value({success, std::move(message)});
        }
    });

    // Wait for completion
    return result.get_future().get();
}

template <typename... Ts>
double MbPerSecond(std::chrono::duration<Ts...> duration, uint64_t bytes) {
    const auto seconds = std::chrono::duration<double>(duration).count();
    const auto bytes_per_second = static_cast<double>(bytes) / seconds;
    return bytes_per_second / kMegaByte;
}

std::string GetTimeString();

}  // namespace api
}  // namespace accerion
