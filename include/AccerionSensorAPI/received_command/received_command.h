/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <array>
#include <condition_variable>
#include <functional>
#include <iostream>
#include <mutex>
#include <vector>

#include "AccerionSensorAPI/commands.h"

namespace accerion {
namespace api {

class ReceivedCommand {
public:
    virtual ~ReceivedCommand() = default;

    // Function used to process the incoming acknowledgement message
    virtual void ProcessAck(RawData raw_data) = 0;
};

template <typename FunctionType>
class ReceivedCommandCallback {
public:
    explicit operator bool() const {
        return function_ != nullptr;
    }

    void Set(FunctionType new_call_back) {
        function_ = std::move(new_call_back);
    }

    template <typename ... DataTypes>
    void Do(DataTypes&&... data) const {
        if (function_) {
            function_(std::forward<DataTypes>(data)...);
        }
    }

private:
    FunctionType function_{};
};

template <typename DataType>
class ReceivedCommandData {
public:
    ReceivedCommandData() = default;

    void Set(DataType d) {
        {
            // Scoped lock only required for updating the data
            std::lock_guard<std::mutex> lock(mutex_);
            data_present_ = true;
            data_ = std::move(d);
        }
        cv_.notify_all();
    }

    void Reset() {
        std::lock_guard<std::mutex> lock(mutex_);
        data_present_ = false;
        data_ = {};
    }

    template <typename Duration>
    DataType Get(Duration time_out, bool& timed_out) {
        std::unique_lock<std::mutex> lock(mutex_);
        const auto success = cv_.wait_for(lock, time_out, [this] { return data_present_; });

        DataType result{};
        if (success) {
            std::swap(data_, result);
            data_present_ = false;
        }
        timed_out = !success;
        return result;
    }

private:
    bool data_present_{false};
    DataType data_{};

    // For a thread-safe updating and getting the data, a mutex is required
    std::mutex mutex_{};

    // Condition variable is used for thread-safe notifications between threads
    std::condition_variable cv_{};
};

}  // namespace api
}  // namespace accerion
