/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <atomic>
#include <thread>
#include <utility>

#include "AccerionSensorAPI/callbacks.h"
#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/file_transfer.h"
#include "AccerionSensorAPI/type_def.h"
#include "received_command.h"

namespace accerion {
namespace api {

class ReceivedCommandFileTransferHandshake : public ReceivedCommand {
public:
    ~ReceivedCommandFileTransferHandshake() override;

    void ProcessAck(RawData raw_data) override;

    void SetFilePath(std::string path) { file_path_ = std::move(path); };
    void SetRemoteIpAddress(const accerion::api::Address& client_ip) { client_ip_ = client_ip; };

    ReceivedCommandData<FileTransferHandshake> data{};
    ReceivedCommandCallback<ProgressCallback> progress_callback{};
    ReceivedCommandCallback<DoneCallback> done_callback{};

private:
    void OpenFileTransferThread(const FileTransferHandshake& handshake);

    static void RunFileTransfer(FileTransferHandshake handshake,
                                const ReceivedCommandCallback<ProgressCallback>& progress_cb,
                                const ReceivedCommandCallback<DoneCallback>& done_cb,
                                const accerion::api::Address& client_address,
                                const std::string& file_path);

    void JoinFileTransferThreads();

    static constexpr std::uint8_t kMaxFileTransferThreads{10};
    std::array<std::thread, kMaxFileTransferThreads> file_transfer_threads_{};
    std::array<std::atomic_bool, kMaxFileTransferThreads> file_transfer_threads_running_{};

    std::string file_path_{};
    accerion::api::Address client_ip_{};
};

}  // namespace api
}  // namespace accerion
