/* Copyright (c) 2021-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/callbacks.h"
#include "received_command.h"


namespace accerion {
namespace api {

class ReceivedCommandFileTransferTask : public ReceivedCommand {
public:
    void ProcessAck(RawData raw_data) override;

    ReceivedCommandCallback<FileTransferTaskStatusCallback> callback{};
    ReceivedCommandCallback<FileTransferTaskProgressCallback>  progress_callback{};

    bool in_progress = false;
};

}  // namespace api
}  // namespace accerion
