/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#ifdef CMAKE_WINDOWS
    // Somehow, somewhere <windows.h> is included. Problem is that min and max macros are defined in there that interfere
    // with the numeric_limits min and max functions, at least under Windows.
    #define NOMINMAX
    #include <io.h>
    #define access _access
    #include <winsock2.h>
#else
    #include <arpa/inet.h>
#endif
