/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <atomic>
#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <sstream>
#include <thread>
#include <vector>

#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/communication/command_parser.h"
#include "AccerionSensorAPI/communication/networking.h"
#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/plain_uint32.h"
#include "AccerionSensorAPI/utils/algorithm.h"
#include "AccerionSensorAPI/utils/enum.h"
#include "AccerionSensorAPI/utils/memory.h"
#include "AccerionSensorAPI/utils/optional.h"

namespace accerion {
namespace api {

using ProcessResponseFunction = std::function<void(RawData)>;
using ProcessResponseFunctions = std::map<CommandId, ProcessResponseFunction>;


// Container for the communication thread, sending outgoing message and processing incoming messages using the callback
// The constructor will only return when the thread is actually running
class CommunicationThread final {
public:
    template <typename Client>
    CommunicationThread(std::unique_ptr<Client> client,
                        const SerialNumber& transmit_serial_number,
                        ParsedMessageCallback parsed_message_callback)
            : outgoing_messages_mutex_{MakeUnique<std::mutex>()},
              outgoing_messages_{MakeUnique<std::vector<Command>>()},
              run_thread_{MakeUnique<std::atomic_bool>(false)},
              thread_{} {
        thread_ = std::thread{&CommunicationThread::Work<Client>,
                              std::move(client),
                              std::ref(*outgoing_messages_mutex_),
                              std::ref(*outgoing_messages_),
                              std::ref(*run_thread_),
                              transmit_serial_number,
                              std::move(parsed_message_callback)};

        // Wait until thread is actually running
        while (!run_thread_->load()) { std::this_thread::sleep_for(std::chrono::milliseconds{5}); }
    }

    ~CommunicationThread();

    CommunicationThread(CommunicationThread&&) noexcept;
    CommunicationThread& operator=(CommunicationThread&&) noexcept = delete;
    CommunicationThread(const CommunicationThread&) = delete;
    CommunicationThread& operator=(const CommunicationThread&) = delete;

    /// @brief Method to add a message with supplied CommandId to the outgoing messages
    void AddOutgoingMessage(CommandId command_id, RawData&& serialized_message = {});

    /// @brief Method to add a message with supplied CommandId and a command that needs to be serialized
    template <typename Object>
    void AddOutgoingMessage(CommandId command_id, const Object& object) {
        AddOutgoingMessage(command_id, object.Serialize());
    }

private:
    /**
      * @brief      Method that is ran on a separate thread that checks for incoming messages and sends out messages
      * @param[in]  client                      Responsible communication client
      * @param[in]  outgoing_messages_mutex     Mutex to access the outgoing messages container
      * @param[in]  outgoing_messages           Outgoing messages container
      * @param[in]  run_thread                  Condition to check whether the thread is running
      * @param[in]  transmit_serial_number      Serial number for the outgoing messages
      * @param[in]  parsed_message_callback     Callback function to call for any incoming messages
      */
    template <typename Client>
    static void Work(std::unique_ptr<Client> client,
                     std::mutex& outgoing_messages_mutex,
                     std::vector<Command>& outgoing_messages,
                     std::atomic_bool& run_thread,
                     SerialNumber transmit_serial_number,
                     ParsedMessageCallback parsed_message_callback) {
        constexpr std::chrono::duration<double> kMinIterationDuration{1. / network_constants::kApiThroughput};

        client->ConnectToServer();

        // State keeping for split messages
        bool last_message_was_broken = false;
        RawData received_message;

        run_thread = true;
        while (run_thread) {
            const auto desired_iteration_end_time = std::chrono::steady_clock::now() + kMinIterationDuration;

            // Receive messages
            while (client->ReceiveMessage()) {
                if (!last_message_was_broken) {
                    received_message.clear();
                }
                received_message.insert(received_message.end(), client->GetReceivedMessage(),
                                        client->GetReceivedMessage() + client->GetReceivedNumOfBytes());
                ParseMessages(last_message_was_broken, received_message, parsed_message_callback);
            }

            // Get outgoing messages
            std::vector<Command> messages_to_transmit{};
            {
                std::lock_guard<std::mutex> lock(outgoing_messages_mutex);
                if (!outgoing_messages.empty()) {
                    Append(messages_to_transmit, outgoing_messages);
                    outgoing_messages = {};
                }
            }

            // Send messages
            client->TransmitMessages(messages_to_transmit, transmit_serial_number);

            // Limit loop throughput
            if (std::chrono::steady_clock::now() < desired_iteration_end_time) {
                std::this_thread::sleep_until(desired_iteration_end_time);
            }

            // Ensure that client remains active
            client->StayAlive();
        }
    }

    void Stop();

    // To make the class movable, all attributes used by the thread should be movable, hence the pointers
    std::unique_ptr<std::mutex> outgoing_messages_mutex_{nullptr};
    std::unique_ptr<std::vector<Command>> outgoing_messages_{nullptr};
    std::unique_ptr<std::atomic<bool>> run_thread_{nullptr};
    std::thread thread_{};
};

ParsedMessageCallback CreateDefaultParsedMessageCallback(const SerialNumber& desired_serial_number,
                                                         const ProcessResponseFunctions& process_response_functions);

}  // namespace api
}  // namespace accerion
