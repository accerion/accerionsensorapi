/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <cstdint>

namespace accerion {
namespace api {
namespace network_constants {

/// Maximum amount of bytes in a UDP message
constexpr auto kMaxBytesInUdpMessage = 32000;
/// Buffer size for TCP specific. A too small buffer size might result in issues when transferring files
constexpr auto kTcpBufferSize = 10001024;
/// The throughput the API has to adhere to. Note this is an upper limit. It will slow the communication threads down
constexpr auto kApiThroughput = 300.;

constexpr auto kMessageHeaderLength = 9;
constexpr auto kMessageFooterLength = 1;

}  // namespace network_constants

/**
 * \brief Values indicating the method of connecting to the sensor
 * You can connect either over TCP or UDP. For UDP, a distinction is made between uni- and broadcasting.
 * Unicasting can reduce network load.
 */
enum class ConnectionType : std::uint8_t {
    kConnectionTcp = 0,
    kConnectionUdpBroadcast = 1,
    kConnectionUdpUnicast = 2,
    kConnectionTcpDisableUdp = 3,
    kConnectionUdpUnicastNoHb = 4
};

} // namespace api
} // namespace accerion
