/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include "AccerionSensorAPI/structs/address.h"
#include "AccerionSensorAPI/structs/plain_uint32.h"
#include "AccerionSensorAPI/structs/version.h"
#include "AccerionSensorAPI/utils/optional.h"

#include <chrono>
#include <mutex>


namespace accerion {
namespace api {

struct SensorDetails {
    Address address;
    SerialNumber serial_number;
    Version software_version;

    bool operator==(const SensorDetails &other) const;
};

struct SensorConnectionDetails {
    SensorDetails details{};
    std::chrono::system_clock::time_point last_message_time_point{};
};

/// Thread-safe
class DetectedSensors {
public:
    void Add(const SensorDetails& sensor_details);

    std::vector<SensorDetails> GetAll(const std::chrono::seconds& filter_by_last_detection) const;

private:
    mutable std::mutex mutex_{};
    std::vector<SensorConnectionDetails> sensors_{};
};

}  // namespace api
}  // namespace accerion
