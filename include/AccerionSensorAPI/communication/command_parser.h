/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <functional>

#include "AccerionSensorAPI/commands.h"
#include "AccerionSensorAPI/crc8.h"
#include "AccerionSensorAPI/structs/plain_uint32.h"

namespace accerion {
namespace api {

using ParsedMessageCallback = std::function<void(SerialNumber, Command&&)>;

void ParseMessages(bool& last_message_was_broken, RawData& message, const ParsedMessageCallback& call_back);

/// Method that wraps the message with all the required data, e.g. serial number, CRC etc.
RawData FormMessage(const SerialNumber& serial_number, const Command& command);

}  // namespace api
}  // namespace accerion
