/* Copyright (c) 2017-2024, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#pragma once

#include <cstring>
#include <sys/types.h>

#include "AccerionSensorAPI/type_def.h"
#include "AccerionSensorAPI/communication/networking.h"

#ifdef CMAKE_WINDOWS
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#else
    #include <unistd.h>
#endif

namespace accerion {
namespace api {

/**
 * @brief      Wrapper class that holds a UDP socket for receiving messages.
 */
class UdpReceiver {
public:
    /**
     * @brief      Constructs a UdpReceiver object
     *
     * @param[in]  receive_port  The receive port
     */
    explicit UdpReceiver(unsigned int receive_port);

    ~UdpReceiver();

    UdpReceiver(const UdpReceiver&) = delete;
    UdpReceiver& operator=(const UdpReceiver&) = delete;
    UdpReceiver(UdpReceiver &&other) noexcept;
    UdpReceiver& operator=(UdpReceiver &&other) noexcept;
    
    /**
     * @brief      Receives a single UDP message
     *
     * @return     True if UDP message is received, false otherwise
     */
    bool ReceiveMessage();

    /**
     * @brief      Gets a uint8_t pointer received message.
     *
     * @return     Pointer to the received message.
     */
#ifdef CMAKE_WINDOWS
    char* GetReceivedMessage() {
        return &receivedMessage_[0];
    }
#else
    uint8_t* GetReceivedMessage() {
        return &receivedMessage_[0];
    }
#endif

    /**
     * @brief      Gets the received number of bytes.
     *
     * @return     The received number of bytes.
     */
    int GetReceivedNumOfBytes() const {
        return receivedNumOfBytes_;
    }

private:
    void CloseSocket();

    sockaddr_in this_address_;  //!< Holds address of the sensor
    sockaddr_in remote_address_;  //!<  Holds address of machine from which UDP messages are coming, set to empty

    static constexpr unsigned int bufferSize_ = network_constants::kMaxBytesInUdpMessage; //!< Maximum buffer size for UDP messages
    std::uint16_t receivePort_; //!< Holds the receiving UDP socket
    int socketEndpoint_; //!< Holds the file descriptor of receiving UDP socket
    
    int receivedNumOfBytes_; //!< Holds number of bytes in received UDP message

    #ifdef CMAKE_WINDOWS
        int socketLength_; //!< Holds size of UDP socket
        char receivedMessage_[bufferSize_]; //!< Holds received message as a byte array
    #else
        socklen_t socketLength_; //!< Holds size of UDP socket
        std::uint8_t receivedMessage_[bufferSize_]; //!< Holds received message as a byte array
    #endif
};

}  // namespace api
}  // namespace accerion
